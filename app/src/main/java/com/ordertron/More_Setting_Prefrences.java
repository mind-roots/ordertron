package com.ordertron;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Model.JSONParser;
import Model.Methods;
import Model.Model_get_notification;
import Model.Utils;

/**
 * Created by abc on 12/16/2015.
 */
public class More_Setting_Prefrences extends AppCompatActivity {

    int REQUEST_CODE = 1;
    Menu menu;
    RelativeLayout rltime;
    Switch push_noti_switch, email_noti_switch;
    public static TextView timeset;
    public static ArrayList<String> time = new ArrayList<String>();
    ListView list_time;
    SharedPreferences prefs;
    String prefix_name, authcode, push_switch_value = "", email_switch_value = "", timezone_value;
    DatabaseQueries query;
    ArrayList<Model_get_notification> pushed = new ArrayList<>();
    boolean b;
    Methods methods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_prefrences);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        onclick();
    }

    private void onclick() {
        rltime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                Intent in = new Intent(More_Setting_Prefrences.this, DialogActivity.class);
                startActivityForResult(in, REQUEST_CODE);

            }
        });
        email_noti_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    email_switch_value = "yes";

                } else {
                    email_switch_value = "no";
                }
            }
        });
        push_noti_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    push_switch_value = "yes";
                } else {
                    push_switch_value = "no";
                }
            }
        });
    }

    private void init() {
        b = Utils.isNetworkConnected(More_Setting_Prefrences.this);
        rltime = (RelativeLayout) findViewById(R.id.rltime);
        timeset = (TextView) findViewById(R.id.timeset);
        push_noti_switch = (Switch) findViewById(R.id.push_noti_switch);
        email_noti_switch = (Switch) findViewById(R.id.email_noti_switch);
        list_time = (ListView) findViewById(R.id.list_time);
        query = new DatabaseQueries(More_Setting_Prefrences.this);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        prefix_name = prefs.getString("prefix_name", null);
        authcode = prefs.getString("auth_code", null);

        pushed = query.get_notification_setting(prefix_name);
        try {
            if (pushed.get(0).push_notification.equals("yes")) {
                push_noti_switch.setChecked(true);
            } else {
                push_noti_switch.setChecked(false);
            }
            if (pushed.get(0).email_notification.equals("yes")) {
                email_noti_switch.setChecked(true);
            } else {
                email_noti_switch.setChecked(false);
            }
        }catch (Exception e){

        }
        timeset.setText(pushed.get(0).time_zone);
        timezone_value = pushed.get(0).time_zone;
        push_switch_value = pushed.get(0).push_notification;
        email_switch_value = pushed.get(0).email_notification;


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_more_prefrences, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
                case R.id.save:
                if (email_noti_switch.isChecked()) {
                    email_switch_value = "yes";
                } else {
                    email_switch_value = "no";
                }
                if (push_noti_switch.isChecked()) {
                    push_switch_value = "yes";
                } else {
                    push_switch_value = "no";
                }
                if (!b) {
                    Utils.conDialog(More_Setting_Prefrences.this);
                } else {
                    new Savenotification().execute();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            timeset.setText(data.getStringExtra("name"));
            Log.e("timeset......", "" + timeset);
            timezone_value = timeset.getText().toString();

        } else {
//            Toast.makeText(getApplicationContext(),
//                    "Problem in Loading Result.", Toast.LENGTH_SHORT).show();
        }

    }


    public class Savenotification extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog;
        String result;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog = new ProgressDialog(More_Setting_Prefrences.this);
            dialog.setMessage("Saving....");
            dialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            result = notification_setting(prefix_name, authcode, email_switch_value, push_switch_value, timezone_value);
            return null;
        }

        @Override
        protected void onPostExecute(Void params) {
            // TODO Auto-generated method stub
            super.onPostExecute(params);
            try {
                dialog.dismiss();
                JSONObject jsonObject = new JSONObject(result);
                String status = jsonObject.optString("Status");

                System.out.println("status value is :: " + status);
                if (status.equals("true")) {
                    query.update_notification_setting(email_switch_value, push_switch_value, timezone_value, prefix_name);
                }  else if (status.equals("false")) {

                    if (jsonObject.has("logout")) {
                        String logout = jsonObject.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = jsonObject.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            methods = new Methods(More_Setting_Prefrences.this);
                            Methods.placeorder=1;
                            methods.dialog(result);


                        }
                    } else {

                        //dialog("Alert!", result, android.R.drawable.ic_dialog_alert);
                        Utils.dialog(More_Setting_Prefrences.this, "Error!", "Problem in updating data.");


                    }
                }

                // finish();
            } catch (Exception e) {

            }

        }

    }
    //*************************Webservice parameter notification setting  *************************

    public String notification_setting(String prefix_name, String auth_code, String email_notification
            , String push_notification, String timezone) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("email_notification", email_notification)
                .appendQueryParameter("push_notification", push_notification)
                .appendQueryParameter("timezone", timezone);
        res = parser.getJSONFromUrl(Utils.notification_setting, builder,More_Setting_Prefrences.this);
        return res;
    }
}

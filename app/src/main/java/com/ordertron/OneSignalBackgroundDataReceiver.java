package com.ordertron;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import DataBaseUtils.DatabaseQueries;
import Model.JSONParser;
import Model.Model_Order_Info;
import Model.Model_Order_Product;
import Model.Utils;

public class OneSignalBackgroundDataReceiver extends WakefulBroadcastReceiver {
    public static boolean isPush = false;


    Intent activityIntent;
    boolean network;
    Context con;
    List<ActivityManager.RunningTaskInfo> tasks;
    Intent parentintent;
    Class<? extends Activity> cls;

    ProgressBar pb;
    boolean classtatus;
    Context context;
    boolean isScreenOn, isNetwork;
    String message, prefix;
    SharedPreferences prefs;
    DatabaseQueries query;
    String gettype,order_info, order_product;

    public void onReceive(Context context, Intent intent) {
        this.context = context;
        prefs = context.getSharedPreferences("login", context.MODE_PRIVATE);
        query = new DatabaseQueries(context);
        prefix = prefs.getString("prefix_name", "");

        isNetwork = Utils.isNetworkConnected(context);
        try {
            PowerManager pm = (PowerManager)
                    context.getSystemService(Context.POWER_SERVICE);
            isScreenOn = pm.isScreenOn();

            Bundle dataBundle = intent.getBundleExtra("data");
            con = context;
            Log.i("OneSignalExample", "Is Your App Active: " + dataBundle.getBoolean("isActive"));
            message = dataBundle.getString("alert");
            //      if (dataBundle.getBoolean("isActive")) {
            Log.i("OneSignalExample", "data additionalData: " + dataBundle.getString("custom"));
            JSONObject customJSON = new JSONObject(dataBundle.getString("custom"));
            if (customJSON.has("a")) {

                JSONObject additionalData = customJSON.getJSONObject("a");
                gettype = additionalData.optString("type");

            }


            //-----------initialization---------------------//
            classtatus = isApplicationSentToBackground(context);

            order_info = query.Getlastsync_id(DatabaseQueries.TABLE_ORDER_INFO, prefix);

            order_product = query.Getlastsync_id(DatabaseQueries.TABLE_ORDER_PRODUCT, prefix);
            if (!isNetwork) {
                Utils.conDialog(context);
            } else {
                //********Deleting table bcz if back order is pladed in any Order then old product was not removing****
                // query.delete_table_data(DatabaseQueries.TABLE_ORDER_INFO, "prefix_name", prefs.getString("prefix_name", null));

                new updatingorder().execute();


            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    private void passIntent() {
        if (Build.VERSION.SDK_INT >= 16) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(con);
            stackBuilder.addParentStack(cls);
            stackBuilder.addNextIntent(activityIntent);
            stackBuilder.startActivities();
        } else {
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            con.startActivity(activityIntent);
        }


    }


    public boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return false;
            }
        }

        return true;
    }


    /**************************************
     * Vibrate device
     ************************************/
    private void vibrate(Context ctx) {
        try {
            Vibrator v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    class updatingorder extends AsyncTask<String, Void, String> {
        String responseorderinfo, responseorderproduct, order_product_last_sync_id, status, order_last_sync_id;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            RelativeLayout layout = new RelativeLayout(context);
            pb = new ProgressBar(context,null,android.R.attr.progressBarStyleLarge);
            pb.setIndeterminate(true);
            pb.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            layout.addView(pb,params);
        }

        @Override
        protected String doInBackground(String... params) {
            responseorderinfo = GetOrderInfoMethod(prefs.getString("prefix_name", null), prefs.getString("username", null), prefs.getString("auth_code", null), "0");
            responseorderproduct = GetOrderProductMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), "0");


//****************************Parsing for Get OrderInfo*********************************************
            try {
                JSONObject obj = new JSONObject(responseorderinfo);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    //   order_last_sync_id = "0";
                    order_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Order_Info modelorder = new Model_Order_Info();
                        modelorder.order_id = obj1.getString("order_id");
                        modelorder.customer_id = obj1.getString("customer_id");
                        modelorder.billing_country = obj1.getString("billing_country");
                        modelorder.billing_state = obj1.getString("billing_state");
                        modelorder.billing_suburb = obj1.getString("billing_suburb");
                        modelorder.billing_add_1 = obj1.getString("billing_add_1");
                        modelorder.billing_add_2 = obj1.getString("billing_add_2");
                        modelorder.billing_add_3 = obj1.getString("billing_add_3");
                        modelorder.billing_postcode = obj1.getString("billing_postcode");
                        modelorder.shipping_country = obj1.getString("shipping_country");
                        modelorder.shipping_state = obj1.getString("shipping_state");
                        modelorder.shipping_suburb = obj1.getString("shipping_suburb");
                        modelorder.shipping_add_1 = obj1.getString("shipping_add_1");
                        modelorder.shipping_add_2 = obj1.getString("shipping_add_2");
                        modelorder.shipping_add_3 = obj1.getString("shipping_add_3");
                        modelorder.shipping_postcode = obj1.getString("shipping_postcode");
                        modelorder.shipping_cost = obj1.getString("shipping_cost");
                        modelorder.sub_total = obj1.getString("sub_total");
                        modelorder.tax_total = obj1.getString("tax_total");
                        modelorder.total = obj1.getString("total");
                        modelorder.order_notes = obj1.getString("order_notes");
                        modelorder.internal_notes = obj1.getString("internal_notes");
                        modelorder.order_status = obj1.getString("order_status");
                        modelorder.back_order = obj1.getString("back_order");
                        modelorder.main_order_id = obj1.getString("main_order_id");
                        //modelorder.delivery_date = obj1.getString("delivery_date");
                        String date=obj1.getString("delivery_date");
                        // String input = "Sat Feb 17 2012";
                        if(date!=null||!date.equals("")){
                            modelorder.delivery_date = date;
                        }else {
                            Date d = null;
                            try {
                                d = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            long milliseconds = d.getTime();
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                            String dateString = formatter.format(new Date(milliseconds));
                            modelorder.delivery_date = dateString;
                        }

                        modelorder.delivery_notes = obj1.getString("delivery_notes");
                        modelorder.created_date = obj1.getString("created_date");
                        modelorder.created_by = obj1.getString("created_by");
                        modelorder.update_date = obj1.getString("update_date");
                        modelorder.updated_by = obj1.getString("updated_by");
                        modelorder.order_name = obj1.getString("order_name");
                        modelorder.order_by = obj1.getString("order_by");
                        modelorder.sink_id = obj1.getString("sink_id");
                        modelorder.type = obj1.getString("type");
                        modelorder.last_sync_id = order_last_sync_id;
                        modelorder.show_deliverydate_order = obj1.getString("show_deliverydate_order");
                        modelorder.mandetory_delivery_order = obj1.getString("mandetory_delivery_order");
                        modelorder.prefix_name = prefs.getString("prefix_name", null);
                        //**calling Databasequries insert function for getporductInventory

                        //    query.getorderinfo(modelorder);
                        if (query.isexist("order_info", "order_id", modelorder.order_id, prefs.getString("prefix_name", null))) {



                            query.updateorderinfosync(modelorder);
                        } else {
                            query.getorderinfo(modelorder);
                        }


                    }


                } else {
                    //data = obj.getString("error");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//***************************Parsing for Get Order Product******************************************
            try {
                JSONObject obj = new JSONObject(responseorderproduct);
                status = obj.getString("Status");



                if (status.equals("true")) {
                    // order_product_last_sync_id = "0";
                    // order_product_last_sync_id = obj.getString("last_sync_id");
                    order_product_last_sync_id = "0";
                    query.delete_table_data(DatabaseQueries.TABLE_ORDER_PRODUCT, "prefix_name", prefs.getString("prefix_name", null));

                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Order_Product modelOrderProduct = new Model_Order_Product();
                        modelOrderProduct.id = obj1.getString("id");
                        modelOrderProduct.order_id = obj1.getString("order_id");
                        modelOrderProduct.product_id = obj1.getString("product_id");
                        modelOrderProduct.product_name = obj1.getString("product_name");
                        modelOrderProduct.product_code = obj1.getString("product_code");
                        modelOrderProduct.qty = obj1.getString("qty");
                        modelOrderProduct.qty_new_order = obj1.getString("qty_new_order");
                        modelOrderProduct.act_qty = obj1.getString("act_qty");
                        modelOrderProduct.unit_price = obj1.getString("unit_price");
                        modelOrderProduct.applied_price = obj1.getString("applied_price");
                        modelOrderProduct.sub_total = obj1.getString("sub_total");
                        modelOrderProduct.sink_id = obj1.getString("sink_id");
                        modelOrderProduct.type = obj1.getString("type");
                        modelOrderProduct.created_date = obj1.getString("created_date");
                        modelOrderProduct.update_date = obj1.getString("update_date");
                        modelOrderProduct.updated_by = obj1.getString("updated_by");
                        modelOrderProduct.last_sync_id = order_product_last_sync_id;
                        modelOrderProduct.prefix_name = prefs.getString("prefix_name", null);
                        //**calling Databasequries insert function for getorderProduct

                        //   query.getorderproduct(modelOrderProduct);
//                        if (query.isexist("order_product", "order_id", modelOrderProduct.order_id, prefs.getString("prefix_name", null))) {
//                            query.updateorderproduct(modelOrderProduct);
//                        } else {
                        query.getorderproduct(modelOrderProduct);
//                        }

                    }


                } else {
                    //   data = obj.getString("error");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pb.setVisibility(View.GONE);
            if (gettype.equals("message") || gettype.equals("Order"))
           // if (gettype.equals("message") )
                if (classtatus && isScreenOn) {

 //******************commented on 7th dec to avoid crash*********************************
                    Intent i = new Intent(context.getApplicationContext(), MyAlertDialog.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("msg", message);
                    context.startActivity(i);

                }


        }
    }


//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderInfoMethod(String prefix_name, String username, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", username)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getorderinfo, builder,con);
        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderProductMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getorderproduct, builder,con);
        return res;
    }


}
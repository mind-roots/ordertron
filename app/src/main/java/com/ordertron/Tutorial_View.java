package com.ordertron;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import Tutorials.ActivityStack;
import Tutorials.CircleIndicator;

/**
 * Created by Abhijeet on 9/16/2016.
 */

public class Tutorial_View extends AppCompatActivity {

    Intent i;
    String value;
    RelativeLayout rl_tuto;
    public static int tutorial = 0;

    int size = 0;
    CustomPagerAdapterNew pagerAdapter;
    public static ViewPager defaultViewpager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial_view);
        rl_tuto = (RelativeLayout) findViewById(R.id.rl_tuto);

    //************************Getting Intent value from Tutorial calss******************************

        value = getIntent().getStringExtra("Title");


        if (value.equals("Orders")) {
            size = 1;
            setTitle("");
            ActivityStack.activity.add(Tutorial_View.this);
            defaultViewpager = (ViewPager) findViewById(R.id.viewpager_default);

            pagerAdapter = new CustomPagerAdapterNew(getSupportFragmentManager(), size);
            defaultViewpager.setAdapter(pagerAdapter);

        } else if (value.equals("Temp")) {
            size = 2;
            setTitle("");
            ActivityStack.activity.add(Tutorial_View.this);
            defaultViewpager = (ViewPager) findViewById(R.id.viewpager_default);
            CircleIndicator defaultIndicator = (CircleIndicator) findViewById(R.id.indicator_default);
            pagerAdapter = new CustomPagerAdapterNew(getSupportFragmentManager(), size);
            defaultViewpager.setAdapter(pagerAdapter);
            defaultIndicator.setViewPager(defaultViewpager);
        } else if (value.equals("history")) {
            size = 3;
            setTitle("");
            ActivityStack.activity.add(Tutorial_View.this);
            defaultViewpager = (ViewPager) findViewById(R.id.viewpager_default);
            pagerAdapter = new CustomPagerAdapterNew(getSupportFragmentManager(), size);
            defaultViewpager.setAdapter(pagerAdapter);
        } else if (value.equals("message")) {
            size = 4;
            setTitle("");
            ActivityStack.activity.add(Tutorial_View.this);
            defaultViewpager = (ViewPager) findViewById(R.id.viewpager_default);
            pagerAdapter = new CustomPagerAdapterNew(getSupportFragmentManager(), size);
            defaultViewpager.setAdapter(pagerAdapter);
        } else if (value.equals("setting")) {
            size = 5;
            setTitle("");
            ActivityStack.activity.add(Tutorial_View.this);
            defaultViewpager = (ViewPager) findViewById(R.id.viewpager_default);
            pagerAdapter = new CustomPagerAdapterNew(getSupportFragmentManager(), size);
            defaultViewpager.setAdapter(pagerAdapter);
        }


    }


}
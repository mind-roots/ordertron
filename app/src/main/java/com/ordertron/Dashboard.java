package com.ordertron;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Adapters.PagerAdapter;
import DataBaseUtils.DatabaseQueries;
import DataBaseUtils.TabUtils;
import Fragments.CartActivity;
import Fragments.HomeActivity;
import Fragments.TemplatesActivity;
import Interfaces.RefreshFragment;
import Model.JSONParser;
import Model.Methods;
import Model.ModelLogin;
import Model.Model_Add_Detail_Customer;
import Model.Model_Add_To_Cart;
import Model.Model_Cat_Prod;
import Model.Model_GetCategories;
import Model.Model_GetPage;
import Model.Model_GetProduct;
import Model.Model_Message;
import Model.Model_Order_Info;
import Model.Model_Order_Product;
import Model.Model_Product_Inventory;
import Model.Model_Template;
import Model.Model_Tier_pricing;
import Model.Model_get_notification;
import Model.ModelgetCustomer;
import Model.Utils;

/**
 * Created by android on 12/4/2015.
 */
public class Dashboard extends ActionBarActivity implements CartActivity.onSaveTemplateListener {
    public static ArrayList<String> arrayList = new ArrayList<String>();
    public static ArrayList<String> cartproduct_id = new ArrayList<String>();
    public static ArrayList<String> cartproduct_qty = new ArrayList<String>();
    public static ArrayList<String> cartproduct_sku = new ArrayList<String>();
    public static Menu menu;
    public static int setting = 0, flag = 1, flag1 = 0, page = 0;
    public static TabLayout tabLayout;
    DatabaseQueries query;
    public static int width, height;
    SharedPreferences prefs, prefs1;
    SharedPreferences.Editor editor;
    public static ViewPager viewPager;
    public static PagerAdapter adapter;
    public static String prefix;
    ProgressBar progressBar;
    public static boolean isVisible = true;
    public static android.support.v7.app.ActionBar actionbar;
    JSONArray pidarr, qtyarr, skuarr;
    //public static ArrayList<String> categorystack = new ArrayList<>();
    public static ArrayList<Model_Message> categorystack = new ArrayList<>();
    // public static ArrayList<Model_GetCategories> getcat = new ArrayList<Model_GetCategories>();
    public static ArrayList<Model_Cat_Prod> mixlist = new ArrayList<>();
    public static ArrayList<Model_Cat_Prod> mixlist22 = new ArrayList<>();
    public static boolean getprev = false, getcatsame = false;
    public static boolean cartupdate = false, tempupdate = false, tempserverupdate = false, reorder = false;
    public static ArrayList<Model_Add_To_Cart> cartitem = new ArrayList<Model_Add_To_Cart>();
    String product_image_id = "0", add_product = "0", product_id = "0", category_id = "0", inventory_id = "0", tier_id = "0", order_info = "0", order_product = "0", template = "0";
    SimpleDateFormat sdf;
    public static String preftime, result, last_order_created, mTimezone;
    long timeDiff, timediff;
    public static int indicator = 0;
    public static ArrayList<Model_Add_To_Cart> item = new ArrayList<>();
    public static TabLayout.Tab cart;
    public static ArrayList<Model_Template> templist = new ArrayList<>();
    boolean b;
    public static ArrayList<Model_Template> item_2 = new ArrayList<Model_Template>();
    public static ArrayList<Model_Template> item_3 = new ArrayList<Model_Template>();
    public static int style;
    Methods methods;
    boolean movefurther = true;
    String responseGetCustomer, responseAddDetailCustomer, responsegetcatagories, responsegetpage, responsegetproducts, responsegetproductsimage,
            responsegetproductsinventory, responsetierpricing, responseorderinfo, responseorderproduct, responsetemplate, responseGetnotificationsetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        //Toast.makeText(getApplicationContext(),"D1",Toast.LENGTH_LONG).show();
//        getActionBar().hide();
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        //    getcountry();
        // List <String > list=getAllCountries();
        if (Build.VERSION.SDK_INT < 21) {
            //   style = Color.TRANSPARENT;
            style = R.style.DialogTheme;
        } else {
            style = R.style.DialogTheme1;
        }
        init();
        tabs();
        pagerset();


        methods = new Methods(Dashboard.this);
      /*  if (SplashActivity.logdirect) {
            if (timediff < 2 && timediff > 0) {
                Dashboard.flag = 1;


            } else {

                hitthreads("update");
            }
        } else {
            hitthreads("insert");
        }*/
    }

    /*  private static String readFileAsString(Context context)
              throws java.io.IOException {
          String base64 = context.getResources().getString(R.string.countriess);
          byte[] data = Base64.decode(base64, Base64.DEFAULT);
          return new String(data, "UTF-8");
      }
      private List<String> getAllCountries() {

              try {
                  List<String >  allCountriesList = new ArrayList<String >();

                  // Read from local file
                  String allCountriesString = readFileAsString(Dashboard.this);
                  Log.d("countrypicker", "country: " + allCountriesString);
                  JSONObject jsonObject = new JSONObject(allCountriesString);
                  Iterator<?> keys = jsonObject.keys();

                  // Add the data to all countries list
                  while (keys.hasNext()) {
                      String key = (String) keys.next();
  //                    Country country = new Country();
  //                    country.setCode(key);
  //                    country.setName(jsonObject.getString(key));
                      allCountriesList.add(jsonObject.getString(key));
                  }

                  // Sort the all countries list based on country name
                 Collections.sort(allCountriesList);

                  // Initialize selected countries with all countries
  //                selectedCountriesList = new ArrayList<Country>();
  //                selectedCountriesList.addAll(allCountriesList);

                  // Return
                  return allCountriesList;

              } catch (Exception e) {
                  e.printStackTrace();
              }

          return null;
      }
      private void getcountry() {

          List<String> countries = new ArrayList<String>();

          // Get ISO countries, create Country object and
          // store in the collection.
          String[] isoCountries = Locale.getISOCountries();
          for (String country : isoCountries) {
              Locale locale = new Locale("en", country);
              String iso = locale.getISO3Country();
              String code = locale.getCountry();
              String name = locale.getDisplayCountry();

              if (!"".equals(iso) && !"".equals(code)
                      && !"".equals(name)) {
                  countries.add(name);
              }
          }
          System.out.println("countries="+countries.toString());
          System.out.println("countries=");
      }

  */
    private void init() {
        b = Utils.isNetworkConnected(Dashboard.this);
        actionbar = getSupportActionBar();
//        actionbar.setHomeButtonEnabled(false);
//        actionbar.setDisplayHomeAsUpEnabled(false);
        query = new DatabaseQueries(Dashboard.this);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        Dashboard.actionbar.setTitle(prefs.getString("company_name", null));
        prefix = prefs.getString("prefix_name", "");

        sdf = new SimpleDateFormat(" dd MMM, yy hh.mm.ss a");
        //Getting screen resolution
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels;

//************************Code to show Message when first time login into app***********************

        prefs1 = getSharedPreferences("tutorial", Dashboard.this.MODE_PRIVATE);
        final String value1 = prefs1.getString("dontshowtutorial", "");
        if (value1.equals("")) {
            Intent i = new Intent(Dashboard.this, FirstView.class);
            startActivity(i);
        } else {

        }
    }

    private void timer() {

        preftime = prefs.getString("time", "");
        if (!preftime.equals("")) {
            Date now = new Date();
            Date dateOne = null;
            String strDate = sdf.format(now);

            try {
                dateOne = sdf.parse(strDate);
            } catch (ParseException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            Date dateTwo = null;
            try {
                dateTwo = sdf.parse(preftime);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            timeDiff = Math.abs(dateTwo.getTime() - dateOne.getTime());
            timediff = timeDiff / 60000;
        }

    }

    public void tabs() {
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tab_home));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tab_products));
        // tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tab_cart));
        cart = tabLayout.newTab().setCustomView(
                TabUtils.renderTabView(Dashboard.this,
                        R.drawable.ic_tab_cart, 0, "" + item.size()));
        tabLayout.addTab(cart);

        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tab_order));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_tab_more));
        tabLayout.setBackground(new ColorDrawable(Color.parseColor("#f9f9f9")));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        setTitle(prefs.getString("company_name", null));
        arrayList.add(prefs.getString("company_name", null));
        arrayList.add("Products");
        arrayList.add("Cart");
        arrayList.add("Templates");
        arrayList.add("More");
        viewPager = (ViewPager) findViewById(R.id.pager);
        Log.i("Authcode:", "" + prefs.getString("auth_code", null));

    }

    public void pagerset() {

        adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(), Dashboard.this);
        viewPager.setAdapter(adapter);


        //tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout) {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {

                super.onPageSelected(position);


                page = position;
                try {
                    RefreshFragment refreshFragment = (RefreshFragment) adapter.instantiateItem(viewPager, position);
                    if (refreshFragment != null) {
                        refreshFragment.refresh();
                    }
                } catch (Exception e) {

                }
                //  actionbar.setTitle(arrayList.get(position));

                if (position == 0) {
                    isVisible = true;
                    actionbar.setHomeButtonEnabled(false);
                    actionbar.setDisplayHomeAsUpEnabled(false);
//                    menu.findItem(R.id.search).setVisible(false);
//
//                    menu.findItem(R.id.add).setVisible(false);
//                    menu.findItem(R.id.edit).setVisible(false);
//                    menu.findItem(R.id.logout).setVisible(true);
//                    menu.findItem(R.id.setting).setVisible(true);
//                    menu.findItem(R.id.done).setVisible(false);
//                    menu.findItem(R.id.clear).setVisible(false);
//                    MenuItem searchItem = menu.findItem(R.id.search);
//
//                    SearchView searchView = (SearchView) searchItem.getActionView();
//
//                    if (!searchView.isIconified()) {
//                        searchView.setIconified(true);
//                    }
//                    searchItem.collapseActionView();
                    //String nam = prefs.getString("company_name", null);
                    //arrayList.set(0, prefs.getString("company_name", null));
                    Dashboard.actionbar.setHomeButtonEnabled(false);
                    Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
                    setTitle(prefs.getString("company_name", null));

                    try {
                        mTimezone = query.get_timezone_setting(prefs.getString("prefix_name", null));
                    } catch (Exception e) {
                    }
                    HomeActivity.txt_business.setText("Welcome " + prefs.getString("customer_business", null));
                    //   HomeActivity.txt_orderplaced.setText("Last order placed on " + savedateformatter(prefs.getString("created_at", null)));
                    try {
                        mTimezone = query.get_timezone_setting(prefs.getString("prefix_name", null));
                    } catch (Exception e) {
                    }
                    String val = prefs.getString("created_at", null);
                    String xon = mTimezone;
                    String order_null = Utils.timezone(prefs.getString("created_at", null), mTimezone, "dd MMM yyyy - hh:mm a");
                    if (order_null == null || order_null.equals("null")) {
                       // HomeActivity.txt_orderplaced.setText("You have not placed any order");
                        HomeActivity.txt_orderplaced.setText("");
                    } else {
                        HomeActivity.txt_orderplaced.setText("Last order placed on " + Utils.timezone1(prefs.getString("created_at", null), mTimezone, "dd MMM yyyy - hh:mm a"));
                    }
                    // HomeActivity.txt_orderplaced.setText("Last order placed on " + Utils.timezone(prefs.getString("created_at", null), mTimezone, "dd MMM yyyy - hh:mm a"));


                } else if (position == 1) {

//
//                    menu.findItem(R.id.add).setVisible(false);
//                    menu.findItem(R.id.edit).setVisible(false);
//                    menu.findItem(R.id.logout).setVisible(false);
//                    menu.findItem(R.id.setting).setVisible(false);
//                    menu.findItem(R.id.search).setVisible(true);
//                    menu.findItem(R.id.done).setVisible(false);
//                    menu.findItem(R.id.clear).setVisible(false);


                    if (categorystack.size() > 0) {
                        Dashboard.actionbar.setHomeButtonEnabled(true);
                        Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);
                        actionbar.setTitle(categorystack.get(categorystack.size() - 1).catname);

                    } else {
                        actionbar.setTitle(arrayList.get(position));
                    }
                    try {
                        MenuItem searchItem = menu.findItem(R.id.search);

                        SearchView searchView = (SearchView) searchItem.getActionView();

                        if (!searchView.isIconified()) {
                            searchView.setIconified(true);
                        }
                        searchItem.collapseActionView();

                    } catch (Exception e) {

                    }
                } else if (position == 2) {

                    actionbar.setHomeButtonEnabled(false);
                    actionbar.setDisplayHomeAsUpEnabled(false);


                    try {

                        MenuItem searchItem = menu.findItem(R.id.search);
                        // Toast.makeText(getApplicationContext(),""+ page ,Toast.LENGTH_LONG).show();
                        SearchView searchView = (SearchView) searchItem.getActionView();

                        if (!searchView.isIconified()) {
                            searchView.setIconified(true);
                        }
                        try {
                            searchItem.collapseActionView();
                        } catch (Exception e) {

                        }
                    } catch (Exception e) {

                    }
                } else if (position == 3) {

                    try {
                        actionbar.setHomeButtonEnabled(false);
                        actionbar.setDisplayHomeAsUpEnabled(false);
//                        menu.findItem(R.id.add).setVisible(true);
//                        menu.findItem(R.id.clear).setVisible(false);
                    } catch (Exception e) {

                    }
//
//                    if (flag1 == 0) {
//                        menu.findItem(R.id.edit).setVisible(true);
//                        menu.findItem(R.id.add).setVisible(true);
//                    } else {
//                        menu.findItem(R.id.edit).setVisible(false);
//                        menu.findItem(R.id.add).setVisible(false);
//                    }
//                    menu.findItem(R.id.logout).setVisible(false);
//                    menu.findItem(R.id.setting).setVisible(false);
//                    menu.findItem(R.id.search).setVisible(false);
//                    menu.findItem(R.id.clear).setVisible(false);
//                    if (flag1 == 1) {
//                        menu.findItem(R.id.done).setVisible(true);
//                        menu.findItem(R.id.add).setVisible(false);
//                    } else {
//                        menu.findItem(R.id.done).setVisible(false);
//                        menu.findItem(R.id.add).setVisible(true);
//
//                    }
//                    MenuItem searchItem = menu.findItem(R.id.search);
//
//                    SearchView searchView = (SearchView) searchItem.getActionView();
//
//                    if (!searchView.isIconified()) {
//                        searchView.setIconified(true);
//                    }
//                    searchItem.collapseActionView();
//                } else if (position == 4) {
//
//                    actionbar.setHomeButtonEnabled(false);
//                    actionbar.setDisplayHomeAsUpEnabled(false);
//                    try {
//                        menu.findItem(R.id.add).setVisible(false);
//                        menu.findItem(R.id.edit).setVisible(false);
//                        menu.findItem(R.id.logout).setVisible(false);
//                        menu.findItem(R.id.setting).setVisible(false);
//                        menu.findItem(R.id.search).setVisible(false);
//                        menu.findItem(R.id.clear).setVisible(false);
//                        menu.findItem(R.id.done).setVisible(false);
//
//                        MenuItem searchItem = menu.findItem(R.id.search);
//
//                        SearchView searchView = (SearchView) searchItem.getActionView();
//
//                        if (!searchView.isIconified()) {
//                            searchView.setIconified(true);
//                        }
//                        searchItem.collapseActionView();
//                    } catch (Exception e) {
//
//                    }
                }

            }
        });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 0:

                        actionbar.setTitle(arrayList.get(tab.getPosition()));
                        Dashboard.actionbar.setHomeButtonEnabled(false);
                        Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
                        break;
                    case 1:

                        if (categorystack.size() > 0) {
                            Dashboard.actionbar.setTitle(categorystack.get(categorystack.size() - 1).catname);
                            Dashboard.actionbar.setHomeButtonEnabled(true);
                            Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);

                            break;
                        } else {
                            Dashboard.actionbar.setHomeButtonEnabled(false);
                            Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
                            actionbar.setTitle(arrayList.get(tab.getPosition()));
                            break;
                        }


                    case 2:

                        actionbar.setTitle(arrayList.get(tab.getPosition()));
                        Dashboard.actionbar.setHomeButtonEnabled(false);
                        Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
                        break;
                    case 3:

                        Dashboard.actionbar.setHomeButtonEnabled(false);
                        Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
                        actionbar.setTitle(arrayList.get(tab.getPosition()));
                        break;
                    case 4:

                        actionbar.setTitle(arrayList.get(tab.getPosition()));
                        break;
                }
                if (flag == 0) {
                    // arrayList.set(0, prefs.getString("company_name", null));


                    HomeActivity.txt_business.setText("Welcome " + prefs.getString("customer_business", null));
                    //      HomeActivity.txt_orderplaced.setText("Last order placed on " + savedateformatter(prefs.getString("created_at", null)));
                    try {
                        mTimezone = query.get_timezone_setting(prefs.getString("prefix_name", null));
                    } catch (Exception e) {
                    }
                    HomeActivity.txt_orderplaced.setText("Last order placed on " + Utils.timezone1(prefs.getString("created_at", null), mTimezone, "dd MMM yyyy - hh:mm a"));


                    Utils.dialog(Dashboard.this, "Sync in Progress", "Please wait. The latest data is being fetched from the server.");

                    //  dialog(android.R.drawable.ic_dialog_alert, "Fetching Data from Server!!.", "Please wait. The latest data is being fetched from the server..");
                } else {

                    if (tab.getPosition() == 1) {
                        if (categorystack.size() > 0) {
                            Dashboard.actionbar.setHomeButtonEnabled(true);
                            Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);
                            Dashboard.actionbar.setTitle(categorystack.get(categorystack.size() - 1).catname);
                        } else {
                            actionbar.setTitle(("Products"));
                        }
                    } else {

                        actionbar.setTitle(arrayList.get(tab.getPosition()));
                    }

                    if (tab.getPosition() == 0) {
                        timer();
                        if (!Utils.isNetworkConnected(Dashboard.this)) {
                            Utils.conDialog(Dashboard.this);

                        } else {
                            if (!Utils.startsync) {
                                // actionbar.setTitle(prefs.getString("company_name", null));

                                syncdata();


                            }
                        }
                    }
                    page = tab.getPosition();
                    viewPager.setCurrentItem(page);

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }


    //***************************** Calling Async Task for Various web services*************************

    private void hitthreads(String gettype) {
       /* viewPager.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });*/
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        tabStrip.setEnabled(false);
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(false);
        }
        try {
            category_id = query.Getlastsync_id(DatabaseQueries.TABLE_GETCATEGORIES, prefix);
            product_id = query.Getlastsync_id(DatabaseQueries.TABLE_GET_PRODUCTS, prefix);
            product_image_id = query.Getlastsync_id(DatabaseQueries.TABLE_GET_PRODUCT_IMAGE, prefix);
            inventory_id = query.Getlastsync_id(DatabaseQueries.TABLE_GET_PRODUCT_INVENTORY, prefix);
            tier_id = query.Getlastsync_id(DatabaseQueries.TABLE_TIER_PRICING, prefix);
            order_info = query.Getlastsync_id(DatabaseQueries.TABLE_ORDER_INFO, prefix);
            template = query.Getlastsync_id(DatabaseQueries.TABLE_ORDER_TEMPLATE, prefix);
            int temp = Integer.parseInt(template);
            int ntemp = temp - 2;
            //  template= String.valueOf(ntemp);
            template = "0";
            order_product = query.Getlastsync_id(DatabaseQueries.TABLE_ORDER_PRODUCT, prefix);
            add_product = query.Getlastsync_id(DatabaseQueries.TABLE_ADD_DETAIL_CUSTOMER, prefix);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!Utils.isNetworkConnected(Dashboard.this)) {
            Utils.conDialog(Dashboard.this);
        } else {


            //********Deleting table bcz if back order is pladed in any Order then old product was not removing****

            // query.delete_table_data(DatabaseQueries.TABLE_ORDER_INFO, "prefix_name", prefs.getString("prefix_name", null));


            new multiple(gettype, category_id, product_id, product_image_id, inventory_id, tier_id, order_info, order_product, template).execute();

        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //logdialog();
        try {
            if (tabLayout.getSelectedTabPosition() != 0) {
                tabLayout.getTabAt(0).select();
                viewPager.setAdapter(adapter);
//            try {
//                RefreshFragment refreshFragment = (RefreshFragment) adapter.instantiateItem(viewPager, 0);
//                if (refreshFragment != null) {
//                    refreshFragment.refresh();
//                }
//            } catch (Exception e) {
//
//            }
                Dashboard.actionbar.setTitle(Dashboard.arrayList.get(0));
                Dashboard.actionbar.setHomeButtonEnabled(false);
                Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);

            } else {
                //logdialog();
                finish();
            }
        } catch (Exception e) {

        }
    }

    private void logdialog() {

        new AlertDialog.Builder(Dashboard.this, style)
                .setTitle("Alert!")
                .setMessage("Do you want to exit?")
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                                finish();


                            }
                        })
                .setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();


                            }
                        })
                .setIcon(android.R.drawable.ic_dialog_alert).show();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        this.menu = menu;
        menuInflater.inflate(R.menu.menu_actionbar, menu);

        return super.onCreateOptionsMenu(menu);


    }


    @Override
    protected void onResume() {
        super.onResume();
//        Toast.makeText(getApplicationContext(),""+ page ,Toast.LENGTH_LONG).show();]
//        if(setting==1){
//            setting=0;
//            String com_name = prefs.getString("company_name", null);
//            if (com_name != null || !com_name.equals("")) {
//                Dashboard.actionbar.setTitle(prefs.getString("company_name", null));
//                //txt_business.setText("Welcome " + prefs.getString("customer_business", null));
//            }
//        }
        prefix = prefs.getString("prefix_name", "");
        if (tabLayout.getSelectedTabPosition() == 3) {
            try {
                menu.findItem(R.id.clear).setVisible(false);
            } catch (Exception e) {

            }
        }

        item_2.clear();
        if (page == 0) {
            Dashboard.actionbar.setHomeButtonEnabled(false);
            Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
            String cname=prefs.getString("company_name", null);
            Dashboard.actionbar.setTitle(prefs.getString("company_name", null));

        }
        else if (page == 1) {
            if (Dashboard.categorystack.size() > 0) {
                Dashboard.actionbar.setHomeButtonEnabled(true);
                Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);
                Dashboard.actionbar.setTitle(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).catname);
            } else {
                Dashboard.actionbar.setHomeButtonEnabled(false);
                Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
                Dashboard.actionbar.setTitle("Products");
            }
        }
//    else {
//            actionbar.setTitle(arrayList.get(page));
//        }


//        tabLayout.getSelectedTabPosition();
//        viewPager.setCurrentItem(page);
        if (flag == 1)

        {
            try {

                String countQuery = "SELECT  * FROM cart_table";
                int count = query.getcartcount(countQuery);
                TabUtils.updateTabBadge(cart, count);
            } catch (Exception e) {
                e.printStackTrace();
            }





        /*viewPager.setOnTouchListener(new View.OnTouchListener() {
            }
            public boolean onTouch(View arg0, MotionEvent arg1) {
                return true;
            }
        });*/
            if (tempserverupdate) {
                tempserverupdate = false;
                try {
                    templist.clear();

                    templist = query.GetTemplate(prefix, "newupdate");
                    if(templist.size()==0){
                        templist = query.GetTemplate(prefix, "new");
                    }
                    item_3.clear();
                    item_3 = query.GetTemplate(prefix, "all");
                    for(int i=0;i<item_3.size();i++){
                        System.out.println("Type="+item_3.get(i).type);
                    }
                    item_2.clear();
                    item_2 = query.GetTemplate(prefix, "off");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!Utils.isNetworkConnected(Dashboard.this)) {

                    // Utils.conDialog(Dashboard.this);

                    Utils.conDialog(Dashboard.this);
                } else {
                    if(item_2.size()>0) {
                        new inserttemp(item_2).execute();
                    }else {
                        new updateteplate().execute();
                    }
                }
            }
            timer();
            if (reorder) {
                reorder = false;
                viewPager.setCurrentItem(2);
            }

            if (SplashActivity.logdirect) {
                if (Utils.startsync) {
                    Utils.startsync = false;
                    if (isVisible) {
                        isVisible = false;
                        viewPager.setCurrentItem(4);

                        if (!Utils.isNetworkConnected(Dashboard.this)) {

                            // Utils.conDialog(Dashboard.this);
                            HomeActivity.img_Update.setBackgroundResource(R.drawable.ic_cross);
                            HomeActivity.txt_Update.setText("Sync failed. No internet connection found");

                            Utils.conDialog(Dashboard.this);
                        } else {
//                            try {
//                                item_2=query.GetTemplate(prefix, "off");
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            if(item_2.size()>0){
//                                new inserttemp(item_2).execute();
//                                //dasasdas
//                            }

                            //delete_data();
                            hitthreads("update");
                        }
                    }
                } else {

                    //  syncdata();


                }
            } else {
                // delete_data();
                SplashActivity.logdirect = true;
                hitthreads("insert");
            }
        }

    }

    public void syncdata() {
        if (timediff < 3 && timediff >= 0) {
            Dashboard.flag = 1;
        } else {
            item_2.clear();

            try {
                item_2 = query.GetTemplate(prefix, "off");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (item_2.size() > 0) {
                viewPager.setCurrentItem(0);
                new inserttemp(item_2).execute();
                //dasasdas
            } else {

                if (isVisible) {
                    isVisible = false;
                    // viewPager.setCurrentItem(0);

                    if (!Utils.isNetworkConnected(Dashboard.this)) {
                        Utils.conDialog(Dashboard.this);
                    } else {

                        try {
                            templist = query.GetTemplate(prefix, "newupdate");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!Utils.isNetworkConnected(Dashboard.this)) {
                            Utils.conDialog(Dashboard.this);
                        } else {

                            new updateteplate().execute();
                        }
//*************************************Below is commentes on 11/8 at 10:30**************************

//                    delete_data();
                        // hitthreads("update");
                    }
                }
            }

        }

    }

    @Override
    public void onSaveTemplate() {
        try {
            TemplatesActivity templatesActivity = (TemplatesActivity) adapter.getItem(3);
            if (templatesActivity != null) {
                templatesActivity.updateTemplatefragment();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class updateteplate extends AsyncTask<String, Void, String> {

        String status;

        @Override
        protected String doInBackground(String... params) {
            if (templist.size() > 0) {
                for (int i = 0; i < templist.size(); i++) {

                    try {
                        JSONArray pcodearr = new JSONArray(templist.get(i).product_code);
                        JSONArray qtyarr = new JSONArray(templist.get(i).qty);
                        StringBuilder codebuilder = new StringBuilder();
                        StringBuilder qtybuilder = new StringBuilder();
                        for (int j = 0; j < pcodearr.length(); j++) {

                            codebuilder.append(pcodearr.get(j) + ",");
                            qtybuilder.append(qtyarr.get(j) + ",");

                        }
                        String pcode = codebuilder.toString();
                        if (pcode.indexOf(",") != -1) {
                            pcode = pcode.substring(0, pcode.length() - 1);
                        }


                        String pqty = qtybuilder.toString();
                        if (pqty.indexOf(",") != -1) {
                            pqty = pqty.substring(0, pqty.length() - 1);
                        }

                        String response = UpdateMethod(prefix, prefs.getString("auth_code", null), templist.get(i).order_template_id,
                                templist.get(i).template_name, pcode, pqty);

                        JSONObject obj = new JSONObject(response);

                        status = obj.getString("Status");

                        if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                            JSONArray data_obj = obj.getJSONArray("Data");
                            for (int k = 0; k < data_obj.length(); k++) {
                                JSONObject obj1 = data_obj.getJSONObject(k);
                                query.update_newtemp(obj1.getString("order_template_id"), obj1.getString("sync_id"), prefix, "newupdate");

                            }
                        } else {
                            status = obj.getString("Status");
                            if (status.equals("false")) {
                                if (obj.has("logout")) {
                                    String logout = obj.optString("logout");
                                    if (logout.equals("yes")) {
                                        JSONObject jobj = obj.optJSONObject("Data");
                                        final String result = jobj.optString("result");
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                methods.dialog(result);
                                            }
                                        });
                                    }
                                } else {
                                    JSONObject data_obj = obj.getJSONObject("Data");
                                    result = data_obj.getString("result");
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Utils.dialog(Dashboard.this, "Error!", result);
                                        }
                                    });
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                status = "true";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (status.equals("true")) {
                    // delete_data();

                    hitthreads("update");
                }
            } catch (Exception e) {
                Toast.makeText(getApplication(), "Unable to update due to network failure", Toast.LENGTH_SHORT).show();

            }
//    finally {
//                delete_data();
//                hitthreads("update");
//            }


        }
    }
    //************************Webservice parameter InsertMethod*************************

    public String UpdateMethod(String prefix_name, String auth_code, String order_template_id, String name, String product_code, String qty) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("template_id", order_template_id)
                .appendQueryParameter("template_name", name)
                .appendQueryParameter("product_id", product_code)
                .appendQueryParameter("qty", qty);
        res = parser.getJSONFromUrl(Utils.update, builder, Dashboard.this);
        return res;
    }
//****************************MENU items click (done and edit on Templates)*************************
 /*   public void edit(MenuItem item) {
        flag1 = 1;
        menu.findItem(R.id.edit).setVisible(false);
        menu.findItem(R.id.done).setVisible(true);
        menu.findItem(R.id.add).setVisible(false);
        TemplatesActivity.tadapter.notifyDataSetChanged();

    }

    public void done(MenuItem item) {
        flag1 = 0;
        menu.findItem(R.id.edit).setVisible(true);
        menu.findItem(R.id.done).setVisible(false);
        menu.findItem(R.id.add).setVisible(true);
        TemplatesActivity.tadapter.notifyDataSetChanged();
    }*/


    //*****************************Calling Multiple AsyncTasks******************************************
//**************************************************************************************************
    public class multiple extends AsyncTask<Void, Void, Void> {
        String status, data, category_id, product_id, product_image_id, inventory_id, tier_id, order_info, order_product, template;
        String last_sync_id, product_last_sync_id, productImage_last_sync_id, productInventory_last_sync_id, tierpriceing_last_sync_id, order_last_sync_id, order_product_last_sync_id, template_last_sync_id;
        String gettype;
        //****Constructor****

        public multiple(String gettype, String category_id, String product_id, String product_image_id, String inventory_id
                , String tier_id, String order_info, String order_product, String template) {
            this.product_id = product_id;
            this.category_id = category_id;
            this.product_image_id = product_image_id;
            this.inventory_id = inventory_id;
            this.tier_id = tier_id;
            this.order_info = order_info;
            this.order_product = order_product;
            this.template = template;
            this.gettype = gettype;
        }


        @Override
        public void onPreExecute() {
            super.onPreExecute();

            movefurther = true;
            flag = 0;
            Date now = new Date();
            String strDate = sdf.format(now);
            prefs.edit().putString("time", strDate).commit();
            tabLayout.setClickable(false);
            if (gettype.equals("update")) {
                try {
                    HomeActivity.img_Update.setVisibility(View.GONE);
                    HomeActivity.progressbar.setVisibility(View.VISIBLE);
                    HomeActivity.txt_Update.setText("Updating products and orders\ninformation from server.");
                } catch (Exception e) {
                }
            }
        }

        @Override
        public Void doInBackground(Void... params) {

            String responseGetCustomer = GetCustomerMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));
            //     String responseAddDetailCustomer = AddDetailMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));
            //  String responsegetcatagories = GetCategoriesMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), category_id);
            //  String responsegetpage = GetPageMethod(prefs.getString("prefix_name", null));
//            String responsegetproducts = GetProductsMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), product_id);
//            String responsegetproductsimage = GetProductsImageMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), product_image_id);
            //   String responsegetproductsinventory = GetProductsInventoryMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), inventory_id);
            // String responsetierpricing = GetTierPricingMethod(prefs.getString("prefix_name", null), prefs.getString("username", null), prefs.getString("auth_code", null), tier_id);
            //  String responseorderinfo = GetOrderInfoMethod(prefs.getString("prefix_name", null), prefs.getString("username", null), prefs.getString("auth_code", null), order_info);
//            String responseorderproduct = GetOrderProductMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), order_product);
            //          String responsetemplate = GetTemplateMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), template, prefs.getString("username", null));
//            String responseGetnotificationsetting = GetNotificationSetting(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));


//*************************Parsing for Get_customer*************************************************
            try {
                JSONObject obj = new JSONObject(responseGetCustomer);
                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONObject data_obj = obj.getJSONObject("Data");
                    ModelgetCustomer modelgetCustomer = new ModelgetCustomer();
                    modelgetCustomer.ws_users_id = data_obj.getString("ws_users_id");
                    modelgetCustomer.business_name = data_obj.getString("business_name");
                    modelgetCustomer.business_abn = data_obj.getString("business_abn");
                    modelgetCustomer.billing_add_id = data_obj.getString("billing_add_id");
                    modelgetCustomer.default_shipping_add_id = data_obj.getString("default_shipping_add_id");
                    modelgetCustomer.external_record_id = data_obj.getString("external_record_id");
                    modelgetCustomer.credit_limit = data_obj.getString("credit_limit");
                    modelgetCustomer.customer_group_name = data_obj.getString("customer_group_name");
                    modelgetCustomer.prefix_name = prefs.getString("prefix_name", null);
//*********calling Databasequries insert function for getcustomer***********************************


                    if (!query.iscustomerExist(prefs.getString("prefix_name", null), modelgetCustomer)) {
                        query.getcustomer(modelgetCustomer);
                    } else {
                        query.update_newcustomer(modelgetCustomer, prefs.getString("prefix_name", null));
                    }
                   /* if (gettype.equals("update")) {
                        //update view
                        if (query.isexist("getcustomer", "ws_users_id", modelgetCustomer.ws_users_id, prefs.getString("prefix_name", null))) {
                            query.updatecustomer(modelgetCustomer);
                        } else {
                            query.getcustomer(modelgetCustomer);
                        }

                    } else {
                        query.getcustomer(modelgetCustomer);
                    }*/

                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (movefurther) {
                responseAddDetailCustomer = AddDetailMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));

            }
//*************************Parsing for Add_detail_customer******************************************
            try {
                JSONObject obj = new JSONObject(responseAddDetailCustomer);
                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Add_Detail_Customer modeladddetail = new Model_Add_Detail_Customer();
                        modeladddetail.id = obj1.getString("id");
                        modeladddetail.ws_users_id = obj1.getString("ws_users_id");
                        modeladddetail.first_name = obj1.getString("first_name");
                        modeladddetail.last_name = obj1.getString("last_name");
                        modeladddetail.add_1 = obj1.getString("add_1");
                        modeladddetail.add_2 = obj1.getString("add_2");
                        modeladddetail.add_3 = obj1.getString("add_3");
                        modeladddetail.suburb = obj1.getString("suburb");
                        modeladddetail.postcode = obj1.getString("postcode");
                        modeladddetail.tel_1 = obj1.getString("tel_1");
                        modeladddetail.tel_2 = obj1.getString("tel_2");
                        modeladddetail.fax = obj1.getString("fax");
                        modeladddetail.email = obj1.getString("email");
                        modeladddetail.default_shipping_add = obj1.getString("default_shipping_add");
                        modeladddetail.default_billing_add = obj1.getString("default_billing_add");
                        modeladddetail.status = obj1.getString("status");
                        modeladddetail.created_date = obj1.getString("created_date");
                        modeladddetail.update_date = obj1.getString("update_date");
                        modeladddetail.updated_by = obj1.getString("updated_by");
                        modeladddetail.country = obj1.getString("country");
                        modeladddetail.state = obj1.getString("state");

                        modeladddetail.prefix_name = prefs.getString("prefix_name", null);
//******************calling Databasequries insert function for add_detail_customer******************

                        if (!query.iscustomerdetailExist(prefs.getString("prefix_name", null), modeladddetail)) {
                            query.add_detail_customer(modeladddetail);
                        } else {
                            query.update_adddetail(modeladddetail, prefs.getString("prefix_name", null));
                        }



                    /*    if (gettype.equals("update")) {
                            //update view
                            if (query.isexist("add_detail_customer", "id", modeladddetail.id, prefs.getString("prefix_name", null))) {
                                query.update_detail_customer(modeladddetail);
                            } else {
                                query.add_detail_customer(modeladddetail);
                            }

                        } else {
                            query.add_detail_customer(modeladddetail);
                        }*/

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            movefurther = false;
                            final String result = jobj.optString("result");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }
                   /* data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Dashboard.this, "Error!", data);
                        }
                    });*/
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responsegetcatagories = GetCategoriesMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), category_id);

            }
//******************************Parsing for Get-Categories******************************************
            try {
                JSONObject obj = new JSONObject(responsegetcatagories);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    last_sync_id = obj.getString("last_sync_id");

//****************************saving data to database through model class***************************
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_GetCategories modelgetcategoris = new Model_GetCategories();
                        modelgetcategoris.id = obj1.getString("id");
                        modelgetcategoris.category_name = obj1.getString("category_name");
                        modelgetcategoris.fullname = obj1.getString("fullname");
                        modelgetcategoris.parent_cat_id = obj1.getString("parent_cat_id");
                        modelgetcategoris.root_cat_id = obj1.getString("root_cat_id");
                        modelgetcategoris.level = obj1.getString("level");
                        modelgetcategoris.sort = obj1.getString("sort");
                        modelgetcategoris.status = obj1.getString("status");
                        modelgetcategoris.created_date = obj1.getString("created_date");
                        modelgetcategoris.update_date = obj1.getString("update_date");
                        modelgetcategoris.updated_by = obj1.getString("updated_by");
                        modelgetcategoris.sid = obj1.getString("sid");
                        modelgetcategoris.type = obj1.getString("type");
                        modelgetcategoris.view_type = obj1.getString("Typed");
                        modelgetcategoris.last_sync_id = last_sync_id;

                        modelgetcategoris.prefix_name = prefs.getString("prefix_name", null);
//********************calling Databasequries insert function for getcategories**********************


                        if (!query.isgetcategorisExist(prefs.getString("prefix_name", null), modelgetcategoris)) {
                            query.getcategories(modelgetcategoris);
                        } else {
                            query.update_getcutomer(modelgetcategoris, prefs.getString("prefix_name", null));
                        }

                       /* if (gettype.equals("update")) {
                            if (query.isexist("getcategories", "id", modelgetcategoris.id, prefs.getString("prefix_name", null))) {
                                query.updatecategory(modelgetcategoris);
                            } else {
                                query.getcategories(modelgetcategoris);
                            }
                         *//*   if (modelgetcategoris.type.equals("update")) {
                                //    update sync
                                if (query.isexist("getcategories", "id", modelgetcategoris.id, prefs.getString("prefix_name", null))) {
                                    query.updatecategory(modelgetcategoris);
                                } else {
                                    query.getcategories(modelgetcategoris);
                                }

                            } else {

                                    query.getcategories(modelgetcategoris);

                            }*//*
                        } else {
                            query.getcategories(modelgetcategoris);
                        }*/

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }
                 /*   data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Dashboard.this, "Error!", data);
                        }
                    });*/
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responsegetpage = GetPageMethod(prefs.getString("prefix_name", null));
            }
//*******************************Parsing for Get-Page***********************************************
            try {
                JSONObject obj = new JSONObject(responsegetpage);
                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************************saving data to database through model class**************************

                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_GetPage modelGetPage = new Model_GetPage();
                        modelGetPage.id = obj1.getString("id");
                        modelGetPage.page_url = obj1.getString("page_url");
                        modelGetPage.status = obj1.getString("status");

                        modelGetPage.prefix_name = prefs.getString("prefix_name", null);

//************************calling Databasequries insert function for getcategories******************


                        if (!query.isgetpageExist(prefs.getString("prefix_name", null), modelGetPage)) {
                            query.getpage(modelGetPage);
                        } else {
                            query.update_getpage(modelGetPage, prefs.getString("prefix_name", null));
                        }


                       /* if (gettype.equals("update")) {
                            //update view
                            if (query.isexist("getpage", "id", modelGetPage.id, prefs.getString("prefix_name", null))) {
                                query.updatepage(modelGetPage);
                            } else {
                                query.getpage(modelGetPage);
                            }

                        } else {
                            query.getpage(modelGetPage);
                        }*/

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            movefurther = false;
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }
                  /*  data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Dashboard.this, "Error!", data);
                        }
                    });*/
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responsegetproducts = GetProductsMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), product_id);

            }
//********************************Parsing for Get Products******************************************
            try {
                JSONObject obj = new JSONObject(responsegetproducts);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    product_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_GetProduct modelgetProduct = new Model_GetProduct();
                        modelgetProduct.order_unit_name = obj1.getString("order_unit_name");
                        modelgetProduct.base_unit_name = obj1.getString("base_unit_name");
                        modelgetProduct.id = obj1.getString("id");
                        modelgetProduct.sink_id = obj1.getString("sink_id");
                        modelgetProduct.cat_id = obj1.getString("cat_id");
                        modelgetProduct.product_code = obj1.getString("product_code");
                        modelgetProduct.name = obj1.getString("name");
                        modelgetProduct.desc = obj1.getString("desc");
                        modelgetProduct.status = obj1.getString("status");
                        modelgetProduct.cost_price = obj1.getString("cost_price");
                        modelgetProduct.min_order_qut = obj1.getString("min_order_qut");
                        modelgetProduct.max_order_qut = obj1.getString("max_order_qut");
                        modelgetProduct.taxable = obj1.getString("taxable");
                        modelgetProduct.base_unit_id = obj1.getString("base_unit_id");
                        modelgetProduct.unit_price = obj1.getString("unit_price");
                        modelgetProduct.order_unit_id = obj1.getString("order_unit_id");
                        modelgetProduct.inventory = obj1.getString("inventory");
                        modelgetProduct.spcl_price = obj1.getString("spcl_price");
                        modelgetProduct.warehouse_name = obj1.getString("warehouse_name");
                        modelgetProduct.warehouse_loc = obj1.getString("warehouse_loc");
                        modelgetProduct.stock_loc = obj1.getString("stock_loc");

                        modelgetProduct.img = obj1.getString("image");
                        modelgetProduct.thumb_image = obj1.getString("thumb_image");

                        modelgetProduct.created_date = obj1.getString("created_date");
                        modelgetProduct.update_date = obj1.getString("update_date");
                        modelgetProduct.updated_by = obj1.getString("updated_by");
                        modelgetProduct.type = obj1.getString("type");
                        modelgetProduct.view_type = obj1.getString("Typed");

                        //**Saving Array to a string in database
                        modelgetProduct.tier_price = obj1.getJSONArray("tier_price").toString();
                        //    modelgetProduct.tier_price = new Gson().toJson((obj1.getString("tier_price")));
                        modelgetProduct.prefix_name = prefs.getString("prefix_name", null);
                        modelgetProduct.last_sync_id = product_last_sync_id;
                        //**calling Databasequries insert function for add_detail_customer
                        System.out.println("Pname" + modelgetProduct.name + "  " + " tier::" + modelgetProduct.tier_price);
                        System.out.println();
                        if (!query.isgetproductExist(prefs.getString("prefix_name", null), modelgetProduct)) {
                            query.getproduct(modelgetProduct);
                        } else {
                            query.update_getproduct(modelgetProduct, prefs.getString("prefix_name", null));
                        }





                      /*  if (gettype.equals("update")) {
                            if (query.isexist("getproducts", "id", modelgetProduct.id, prefs.getString("prefix_name", null))) {
                                query.updateproduct(modelgetProduct);
                            } else {
                                query.getproduct(modelgetProduct);
                            }
                           *//* if (modelgetProduct.type.equals("update")) {
                                //    update sync

                                if (query.isexist("getproducts", "id", modelgetProduct.id, prefs.getString("prefix_name", null))) {
                                    query.updateproduct(modelgetProduct);
                                } else {
                                    query.getproduct(modelgetProduct);
                                }
                            } else {

                                    query.getproduct(modelgetProduct);


                            }*//*
                        } else {
                            query.getproduct(modelgetProduct);
                        }*/


                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            movefurther = false;
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }
                  /*  data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Dashboard.this, "Error!", data);
                        }
                    });*/
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//            if (movefurther) {
//                responsegetproductsimage = GetProductsImageMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), product_image_id);
//
//            }
////**************************Parsing for Get Products Image******************************************
//            try {
//                JSONObject obj = new JSONObject(responsegetproductsimage);
//                status = obj.getString("Status");
//
//                if (status.equals("true")) {
//                    productImage_last_sync_id = obj.getString("last_sync_id");
//                    //**saving data to database through model class
//                    JSONArray data_arr = obj.getJSONArray("Data");
//                    for (int i = 0; i < data_arr.length(); i++) {
//                        JSONObject obj1 = data_arr.getJSONObject(i);
//                        Model_Product_Image modelProductimage = new Model_Product_Image();
//                        modelProductimage.id = obj1.getString("id");
//                        modelProductimage.sink_id = obj1.getString("sink_id");
//                        modelProductimage.product_id = obj1.getString("product_id");
//                        modelProductimage.image = obj1.getString("image");
//                        modelProductimage.thumb_image = obj1.getString("thumb_image");
//                        modelProductimage.status = obj1.getString("status");
//                        modelProductimage.is_main = obj1.getString("is_main");
//                        modelProductimage.type = obj1.getString("type");
//                        modelProductimage.created_date = obj1.getString("created_date");
//                        modelProductimage.update_date = obj1.getString("update_date");
//                        modelProductimage.updated_by = obj1.getString("updated_by");
//                        modelProductimage.last_sync_id = productImage_last_sync_id;
//
//                        modelProductimage.prefix_name = prefs.getString("prefix_name", null);
//                        //**calling Databasequries insert function for add_detail_customer
//
//
//                        if (!query.isgetproductImageExist(prefs.getString("prefix_name", null), modelProductimage)) {
//                            query.getproductImage(modelProductimage);
//                        } else {
//                            query.update_getproductImage(modelProductimage, prefs.getString("prefix_name", null));
//                        }
//
//
//                     /*   if (gettype.equals("update")) {
//                            if (query.isexist("get_product_image", "id", modelProductimage.id, prefs.getString("prefix_name", null))) {
//                                query.updateproductImage(modelProductimage);
//                            } else {
//                                query.getproductImage(modelProductimage);
//                            }
//                           *//* if (modelProductimage.type.equals("update")) {
//                                //    update sync
//                                if (query.isexist("get_product_image", "id", modelProductimage.id, prefs.getString("prefix_name", null))) {
//                                    query.updateproductImage(modelProductimage);
//                                } else {
//                                    query.getproductImage(modelProductimage);
//                                }
//
//                            } else {
//
//                                    query.getproductImage(modelProductimage);
//
//                            }*//*
//                        } else {
//                            query.getproductImage(modelProductimage);
//                        }*/
//
//                    }
//
//
//                } else {
//                    if (obj.has("logout")) {
//                        String logout = obj.optString("logout");
//                        if (logout.equals("yes")) {
//                            movefurther = false;
//                            JSONObject jobj = obj.optJSONObject("Data");
//                            final String result = jobj.optString("result");
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                    methods.dialog(result);
//                                }
//                            });
//                        }
//                    }
//                 /*   data = obj.getString("error");
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Utils.dialog(Dashboard.this, "Error!", data);
//                        }
//                    });*/
//                }
//
//            } catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
            if (movefurther) {
                responsegetproductsinventory = GetProductsInventoryMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), inventory_id);
            }
//**********************Parsing for Get Products Inventory******************************************
            try {
                JSONObject obj = new JSONObject(responsegetproductsinventory);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    productInventory_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Product_Inventory modelinventory = new Model_Product_Inventory();
                        modelinventory.id = obj1.getString("id");
                        modelinventory.product_id = obj1.getString("product_id");
                        modelinventory.sink_id = obj1.getString("sink_id");
                        modelinventory.aval_stock = obj1.getString("aval_stock");
                        modelinventory.reserve_qut = obj1.getString("reserve_qut");
                        modelinventory.bck_ord_aval = obj1.getString("bck_ord_aval");
                        modelinventory.status = obj1.getString("status");
                        modelinventory.warehouse_name = obj1.getString("warehouse_name");
                        modelinventory.warehouse_location = obj1.getString("warehouse_location");
                        modelinventory.stock_location = obj1.getString("stock_location");
                        modelinventory.created_date = obj1.getString("created_date");
                        modelinventory.update_date = obj1.getString("update_date");
                        modelinventory.updated_by = obj1.getString("updated_by");
                        modelinventory.type = obj1.getString("type");
                        modelinventory.last_sync_id = productInventory_last_sync_id;

                        modelinventory.prefix_name = prefs.getString("prefix_name", null);
                        //**calling Databasequries insert function for getporductInventory


                        if (!query.isgetproductInventExist(prefs.getString("prefix_name", null), modelinventory)) {
                            query.getproductInventory(modelinventory);
                        } else {
                            query.update_getproductinventory(modelinventory, prefs.getString("prefix_name", null));
                        }

                      /*  if (gettype.equals("update")) {
                            if (query.isexist("getproduct_inventory", "id", modelinventory.id, prefs.getString("prefix_name", null))) {
                                query.updateproductInventory(modelinventory);
                            } else {
                                query.getproductInventory(modelinventory);
                            }
                           *//* if (modelinventory.type.equals("update")) {
                                //    update sync
                                if (query.isexist("getproduct_inventory", "id", modelinventory.id, prefs.getString("prefix_name", null))) {
                                    query.updateproductInventory(modelinventory);
                                } else {
                                    query.getproductInventory(modelinventory);
                                }

                            } else {

                                    query.getproductInventory(modelinventory);

                            }*//*
                        } else {
                            query.getproductInventory(modelinventory);
                        }*/


                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            movefurther = false;
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }
                 /*   data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Dashboard.this, "Error!", data);
                        }
                    });*/
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responsetierpricing = GetTierPricingMethod(prefs.getString("prefix_name", null), prefs.getString("username", null), prefs.getString("auth_code", null), tier_id);

            }
//****************************Parsing for Get Tier Pricing******************************************
            try {
                JSONObject obj = new JSONObject(responsetierpricing);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    tierpriceing_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Tier_pricing modeltier = new Model_Tier_pricing();
                        modeltier.id = obj1.getString("id");
                        modeltier.product_id = obj1.getString("product_id");
                        modeltier.customer_group_id = obj1.getString("customer_group_id");
                        modeltier.quantity_upto = obj1.getString("quantity_upto");
                        modeltier.discount_type = obj1.getString("discount_type");
                        modeltier.discount = obj1.getString("discount");
                        modeltier.created_date = obj1.getString("created_date");
                        modeltier.update_date = obj1.getString("update_date");
                        modeltier.updated_by = obj1.getString("updated_by");
                        modeltier.sink_id = obj1.getString("sink_id");
                        modeltier.type = obj1.getString("type");
                        modeltier.last_sync_id = tierpriceing_last_sync_id;

                        modeltier.prefix_name = prefs.getString("prefix_name", null);
                        //**calling Databasequries insert function for getporductInventory

                        if (!query.isgetproducttierExist(prefs.getString("prefix_name", null), modeltier)) {
                            query.gettierpricing(modeltier);
                        } else {
                            query.update_getproducttier(modeltier, prefs.getString("prefix_name", null));
                        }


                        /*if (gettype.equals("update")) {
                            if (query.isexist("tier_pricing", "id", modeltier.id, prefs.getString("prefix_name", null))) {
                                query.updatetierpricing(modeltier);
                            } else {
                                query.gettierpricing(modeltier);
                            }
                            *//*if (modeltier.type.equals("update")) {
                                //    update sync
                                if (query.isexist("tier_pricing", "id", modeltier.id, prefs.getString("prefix_name", null))) {
                                    query.updatetierpricing(modeltier);
                                } else {
                                    query.gettierpricing(modeltier);
                                }

                            } else {
                                query.gettierpricing(modeltier);
                            }*//*
                        } else {
                            query.gettierpricing(modeltier);
                        }*/
                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            movefurther = false;
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }
                /*    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Dashboard.this, "Error!", data);
                        }
                    });*/
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responseorderinfo = GetOrderInfoMethod(prefs.getString("prefix_name", null), prefs.getString("username", null), prefs.getString("auth_code", null), order_info);

            }
//****************************Parsing for Get OrderInfo*********************************************
            try {
                JSONObject obj = new JSONObject(responseorderinfo);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    order_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Order_Info modelorder = new Model_Order_Info();
                        modelorder.order_id = obj1.getString("order_id");
                        modelorder.customer_id = obj1.getString("customer_id");
                        modelorder.billing_country = obj1.getString("billing_country");
                        modelorder.billing_state = obj1.getString("billing_state");
                        modelorder.billing_suburb = obj1.getString("billing_suburb");
                        modelorder.billing_add_1 = obj1.getString("billing_add_1");
                        modelorder.billing_add_2 = obj1.getString("billing_add_2");
                        modelorder.billing_add_3 = obj1.getString("billing_add_3");
                        modelorder.billing_postcode = obj1.getString("billing_postcode");
                        modelorder.shipping_country = obj1.getString("shipping_country");
                        modelorder.shipping_state = obj1.getString("shipping_state");
                        modelorder.shipping_suburb = obj1.getString("shipping_suburb");
                        modelorder.shipping_add_1 = obj1.getString("shipping_add_1");
                        modelorder.shipping_add_2 = obj1.getString("shipping_add_2");
                        modelorder.shipping_add_3 = obj1.getString("shipping_add_3");
                        modelorder.shipping_postcode = obj1.getString("shipping_postcode");
                        modelorder.shipping_cost = obj1.getString("shipping_cost");
                        modelorder.sub_total = obj1.getString("sub_total");
                        modelorder.tax_total = obj1.getString("tax_total");
                        modelorder.total = obj1.getString("total");
                        modelorder.order_notes = obj1.getString("order_notes");
                        modelorder.internal_notes = obj1.getString("internal_notes");
                        modelorder.order_status = obj1.getString("order_status");
                        modelorder.back_order = obj1.getString("back_order");
                        modelorder.main_order_id = obj1.getString("main_order_id");
                        //   modelorder.delivery_date = obj1.getString("delivery_date");

                        String date = obj1.getString("delivery_date");

                        SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd");
                        Date d = null;
                        try {
                            d = new SimpleDateFormat("MMM d, yyyy H:mm a").parse(date);
                            modelorder.delivery_date = sim.format(d);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat sim2 = new SimpleDateFormat("yyyy-MM-dd");
                        Date d2 = null;
                        try {
                            d2 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
                            modelorder.delivery_date = sim2.format(d2);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        modelorder.delivery_notes = obj1.getString("delivery_notes");
                        modelorder.created_date = obj1.getString("created_date");
                        modelorder.created_by = obj1.getString("created_by");
                        modelorder.update_date = obj1.getString("update_date");
                        modelorder.updated_by = obj1.getString("updated_by");
                        modelorder.order_name = obj1.getString("order_name");
                        modelorder.order_by = obj1.getString("order_by");
                        modelorder.sink_id = obj1.getString("sink_id");
                        modelorder.type = obj1.getString("type");
                        modelorder.last_sync_id = order_last_sync_id;
                        last_order_created = obj1.getString("order_name");
                        Log.i("LLLLLLL:", "" + last_order_created);
                        modelorder.prefix_name = prefs.getString("prefix_name", null);

                        modelorder.show_deliverydate_order = obj1.getString("show_deliverydate_order");
                        modelorder.mandetory_delivery_order = obj1.getString("mandetory_delivery_order");
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("created_at", last_order_created);
                        editor.commit();
                        //**calling Databasequries insert function for getporductInventory
                        if (query.isorderExist(prefs.getString("prefix_name", null), modelorder)) {
                            query.update_neworder(modelorder, prefs.getString("prefix_name", null));

                        } else {
                            query.getorderinfo(modelorder);
                        }


                     /*   if (gettype.equals("update")) {

                            if (query.isexist("order_info", "order_id", modelorder.order_id, prefs.getString("prefix_name", null))) {
                                query.updateorderinfosync(modelorder);
                            } else {
                                query.getorderinfo(modelorder);
                            }

                           *//* if (modelorder.type.equals("update")) {
                                //    update sync
                                //   query.updateorderinfo(modelorder);
                                if (query.isexist("order_info", "order_id", modelorder.order_id, prefs.getString("prefix_name", null))) {
                                    query.updateorderinfosync(modelorder);
                                } else {
                                    query.getorderinfo(modelorder);
                                }

                            } else {
                                query.getorderinfo(modelorder);
                            }*//*
                        } else {
                            query.getorderinfo(modelorder);
                        }*/
                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            movefurther = false;
                            final String result = jobj.optString("result");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }
                  /*  data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Dashboard.this, "Error!", data);
                        }
                    });*/
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responseorderproduct = GetOrderProductMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), "0");
            }

//***************************Parsing for Get Order Product******************************************
            try {
                JSONObject obj = new JSONObject(responseorderproduct);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    //   order_product_last_sync_id = obj.getString("last_sync_id");
                    order_product_last_sync_id = "0";
//                      if(!order_product_last_sync_id.equals("0"))
                    query.delete_table_data(DatabaseQueries.TABLE_ORDER_PRODUCT, "prefix_name", prefs.getString("prefix_name", null));

//                      query.delete_table_data(DatabaseQueries.TABLE_ORDER_PRODUCT, "prefix_name", prefs.getString("prefix_name", null));
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Order_Product modelOrderProduct = new Model_Order_Product();
                        modelOrderProduct.id = obj1.getString("id");
                        modelOrderProduct.order_id = obj1.getString("order_id");
                        modelOrderProduct.product_id = obj1.getString("product_id");
                        modelOrderProduct.product_name = obj1.getString("product_name");
                        modelOrderProduct.product_code = obj1.getString("product_code");
                        modelOrderProduct.qty = obj1.getString("qty");
                        modelOrderProduct.qty_new_order = obj1.getString("qty_new_order");
                        modelOrderProduct.act_qty = obj1.getString("act_qty");
                        modelOrderProduct.unit_price = obj1.getString("unit_price");
                        modelOrderProduct.applied_price = obj1.getString("applied_price");
                        modelOrderProduct.sub_total = obj1.getString("sub_total");
                        modelOrderProduct.sink_id = obj1.getString("sink_id");
                        modelOrderProduct.type = obj1.getString("type");
                        // modelOrderProduct.image = obj1.getString("image");


                        modelOrderProduct.created_date = obj1.getString("created_date");
                        modelOrderProduct.update_date = obj1.getString("update_date");
                        modelOrderProduct.updated_by = obj1.getString("updated_by");
                        modelOrderProduct.last_sync_id = order_product_last_sync_id;


                        modelOrderProduct.prefix_name = prefs.getString("prefix_name", null);
                        System.out.println("productcccc:" + modelOrderProduct.product_id);
                        //**calling Databasequries insert function for getorderProduct

//                        if (!query.isorderproductExist(prefs.getString("prefix_name", null), modelOrderProduct)) {
                        query.getorderproduct(modelOrderProduct);
//                        } else {
                        //                          query.update_neworderproduct(modelOrderProduct, prefs.getString("prefix_name", null));
//                        }


                       /* if (gettype.equals("update")) {
                            if (query.isexist("order_product", "order_id", modelOrderProduct.order_id, prefs.getString("prefix_name", null))) {
                                query.updateorderproduct(modelOrderProduct);
                            } else {
                                query.getorderproduct(modelOrderProduct);
                            }
                            *//*if (modelOrderProduct.type.equals("update")) {
                                //    update sync
                                if (query.isexist("order_product", "order_id", modelOrderProduct.order_id, prefs.getString("prefix_name", null))) {
                                    query.updateorderproduct(modelOrderProduct);
                                } else {
                                    query.getorderproduct(modelOrderProduct);
                                }
                            } else {
                                query.getorderproduct(modelOrderProduct);
                            }*//*
                        } else {
                            query.getorderproduct(modelOrderProduct);
                        }*/
                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            movefurther = false;
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }
                 /*   data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Dashboard.this, "Error!", data);
                        }
                    });*/
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (movefurther) {
                try {
                    template = query.Getlastsync_id(DatabaseQueries.TABLE_ORDER_TEMPLATE, prefix);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (template == null) {
                    template = "0";
                }

                responsetemplate = GetTemplateMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), template, prefs.getString("username", null));
            }
            System.out.println("Responsetemplate::: " + responsetemplate);
//*************************Parsing for Get Order Templates******************************************
            try {
                JSONObject obj = new JSONObject(responsetemplate);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    template_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Template modelTemplate = new Model_Template();
                        modelTemplate.order_template_id = obj1.getString("order_template_id");
                        modelTemplate.customer_id = obj1.getString("customer_id");
                        modelTemplate.template_name = obj1.getString("template_name");
                        modelTemplate.created_date = obj1.getString("created_date");
                        modelTemplate.created_by = obj1.getString("created_by");
                        modelTemplate.sink_id = obj1.getString("sink_id");

                            modelTemplate.type = obj1.getString("type");

                        modelTemplate.prefix_name = prefs.getString("prefix_name", null);

                        //**Saving Array to a string in database

                        modelTemplate.product_id = obj1.getJSONArray("product_id").toString();
                        modelTemplate.product_code = obj1.getJSONArray("product_code").toString();
                        modelTemplate.qty = obj1.getJSONArray("qty").toString();
                        modelTemplate.last_sync_id = template_last_sync_id;
                        System.out.println("modelTemplate.product_id:" + modelTemplate.product_id);
                        System.out.println("modelTemplate.product_code:" + modelTemplate.product_code);
                        System.out.println("modelTemplate.qty:" + modelTemplate.qty);
                        //**calling Databasequries insert function for gettemplates

                        if (!query.isTemplateExist(modelTemplate.prefix_name, modelTemplate.order_template_id)) {
                            query.gettemplate(modelTemplate);
                        } else {
                            query.update_newtemp2(modelTemplate, prefs.getString("prefix_name", null));

                        }
                     /*   if (gettype.equals("update")) {
                            if (query.isexist("order_template", "order_template_id", modelTemplate.order_template_id, prefs.getString("prefix_name", null))) {
                                query.updatetemplate(modelTemplate);
                            } else {
                                query.gettemplate(modelTemplate);
                            }
                            *//*if (modelTemplate.type.equals("update")) {
                                //    update sync

                                if (query.isexist("order_template", "order_template_id", modelTemplate.order_template_id, prefs.getString("prefix_name", null))) {
                                    query.updatetemplate(modelTemplate);
                                } else {
                                    query.gettemplate(modelTemplate);
                                }
                            } else {
                                query.gettemplate(modelTemplate);
                            }*//*
                        } else {
                            query.gettemplate(modelTemplate);
                        }*/
                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            movefurther = false;
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }
                  /*  data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Dashboard.this, "Error!", data);
                        }
                    });*/
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responseGetnotificationsetting = GetNotificationSetting(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));
            }
//---------------------parsing for getnotification settings------------
            try {
                JSONObject jsonObject = new JSONObject(responseGetnotificationsetting);
                String status = jsonObject.optString("Status");
                if (status.equals("true")) {
                    query.delete_table_data(DatabaseQueries.TABLE_NOTIFICATION_SETTINGS, "prefix_name", prefs.getString("prefix_name", null));
                    Model_get_notification item = new Model_get_notification();
                    item.Currency_code = jsonObject.optString("currency_code");
                    item.Currency_symbol = jsonObject.optString("currency_symbol");
                    item.show_deliverydate = jsonObject.optString("show_deliverydate");
                    item.mandetory_delivery = jsonObject.optString("mandetory_delivery");
                    item.time_zone = jsonObject.optString("timezone");
                    item.push_notification = jsonObject.optString("push_notification");
                    item.email_notification = jsonObject.optString("email_notification");
                    item.Prefix_name = prefs.getString("prefix_name", null);
                    query.add_get_notification(item);

//updating shipping cost once find in Login service now if there is change from server updation is done on syncing***
                    ModelLogin modelLogin = new ModelLogin();
                    modelLogin.currency_symbol = jsonObject.optString("currency_symbol");
                    modelLogin.shipping_status = jsonObject.optString("shipping_status");
                    modelLogin.shipping_cost = jsonObject.optString("shipping_cost");
                    modelLogin.tax_label = jsonObject.optString("tax_label");
                    modelLogin.tax_percentage = jsonObject.optString("tax_percentage");
                    editor = prefs.edit();
                    editor.putString("currency_symbol", modelLogin.currency_symbol);
                    editor.putString("shipping_status", modelLogin.shipping_status);
                    editor.putString("shipping_cost", modelLogin.shipping_cost);
                    editor.putString("tax_label", modelLogin.tax_label);
                    editor.putString("tax_percentage", modelLogin.tax_percentage);
                    editor.commit();
                    query.updateLogin(modelLogin, prefs.getString("prefix_name", null));
                    /* if (gettype.equals("update")) {
                        //update view
                        query.update_get_notification(item);
                    } else {
                        query.add_get_notification(item);
                    }*/
                } else {
                    if (jsonObject.has("logout")) {
                        String logout = jsonObject.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = jsonObject.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    }
                }

            } catch (Exception e) {

            }
            return null;
        }

        @Override
        public void onPostExecute(Void o) {
            super.onPostExecute(o);

            if (gettype.equals("update")) {
                try {
                    HomeActivity.progressbar.setVisibility(View.GONE);
                    HomeActivity.img_Update.setVisibility(View.VISIBLE);
                    //  HomeActivity.img_Update.setBackgroundResource(R.drawable.tick);

                    HomeActivity.txt_Update.setText("All products and orders information \n are up to date.");
                } catch (Exception e) {
                }
            }
            flag = 1;
            //  HomeActivity.txt_orderplaced.setText("Last order placed on " + savedateformatter(prefs.getString("created_at", null)));

            try {
                actionbar.setHomeButtonEnabled(false);
                actionbar.setDisplayHomeAsUpEnabled(false);
                viewPager.setAdapter(adapter);
                try {
                    RefreshFragment refreshFragment = (RefreshFragment) adapter.instantiateItem(viewPager, 0);
                    if (refreshFragment != null) {
                        refreshFragment.refresh();
                    }
                } catch (Exception e) {

                }
            } catch (Exception e) {

            }

            //  viewPager.setOnTouchListener(null);
            LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
            tabStrip.setEnabled(true);
            for (int i = 0; i < tabStrip.getChildCount(); i++) {
                tabStrip.getChildAt(i).setClickable(true);
            }
        }
    }

//*==*==*==*==*==*==*==**************Webservice parameter GetCustomerMethod*************************

    public String GetCustomerMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.getcustomer, builder, Dashboard.this);
        return res;
    }

//************************Webservice parameter GetCategories****************************************

    public String GetCategoriesMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getcategories, builder, Dashboard.this);
        return res;
    }

//******************************Webservice parameter GetPage****************************************

    public String GetPageMethod(String prefix_name) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name);
        res = parser.getJSONFromUrl(Utils.getpage, builder, Dashboard.this);
        return res;
    }


//******************************Webservice parameter AddDetailMethod********************************

    public String AddDetailMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.adddetailcustomer, builder, Dashboard.this);
        return res;
    }

//************************Webservice parameter GetProductsMethod ***********************************

    public String GetProductsMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproducts, builder, Dashboard.this);
        return res;
    }

//***************************Webservice parameter GetProductsImageMethod ***************************

    public String GetProductsImageMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproductsimage, builder, Dashboard.this);
        return res;
    }

//************************Webservice parameter GetProductsINVENTORYMethod **************************

    public String GetProductsInventoryMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproductsinventory, builder, Dashboard.this);
        return res;
    }

//************************Webservice parameter GetProductsINVENTORYMethod **************************

    public String GetTierPricingMethod(String prefix_name, String username, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", username)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproducttierpricing, builder, Dashboard.this);
        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderInfoMethod(String prefix_name, String username, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", username)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getorderinfo, builder, Dashboard.this);

        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderProductMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);

        res = parser.getJSONFromUrl(Utils.getorderproduct, builder, Dashboard.this);
        System.out.println("sync res:-  " + res + " " + "" + sid);
        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetTemplateMethod(String prefix_name, String auth_code, String sid, String username) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid)
                .appendQueryParameter("username", username);
        res = parser.getJSONFromUrl(Utils.getordertemplate, builder, Dashboard.this);
        return res;
    }
    //*************************Webservice parameter getNotificationSetting *************************

    public String GetNotificationSetting(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.getNotification_setting, builder, Dashboard.this);
        return res;
    }

    //********************************************Delete Date from Tables***************************
    public void delete_data() {

        try {

            query.delete_table_data(DatabaseQueries.TABLE_GETCUSTOMER, "prefix_name", prefs.getString("prefix_name", null));
            query.delete_table_data(DatabaseQueries.TABLE_ADD_DETAIL_CUSTOMER, "prefix_name", prefs.getString("prefix_name", null));
            query.delete_table_data(DatabaseQueries.TABLE_GETCATEGORIES, "prefix_name", prefs.getString("prefix_name", null));
            query.delete_table_data(DatabaseQueries.TABLE_GETPAGE, "prefix_name", prefs.getString("prefix_name", null));
            query.delete_table_data(DatabaseQueries.TABLE_GET_PRODUCTS, "prefix_name", prefs.getString("prefix_name", null));
            query.delete_table_data(DatabaseQueries.TABLE_GET_PRODUCT_IMAGE, "prefix_name", prefs.getString("prefix_name", null));
            query.delete_table_data(DatabaseQueries.TABLE_GET_PRODUCT_INVENTORY, "prefix_name", prefs.getString("prefix_name", null));
            query.delete_table_data(DatabaseQueries.TABLE_TIER_PRICING, "prefix_name", prefs.getString("prefix_name", null));
            query.delete_table_data(DatabaseQueries.TABLE_ORDER_INFO, "prefix_name", prefs.getString("prefix_name", null));
            query.delete_table_data(DatabaseQueries.TABLE_ORDER_PRODUCT, "prefix_name", prefs.getString("prefix_name", null));
            query.delete_table_data(DatabaseQueries.TABLE_ORDER_TEMPLATE, "prefix_name", prefs.getString("prefix_name", null));
            //query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
            query.delete_table_data(DatabaseQueries.TABLE_NOTIFICATION_SETTINGS, "prefix_name", prefs.getString("prefix_name", null));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String savedateformatter(String senddate) {

        // TODO Auto-generated method stub
        String setdate = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = format1.parse(senddate);
            format1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            setdate = format1.format(newDate);
        } catch (Exception e) {
            setdate = "NA";
        }
        return setdate;

    }

//***********************Inserting Offline created Templates************************************

    class inserttemp extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        String status, produt_id = "", qtty = "";
        String response;
        JSONObject data_obj;
        ArrayList<Model_Template> item_2;
        String temp_order_id, last_sink_id;

        public inserttemp(ArrayList<Model_Template> item_2) {

            this.item_2 = item_2;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Dashboard.this);
            dialog.setMessage("Inserting Template...");
            dialog.setCancelable(false);
            dialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            for (int i = 0; i < item_2.size(); i++) {
                if (item_2.size() > 0 && item_2.get(i).type.equals("newupdate")) {
                    try {
                        pidarr = new JSONArray(item_2.get(i).product_id);
                        qtyarr = new JSONArray(item_2.get(i).qty);
                        skuarr = new JSONArray(item_2.get(i).product_code);


                        StringBuilder idbuilder = new StringBuilder();
                        StringBuilder qtybuilder = new StringBuilder();
                        StringBuilder codebuilder = new StringBuilder();

//                        pidarr.put(item_2.get(i).product_id);
//                        qtyarr.put(item_2.get(i).qty);
//                        skuarr.put(item_2.get(i).product_code);

                        for (int m = 0; m < pidarr.length(); m++) {


                            try {
                                idbuilder.append(pidarr.getString(m) + ",");
                                qtybuilder.append(qtyarr.getString(m) + ",");
                                codebuilder.append(skuarr.getString(m) + ",");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                        //for (int e = 0;e < pidarr.length();e++) {
                        String pid = idbuilder.toString();
                        if (pid.indexOf(",") != -1) {
                            pid = pid.substring(0, pid.length() - 1);
                        }
                        String pqty = qtybuilder.toString();
                        if (pqty.indexOf(",") != -1) {
                            pqty = pqty.substring(0, pqty.length() - 1);
                        }
                        String pcode = codebuilder.toString();
                        if (pcode.indexOf(",") != -1) {
                            pcode = pcode.substring(0, pcode.length() - 1);
                        }
                        //   }
//                    try {
//                        String q= String.valueOf(new JSONArray(item.get(0).prod_sku));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    String type = "new1";
//                    cartproduct_id.add(pid);
//                    cartproduct_qty.add(pqty);
//                    cartproduct_sku.add(pcode);
                        //popdialog(pid, pqty, pcode);
                        qtty = pqty;
                        produt_id = pcode;
//                    try {
//                        qtty =qtyarr.getString(i);
//                        produt_id = skuarr.getString(i);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }

//                    produt_id=item_2.get(i).product_code;

//                   cartproduct_id.set(i, cartproduct_id.get(i));
//                     cartproduct_qty.set(i, cartproduct_qty.get(i));
//
//                    produt_id  = cartproduct_sku.get(i);
//                    qtty   = cartproduct_qty.get(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    // cartproduct_id.set(i, "");
                    qtty = "";
                    produt_id = "";
//                    produt_id="";
//                            qtty="";
                }
                response = InsertMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null),
                        item_2.get(i).template_name, produt_id, qtty);
                System.out.println("Res:: " + response);

                try

                {
                    JSONObject obj = new JSONObject(response);

                    status = obj.getString("Status");

                    if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                        data_obj = obj.getJSONObject("Data");
                        temp_order_id = data_obj.getString("order_template_id");
                        last_sink_id = data_obj.getString("sync_id");
                        query.update_offtemp(item_2.get(i).order_template_id, temp_order_id, last_sink_id, prefs.getString("prefix_name", null), "updated");
//
//                        Model_Template a = new Model_Template();
//                        a.template_name = item_2.get(i).template_name;
//                        a.order_template_id = temp_order_id;
//                        a.product_id = pidarr.toString();
//                        a.product_code = skuarr.toString();
//                        a.qty = qtyarr.toString();
//                        a.prefix_name = prefs.getString("prefix_name", "");
//                        a.type = "new";
//                        query.gettemplate(a);
                        // query.gettemplate(a);
                        //  item.add(a);

                        Dashboard.tempupdate = true;

                    } else {
                        status = obj.getString("Status");
                        if (status.equals("false")) {
                            JSONObject data_obj = obj.getJSONObject("Data");
                            //    result = data_obj.getString("result");

                        }
                    }

                } catch (
                        JSONException e
                        )

                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            try {
                if (status.equals("true")) {
                    //for (int i = 0; i < item_2.size(); i++) {

                    //query.deleteTitle(item_2.get(i).order_template_id,"off");
                    // item_2.remove(i);

                    //  }
                    try {
                        templist = query.GetTemplate(prefix, "newupdate");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!Utils.isNetworkConnected(Dashboard.this)) {
                        Utils.conDialog(Dashboard.this);
                    } else {
                        if(templist.size()>0) {
                            new updateteplate().execute();
                        }
                    }
                    setTitle(arrayList.get(tabLayout.getSelectedTabPosition()));
//                try {
//                    item = query.GetTemplate(prefix, "all");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                if (item.size() > 0) {
//                    newmenu.findItem(R.id.edit).setVisible(true);
//                    noordertemp.setVisibility(View.GONE);
//                    temp_list.setVisibility(View.VISIBLE);
//                    tadapter = new TemplateAdapter(getActivity(), item);
//                    temp_list.setAdapter(tadapter);
//                } else {
//                    noordertemp.setVisibility(View.VISIBLE);
//                    temp_list.setVisibility(View.GONE);
                } else {
//                try{
//                    if (data_obj.has("logout")) {
//                        String logout = data_obj.optString("logout");
//                        if (logout.equals("yes")) {
//                            JSONObject jobj = data_obj.optJSONObject("Data");
//                            final String result = jobj.optString("result");
//
//
//                            methods.dialog(result);
//
//                        }
//                    } else {
//                        Toast.makeText(getActivity(), "Template not Saved", Toast.LENGTH_SHORT).show();
//                    }
//                }catch(Exception e){
//
//                }
                }
            } catch (Exception e) {
            }
        }

    }
    //************************Webservice parameter InsertMethod*************************

    public String InsertMethod(String prefix_name, String auth_code, String name, String product_code, String qty) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("template_name", name)
                .appendQueryParameter("product_id", product_code)
                .appendQueryParameter("qty", qty);
        res = parser.getJSONFromUrl(Utils.insert_temp, builder, Dashboard.this);
        return res;
    }
}

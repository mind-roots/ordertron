package com.ordertron;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Abhijeet on 10/6/2016.
 */

public class FirstView extends Activity{
    SharedPreferences prefs1;
    SharedPreferences.Editor editor1;
    TextView textlasm;
    Button tour,skip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firstview);
        init();
        clicks();
        //textlasm.setText();
    }

    private void clicks() {
        tour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FirstView.this, ViewpagerActivity.class);
                     startActivity(i);
                editor1=prefs1.edit();
                editor1.putString("dontshowtutorial", "dontshowtutorial");
                editor1.commit();
                finish();
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor1=prefs1.edit();
                editor1.putString("dontshowtutorial", "dontshowtutorial");
                editor1.commit();

                finish();
            }
        });

    }

    private void init() {
        textlasm=(TextView)findViewById(R.id.textlasm);
        skip=(Button)findViewById(R.id.skip);
        tour=(Button)findViewById(R.id.tour);
        prefs1 = getSharedPreferences("tutorial", MODE_PRIVATE);
    }
}

package com.ordertron;

import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by Abhijeet on 1/4/2016.
 */
public class View_Product_detail extends AppCompatActivity {
    ImageView image,defimage;
    TextView product_name, product_code, txt_apl_price, txt_price, desc, max_qty, min_qty, avail_stock, category, txt_discount;
    SharedPreferences prefs;
    String currency_symbol;
    ActionBar actionBar;
    ProgressBar progressBar;
    RelativeLayout t_lay;
    NumberFormat formatter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    //*********************************Initialization****************************************************
    private void init() {
        formatter = new DecimalFormat("#0.00");
        actionBar = getSupportActionBar();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        image = (ImageView) findViewById(R.id.image);
        defimage = (ImageView) findViewById(R.id.defimage);
        product_name = (TextView) findViewById(R.id.product_name);
        product_code = (TextView) findViewById(R.id.product_code);
        txt_apl_price = (TextView) findViewById(R.id.txt_apl_price);
        txt_price = (TextView) findViewById(R.id.txt_price);
        category = (TextView) findViewById(R.id.category);
        avail_stock = (TextView) findViewById(R.id.avail_stock);
        min_qty = (TextView) findViewById(R.id.min_qty);
        max_qty = (TextView) findViewById(R.id.max_qty);
        desc = (TextView) findViewById(R.id.desc);
        txt_discount = (TextView) findViewById(R.id.txt_discount);
        t_lay = (RelativeLayout) findViewById(R.id.t_lay);

        prefs = getSharedPreferences("login", MODE_PRIVATE);
        currency_symbol = prefs.getString("currency_symbol", null);
//***************************************setting values*********************************************
        String url = Product_detail.image;
        if (url != null) {
            url = url.replaceAll(" ", "%20"); // remove all whitespaces
        }

        // Picasso.with(View_Product_detail.this).load(url).placeholder(R.drawable.default_image).into(image);
        Glide.with(getApplicationContext())
                .load(url)
                .placeholder(R.drawable.default_item_icon)
                .crossFade()
                .thumbnail(0.1f)
                .into(image);
//        Picasso.with(getApplicationContext()).load(url)
//
//                .into(image, new Callback() {
//
//                    @Override
//                    public void onSuccess() {
//                        // TODO Auto-generated method stub
//                        progressBar.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onError() {
//                        // TODO Auto-generated method stub
//                        progressBar.setVisibility(View.GONE);
//                        defimage.setVisibility(View.VISIBLE);
//
//                    }
//                });

        product_name.setText(Product_detail.name);
        actionBar.setTitle(Product_detail.name);
        product_code.setText(Product_detail.product_code);

        desc.setText(Product_detail.desc);
        min_qty.setText(Product_detail.min_order_qut+" "+Product_detail.Order_unit_name);
        max_qty.setText(Product_detail.max_order_qut+" "+Product_detail.Order_unit_name);

        category.setText(Product_detail.fullcatname);
        avail_stock.setText(Product_detail.avail_stock1);

        if (Product_detail.spcl_price.equals("0")) {
            txt_apl_price.setText(currency_symbol + formatter.format(Double.parseDouble(Product_detail.price)) + "/" + Product_detail.base_unit_name);
        } else {
            txt_price.setText(currency_symbol + formatter.format(Double.parseDouble(Product_detail.price)) + "/" + Product_detail.base_unit_name);

            //*************'stoke on text ****************************
            txt_price.setPaintFlags(txt_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            txt_apl_price.setText(currency_symbol + formatter.format(Double.parseDouble(Product_detail.spcl_price)) + "/" + Product_detail.base_unit_name);
        }


        try {
            JSONArray jarr = new JSONArray(Product_detail.tier_price);
            // if (jarr!=null||)
            /*"quantity_upto":"5",
                    "discount_type":"Percentage",
                    "discount":"10"*/
            if (jarr != null) {
                String setdiscount = "";
                if (jarr.length() != 0) {
                    for (int i = 0; i < jarr.length(); i++) {
                        JSONObject obj = jarr.getJSONObject(i);
                        String quantity_upto = obj.optString("quantity_upto");
                        String discount = obj.optString("discount");
                        if (i == jarr.length() - 1) {
                            setdiscount = setdiscount + "Buy " + quantity_upto + " or more - " + discount + "%";
                        } else {
                            setdiscount = setdiscount + "Buy " + quantity_upto + " or more - " + discount + "%\n";
                        }


                    }
                    txt_discount.setText(setdiscount);
                }else{
                    t_lay.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
        }
    }
}

package com.ordertron;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.JSONParser;
import Model.Methods;
import Model.Utils;

public class DialogActivity extends Activity {

    SharedPreferences prefs, prefs_auth;
    ProgressDialog dialog2;
    ListView list_time;
    String auth_code, prefixx;
    ArrayAdapter<String> arrayadp;
    ArrayList<String> timezone = new ArrayList<String>();
    boolean b;
    Methods methods;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog);
        list_time = (ListView) findViewById(R.id.list_time);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        b = Utils.isNetworkConnected(DialogActivity.this);


        // user = prefs.getString("user", null);

//		idAdapter = new ArrayAdapter<String>(this,
//				android.R.layout.simple_spinner_item, idArray);
//		sdf = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss");
//		idAdapter
//				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		list_time.setAdapter(idAdapter);
//		getGMTTime();

        list_time.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in = new Intent();
                String name = arrayadp.getItem(position);
                System.out.print("NAME:: " + name);
                in.putExtra("name", name);
                setResult(RESULT_OK, in);
                finish();
            }
        });
        if (timezone.size() > 0) {
            arrayadp = new ArrayAdapter<String>(DialogActivity.this, android.R.layout.simple_list_item_1, timezone);
            list_time.setAdapter(arrayadp);
        } else {
            if (!b) {
                Utils.conDialog(DialogActivity.this);
            } else {
                new Timezone().execute();
            }
        }


//		Log.e("size......", ""+Prefrences.time.size());


//		list_time.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				// TODO Auto-generated method stub
//
//				    String resultdate =arrayadp.getItem(position);
//
//				More_Prefrences.timeset.setText(resultdate);
//				finish();
//			}
//
//
//		});
    }

    private class Timezone extends AsyncTask<Void, Void, Void> {

        String status;
        JSONObject obj;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            dialog2 = ProgressDialog.show(DialogActivity.this,
                    "Loading", "Please Wait..", true);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String responseTimezone = TimezoneMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));
            System.out.print("TIMEZONE RESPOSNCE::: " + responseTimezone);
            try {
                obj = new JSONObject(responseTimezone);
                status = obj.getString("Status");

                if (status.equals("true")) {


                    JSONArray data_obj = obj.getJSONArray("Data");
                    for (int i = 0; i < data_obj.length(); i++) {
                        JSONObject obj1 = data_obj.getJSONObject(i);
                        JSONArray array = obj1.getJSONArray("timezones");
                        for (int k = 0; k < array.length(); k++) {
                            timezone.add(array.getString(k));

                        }
                    }


                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            if (obj.has("logout")) {
                                String logout = obj.optString("logout");
                                if (logout.equals("yes")) {
                                    JSONObject jobj = obj.optJSONObject("Data");
                                    final String result = jobj.optString("result");
                                    dialog(DialogActivity.this, "Error!", "Problem in updating data.");
//                                    methods = new Methods(DialogActivity.this);
//                                    Methods.placeorder=1;
//                                    methods.dialog(result);


                                }
                            } else {

                                //dialog("Alert!", result, android.R.drawable.ic_dialog_alert);


                            }

                            //dialog("Alert!", result, android.R.drawable.ic_dialog_alert);




                            //  Utils.dialog(DialogActivity.this,"Error!","Data not available.");

                        }
                    });
                    // Toast.makeText(getApplicationContext(), "Status false", Toast.LENGTH_SHORT).show();
                    //System.out.println("Status false::: " );
                }

//						Log.e("timearray", "" + time);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dialog2.dismiss();
            for (int k = 0; k < timezone.size(); k++) {
                System.out.println("Timezone::: " + timezone.get(k));
            }
            arrayadp = new ArrayAdapter<String>(DialogActivity.this, android.R.layout.simple_list_item_1, timezone);
            list_time.setAdapter(arrayadp);
//				list_time.setAdapter(arrayadp);
//				Log.e("Timesize......", ""+Prefrences.time.size());
        }
    }


    //************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String TimezoneMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);

        res = parser.getresFromUrl(Utils.timezone, builder);
        return res;
    }

    // *******************************************Alert Dialog**************************************
    public void dialog(final Context ctx, String title, String msg) {

        new AlertDialog.Builder(ctx, Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete

                                dialog.dismiss();
                                ((Activity)ctx).finish();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

//	private void getGMTTime() {
//		current = Calendar.getInstance();
////		txtCurrentTime.setText("" + current.getTime());
//		miliSeconds = current.getTimeInMillis();
//		TimeZone tzCurrent = current.getTimeZone();
//		int offset = tzCurrent.getRawOffset();
//		if (tzCurrent.inDaylightTime(new Date())) {
//			offset = offset + tzCurrent.getDSTSavings();
//		}
//		miliSeconds = miliSeconds - offset;
//		resultdate = new Date(miliSeconds);
//		System.out.println(sdf.format(resultdate));
//	}
}

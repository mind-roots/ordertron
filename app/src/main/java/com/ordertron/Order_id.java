package com.ordertron;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Fragments.OrderDetail_Fragment;
import Fragments.Product_Order;
import Model.Model_Order_Info;
import Model.Model_Order_Product;

/**
 * Created by Abhijeet on 12/16/2015.
 */
public class Order_id extends AppCompatActivity {

    public static Menu menu;
    RelativeLayout rl_product_click, rl_order_click;
    TextView txt_ordrs, txt_product;
    public static String shipping_cost, sub_total, total;

    public static String order_status, order_id;
    DatabaseQueries databaseQueries;
    SharedPreferences prefs;
    public static String prefix_name,auth_code;
    public static ArrayList<Model_Order_Info> order_infolist = new ArrayList<>();
    public static ArrayList<Model_Order_Product> ordrprod = new ArrayList<Model_Order_Product>();

    LinearLayout data_view;
    ImageView no_data;
    ActionBar actionBar;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_id);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        getsetdata();
        clickevents();
        if (order_infolist.size() > 0) {
            data_view.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);
            initialfrag();
        } else {
            finish();
            data_view.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }


    }

    private void initialfrag() {


        rl_product_click.setBackgroundResource(R.drawable.product_unselect);
        rl_order_click.setBackgroundResource(R.drawable.order_select);
        txt_ordrs.setText("Order Details");
        txt_product.setText("Products in Order");
        txt_ordrs.setTextColor(Color.parseColor("#ffffff"));
        txt_product.setTextColor(Color.parseColor("#30a9dd"));
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        OrderDetail_Fragment ff = new OrderDetail_Fragment();
        fragmentTransaction.add(R.id.fragment_container, ff);
        fragmentTransaction.commit();

    }

    private void getsetdata() {
        order_infolist.clear();
        try {
            String query = "SELECT * FROM order_info WHERE prefix_name ='" + prefix_name + "' AND order_status ='" + order_status + "' AND order_id ='" + order_id + "'";
            order_infolist = databaseQueries.Select_orders(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ordrprod.clear();
        try {
            ordrprod = databaseQueries.GetProductWimage("order_product", order_id, prefix_name);
            //   Toast.makeText(getApplication(),""+ordrprod.size(),Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        actionBar = getSupportActionBar();
        databaseQueries = new DatabaseQueries(Order_id.this);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        prefix_name = prefs.getString("prefix_name", null);
        auth_code = prefs.getString("auth_code", null);

        rl_product_click = (RelativeLayout) findViewById(R.id.rl_product_click);
        rl_order_click = (RelativeLayout) findViewById(R.id.rl_order_click);
        txt_ordrs = (TextView) findViewById(R.id.txt_ordrs);
        txt_product = (TextView) findViewById(R.id.txt_product);
        no_data = (ImageView) findViewById(R.id.no_data);
        data_view = (LinearLayout) findViewById(R.id.data_view);


        Intent intent = getIntent();
        order_status = intent.getStringExtra("order_status");
        //     Toast.makeText(Order_id.this,order_status,Toast.LENGTH_LONG).show();
        order_id = intent.getStringExtra("order_id");
        actionBar.setTitle("Order # " + order_id);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.order, menu);
        this.menu = menu;

        if (order_status.equals("Submitted")) {
            menu.findItem(R.id.edit).setVisible(true);
            menu.findItem(R.id.update).setVisible(false);
        } else {
            menu.findItem(R.id.edit).setVisible(false);
            menu.findItem(R.id.update).setVisible(false);
        }


        return true;
    }

    public void menu_edit(MenuItem item) {
        Intent intent = new Intent(Order_id.this, Order_Edit.class);
        intent.putExtra("order_id", order_id);

        startActivity(intent);


    }

    public void menu_update(MenuItem item) {

    }


    public void clickevents() {

        rl_order_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_product_click.setBackgroundResource(R.drawable.product_unselect);
                rl_order_click.setBackgroundResource(R.drawable.order_select);
                txt_ordrs.setTextColor(Color.parseColor("#ffffff"));
                txt_product.setTextColor(Color.parseColor("#30a9dd"));
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                OrderDetail_Fragment ff = new OrderDetail_Fragment();
                fragmentTransaction.replace(R.id.fragment_container, ff);
                fragmentTransaction.commit();


            }
        });

        rl_product_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_product_click.setBackgroundResource(R.drawable.product_select);
                rl_order_click.setBackgroundResource(R.drawable.order_unselect);
                txt_ordrs.setTextColor(Color.parseColor("#30a9dd"));
                txt_product.setTextColor(Color.parseColor("#ffffff"));
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Product_Order ff = new Product_Order();
                fragmentTransaction.replace(R.id.fragment_container, ff);
                fragmentTransaction.commit();


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Orders_listing.cancelorder) {
            Orders_listing.cancelorder=false;
            finish();
        } else {
            getsetdata();
            if (order_infolist.size() > 0) {
                data_view.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.GONE);
                initialfrag();
            } else {
                finish();
                data_view.setVisibility(View.GONE);
                no_data.setVisibility(View.VISIBLE);
            }
        }
    }
}

package com.ordertron;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Adapters.Order_edit_adapter;
import DataBaseUtils.DatabaseQueries;
import Fragments.OrderDetail_Fragment;
import Model.JSONParser;
import Model.Methods;
import Model.Model_Order_Info;
import Model.Model_Order_Product;
import Model.Model_get_notification;
import Model.Utils;

/**
 * Created by Abhijeet on 12/17/2015.
 */
public class Order_Edit extends AppCompatActivity {
    //     TextView billing_add, shipping_add;
//    EditText edt_order_notes,  edt_order_by;
    public static RecyclerView product_list;
    public static Order_edit_adapter productAdapter;
    DatabaseQueries databaseQueries;
    Menu menu;
    public static ArrayList<Model_Order_Product> ordrprod = new ArrayList<>();
    public static ArrayList<Model_Order_Info> order_infolist = new ArrayList<>();
    // public static boolean value;
    public static String billing_add_1, billing_add_2, billing_add_3, billing_suburb, billing_state, billing_country, billing_postcode, shipping_add_1, shipping_add_2, shipping_add_3, shipping_suburb, shipping_state, shipping_country, shipping_postcode, order_id;
    public static String order_notes, delivery_notes, order_by, delivery_date;
    public static String Full_billing_adress, Full_shipping_adress;
    //    LinearLayout cancel_order;
    String prefix_name, auth_code, username, nedate;
    SharedPreferences prefs;
    RecyclerView.LayoutManager mLayoutManager;
    public static boolean edit = false, prodedit = false;
    boolean b;
    ActionBar actionbar;
    String showdeliverydate, mand_date, get_date, daa = "";
    public static ArrayList<Model_get_notification> showdelivery = new ArrayList<>();
    Methods methods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_edit);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        clickEvents();
        getsetdata();
        methods = new Methods(Order_Edit.this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    private void getsetdata() {
        try {

            String query = "SELECT * FROM order_info WHERE prefix_name ='" + prefix_name + "' AND order_id ='" + order_id + "'";
            order_infolist = databaseQueries.Select_orders(query);

            ordrprod.clear();
            ordrprod = databaseQueries.GetProductWimage("order_product", order_id, prefix_name);

        } catch (Exception e) {
            e.printStackTrace();

        }

//*******************if order is empty then finish the activity else set the data*******************
if(Order_id.order_infolist.size()==0){
    finish();
}else {
    billing_add_1 = Order_id.order_infolist.get(0).billing_add_1;
    billing_add_2 = Order_id.order_infolist.get(0).billing_add_2;
    billing_add_3 = Order_id.order_infolist.get(0).billing_add_3;
    billing_suburb = Order_id.order_infolist.get(0).billing_suburb;
    billing_state = Order_id.order_infolist.get(0).billing_state;
    billing_country = Order_id.order_infolist.get(0).billing_country;
    billing_postcode = Order_id.order_infolist.get(0).billing_postcode;
    shipping_add_1 = Order_id.order_infolist.get(0).shipping_add_1;
    shipping_add_2 = Order_id.order_infolist.get(0).shipping_add_2;
    shipping_add_3 = Order_id.order_infolist.get(0).shipping_add_3;
    shipping_suburb = Order_id.order_infolist.get(0).shipping_suburb;
    shipping_state = Order_id.order_infolist.get(0).shipping_state;
    shipping_country = Order_id.order_infolist.get(0).shipping_country;
    shipping_postcode = Order_id.order_infolist.get(0).shipping_postcode;

    order_notes = Order_id.order_infolist.get(0).order_notes;
    delivery_date = Order_id.order_infolist.get(0).delivery_date;
    delivery_notes = Order_id.order_infolist.get(0).delivery_notes;
    order_by = Order_id.order_infolist.get(0).order_by;

    Full_billing_adress = Order_id.order_infolist.get(0).billing_add_1;

    if (!Order_id.order_infolist.get(0).billing_add_2.equals("")) {
        Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_add_2;
    }
    if (!Order_id.order_infolist.get(0).billing_add_3.equals("")) {
        Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_add_3;
    }
    if (!Order_id.order_infolist.get(0).billing_suburb.equals("")) {
        Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_suburb;
    }
    if (!Order_id.order_infolist.get(0).billing_state.equals("")) {
        Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_state;
    }
    if (!Order_id.order_infolist.get(0).billing_country.equals("")) {
        Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_country;
    }
    if (!Order_id.order_infolist.get(0).billing_postcode.equals("")) {
        Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_postcode;
    }

    Full_shipping_adress = Order_id.order_infolist.get(0).shipping_add_1;

    if (!Order_id.order_infolist.get(0).shipping_add_2.equals("")) {
        Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_add_2;
    }
    if (!Order_id.order_infolist.get(0).shipping_add_3.equals("")) {
        Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_add_3;
    }
    if (!Order_id.order_infolist.get(0).shipping_suburb.equals("")) {
        Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_suburb;
    }
    if (!Order_id.order_infolist.get(0).shipping_state.equals("")) {
        Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_state;
    }
    if (!Order_id.order_infolist.get(0).shipping_country.equals("")) {
        Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_country;
    }
    if (!Order_id.order_infolist.get(0).shipping_postcode.equals("")) {
        Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_postcode;
    }

       /* billing_add.setText(Full_billing_adress);
        shipping_add.setText(Full_shipping_adress);
        edt_order_notes.setText(order_notes);
        edt_delivery_notes.setText(delivery_notes);
        edt_order_by.setText(order_by);*/


    productAdapter = new Order_edit_adapter(Order_Edit.this, ordrprod);
    product_list.setAdapter(productAdapter);
}
    }

    public void init() {
        actionbar = getSupportActionBar();
        b = Utils.isNetworkConnected(Order_Edit.this);
        databaseQueries = new DatabaseQueries(getApplication());
//        cancel_order = (LinearLayout) findViewById(R.id.canceloreder_ll);
//        billing_add = (TextView) findViewById(R.id.billing_address);
//        shipping_add = (TextView) findViewById(R.id.shipping_address);
//        edt_order_notes = (EditText) findViewById(R.id.order_notes);
//        edt_delivery_notes = (EditText) findViewById(R.id.delivery_notes);
//        edt_order_by = (EditText) findViewById(R.id.placed_by);
        product_list = (RecyclerView) findViewById(R.id.product_list);
        mLayoutManager = new LinearLayoutManager(Order_Edit.this);
        product_list.setLayoutManager(mLayoutManager);


        prefs = getSharedPreferences("login", MODE_PRIVATE);
        prefix_name = prefs.getString("prefix_name", null);
        auth_code = prefs.getString("auth_code", null);
        username = prefs.getString("username", null);

        Intent i = getIntent();
        order_id = i.getStringExtra("order_id");
        actionbar.setTitle("Order # " + order_id);

    }

    //***********************click events for UI********************************************

    public void clickEvents() {
       /* billing_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Order_Edit.this, Edit_address.class);
                intent.putExtra("type", "billing");
                intent.putExtra("billing_suburb", billing_suburb);
                intent.putExtra("billing_add1", billing_add_1);
                intent.putExtra("billing_add2", billing_add_2);
                intent.putExtra("billing_add3", billing_add_3);
                intent.putExtra("billing_state", billing_state);
                intent.putExtra("billing_country", billing_country);
                intent.putExtra("billing_postcode", billing_postcode);

                startActivity(intent);
            }
        });

        shipping_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Order_Edit.this, Edit_address.class);
                intent.putExtra("type", "shipping");
                intent.putExtra("shipping_add1", shipping_add_1);
                intent.putExtra("shipping_add2", shipping_add_2);
                intent.putExtra("shipping_add3", shipping_add_3);
                intent.putExtra("shipping_suburb", shipping_suburb);
                intent.putExtra("shipping_state", shipping_state);
                intent.putExtra("shipping_country", shipping_country);
                intent.putExtra("shipping_postcode", shipping_postcode);

                startActivity(intent);
            }
        });

        edt_order_notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_order_notes.setCursorVisible(true);
            }
        });
        edt_delivery_notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_delivery_notes.setCursorVisible(true);
            }
        });
        edt_order_by.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_order_by.setCursorVisible(true);
            }
        });
        cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!b) {
                    Utils.conDialog(Order_Edit.this);
                } else {
                    new cancelOrder().execute();
                }


            }
        });*/
    }

    //**********Action Bar Function*********************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.order, menu);
        this.menu = menu;
        menu.findItem(R.id.edit).setVisible(false);
        menu.findItem(R.id.update).setVisible(true);
        return true;
    }

    public void menu_edit(MenuItem item) {

    }

    public void menu_update(MenuItem item) {

        if (!b) {
            Utils.conDialog(Order_Edit.this);
        } else {
            try {
                showdelivery = databaseQueries.get_notification_setting(prefs.getString("prefix_name", null));
                OrderDetail_Fragment.orderinfo = databaseQueries.getmand_date(prefs.getString("prefix_name", null), Order_id.order_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (int i = 0; i < showdelivery.size(); i++) {
                showdeliverydate = showdelivery.get(i).show_deliverydate;
                mand_date = showdelivery.get(i).mandetory_delivery;
                //**********************getting particular order delivery date ************************************

                OrderDetail_Fragment.show_deliverydate_order = OrderDetail_Fragment.orderinfo.get(0).show_deliverydate_order;
                OrderDetail_Fragment.mandetory_delivery_order = OrderDetail_Fragment.orderinfo.get(0).mandetory_delivery_order;
            }
            if (OrderDetail_Fragment.mandetory_delivery_order.equals("yes")) {
                if (Order_edit_adapter.setdiv.getText().toString().equals("") || Order_edit_adapter.edt_order_by.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please provide the Mandatory Fields", Toast.LENGTH_SHORT).show();
                } else {
                    new updation().execute();
                }
            } else {
                if (Order_edit_adapter.edt_order_by.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please provide the Mandatory Fields", Toast.LENGTH_SHORT).show();
                } else {
                    new updation().execute();
                }
            }

        }


    }

    public class updation extends AsyncTask<String, Void, String> {
        String response, prodstatus = "false", addstatus = "false";
        ProgressDialog dialog;
        String addresponse, text_order_notes, text_delivery_notes, text_order_by, delivery_date1;
        String responsegetproductsinventory;
        String DATE_DASH_FORMAT = "yyyy-MM-dd";
        String DATE_FORMAT = "dd/MM/yyyy";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Order_Edit.this);
            dialog.setMessage("Updating order...");
            dialog.show();
            dialog.setCancelable(false);
            text_delivery_notes = Order_edit_adapter.edt_delivery_notes.getText().toString();
            text_order_notes = Order_edit_adapter.edt_order_notes.getText().toString();
            text_order_by = Order_edit_adapter.edt_order_by.getText().toString();
            delivery_date1 = Order_edit_adapter.setdiv.getText().toString();
            if (delivery_date1 == null) {
                delivery_date1 = "";
            }

            SimpleDateFormat sim2 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date d = new SimpleDateFormat(" EEE, dd MMM yyyy").parse(delivery_date1);
                delivery_date1 = sim2.format(d);
                nedate = delivery_date1;
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... params) {
            //---------------------updating order info-----------------------//
            addresponse = updateOrder(order_id, prefix_name, username, auth_code, billing_country, billing_state, billing_suburb
                    , billing_add_1, billing_add_2, billing_add_3, billing_postcode, shipping_country, shipping_state, shipping_suburb
                    , shipping_add_1, shipping_add_2, shipping_add_3, shipping_postcode, text_order_notes, text_order_by, text_delivery_notes, delivery_date1);
            try {

                JSONObject jsonObject = new JSONObject(addresponse);
                addstatus = jsonObject.getString("Status");
                if (addstatus.equals("true")) {
                    JSONObject jsonObject1 = jsonObject.getJSONObject("Data");
                    String order_id = jsonObject1.getString("order_id");

                    databaseQueries.updata_orderadd_info(order_id, prefix_name, billing_country, billing_state, billing_suburb
                            , billing_add_1, billing_add_2, billing_add_3, billing_postcode, shipping_country, shipping_state, shipping_suburb
                            , shipping_add_1, shipping_add_2, shipping_add_3, shipping_postcode, text_order_notes, text_order_by, text_delivery_notes, nedate);

                } else {
                    if (jsonObject.has("logout")) {
                        String logout = jsonObject.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = jsonObject.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    methods.dialog(result);
                                }
                            });


                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            //---------------updating editable products-----------------//
            if (prodedit) {
                prodedit = false;
                StringBuilder qtybuilder = new StringBuilder();
                StringBuilder pcodebuilder = new StringBuilder();
                for (int i = 0; i < ordrprod.size(); i++) {

                    pcodebuilder.append(ordrprod.get(i).product_code + ",");
                    qtybuilder.append(ordrprod.get(i).qty + ",");

                }
                for (int i = 0; i < Order_edit_adapter.removeids.size(); i++) {

                    pcodebuilder.append(Order_edit_adapter.removeids.get(i) + ",");
                    qtybuilder.append("0" + ",");

                }
                String pcode = pcodebuilder.toString();
                if (pcode.indexOf(",") != -1) {
                    pcode = pcode.substring(0, pcode.length() - 1);
                }
                String pqty = qtybuilder.toString();
                if (pqty.indexOf(",") != -1) {
                    pqty = pqty.substring(0, pqty.length() - 1);
                }


                response = update_prod_order(pcode, pqty, order_id);
                try {
                    JSONObject obj = new JSONObject(response);
                    prodstatus = obj.optString("Status");
                    if (prodstatus.equals("true")) {
                        JSONObject jobj = obj.optJSONObject("Data");

                        String sub_total = jobj.optString("sub_total");
                        String tax_total = jobj.optString("tax_total");
                        String total = jobj.optString("total");
                        String shipping_cost = jobj.optString("shipping_cost");
                        String order_id = jobj.optString("order_id");
                        //update order info table
                        databaseQueries.updateorderinfo(prefix_name, order_id, shipping_cost, total, tax_total, sub_total);
                        databaseQueries.delete_order_prod(order_id, prefix_name);
                        responsegetproductsinventory = GetProductsInventoryMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), "0");

                        String responseorderproduct = GetOrderProductMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));


//***************************Parsing for Get Order Product******************************************
                        try {
                            JSONObject objj = new JSONObject(responseorderproduct);
                            String status = obj.getString("Status");

                            if (status.equals("true")) {
                                String order_product_last_sync_id = objj.getString("last_sync_id");
                                //**saving data to database through model class
                                JSONArray data_arr = objj.getJSONArray("Data");
                                for (int i = 0; i < data_arr.length(); i++) {
                                    JSONObject obj1 = data_arr.getJSONObject(i);
                                    Model_Order_Product modelOrderProduct = new Model_Order_Product();
                                    String oid = obj1.getString("order_id");
                                    modelOrderProduct.id = obj1.getString("id");
                                    modelOrderProduct.order_id = obj1.getString("order_id");
                                    modelOrderProduct.product_id = obj1.getString("product_id");
                                    modelOrderProduct.product_name = obj1.getString("product_name");
                                    modelOrderProduct.product_code = obj1.getString("product_code");
                                    modelOrderProduct.qty = obj1.getString("qty");
                                    modelOrderProduct.qty_new_order = obj1.getString("qty_new_order");
                                    modelOrderProduct.act_qty = obj1.getString("act_qty");
                                    modelOrderProduct.unit_price = obj1.getString("unit_price");
                                    modelOrderProduct.applied_price = obj1.getString("applied_price");
                                    modelOrderProduct.sub_total = obj1.getString("sub_total");
                                    modelOrderProduct.sink_id = obj1.getString("sink_id");
                                    modelOrderProduct.type = obj1.getString("type");
                                    modelOrderProduct.created_date = obj1.getString("created_date");
                                    modelOrderProduct.update_date = obj1.getString("update_date");
                                    modelOrderProduct.updated_by = obj1.getString("updated_by");
                                    modelOrderProduct.last_sync_id = order_product_last_sync_id;


                                    modelOrderProduct.prefix_name = prefs.getString("prefix_name", null);
                                    //**calling Databasequries insert function for getorderProduct
                                    if (oid.equals(order_id)) {
                                        databaseQueries.getorderproduct(modelOrderProduct);
                                    }

//                                    if (databaseQueries.isexist("order_product", "order_id", modelOrderProduct.order_id, prefs.getString("prefix_name", null))) {
//                                        databaseQueries.updateorderproduct(modelOrderProduct);
//                                    } else {
//                                        databaseQueries.getorderproduct(modelOrderProduct);
//                                    }

                                }


                            } else {
                                if (objj.has("logout")) {
                                    String logout = objj.optString("logout");
                                    if (logout.equals("yes")) {
                                        JSONObject jobjj = objj.optJSONObject("Data");
                                        final String result = jobjj.optString("result");
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                methods.dialog(result);
                                            }
                                        });


                                    }
                                }
                                //  data = obj.getString("error");
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        //-----------update inventory----------------//
                        try {
                            JSONObject obj1 = new JSONObject(responsegetproductsinventory);
                            String status = obj.getString("Status");

                            if (status.equals("true")) {
                                String productInventory_last_sync_id = obj1.getString("last_sync_id");
                                //**saving data to database through model class
                                JSONArray data_arr = obj.getJSONArray("Data");
                                if (data_arr != null) {
                                    if (data_arr.length() != 0) {
                                        for (int i = 0; i < data_arr.length(); i++) {

                                            JSONObject jobj1 = data_arr.getJSONObject(i);
                                            String id = jobj1.getString("id");
                                            String product_id = jobj1.getString("product_id");
                                            String sink_id = jobj1.getString("sink_id");
                                            String aval_stock = jobj1.getString("aval_stock");
                                            String reserve_qut = jobj1.getString("reserve_qut");
                                            String bck_ord_aval = jobj1.getString("bck_ord_aval");
                                            String instatus = jobj1.getString("status");
                                            String type = jobj1.getString("type");

                                            databaseQueries.updateinventdb(id, product_id, sink_id, aval_stock, reserve_qut, bck_ord_aval, instatus, type, productInventory_last_sync_id, prefix_name);

                                        }
                                    }
                                }


                            } else {
                                if (obj1.has("logout")) {
                                    String logout = obj1.optString("logout");
                                    if (logout.equals("yes")) {
                                        JSONObject jobjj = obj1.optJSONObject("Data");
                                        final String result = jobjj.optString("result");
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                methods.dialog(result);
                                            }
                                        });


                                    }
                                }
                                //data = obj.getString("error");
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//Toast.makeText(getApplicationContext(),"addstaturs= "+addstatus+" porsdstatus= "+prodstatus,Toast.LENGTH_SHORT).show();
            try {
                if (addstatus.equals("true") || prodstatus.equals("true")) {
                    Order_edit_adapter.removeids.clear();
                    dialog.dismiss();
                    dialog("Success", "Your order has been updated.", android.R.drawable.ic_dialog_info);

                } else {
                    dialog("Error", "Your order has not been updated due to some technical issue. Please try again later.", android.R.drawable.ic_dialog_info);
                    dialog.dismiss();
                }
            }catch (Exception e){

            }
        }
    }


    //*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderProductMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", "1");
        res = parser.getJSONFromUrl(Utils.getorderproduct, builder, Order_Edit.this);
        return res;
    }

    public String GetProductsInventoryMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproductsinventory, builder, Order_Edit.this);
        return res;
    }
    //*************************Webservice parameter update_order  *************************

    public String updateOrder(String order_id, String prefix_name, String user_name, String auth_code, String billing_country
            , String billing_state, String billing_suburb, String billing_add_1, String billing_add_2, String billing_add_3
            , String billing_postcode, String shipping_country, String shipping_state, String shipping_suburb
            , String shipping_add_1, String shipping_add_2, String shipping_add_3, String shipping_postcode
            , String order_notes, String order_by, String delivery_notes, String delivery_date) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("order_id", order_id)
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", user_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("billing_country", billing_country)
                .appendQueryParameter("billing_state", billing_state)
                .appendQueryParameter("billing_suburb", billing_suburb)
                .appendQueryParameter("billing_add_1", billing_add_1)
                .appendQueryParameter("billing_add_2", billing_add_2)
                .appendQueryParameter("billing_add_3", billing_add_3)
                .appendQueryParameter("billing_postcode", billing_postcode)
                .appendQueryParameter("shipping_country", shipping_country)
                .appendQueryParameter("shipping_state", shipping_state)
                .appendQueryParameter("shipping_suburb", shipping_suburb)
                .appendQueryParameter("shipping_add_1", shipping_add_1)
                .appendQueryParameter("shipping_add_2", shipping_add_2)
                .appendQueryParameter("shipping_add_3", shipping_add_3)
                .appendQueryParameter("shipping_postcode", shipping_postcode)
                .appendQueryParameter("order_notes", order_notes)
                .appendQueryParameter("order_by", order_by)
                .appendQueryParameter("delivery_notes", delivery_notes)
                .appendQueryParameter("delivery_date", delivery_date);
        res = parser.getJSONFromUrl(Utils.order_edit, builder, Order_Edit.this);
        return res;
    }


    //*************************Webservice parameter update_prod_order  *************************

    public String update_prod_order(String prod_code, String prodqty, String orderId) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("order_id", orderId)
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("username", username)
                .appendQueryParameter("product_code", prod_code)
                .appendQueryParameter("qty", prodqty);
        res = parser.getJSONFromUrl(Utils.orderproduct_edit, builder, Order_Edit.this);
        return res;
    }

    public void dialog(String title, String msg, int icon) {
//        new AlertDialog.Builder(Order_Edit.this, R.style.DialogTheme)
//                .setTitle(title)
//                .setMessage(msg)
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        dialog.dismiss();
//                        finish();
//
//                    }
//                })
//
//                .setIcon(icon)
//                .show();

        new AlertDialog.Builder(Order_Edit.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        finish();
                    }
                }).show();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (edit) {
            edit = false;
            Order_edit_adapter.total.clear();
            productAdapter.notifyDataSetChanged();
        }
    }
}

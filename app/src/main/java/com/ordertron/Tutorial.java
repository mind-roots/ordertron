package com.ordertron;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by Abhijeet on 9/16/2016.
 */

public class Tutorial extends AppCompatActivity {

    RelativeLayout rl_order,rl_templates,rl_history,rl_message,rl_setmore;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        clicks();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void init() {
        rl_order=(RelativeLayout)findViewById(R.id.rl_order);
        rl_templates=(RelativeLayout)findViewById(R.id.rl_templates);
        rl_history=(RelativeLayout)findViewById(R.id.rl_history);
        rl_message=(RelativeLayout)findViewById(R.id.rl_message);
        rl_setmore=(RelativeLayout)findViewById(R.id.rl_setmore);
    }

    private void clicks() {

        rl_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tutorial.this, Tutorial_View.class);

                intent.putExtra("Title", "Orders");
                startActivity(intent);
            }
        });
        rl_templates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tutorial.this, Tutorial_View.class);

                intent.putExtra("Title", "Temp");
                startActivity(intent);
            }
        });
        rl_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tutorial.this, Tutorial_View.class);

                intent.putExtra("Title", "history");
                startActivity(intent);
            }
        });
        rl_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tutorial.this, Tutorial_View.class);

                intent.putExtra("Title", "message");
                startActivity(intent);
            }
        });
        rl_setmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tutorial.this, Tutorial_View.class);

                intent.putExtra("Title", "setting");
                startActivity(intent);
            }
        });
    }
}

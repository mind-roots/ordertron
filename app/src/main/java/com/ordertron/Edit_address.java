package com.ordertron;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import Model.Utils;

/**
 * Created by Abhijeet on 12/17/2015.
 */
public class Edit_address extends AppCompatActivity {
    RelativeLayout rl_click;
    public static EditText edit_add1, edit_add2, edit_add3, edit_suburb, edit_postcode,edit_state;
    TextView edit_country;
    // Spinner edit_state;
    public static String item_state, type;
    String billing_suburb, billing_add1, billing_add2, billing_add3, billing_state, billing_country, billing_postcode, shipping_add1, shipping_add2, shipping_add3, shipping_suburb, shipping_state, shipping_country, shipping_postcode;
    Boolean val;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_billing_add);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

        //setSpinnerdate();


        click();

    }

    public void init() {
        rl_click = (RelativeLayout) findViewById(R.id.rl_click);
        edit_add1 = (EditText) findViewById(R.id.edit_add1);
        edit_add2 = (EditText) findViewById(R.id.edit_add2);
        edit_add3 = (EditText) findViewById(R.id.edit_add3);
        edit_suburb = (EditText) findViewById(R.id.edit_suburb);
        edit_state = (EditText) findViewById(R.id.edit_state);
        edit_country = (TextView) findViewById(R.id.edit_country);
        edit_postcode = (EditText) findViewById(R.id.edit_postcode);

        Intent i = getIntent();
        type = i.getStringExtra("type");
        billing_suburb = i.getStringExtra("billing_suburb");
        billing_add1 = i.getStringExtra("billing_add1");
        billing_add2 = i.getStringExtra("billing_add2");
        billing_add3 = i.getStringExtra("billing_add3");
        billing_state = i.getStringExtra("billing_state");
        billing_country = i.getStringExtra("billing_country");
        billing_postcode = i.getStringExtra("billing_postcode");
        shipping_add1 = i.getStringExtra("shipping_add1");
        shipping_add2 = i.getStringExtra("shipping_add2");
        shipping_add3 = i.getStringExtra("shipping_add3");
        shipping_suburb = i.getStringExtra("shipping_suburb");
        shipping_state = i.getStringExtra("shipping_state");
        shipping_country = i.getStringExtra("shipping_country");
        shipping_postcode = i.getStringExtra("shipping_postcode");


        if (type.equals("billing")) {
            edit_add1.setText("" + billing_add1);
            edit_add1.setSelection(edit_add1.getText().length());
            if (billing_add2.equals("null")) {
            } else {
                edit_add2.setText("" + billing_add2);
            }
            if (billing_add3.equals("null")) {
            } else {
                edit_add3.setText("" + billing_add3);
            }
            if (billing_suburb.equals("null")) {
            } else {
                edit_suburb.setText("" + billing_suburb);
            }
            item_state = billing_state;
            val = Boolean.valueOf(item_state);
            edit_state.setSelected(val);

            edit_state.setText(""+billing_state);
            edit_country.setText("" + billing_country);
            edit_postcode.setText("" + billing_postcode);

        }
        if (type.equals("shipping")) {
            edit_add1.setText("" + shipping_add1);
            edit_add1.setSelection(edit_add1.getText().length());
            if (shipping_add2.equals("null")) {
            } else {
                edit_add2.setText("" + shipping_add2);
            }
            if (shipping_add3.equals("null")) {
            } else {
                edit_add3.setText("" + shipping_add3);
            }
            if (shipping_suburb.equals("null")) {
            } else {
                edit_suburb.setText("" + shipping_suburb);
            }
            item_state = shipping_state;
            edit_state.setText(""+shipping_state);

            edit_country.setText("" + shipping_country);
            edit_postcode.setText("" + shipping_postcode);
//            edit_phone.setText("" + OrderDetail_Fragment.phone);
//            edit_fax.setText("" + OrderDetail_Fragment.fax);
        }
    }

    public void click() {
        edit_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.MyDialogSingle(Edit_address.this,edit_country,edit_country.getText().toString());
            }
        });
        rl_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order_Edit.edit=true;
                if (edit_add1.length() == 0) {
                    Toast.makeText(Edit_address.this, "Please fill all required feilds", Toast.LENGTH_LONG).show();
                } else if (edit_suburb.length() == 0) {
                    Toast.makeText(Edit_address.this, "Please fill all required feilds", Toast.LENGTH_LONG).show();
                } else if (edit_country.length() == 0) {
                    Toast.makeText(Edit_address.this, "Please fill all required feilds", Toast.LENGTH_LONG).show();
                }
//                else if (edit_postcode.length() == 0) {
//                    Toast.makeText(Edit_address.this, "Please fill all required feilds", Toast.LENGTH_LONG).show();
//                }
                else if (edit_state.length() == 0) {
                    Toast.makeText(Edit_address.this, "Please fill all required feilds", Toast.LENGTH_LONG).show();
                } else {
                    if (type.equals("billing")) {


//                        Order_Edit.billing_add.setText("" + edit_suburb.getText().toString() + "," + edit_add1.getText().toString()
//                                + "," + edit_add2.getText().toString() + "," + edit_add3.getText().toString() + "," + Boolean.valueOf(val).toString() + "," + edit_country.getText().toString()
//                                + "," + edit_postcode.getText().toString() + "," + edit_phone.getText().toString() + "," + edit_fax.getText().toString());
                        Order_Edit.billing_add_1 = edit_add1.getText().toString();
                        Order_Edit.billing_add_2 = edit_add2.getText().toString();
                        Order_Edit.billing_add_3 = edit_add3.getText().toString();
                        Order_Edit.billing_suburb = edit_suburb.getText().toString();
                        // int pos = edit_state.getSelectedItemPosition();
                        Order_Edit.billing_state = edit_state.getText().toString();
                        // Order_Edit.billing_state = Boolean.valueOf(val).toString();
                        Order_Edit.billing_country = edit_country.getText().toString();
                        Order_Edit.billing_postcode = edit_postcode.getText().toString();


                        String Full_billing_adress = Order_Edit.billing_add_1;

                        if (Order_Edit.billing_add_2.length() != 0) {
                            Full_billing_adress = Full_billing_adress + ", " + Order_Edit.billing_add_2;
                        }
                        if (Order_Edit.billing_add_3.length() != 0) {
                            Full_billing_adress = Full_billing_adress + ", " + Order_Edit.billing_add_3;
                        }

                        Full_billing_adress = Full_billing_adress + ", " + Order_Edit.billing_suburb;


                        Full_billing_adress = Full_billing_adress + ", " + Order_Edit.billing_state;


                        Full_billing_adress = Full_billing_adress + ", " + Order_Edit.billing_country;


                        Full_billing_adress = Full_billing_adress + ", " + Order_Edit.billing_postcode;


                        // Order_Edit.billing_add.setText(Full_billing_adress);
                        Order_Edit.Full_billing_adress = Full_billing_adress;
                        finish();

                    } else if (type.equals("shipping")) {

//                        Order_Edit.shipping_add.setText("" + edit_suburb.getText().toString() + "," + edit_add1.getText().toString()
//                                + "," + edit_add2.getText().toString() + "," + edit_add3.getText().toString() + "," + item_state + "," + edit_country.getText().toString()
//                                + "," + edit_postcode.getText().toString() + "," + edit_phone.getText().toString() + "," + edit_fax.getText().toString());
                        Order_Edit.shipping_add_1 = edit_add1.getText().toString();
                        Order_Edit.shipping_add_2 = edit_add2.getText().toString();
                        Order_Edit.shipping_add_3 = edit_add3.getText().toString();
                        Order_Edit.shipping_suburb = edit_suburb.getText().toString();
                        //  int pos = edit_state.getSelectedItemPosition();
                        Order_Edit.shipping_state = edit_state.getText().toString();
                        Order_Edit.shipping_country = edit_country.getText().toString();
                        Order_Edit.shipping_postcode = edit_postcode.getText().toString();


                        String Full_shipping_adress = Order_Edit.shipping_add_1;

                        if (Order_Edit.shipping_add_2.length() != 0) {
                            Full_shipping_adress = Full_shipping_adress + ", " + Order_Edit.shipping_add_2;
                        }
                        if (Order_Edit.shipping_add_3.length() != 0) {
                            Full_shipping_adress = Full_shipping_adress + ", " + Order_Edit.shipping_add_3;
                        }

                        Full_shipping_adress = Full_shipping_adress + ", " + Order_Edit.shipping_suburb;

                        Full_shipping_adress = Full_shipping_adress + ", " + Order_Edit.shipping_state;

                        Full_shipping_adress = Full_shipping_adress + ", " + Order_Edit.shipping_country;

                        Full_shipping_adress = Full_shipping_adress + ", " + Order_Edit.shipping_postcode;

                        //  Order_Edit.shipping_add.setText(Full_shipping_adress);
                        Order_Edit.Full_shipping_adress = Full_shipping_adress;
                        finish();
                    }
                }

            }
        });
    }
}

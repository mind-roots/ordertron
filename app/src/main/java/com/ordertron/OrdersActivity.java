package com.ordertron;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import DataBaseUtils.DatabaseQueries;

/**
 * Created by android on 12/14/2015.
 */
public class OrdersActivity extends ActionBarActivity {
    LinearLayout linear_all_orders, linear_submitd_orders, linear_processing_orders,
            linear_completed_orders, linear_back_orders, linear_cancel_orders;
    TextView txt_all_count, txt_sub_count, txt_cancl_count, txt_process_count, txt_complet_count, txt_back_count;
    //    public static ArrayList<Model_Order_Info> allOrder_arrayList = new ArrayList<Model_Order_Info>();
//    public static ArrayList<Model_Order_Info> submitted_arrayList = new ArrayList<Model_Order_Info>();
//    public static ArrayList<Model_Order_Info> cancel_arrayList = new ArrayList<Model_Order_Info>();
    DatabaseQueries databaseQueries;
    SharedPreferences prefs;
    String prefix_name;
    int allcount = 0, subcount = 0, cancelcount = 0, processcount = 0, compltcount = 0, backcount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orders);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        onit();

    }

    private void init() {
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        prefix_name = prefs.getString("prefix_name", null);
        txt_all_count = (TextView) findViewById(R.id.txt_all_count);
        txt_sub_count = (TextView) findViewById(R.id.txt_sub_count);
        txt_cancl_count = (TextView) findViewById(R.id.txt_cancl_count);
        txt_process_count = (TextView) findViewById(R.id.txt_process_count);
        txt_complet_count = (TextView) findViewById(R.id.txt_complet_count);
        txt_back_count = (TextView) findViewById(R.id.txt_back_count);
        databaseQueries = new DatabaseQueries(OrdersActivity.this);
        linear_all_orders = (LinearLayout) findViewById(R.id.linear_all_orders);
        linear_submitd_orders = (LinearLayout) findViewById(R.id.linear_submitd_orders);
        linear_processing_orders = (LinearLayout) findViewById(R.id.linear_processing_orders);
        linear_completed_orders = (LinearLayout) findViewById(R.id.linear_completed_orders);
        linear_back_orders = (LinearLayout) findViewById(R.id.linear_back_orders);
        linear_cancel_orders = (LinearLayout) findViewById(R.id.linear_cancel_orders);

        try {
//            allOrder_arrayList = databaseQueries.Select_order_id();
//            submitted_arrayList = databaseQueries.Select_submittedorder_id();
//            cancel_arrayList = databaseQueries.Select_cancelorder_id();

            allcount = databaseQueries.getallcount(prefix_name, "all");
            subcount = databaseQueries.getallcount(prefix_name, "Submitted");
            processcount = databaseQueries.getallcount(prefix_name, "Processing");
            compltcount = databaseQueries.getallcount(prefix_name, "Completed");
            backcount = databaseQueries.getallcount(prefix_name, "Back");
            cancelcount = databaseQueries.getallcount(prefix_name, "Cancelled");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //*********Setting values for All order , submitted order .............................

        txt_all_count.setText(String.valueOf(allcount) + " Orders");
        txt_sub_count.setText(String.valueOf(subcount) + " Orders");
        txt_process_count.setText(String.valueOf(processcount) + " Orders");
        txt_complet_count.setText(String.valueOf(compltcount) + " Orders");
        txt_back_count.setText(String.valueOf(backcount) + " Orders");
        txt_cancl_count.setText(String.valueOf(cancelcount) + " Orders");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
              finish();
                break;
        }
        return true;
    }
    private void onit() {

        linear_all_orders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(OrdersActivity.this, Orders_listing.class);
                intent.putExtra("Type", "all");
                intent.putExtra("Title", "All Orders");
                startActivity(intent);
            }
        });
        linear_submitd_orders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrdersActivity.this, Orders_listing.class);
                intent.putExtra("Type", "Submitted");
                intent.putExtra("Title", "Submitted Orders");
                startActivity(intent);
            }
        });
        linear_processing_orders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrdersActivity.this, Orders_listing.class);
                intent.putExtra("Type", "Processing");
                intent.putExtra("Title", "Processing Orders");
                startActivity(intent);
            }
        });
        linear_completed_orders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrdersActivity.this, Orders_listing.class);
                intent.putExtra("Type", "Completed");
                intent.putExtra("Title", "Completed Orders");
                startActivity(intent);
            }
        });
        linear_back_orders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrdersActivity.this, Orders_listing.class);
                intent.putExtra("Type", "Back Order");
                intent.putExtra("Title", "Back Orders");
                startActivity(intent);
            }
        });
        linear_cancel_orders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrdersActivity.this, Orders_listing.class);
                intent.putExtra("Type", "Cancelled");
                intent.putExtra("Title", "Cancelled Orders");
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Dashboard.reorder){
            finish();
        }
        try {
            allcount = databaseQueries.getallcount(prefix_name, "all");
            subcount = databaseQueries.getallcount(prefix_name, "Submitted");
            processcount = databaseQueries.getallcount(prefix_name, "Processing");
            compltcount = databaseQueries.getallcount(prefix_name, "Completed");
            backcount = databaseQueries.getallcount(prefix_name, "Back Order");
            cancelcount = databaseQueries.getallcount(prefix_name, "Cancelled");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //*********Setting values for All order , submitted order .............................
        txt_all_count.setText(String.valueOf(allcount) + " Orders");
        txt_sub_count.setText(String.valueOf(subcount) + " Orders");
        txt_process_count.setText(String.valueOf(processcount) + " Orders");
        txt_complet_count.setText(String.valueOf(compltcount) + " Orders");
        txt_back_count.setText(String.valueOf(backcount) + " Orders");
        txt_cancl_count.setText(String.valueOf(cancelcount) + " Orders");
    }
}
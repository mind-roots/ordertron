package com.ordertron;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;

import Adapters.Template_ProductAdapter;
import DataBaseUtils.DatabaseQueries;
import Fragments.CartActivity;
import Model.Model_Add_To_Cart;
import Model.Model_GetProduct;
import Model.Model_Template;

/**
 * Created by Abhijeet on 12/7/2016.
 */
public class Template_Products extends AppCompatActivity {


    public static String name, qty, products, tempid, prefix, productscode;

    RecyclerView recyclerView;
    LinearLayoutManager manager;
    DatabaseQueries query;
    public static Template_ProductAdapter adapter;
    Button btn_cart, btn_product;
    public static ArrayList<Model_GetProduct> item = new ArrayList<Model_GetProduct>();
    SharedPreferences prefs;
    public static Menu newmenu;
    public static int temp_pr_edit = 0;
    ArrayList<String> cartlist = new ArrayList<>();
    ActionBar actionBar;
    public static ImageView no_order_product;
    int exceed = 0, one = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_products);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        clickevent();


    }


    private void init() {

        Intent it = getIntent();
        actionBar = getSupportActionBar();
        query = new DatabaseQueries(Template_Products.this);
        recyclerView = (RecyclerView) findViewById(R.id.temp_list);
        manager = new LinearLayoutManager(Template_Products.this);
        btn_cart = (Button) findViewById(R.id.btn_cart);
        btn_product = (Button) findViewById(R.id.btn_product);
        no_order_product = (ImageView) findViewById(R.id.no_order_product);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        prefix = prefs.getString("prefix_name", "");
        name = it.getStringExtra("name");

        tempid = it.getStringExtra("tempid");


        try {


            ArrayList<Model_Template> arr = query.GetTemp_Detail(prefix, tempid);
            item.clear();
            JSONArray productarray = new JSONArray(arr.get(0).product_id);
            JSONArray qtyarray = new JSONArray(arr.get(0).qty);
            JSONArray pcodearray = new JSONArray(arr.get(0).product_code);

            item = query.GetTemplate_product(qtyarray, productarray, prefix);

        } catch (Exception e) {
            e.printStackTrace();
        }
        actionBar.setTitle(name);
        recyclerView.setLayoutManager(manager);
        if (item.size() > 0) {

            no_order_product.setVisibility(View.GONE);
            adapter = new Template_ProductAdapter(Template_Products.this, item);
            recyclerView.setAdapter(adapter);
        } else {

            no_order_product.setVisibility(View.VISIBLE);
        }
    }

    //**********************************clickevents*************************************************
    private void clickevent() {

        btn_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dashboard.indicator = 1;
                Intent i = new Intent(Template_Products.this, Add_Product.class);
                i.putExtra("tempid", tempid);

                // i.putExtra("tempid", name.get(position).order_template_id);
                startActivity(i);
            }
        });
        btn_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartActivity.addtocart = 1;
                if (item.size() == 0) {
                    dialogww(Template_Products.this, "Place products in cart?", "There is no product in cart. Please tap on add products button.");


                } else {
                    try {
                        cartlist = query.GetCart();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (cartlist.size() > 0) {
                        dialog("Warning", "Using this template will clear any items you have in the cart.\n" +
                                "Are you sure you want to start a new order with this template? ", android.R.drawable.ic_dialog_alert);
                    } else {
                        query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
                        addcart();
                    }
                }

            }
        });

    }

    public void dialog(String title, String msg, int icon) {
        new AlertDialog.Builder(Template_Products.this, Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                query.delete(DatabaseQueries.TABLE_ADD_TO_CART);


                                Dashboard.cartupdate = true;
                                dialog.dismiss();
                                addcart();
                            }
                        })
                .setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {

                            }
                        })
                .setIcon(icon).show();


    }

    private void addcart() {
        for (int i = 0; i < Template_ProductAdapter.data.size(); i++) {
            Model_GetProduct get = Template_ProductAdapter.data.get(i);
            //*****************saving data to database through model class**************************************
            Model_Add_To_Cart model_add_to_cart = new Model_Add_To_Cart();
            model_add_to_cart.prod_id = get.id;
            model_add_to_cart.prod_name = get.name;
            model_add_to_cart.prod_sku = get.product_code;
            model_add_to_cart.qty = get.qty;
            model_add_to_cart.price = get.unit_price;
            model_add_to_cart.spcl_price = get.spcl_price;
            model_add_to_cart.max_qty = get.max_order_qut;
            model_add_to_cart.min_qty = get.min_order_qut;
            model_add_to_cart.base_unit = get.base_unit_name;
            model_add_to_cart.order_unit = get.order_unit_name;
            model_add_to_cart.taxable = get.taxable;
            model_add_to_cart.inventory = get.inventory;
            // model_add_to_cart.status = get.status;

            //*********calling Databasequries insert function for getcustomer***********************************
            String availstock = query.getinvent(get.id, prefix);
            String max = get.qty;
            String mi = get.min_order_qut;
            if (max.equals("")) {
                max = String.valueOf(0);
            }
            String maxq = get.max_order_qut;
            if (maxq.equals("")) {
                maxq = "No Limits";
            }
            if (availstock == null) {
                availstock = "0";
            }
            if (availstock != null && !availstock.equals("0") && (Float.parseFloat(max) > Float.parseFloat(availstock))) {
                exceed = 1;

            } else if (maxq != null && !maxq.equals("No Limits") && (Float.parseFloat(max) > Float.parseFloat(maxq))) {


                exceed = 1;

            } else if (!mi.equals("") && (Float.parseFloat(mi) > Float.parseFloat(max))) {


                exceed = 1;


            } else {
                exceed = 0;
                if (get.status.equalsIgnoreCase("Active")) {
                    query.add_to_cart(model_add_to_cart);
                } else {
                    one = 1;
                }


            }
            if (exceed == 1) {
                one = 1;
            }

        }

        if (one == 1) {
            one = 0;
            Toast.makeText(getApplication(), "1 or more line item(s) in your template does not comply with Min / Max Order Qty’s/ Active and have not been added to cart", Toast.LENGTH_LONG).show();
            Dashboard.cartupdate = true;
            Dashboard.tabLayout.getSelectedTabPosition();
            Dashboard.tabLayout.getTabAt(2).select();

            finish();
        } else {
            if (Template_ProductAdapter.data.size() == 0) {
                dialogww(Template_Products.this, "Place products in cart?", "There is no product in cart. Please tap on add products button.");


            } else {
                dialogcart(android.R.drawable.ic_menu_save, "Success", "Product(s) added to the Cart.");

            }
        }
    }

    private void dialogcart(int ic_menu_save, String title, String msg) {

        new AlertDialog.Builder(Template_Products.this, Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                Dashboard.cartupdate = true;
                                Dashboard.tabLayout.getSelectedTabPosition();
                                Dashboard.tabLayout.getTabAt(2).select();

                                finish();
                            }
                        })

                .setIcon(ic_menu_save).show();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Dashboard.tempupdate) {
            Dashboard.tempupdate = false;
            ArrayList<Model_Template> arr = new ArrayList<>();
            try {
                arr = query.GetTemp_Detail(prefix, tempid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                products = arr.get(0).product_id;
                qty = arr.get(0).qty;
                productscode = arr.get(0).product_code;
                JSONArray productarray = new JSONArray(arr.get(0).product_id);
                JSONArray qtyarray = new JSONArray(arr.get(0).qty);
                item.clear();
                item = query.GetTemplate_product(qtyarray, productarray, prefix);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (item.size() != 0) {
                no_order_product.setVisibility(View.GONE);
                try {
                    newmenu.findItem(R.id.edit).setVisible(true);
                    newmenu.findItem(R.id.done).setVisible(false);
                } catch (Exception e) {

                }
                adapter = new Template_ProductAdapter(Template_Products.this, item);
                recyclerView.setAdapter(adapter);
            } else {
                try {
                    no_order_product.setVisibility(View.VISIBLE);
                    newmenu.findItem(R.id.edit).setVisible(false);
                    newmenu.findItem(R.id.done).setVisible(false);
                } catch (Exception e) {

                }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        this.newmenu = menu;
        inflater.inflate(R.menu.menu_actionbar, newmenu);
        temp_pr_edit = 0;
//        newmenu.findItem(R.id.edit).setVisible(true);
//        newmenu.findItem(R.id.done).setVisible(false);
        if (item.size() > 0) {
            newmenu.findItem(R.id.edit).setVisible(true);
            newmenu.findItem(R.id.done).setVisible(false);

        } else {
            newmenu.findItem(R.id.edit).setVisible(false);
            newmenu.findItem(R.id.done).setVisible(false);

        }
        return super.onCreateOptionsMenu(newmenu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.edit:
                temp_pr_edit = 1;
                newmenu.findItem(R.id.edit).setVisible(false);
                newmenu.findItem(R.id.done).setVisible(true);
                adapter.notifyDataSetChanged();
                return true;
            case R.id.done:
                temp_pr_edit = 0;
                newmenu.findItem(R.id.edit).setVisible(true);
                newmenu.findItem(R.id.done).setVisible(false);
                adapter.notifyDataSetChanged();
                return true;
        }
        return false;
    }

    // *******************************************Alert Dialog**************************************
    public void dialogww(Context ctx, String title, String msg) {

        new AlertDialog.Builder(ctx, Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete

                                dialog.dismiss();
                            }
                        }).show();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        temp_pr_edit = 0;
        finish();
    }
}

package com.ordertron;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;


import com.instabug.library.IBGInvocationEvent;
import com.instabug.library.Instabug;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.util.List;

public class App extends Application {

    public static String objectId;
    public static boolean IS_APP_RUNNING = false;
    SharedPreferences prefs;
    public static Context context;
    String error;
    public static String additionalMessage = "";
    JSONObject obj;
    public static String type = "home";
    public static boolean isOpen;
    List<ActivityManager.RunningTaskInfo> tasks;
    Class<? extends Activity> cls;


    boolean classtatus;

    @Override
    public void onCreate() {
        super.onCreate();


        //--------------following code should be uncommented for instabug

        //****************For LIVE******************************************************************
//        new Instabug.Builder(this, "29521d6ed1d9fb4469cbc498b727e244")
//                .setInvocationEvent(IBGInvocationEvent.IBGInvocationEventShake)
//                .setShouldShowIntroDialog(false)
//                .build();



        //****************For BETA Version******************************************************************
//        new Instabug.Builder(this, "a0121fd3788dede17163d12edf804456")
//                .setInvocationEvent(IBGInvocationEvent.IBGInvocationEventShake)
//                .setShouldShowIntroDialog(false)
//                .build();


        //****************on signal implementation*****************//

        try {
            OneSignal.startInit(this)
                    .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                    .setAutoPromptLocation(true)
                    .init();
            OneSignal.enableNotificationsWhenActive(false);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

        @Override
        public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {

            System.out.println("Data: "+additionalData);
            classtatus = isApplicationSentToBackground();
            try {
                if (additionalData != null) {
                    //   Utils.startsync=true;
                    if (additionalData.has("actionSelected"))
                        additionalMessage += "Pressed ButtonID: " + additionalData.getString("actionSelected");
                    isOpen = isActive;
//                    if(isActive&&classtatus){
//                        Intent i = new Intent(context.getApplicationContext(), MyAlertDialog.class);
//                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        i.putExtra("msg", message);
//                        context.startActivity(i);
//                        return;
//                    }
                    obj = new JSONObject(additionalData.toString());
                    type = obj.optString("type");
//                    if (type.equals("Order")) {
//                       Utils.startsync = true;
//
//                        if (classtatus) {
//                            Intent i = new Intent(context.getApplicationContext(), MyAlertDialog.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            i.putExtra("msg", message);
//                            context.startActivity(i);
//                        }else{
//                            Toast.makeText(getApplicationContext(),"received", Toast.LENGTH_SHORT).show();
////                            android.os.Process.killProcess(android.os.Process.myPid());
////                            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
////                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
////                            startActivity(intent);
//                        }
//                    }if (type.equals("message")) {
//                       Utils.startsync = true;
//
//                        if (classtatus) {
//                            Intent i = new Intent(context.getApplicationContext(), MyAlertDialog.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            i.putExtra("msg", message);
//                            context.startActivity(i);
//                        }else{
//                            Toast.makeText(getApplicationContext(),"received", Toast.LENGTH_SHORT).show();
////                            android.os.Process.killProcess(android.os.Process.myPid());
////                            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
////                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
////                            startActivity(intent);
//                        }
//                    }

                    System.out.println("values in one signal" + type);
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }


        }


    }

    public boolean isApplicationSentToBackground() {
        ActivityManager am = (ActivityManager)
                getSystemService(Context.ACTIVITY_SERVICE);
        tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(getPackageName())) {
                return false;
            }
        }

        return true;
    }
} 
package com.ordertron;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import DataBaseUtils.DatabaseQueries;
import Model.JSONParser;
import Model.Methods;
import Model.Model_Add_Detail_Customer;
import Model.Utils;

/**
 * Created by admin on 1/8/2016.
 */
public class Add_Shipping_Address extends AppCompatActivity {
    EditText address1_txt, address2_txt, address3_txt, suburb_txt, postcode_txt, tele2_txt, tele1_txt, fax_txt, stateSpinner;
    TextView country_txt;
    String country_name, prefix_name, username, authcode, item_state;
    SharedPreferences prefs, prefs1;
    RelativeLayout addAddress_rl;
    DatabaseQueries query;
    boolean b;
    Methods methods;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_shipping_address);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        query = new DatabaseQueries(Add_Shipping_Address.this);
        init();
        //  setSpinnerdate();
        onclick();

        methods = new Methods(Add_Shipping_Address.this);
    }

    private void onclick() {
        addAddress_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValidation();
                hidekeyboard();

            }
        });
        country_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.MyDialogSingle(Add_Shipping_Address.this,country_txt,country_txt.getText().toString());
            }
        });
    }


    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void setValidation() {
        if (address1_txt.getText().toString().length() == 0) {
            Toast.makeText(getApplicationContext(), "Address 1 is required!", Toast.LENGTH_LONG).show();
        } else if (suburb_txt.getText().toString().length() == 0) {
            Toast.makeText(getApplicationContext(), "Suburb is required!", Toast.LENGTH_LONG).show();
        } else if (stateSpinner.getText().toString().length() == 0) {
            Toast.makeText(getApplicationContext(), "State is required!", Toast.LENGTH_LONG).show();
        } else if (country_txt.getText().toString().length() == 0||country_txt.getText().toString().equals("Select Country")) {
            Toast.makeText(getApplicationContext(), "Country is required!", Toast.LENGTH_LONG).show();
        }
//        else if (postcode_txt.getText().toString().length() == 0) {
//            Toast.makeText(getApplicationContext(), "PostCode is required!", Toast.LENGTH_LONG).show();
//        }

//        else if (tele1_txt.getText().toString().length() == 0) {
//            Toast.makeText(getApplicationContext(), "Telephone1 is required!", Toast.LENGTH_LONG).show();
//        }

        else {
            if (!b) {
                Utils.conDialog(Add_Shipping_Address.this);
            } else {
                new Add_Customer_Address().execute();
            }
        }

    }

    /*private void setSpinnerdate() {
        String[] items = new String[]{"South Australia", "Queensland", "Northern Territory", "Tasmania", "Victoria", "Western Australia", "New South Wales"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, items);

        stateSpinner.setAdapter(adapter);

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
                item_state = (String) parent.getItemAtPosition(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }*/

    private void init() {
        b = Utils.isNetworkConnected(Add_Shipping_Address.this);
        stateSpinner = (EditText) findViewById(R.id.state_txt);
        address1_txt = (EditText) findViewById(R.id.address1_txt);
        address2_txt = (EditText) findViewById(R.id.address2_txt);
        address3_txt = (EditText) findViewById(R.id.address3_txt);
        suburb_txt = (EditText) findViewById(R.id.suburb_txt);
        postcode_txt = (EditText) findViewById(R.id.postcode_txt);
        fax_txt = (EditText) findViewById(R.id.fax_txt);
        tele1_txt = (EditText) findViewById(R.id.tele1_txt);
        tele2_txt = (EditText) findViewById(R.id.tele2_txt);

        country_txt = (TextView) findViewById(R.id.country_txt);
        addAddress_rl = (RelativeLayout) findViewById(R.id.addaddress_rl);
        Intent i = new Intent();
        // i = getIntent();
//        country_name = i.getStringExtra("country_name");
//        country_txt.setText(country_name);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        prefix_name = prefs.getString("prefix_name", null);
        username = prefs.getString("username", null);
        authcode = prefs.getString("auth_code", null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

//-----------------------add_address_customer ASYNC-----------------------

    public class Add_Customer_Address extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog;
        String result, country, suburb, postcode, address1, address2, adress3, tele1, tele2, fax;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Add_Shipping_Address.this);
            dialog.setMessage("Saving....");
            dialog.show();
            country = country_txt.getText().toString();
            suburb = suburb_txt.getText().toString();
            postcode = postcode_txt.getText().toString();
            address1 = address1_txt.getText().toString();
            address2 = address2_txt.getText().toString();
            adress3 = address3_txt.getText().toString();
            tele1 = tele1_txt.getText().toString();
            tele2 = tele2_txt.getText().toString();
            fax = fax_txt.getText().toString();
            item_state = stateSpinner.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... params) {
            result = add_customer_address(prefix_name, username, authcode, country, item_state,
                    suburb, postcode, address1, address2, adress3, fax, tele1, tele2);
            System.out.println("response  is: " + result);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                dialog.dismiss();
                JSONObject jsonObject = new JSONObject(result);
                String status = jsonObject.optString("Status");
                if (status.equals("true")) {
                    JSONObject jsonObject1 = jsonObject.getJSONObject("Data");
                    String id = jsonObject1.optString("id");
                    Model_Add_Detail_Customer modeladddetail = new Model_Add_Detail_Customer();
                    modeladddetail.id = id;
                    modeladddetail.prefix_name = prefix_name;
                    modeladddetail.country = country;
                    modeladddetail.state = item_state;
                    modeladddetail.suburb = suburb;
                    modeladddetail.postcode = postcode;
                    modeladddetail.add_1 = address1;
                    modeladddetail.add_2 = address2;
                    modeladddetail.add_3 = adress3;
                    modeladddetail.fax = fax;
                    modeladddetail.tel_1 = tele1;
                    modeladddetail.tel_2 = tele2;
                    modeladddetail.default_billing_add = PlaceOrderActivity.billing_address_id;
                    modeladddetail.default_shipping_add = PlaceOrderActivity.shipping_address_id;

                    if(!query.iscustomerdetailExist(prefs.getString("prefix_name", null),modeladddetail)) {
                        query.add_detail_customer(modeladddetail);
                    }else{
                        query.update_detailcustomer(modeladddetail,prefs.getString("prefix_name", null));
                    }

                    //     Edit_Shipping_Address_Activity.new_add=true;


                    finish();
                } else if (status.equals("false")) {


                    if (jsonObject.has("logout")) {
                        String logout = jsonObject.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = jsonObject.optJSONObject("Data");
                            final String result = jobj.optString("result");

                            methods.dialog(result);

                        }
                    }

                    JSONObject jsonObject1 = jsonObject.getJSONObject("Data");
                    String result = jsonObject1.optString("result");
                    // Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

            }
        }
    }

    //*************************Webservice parameter add_customer_address  *************************

    public String add_customer_address(String prefix_name, String user_name, String auth_code, String country
            , String state, String suburb, String postcode, String address1, String address2, String address3, String fax, String tele1, String tele2) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", user_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("country", country)
                .appendQueryParameter("state", state)
                .appendQueryParameter("suburb", suburb)
                .appendQueryParameter("postcode", postcode)
                .appendQueryParameter("address1", address1)
                .appendQueryParameter("address2", address2)
                .appendQueryParameter("address3", address3)
                .appendQueryParameter("fax", fax)
                .appendQueryParameter("telephone1", tele1)
                .appendQueryParameter("telephone2", tele2);
        res = parser.getJSONFromUrl(Utils.add_customer_address, builder,Add_Shipping_Address.this);
        return res;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hidekeyboard();
    }
}

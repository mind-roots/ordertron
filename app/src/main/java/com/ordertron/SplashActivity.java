package com.ordertron;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;


public class SplashActivity extends Activity {
    SharedPreferences prefs,prefs1;
    private static int SPLASH_TIME_OUT = 3000;
    public static boolean logdirect = false;
    boolean activityStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (activityStarted
                && getIntent() != null
                && (getIntent().getFlags() & Intent.FLAG_ACTIVITY_REORDER_TO_FRONT) != 0) {
            finish();
            return;
        }
        setContentView(R.layout.activity_splash);


        activityStarted = true;
        prefs = getSharedPreferences("login", SplashActivity.this.MODE_PRIVATE);
        prefs1 = getSharedPreferences("tutorial", SplashActivity.this.MODE_PRIVATE);
        final String value = prefs.getString("prefix_name", "");
        final String value1 = prefs1.getString("dontshowtutorial", "");
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (value.equals("")) {
                    logdirect = false;
                    if (value1.equals("")) {

//                        Intent i = new Intent(SplashActivity.this, FirstViewSplash.class);
//                        startActivity(i);
                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(i);
                    }else{
                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(i);
                    }

                } else {
                    logdirect = true;
                    Intent i = new Intent(SplashActivity.this, Dashboard.class);
                    startActivity(i);
                }

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

}
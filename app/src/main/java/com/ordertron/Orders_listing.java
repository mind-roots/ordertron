package com.ordertron;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import Adapters.AllOrdersApapter;
import DataBaseUtils.DatabaseQueries;
import Model.Model_Order_Info;

/**
 * Created by user on 1/22/2016.
 */
public class Orders_listing extends ActionBarActivity {

    public static String id_pos;
    String order_id;
    //  AllOrdersApapter arrayAdapter;
    AllOrdersApapter arrayAdapter;
    RecyclerView list_all_orders;
    DatabaseQueries databaseQueries;
    SharedPreferences prefs;
    String prefix_name;
    public static ArrayList<Model_Order_Info> allOrder_arrayList = new ArrayList<Model_Order_Info>();
    RecyclerView.LayoutManager mLayoutManager;
    ImageView no_data;
    String type, title;
    String query;
    ActionBar actionbar;
    SimpleDateFormat format;
    Date d1, d2;
    public static boolean cancelorder = false;
    String timezone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_orders);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

        getsetdata();


    }

    private void getsetdata() {
        try {
            if (type.equals("all")) {
                query = "SELECT * FROM order_info WHERE prefix_name ='" + prefix_name + "'";
            } else {
                query = "SELECT * FROM order_info WHERE prefix_name ='" + prefix_name + "' AND order_status ='" + type + "'";
            }
            allOrder_arrayList.clear();
            allOrder_arrayList = databaseQueries.Select_orders(query);
            timezone = databaseQueries.get_timezone_setting(prefix_name);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (allOrder_arrayList.size() > 0) {

            Collections.reverse(allOrder_arrayList);
           /* Collections.sort(allOrder_arrayList, new Comparator<Model_Order_Info>() {
                public int compare(Model_Order_Info m1, Model_Order_Info m2) {
                    try {
                         d1 = format.parse(m1.created_date);
                        d2 = format.parse(m2.created_date);
                    } catch (Exception e) {
                    }
                    return m1.created_date.compare(m2.created_date);
                }
            });*/

            no_data.setVisibility(View.GONE);
            arrayAdapter = new AllOrdersApapter(Orders_listing.this, allOrder_arrayList,timezone);
            list_all_orders.setAdapter(arrayAdapter);
        } else {
            no_data.setVisibility(View.VISIBLE);
        }
    }


    public void init() {
        format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        databaseQueries = new DatabaseQueries(Orders_listing.this);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        prefix_name = prefs.getString("prefix_name", null);
        no_data = (ImageView) findViewById(R.id.no_data);
        list_all_orders = (RecyclerView) findViewById(R.id.list_all_orders);
        mLayoutManager = new LinearLayoutManager(Orders_listing.this);
        list_all_orders.setLayoutManager(mLayoutManager);
        Intent intent = getIntent();
        type = intent.getStringExtra("Type");
        title = intent.getStringExtra("Title");

        actionbar = getSupportActionBar();
        actionbar.setTitle(title);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getsetdata();
        if (Dashboard.reorder){
            finish();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}

package com.ordertron;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Adapters.MessageAdap;
import DataBaseUtils.DatabaseQueries;
import Model.JSONParser;
import Model.ModelLogin;
import Model.Model_Message;
import Model.Utils;

/**
 * Created by android on 12/7/2015.
 */
public class MessageActivity extends AppCompatActivity {
    RecyclerView msglist;
    ProgressBar bottomBar;
    int page = 1;
    SharedPreferences prefs;
    MessageAdap adp;
    ProgressDialog dialog;
    public static final String mypreference = "mypref";
    public static ArrayList<Model_Message> item = new ArrayList<Model_Message>();
    ArrayList<ModelLogin> login = new ArrayList<>();
    ImageView no_info;
    LinearLayout ll_msglist;
    String status;
    LinearLayoutManager mLayoutManager;
    JSONArray jarr;
    boolean connect;
    DatabaseQueries query;
    String allprefix, allauthcode;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        connect = Utils.isNetworkConnected(MessageActivity.this);
        try {
            login = query.getLogin();
        } catch (Exception e) {
        }
        StringBuilder prefixbuilder = new StringBuilder();
        StringBuilder authbuilder = new StringBuilder();
        for (int i = 0; i < login.size(); i++) {
            prefixbuilder.append(login.get(i).prefix_name + ",");
            authbuilder.append(login.get(i).auth_code + ",");
        }
        allprefix = prefixbuilder.toString();
        if (allprefix.indexOf(",") != -1) {
            allprefix = allprefix.substring(0, allprefix.length() - 1);
        }
        allauthcode = authbuilder.toString();
        if (allauthcode.indexOf(",") != -1) {
            allauthcode = allauthcode.substring(0, allauthcode.length() - 1);
        }
        if (!connect) {
            Utils.conDialog(MessageActivity.this);
            no_info.setVisibility(View.VISIBLE);
        } else {
            new Message(page).execute();
        }
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putString("prefix_name", prefix_name);
//        editor.putString("auth_code", auth_code);
//        editor.commit();
    }

    private void init() {
        query = new DatabaseQueries(MessageActivity.this);
        msglist = (RecyclerView) findViewById(R.id.msglist);
        mLayoutManager = new LinearLayoutManager(MessageActivity.this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        msglist.setLayoutManager(mLayoutManager);
        bottomBar = (ProgressBar) findViewById(R.id.bottomBar);
        prefs = getSharedPreferences("login", Context.MODE_PRIVATE);
        no_info = (ImageView) findViewById(R.id.no_info);
        ll_msglist = (LinearLayout) findViewById(R.id.ll_msglist);
        msglist.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (jarr.length() > 0) {

                    int jk = mLayoutManager
                            .findLastCompletelyVisibleItemPosition();

                    int s;

                    s = item.size();

                    if (jk == s - 1) {
                        page = page + 1;


                        if (!connect) {
                            Utils.conDialog(MessageActivity.this);
                            no_info.setVisibility(View.VISIBLE);
                        } else {
                            new Message(page).execute();
                        }


                    }
                }

            }
        });


    }

    public String GetMessage(String company_code, String auth_code, String page) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("company_code", company_code)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("page", page);
        System.out.println("res="+Utils.get_messages+builder);
        res = parser.getJSONFromUrl(Utils.get_messages, builder,MessageActivity.this);


        return res;
    }

//*****************************************GETTING MESSAGES FROM  WEB-SERVICE************************

    class Message extends AsyncTask<Void, Void, Void> {

        String data;
        int page;

        public Message(int page) {

            this.page = page;
        }

        @Override
        protected void onPreExecute() {


            if (page == 1) {

                dialog = ProgressDialog.show(MessageActivity.this,
                        "Loading", "Please Wait..", true);
                dialog.setCancelable(false);
            } else {

                bottomBar.setVisibility(View.VISIBLE);

            }

        }

        @Override
        protected Void doInBackground(Void... params) {


            try {

                String responsegetmsg = GetMessage(allprefix, allauthcode, String.valueOf(page));
                Log.e("MessageREsponse", "" + responsegetmsg);
                JSONObject obj = new JSONObject(responsegetmsg);

                status = obj.optString("Status");
                data = obj.optString("Data");
                if (status.equals("true")) {
                    jarr = obj.getJSONArray("Data");

                    for (int i = 0; i < jarr.length(); i++) {
                        Model_Message msg = new Model_Message();
                        JSONObject obj2 = jarr.getJSONObject(i);
                        msg.message_text = obj2.getString("message_text");
                        msg.wholesaler_name = obj2.getString("wholesaler_name");
                        msg.sent_date = savedateformatter(obj2.optString("sent_date"));


                        item.add(msg);
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
try {
    if (status.equals("true")) {


        if (item.size() != 0) {
            try {
                no_info.setVisibility(View.GONE);
                ll_msglist.setVisibility(View.VISIBLE);
            } catch (Exception e) {

            }
            if (page == 1) {
                dialog.dismiss();
                adp = new MessageAdap(MessageActivity.this, item);
                msglist.setAdapter(adp);
            } else {
                bottomBar.setVisibility(View.GONE);
                adp.notifyDataSetChanged();

            }


        } else {
            if (page == 1) {
                dialog.dismiss();
            } else {
                bottomBar.setVisibility(View.GONE);
            }
            if (item.size() != 0) {
            } else {
                no_info.setVisibility(View.VISIBLE);
                ll_msglist.setVisibility(View.GONE);
            }
        }

    } else {
        if (item.size() != 0) {
        } else {
            no_info.setVisibility(View.VISIBLE);
            ll_msglist.setVisibility(View.GONE);
        }
        if (page == 1) {
            dialog.dismiss();

        } else {
            bottomBar.setVisibility(View.GONE);


        }
    }

}catch ( Exception e){

}
        }

    }

    public String savedateformatter(String senddate) {

        // TODO Auto-generated method stub
        String setdate = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = format1.parse(senddate);
            format1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            setdate = format1.format(newDate);
        } catch (Exception e) {
            setdate = "NA";
        }
        return setdate;

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        item.clear();
        super.onDestroy();
    }

}

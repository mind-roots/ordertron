package com.ordertron;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;

import Adapters.Get_product_catAdapter;
import Adapters.Prod_Cat_Template_Adpter;
import DataBaseUtils.DatabaseQueries;
import Model.Model_Cat_Prod;
import Model.Model_GetProduct;
import Model.Model_Message;


/**
 * Created by Abhijeet on 12/9/2016.
 */
public class Add_Product extends AppCompatActivity {
    public static RecyclerView recyclerview, prodrecyclerview;
    ProgressBar progressBar;
    Intent intent;
    DatabaseQueries databaseQueries;
    SharedPreferences prefs;
    public static Prod_Cat_Template_Adpter mixAdapter;
    Get_product_catAdapter getcatAdpa;
    String parent_cat_id = "0";
    public static LinearLayoutManager llm;
    public static android.support.v7.app.ActionBar actionbar;
    public static ArrayList<Model_Message> categorystack = new ArrayList<>();
    public static ArrayList<Model_Cat_Prod> mixlistt = new ArrayList<>();
    public static boolean getprev = false, getcatsame = false;
    //  public static String products, qty, productscode;
    public static String tempid;
    Menu menu;
    ArrayList<Model_GetProduct> allprod = new ArrayList<>();
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_categories);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

        if (categorystack.size() != 0) {
            getdata(categorystack.get(categorystack.size() - 1).parentcatid);

            actionbar.setHomeButtonEnabled(true);
            actionbar.setDisplayHomeAsUpEnabled(true);
        } else {
            getdata(parent_cat_id);
            actionbar.setTitle("Products");
        }

    }


    //************************************UI initialization*************************************************
    private void init() {

        actionbar = getSupportActionBar();
        intent = getIntent();
        int vl = intent.getIntExtra("indicator", 0);
//        qty = intent.getStringExtra("qty");
//        products = intent.getStringExtra("products");
//        productscode = intent.getStringExtra("productscode");
        tempid= intent.getStringExtra("tempid");
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        prodrecyclerview = (RecyclerView) findViewById(R.id.prodrecyclerview);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        databaseQueries = new DatabaseQueries(this);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        llm = new LinearLayoutManager(this);
    }


    //************************************getdata*************************************************
    public void getdata(String parent_id) {
      /*  try {
        //    getcat = databaseQueries.Get_Product_Catagory(prefs.getString("prefix_name", null), parent_id);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (getcat.size() != 0) {
            get_catagoryAdapter = new Add_Adapter_Template(this, getcat);
            recyclerview.setLayoutManager(llm);
            recyclerview.setAdapter(get_catagoryAdapter);
        }*/


        try {
            //   Dashboard.mixlist = databaseQueries.Get_Product_Catagory(prefs.getString("prefix_name", null), parent_id);
            mixlistt = databaseQueries.Get_Prod_Cat(prefs.getString("prefix_name", null), parent_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mixlistt.size() != 0) {
            //  get_catagoryAdapter = new Get_CatagoryAdapter(getActivity(), Dashboard.getcat);
            mixAdapter = new Prod_Cat_Template_Adpter(Add_Product.this, mixlistt);
            recyclerview.setLayoutManager(llm);
            recyclerview.setAdapter(mixAdapter);
        }


        //-----------only products-------------//
        try {
            allprod = databaseQueries.Get_Product_id(prefs.getString("prefix_name", null));

        } catch (Exception e) {
            e.printStackTrace();
        }
        getcatAdpa = new Get_product_catAdapter(Add_Product.this, allprod, "template");
        prodrecyclerview.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(Add_Product.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        prodrecyclerview.setLayoutManager(llm);
        prodrecyclerview.setAdapter(getcatAdpa);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu newmenu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.menu_actionbar, newmenu);
        newmenu.findItem(R.id.search).setVisible(true);
        final MenuItem searchItem = newmenu.findItem(R.id.search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);'
                if (s.length() != 0) {
                    prodrecyclerview.setVisibility(View.VISIBLE);
                    recyclerview.setVisibility(View.GONE);
                } else {
                    prodrecyclerview.setVisibility(View.GONE);
                    recyclerview.setVisibility(View.VISIBLE);
                }
                getcatAdpa.filter(s);
                return false;
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
//                Toast.makeText(getActivity(),"value:"+String.valueOf(Dashboard.categorystack.size()),Toast.LENGTH_SHORT).show();
                if (getcatsame) {
                    if (categorystack.size() != 0) {
                        String parent_cat_id = categorystack.get(categorystack.size() - 1).parentcatid;
                        categorystack.remove(categorystack.size() - 1);


                        if (categorystack.size() > 0) {
                            actionbar.setTitle(categorystack.get(categorystack.size() - 1).catname);
                        } else {
                            getcatsame = false;
                            actionbar.setTitle("Products");
                        }
                        getdata(parent_cat_id);
                    }

                } else {
                    finish();
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Dashboard.page=1;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getprev) {
            getprev = false;
            String parent_cat_id = categorystack.get(categorystack.size() - 1).parentcatid;
            categorystack.remove(categorystack.size() - 1);
            if (categorystack.size() > 0) {
                actionbar.setTitle(categorystack.get(categorystack.size() - 1).catname);
            } else {
                actionbar.setTitle("Products");
            }
            getdata(parent_cat_id);
        }
    }

}

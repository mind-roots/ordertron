package com.ordertron;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import Adapters.TemplateAdapter;
import DataBaseUtils.DatabaseQueries;
import Model.Model_Add_To_Cart;
import Model.Model_Template;
import Model.Utils;

/**
 * Created by Abhijeet on 1/4/2016.
 */
public class Product_detail extends Activity {

    EditText enter_qty;
    TextView txtproduct_name, txtminqty, txtmaxqty, txtorder_unit, txtprice1, txtprice2, txtavail_stock, txt_tier;
    public static String prod_id, name, image, product_code, price, desc,
            min_order_qut, max_order_qut, prodct_qty, catname,fullcatname,
            avail_stock1, spcl_price, enter_quant, taxable, base_unit_name, Order_unit_name, tier_price;
    Button view_detail, add_to_cart;
    RelativeLayout rl_cancel,rlsof;
    DatabaseQueries query;
    String qty, products, inventory, currency_symbol, prefix, tempid;
    String productscode;
    String Intenttype;
    SharedPreferences prefs;
    ArrayList<String> cartlist = new ArrayList<>();
    JSONArray qtyarr, prodidarr, pcodearr;
    int i = 0;
    ArrayList<Model_Template> arr = new ArrayList<>();
    JSONArray jarr;
    NumberFormat formatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail);
        query = new DatabaseQueries(Product_detail.this);
        init();
        clickevents();



    }

    @Override
    protected void onResume() {
        super.onResume();
        InputMethodManager inputMethodManager=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

//***************************************Intitializing UI and setting values****************************

    private void init() {
        formatter = new DecimalFormat("#0.00");
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        currency_symbol = prefs.getString("currency_symbol", null);
        prefix = prefs.getString("prefix_name", "");
        txtproduct_name = (TextView) findViewById(R.id.product_name);
        txtminqty = (TextView) findViewById(R.id.minqty);
        txt_tier = (TextView) findViewById(R.id.txt_tier);
        txtmaxqty = (TextView) findViewById(R.id.maxqty);
        txtorder_unit = (TextView) findViewById(R.id.order_unit);
        txtprice1 = (TextView) findViewById(R.id.price1);
        txtprice2 = (TextView) findViewById(R.id.price2);
        txtavail_stock = (TextView) findViewById(R.id.avail_stock);
        enter_qty = (EditText) findViewById(R.id.enter_qty);
        rl_cancel = (RelativeLayout) findViewById(R.id.rl_cancel);
        rlsof = (RelativeLayout) findViewById(R.id.rlsof);
        view_detail = (Button) findViewById(R.id.view_detail);
        add_to_cart = (Button) findViewById(R.id.add_to_cart);


        Intent intent = getIntent();

        Intenttype = intent.getStringExtra("Intenttype");
        if (Intenttype.equals("template")) {
//            productscode = intent.getStringExtra("productscode");
//            qty = intent.getStringExtra("qty");
//            products = intent.getStringExtra("products");

            tempid = intent.getStringExtra("tempid");
            try {
                arr = query.GetTemp_Detail(prefix, tempid);

                products = arr.get(0).product_id;
                qty = arr.get(0).qty;
                productscode = arr.get(0).product_code;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

// ****************************Gettin Intent from  Product_Cat_Adapter.*****************************

        prod_id = intent.getStringExtra("productid");
        catname = intent.getStringExtra("catname");
        fullcatname= intent.getStringExtra("fullcatname");
        name = intent.getStringExtra("name");
        image = intent.getStringExtra("image");
        // thumb_image = intent.getStringExtra("thumb_image");
        product_code = intent.getStringExtra("product_code");
        price = intent.getStringExtra("price");
        desc = intent.getStringExtra("desc");
        min_order_qut = intent.getStringExtra("min_order_qut");
        max_order_qut = intent.getStringExtra("max_order_qut");
        inventory = intent.getStringExtra("inventory");
        avail_stock1 = intent.getStringExtra("avail_stock");
        spcl_price = intent.getStringExtra("spcl_price");
        taxable = intent.getStringExtra("taxable");
        base_unit_name = intent.getStringExtra("base_unit_name");
        Order_unit_name = intent.getStringExtra("order_unit_name");
        tier_price = intent.getStringExtra("tier_price");
        try {
            jarr = new JSONArray(tier_price);

        } catch (Exception e) {
        }
        if (jarr.length() > 0) {
            txt_tier.setVisibility(View.VISIBLE);
            txtproduct_name.setText(name + " *");
            txt_tier.setText("* Product consist of tier price");
        } else {
            txt_tier.setVisibility(View.GONE);
            txtproduct_name.setText(name);
        }

        if (min_order_qut.length() != 0) {
            txtminqty.setText(min_order_qut);
        } else {
            txtminqty.setText("1");
        }

        if (max_order_qut.length() != 0) {
            if(max_order_qut.equals("No Limits")){
                txtmaxqty.setText("No Limit");
            }else {
                txtmaxqty.setText(max_order_qut);
            }

        } else {
            txtmaxqty.setText("No Limit");

        }
        txtorder_unit.setText(Order_unit_name);
        if (Dashboard.indicator == 0) {
            add_to_cart.setText("Add To Cart");
        } else {
            add_to_cart.setText("Add To Template");
        }
        if (spcl_price.equals("0")||spcl_price.equals(" ")||spcl_price.equals("")) {
            txtprice1.setText(currency_symbol + formatter.format(Double.parseDouble(price)) + "/" + base_unit_name);
            //txtprice2.setVisibility(View.GONE);
        } else {
            txtprice1.setText(currency_symbol + formatter.format(Double.parseDouble(spcl_price)) + "/" + base_unit_name);
            //  txtprice2.setVisibility(View.VISIBLE);
            //*************'stoke on text ****************************
            // txtprice1.setPaintFlags(txtprice1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            //txtprice2.setText(currency_symbol +" "+ formatter.format(Double.parseDouble(spcl_price)) + "/" + base_unit_name);
        }
        if(avail_stock1.equals("No Limits")){
            txtavail_stock.setText("No Limit");
        }else{

            txtavail_stock.setText(avail_stock1);

        }

    }


    private void clickevents() {


        rl_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Finish the current activity on cancel button
                hidekeyboard();
                finish();
                overridePendingTransition(0, 0);
            }
        });

        view_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open New Activity for product detail
                hidekeyboard();
                Intent i = new Intent(Product_detail.this, ScrollingActivity.class);
                startActivity(i);
            }
        });


        add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidekeyboard();
//                try {
//                    File sd = Environment.getExternalStorageDirectory();
//                    File data = Environment.getDataDirectory();
//
//                    if (sd.canWrite()) {
//                        String currentDBPath = "//data/com.ordertronstaging/databases/ordertron_database";
//                        String backupDBPath = "ordertron_database/";
//                        File currentDB = new File(data, currentDBPath);
//                        File backupDB = new File(sd, backupDBPath);
//
//                        if (currentDB.exists()) {
//                            FileChannel src = new FileInputStream(currentDB).getChannel();
//                            FileChannel dst = new FileOutputStream(backupDB).getChannel();
//                            dst.transferFrom(src, 0, src.size());
//                            src.close();
//                            dst.close();
//                            Toast.makeText(getApplicationContext(), "Backup is successful to SD card", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                } catch (Exception e) {
//                }
//







                if (Dashboard.indicator == 0) {

                    boolean exist = selectcart(prod_id);
                    if (!exist) {
                        float min = 0, max = 0, enter = 0, stock = 0;
                        enter_quant = enter_qty.getText().toString();
                        try {
                            if (min_order_qut.length() != 0) {
                                min = Float.parseFloat(min_order_qut);
                            }
                            if (max_order_qut.length() != 0) {
                                max = Float.parseFloat(max_order_qut);
                            }
                            enter = Float.parseFloat(enter_quant);
                            enter=Utils.round(enter, 2);
                            enter_quant=String.valueOf(enter);
                            if (!avail_stock1.equals("No Limits")) {
                                stock = Float.parseFloat(avail_stock1);
                                if (max == 0) {
                                    max_order_qut = avail_stock1;
                                }
                            }
                        } catch (NumberFormatException nfe) {
                            System.out.println("Could not parse " + nfe);
                        }
                        if (avail_stock1 != null && (enter > stock) && !avail_stock1.equals("No Limits")) {
                            Toast.makeText(getApplicationContext(),
                                    "The ordered product quantity is more than the available stock.", Toast.LENGTH_LONG).show();

                            //    dialog(android.R.drawable.ic_menu_save, "Product Stock", "The ordered product quantity is more than the available stock.", 1);
                        } else if ((enter < min)) {
                            //    dialog(android.R.drawable.ic_menu_save, "Minimum Order Quantity", "The ordered product quantity is less than the minimum allowed cart quantity.", 1);
                            Toast.makeText(getApplicationContext(), "The ordered product quantity is less than the minimum allowed cart quantity.", Toast.LENGTH_LONG).show();
                        } else if (enter > max && max != 0) {
                            //    dialog(android.R.drawable.ic_menu_save, "Maximum Order Quantity", "The ordered product quantity is more than the maximum allowed cart quantity.", 1);
                            Toast.makeText(getApplicationContext(), "The ordered product quantity is more than the maximum allowed cart quantity.", Toast.LENGTH_LONG).show();
                        } else {


                            //*****************saving data to database through model class**************************************
                            Model_Add_To_Cart model_add_to_cart = new Model_Add_To_Cart();
                            model_add_to_cart.prod_id = prod_id;
                            model_add_to_cart.prod_name = name;
                            model_add_to_cart.prod_sku = product_code;
                            model_add_to_cart.qty = enter_quant;
                            model_add_to_cart.price = price;
                            model_add_to_cart.spcl_price = spcl_price;
                            model_add_to_cart.max_qty = max_order_qut;
                            model_add_to_cart.min_qty = min_order_qut;
                            model_add_to_cart.base_unit = base_unit_name;
                            model_add_to_cart.order_unit = Order_unit_name;
                            model_add_to_cart.taxable = taxable;
                            model_add_to_cart.inventory = inventory;
                            //*********calling Databasequries insert function for getcustomer***********************************
                            query.add_to_cart(model_add_to_cart);
                            Toast.makeText(getApplication(), "Product has been added to Cart." , Toast.LENGTH_LONG).show();
                            // dialog(android.R.drawable.ic_menu_save, "Success", "Product has been added to Cart.", 0);
                            Dashboard.cartupdate = true;
                            finish();

                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Product already exist", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    float min = 0, max = 0, enter = 0, stock = 0;

                    enter_quant = enter_qty.getText().toString();
                    try {
                        if (min_order_qut.length() != 0) {
                            min = Float.parseFloat(min_order_qut);
                        }
                        if (max_order_qut.length() != 0) {
                            max = Float.parseFloat(max_order_qut);
                        }
                        enter = Float.parseFloat(enter_quant);
                        enter=Utils.round(enter, 2);
                        enter_quant=String.valueOf(enter);
                        if (!avail_stock1.equals("No Limits")) {
                            stock = Float.parseFloat(avail_stock1);
                        }
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }

//***********31/8/16 Commented by Abhijeet to avoid max quantity or Available stock check while adding to Template***********************************

//                    if (avail_stock1 != null && (enter > stock) && !avail_stock1.equals("No Limits")) {
//                        //  dialog(android.R.drawable.ic_menu_save, "Product Stock", "The ordered product quantity is more than the available stock.", 1);
//                        //      Utils.simDialog(Product_detail.this, "Product Stock", "The ordered product quantity is more than the available stock.");
//                        Toast.makeText(getApplicationContext(),
//                                "The ordered product quantity is more than the available stock.", Toast.LENGTH_LONG).show();
//                    } else if ((enter < min)) {
//                        //  dialog(android.R.drawable.ic_menu_save, "Minimum Order Quantity", "The ordered product quantity is less than the minimum allowed cart quantity.", 1);
//                        //     Utils.simDialog(Product_detail.this, "Minimum Order Quantity", "The ordered product quantity is less than the minimum allowed cart quantity.");
//
//                        Toast.makeText(getApplicationContext(), "The ordered product quantity is less than the minimum allowed cart quantity.", Toast.LENGTH_LONG).show();
//                    } else if (enter > max && max != 0) {
//                        //  dialog(android.R.drawable.ic_menu_save, "Maximum Order Quantity", "The ordered product quantity is more than the maximum allowed cart quantity.", 1);
//                        //     Utils.simDialog(Product_detail.this, "Maximum Order Quantity", "The ordered product quantity is more than the maximum allowed cart quantity.");
//
//                        Toast.makeText(getApplicationContext(), "The ordered product quantity is more than the maximum allowed cart quantity.", Toast.LENGTH_LONG).show();
//                    } else {
                    //add Product to cart here
                    try {

                        boolean exist = false;
                        if (qty != null && qty.length()>0 && products.length()>0 && products != null) {
//                                if(!Utils.isNetworkConnected(getApplicationContext())){
//                                    enter_quant = enter_qty.getText().toString();
//
//                                    qtyarr.put(enter_quant);
//                                    prodidarr.put(prod_id);
//                                    pcodearr.put(product_code);
//
//                                    query.update_prouct_Temp(pcodearr.toString(), prodidarr.toString(), qtyarr.toString(), TemplateAdapter.sendtempid, prefs.getString("prefix_name", null), "newupdate");
//                                    Dashboard.tempserverupdate = true;
//                                    Dashboard.tempupdate = true;
//                                    Toast.makeText(getApplication(), "Product added to Template", Toast.LENGTH_LONG).show();
//
//                                }else{

                            qtyarr = new JSONArray(qty);
                            prodidarr = new JSONArray(products);
                            pcodearr = new JSONArray(productscode);

//                                }

                            for (int i = 0; i < prodidarr.length(); i++) {

                                if (prodidarr.getString(i).equals(prod_id)) {
                                    Toast.makeText(getApplication(), "Product already exist" , Toast.LENGTH_LONG).show();
                                    //Toast.makeText(getApplication(), "Product already exist" + prodidarr.getString(i), Toast.LENGTH_LONG).show();
                                    exist = true;

                                }
                            }
                            if (!exist) {
                                if((enter_quant.equals(""))) {
                                    Toast.makeText(getApplicationContext(), "Please Enter Quantity", Toast.LENGTH_LONG).show();
                                }else {
                                    qtyarr.put(enter_quant);
                                    prodidarr.put(prod_id);
                                    pcodearr.put(product_code);
                                    query.update_prouct_Temp(pcodearr.toString(), prodidarr.toString(), qtyarr.toString(), TemplateAdapter.sendtempid, prefs.getString("prefix_name", null), "newupdate");
                                    Dashboard.tempserverupdate = true;
                                    Dashboard.tempupdate = true;
                                    Toast.makeText(getApplication(), "Product added to Template", Toast.LENGTH_LONG).show();
                                    finish();
                                }

                            }
                        } else {
                            if ((enter_quant.equals(""))) {
//
                                Toast.makeText(getApplicationContext(), "Please Enter Quantity", Toast.LENGTH_LONG).show();
                            }else {
                                qtyarr = new JSONArray();
                                prodidarr = new JSONArray();
                                pcodearr = new JSONArray();
                                qtyarr.put(enter_quant);
                                prodidarr.put(prod_id);
                                pcodearr.put(product_code);

                                query.update_prouct_Temp(pcodearr.toString(), prodidarr.toString(), qtyarr.toString(), TemplateAdapter.sendtempid, prefs.getString("prefix_name", null), "newupdate");
                                Dashboard.tempserverupdate = true;
                                Dashboard.tempupdate = true;
                                Toast.makeText(getApplication(), "Product added to Template", Toast.LENGTH_LONG).show();
                                finish();
                            }

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    //}
                }
            }
        });
    }

    public boolean selectcart(String p_id) {
        boolean rt = false;
        try {
            cartlist = query.GetCart();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < cartlist.size(); i++) {
            String id = cartlist.get(i).toString();
            System.out.print("list=" + id);
            System.out.print("list=" + p_id);
            if (cartlist.get(i).toString().equals(p_id)) {
                rt = true;

            }
        }
        return rt;
    }

    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

//    private void dialog(int ic_menu_save, String title, String msg, final int type) {
//
//        hidekeyboard();
//        int style = 0;
//       /* if (Build.VERSION.SDK_INT > 20) {
//
//        } else {
//            style=R.style.DialogTheme1;
//        }*/
//        new AlertDialog.Builder(Product_detail.this, Dashboard.style)
//                .setTitle(title)
//                .setMessage(msg)
//                .setCancelable(false)
//                .setPositiveButton(android.R.string.yes,
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,
//                                                int which) {
//                                // continue with delete
//                                if (type == 0) {
//                                    dialog.dismiss();
//                                    Dashboard.cartupdate = true;
//                                    finish();
//                                }
//                            }
//                        })
//
//                .setIcon(ic_menu_save).show();
//
//
//    }
}

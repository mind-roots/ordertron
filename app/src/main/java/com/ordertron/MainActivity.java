package com.ordertron;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import Model.JSONParser;
import Model.ModelLogin;
import Model.Utils;

public class MainActivity extends AppCompatActivity {
    JSONParser jsonParser;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    EditText edt_companycode, edt_Username, edt_Password;
    boolean b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        init();
        if (!b) {
            Utils.conDialog(MainActivity.this);
        } else {
            new login().execute();
        }
    }

    public void init() {
        b = Utils.isNetworkConnected(MainActivity.this);
        jsonParser = new JSONParser();
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        edt_companycode = (EditText) findViewById(R.id.edt_companycode);
        edt_Username = (EditText) findViewById(R.id.edt_Username);
        edt_Password = (EditText) findViewById(R.id.edt_Password);
    }

    //********************************************login parsing*****************************************
    class login extends AsyncTask {
        String status, data;
        String company_code = edt_companycode.getText().toString();
        String username = edt_companycode.getText().toString();
        String password = edt_companycode.getText().toString();
        String device_id = prefs.getString("objectId", null);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Object doInBackground(Object[] params) {
            String response = LoginMethod(company_code, username,
                    password, device_id);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("Status");

                if (status.equals("true")) {

                    JSONObject data_obj = obj.getJSONObject("data");
                    ModelLogin modelLogin = new ModelLogin();
                    modelLogin.auth_code = data_obj.getString("auth_code");
                    modelLogin.company_name = data_obj.getString("company_name");
                    modelLogin.company_logo = data_obj.getString("company_logo");
                    modelLogin.currency_code = data_obj.getString("currency_code");
                    modelLogin.currency_symbol = data_obj.getString("currency_symbol");
                    modelLogin.shipping_status = data_obj.getString("shipping_status");
                    modelLogin.shipping_cost = data_obj.getString("shipping_cost");
                    modelLogin.tax_label = data_obj.getString("tax_label");
                    modelLogin.message_count = data_obj.getString("message_count");
                    modelLogin.tax_percentage = data_obj.getString("tax_percentage");

                    editor = prefs.edit();
                    editor.putString("auth_code", modelLogin.auth_code);
                    editor.putString("company_name", modelLogin.company_name);
                    editor.putString("currency_symbol", modelLogin.currency_symbol);
                    editor.putString("shipping_status", modelLogin.shipping_status);
                    editor.putString("shipping_cost", modelLogin.shipping_cost);
                    editor.putString("tax_label", modelLogin.tax_label);
                    editor.putString("message_count", modelLogin.message_count);
                    editor.putString("tax_percentage", modelLogin.tax_percentage);
                    editor.commit();

                } else {
                    data = obj.getString("error");

                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if(status.equals("true")){

            }else {
                Utils.dialog(MainActivity.this,"Error!",data);
            }
        }

        //************************Webservice parameter LoginMethod*************************
        String LoginMethod(String prefix_name, String username, String password, String device_id) {
            String res = null;
            JSONParser parser = new JSONParser();

            Uri.Builder builder = new Uri.Builder().appendQueryParameter("service_type", "start_report")
                    .appendQueryParameter("prefix_name", prefix_name)
                    .appendQueryParameter("username", username)
                    .appendQueryParameter("password", password)
                    .appendQueryParameter("device_id", device_id)
                    .appendQueryParameter("type", "android");
            res = parser.getJSONFromUrl(Utils.login, builder,MainActivity.this);
            return res;
        }

    }
}

package com.ordertron;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Interfaces.RefreshFragment;
import Model.JSONParser;
import Model.ModelLogin;
import Model.Model_Add_Detail_Customer;
import Model.Model_GetCategories;
import Model.Model_GetPage;
import Model.Model_GetProduct;
import Model.Model_Order_Info;
import Model.Model_Order_Product;
import Model.Model_Product_Inventory;
import Model.Model_Template;
import Model.Model_Tier_pricing;
import Model.Model_get_notification;
import Model.ModelgetCustomer;
import Model.Utils;

/**
 * Created by Abhijeet on 12/21/2015.
 */
public  class Add_Account extends AppCompatActivity implements RefreshFragment {
    EditText edt_companycode, edt_username, edt_pass;
    //Button btn_add_account;
    Menu menu;
    ProgressBar progressbar;
    DatabaseQueries query;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    boolean b;
    String prefix, auth_code, username;
    String token_id;
    ArrayList<ModelLogin> log=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_another_acc);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        clickevents();
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                token_id = userId;
                Log.d("debug", "User:" + userId);
                System.out.println("playerid in yourAppclass::" + token_id);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);
            }
        });
        //------------Abheejiit code-----------------//
    }

    public void init() {
        b = Utils.isNetworkConnected(Add_Account.this);
        edt_companycode = (EditText) findViewById(R.id.edt_companycode);
        edt_username = (EditText) findViewById(R.id.edt_username);
        edt_pass = (EditText) findViewById(R.id.edt_pass);
        //btn_add_account = (Button) findViewById(R.id.btn_add_account);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        query = new DatabaseQueries(Add_Account.this);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);

    }

    //@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.submitorder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.submit:
                // EITHER CALL THE METHOD HERE OR DO THE FUNCTION DIRECTLY
                if (!b) {
                    Utils.conDialog(Add_Account.this);
                } else {
                    if (edt_companycode.length() == 0) {
                        Toast.makeText(getApplicationContext(),
                                "Please enter company code!", Toast.LENGTH_SHORT).show();

                    } else if (edt_username.length() == 0) {
                        Toast.makeText(getApplicationContext(),
                                "Please enter username!", Toast.LENGTH_SHORT).show();
                    } else if (edt_pass.length() == 0) {
                        Toast.makeText(getApplicationContext(),
                                "Please enter password!", Toast.LENGTH_SHORT).show();

                    } else {
                        hidekeyboard();
                        String countQuery = "SELECT  * FROM login WHERE prefix_name ='" + edt_companycode.getText().toString() + "'";
                        int count = query.getcartcount(countQuery);
                        if (count > 0) {
                            dialog("Sign in Error", "You are already logged in to this account. Please check your login credentials.", android.R.drawable.ic_dialog_alert);


                        } else {
                            query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
                            Dashboard.item.clear();
                            Dashboard.categorystack.clear();
                            new login().execute();

                        }
                    }
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void clickevents() {

    }

    public void dialog(String title, String msg, int icon) {

        new AlertDialog.Builder(Add_Account.this, Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete

                            }
                        })

                .setIcon(icon).show();


    }

    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void refresh() {
        Dashboard.actionbar.setHomeButtonEnabled(false);
        Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
        String na=prefs.getString("company_name", null);
        Dashboard.actionbar.setTitle(prefs.getString("company_name", null));
    }

    //********************************************login parsing*************************************
    public class login extends AsyncTask {
        String status = "false", result;
        String company_code, password, device_id;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            company_code = edt_companycode.getText().toString();
            username = edt_username.getText().toString();
            password = edt_pass.getText().toString();
            // device_id = prefs.getString("objectId", null);
            device_id=token_id;
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            String response = LoginMethod(company_code, username,
                    password, device_id);


            System.out.println("LOGIN RESPONCE:::" + response);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONObject data_obj = obj.getJSONObject("Data");
                    ModelLogin modelLogin = new ModelLogin();
                    modelLogin.auth_code = data_obj.getString("auth_code");
                    modelLogin.company_name = data_obj.getString("company_name");
                    modelLogin.company_logo = data_obj.getString("company_logo");
                    modelLogin.currency_code = data_obj.getString("currency_code");
                    modelLogin.currency_symbol = data_obj.getString("currency_symbol");
                    modelLogin.shipping_status = data_obj.getString("shipping_status");
                    modelLogin.shipping_cost = data_obj.getString("shipping_cost");
                    modelLogin.tax_label = data_obj.getString("tax_label");
                    modelLogin.message_count = data_obj.getString("message_count");
                    modelLogin.tax_percentage = data_obj.getString("tax_percentage");
                    modelLogin.customer_business = data_obj.getString("customer_business");
                    modelLogin.prefix_name = data_obj.getString("company_prefix");
                    prefix = data_obj.getString("company_prefix");
                    auth_code = data_obj.getString("auth_code");
//*******************Saving Login data to shared Prefrences*****************************************
                    editor = prefs.edit();
                    editor.putString("prefix_name", modelLogin.prefix_name);
                    editor.putString("auth_code", modelLogin.auth_code);
                    editor.putString("company_name", modelLogin.company_name);
                    editor.putString("currency_symbol", modelLogin.currency_symbol);
                    editor.putString("shipping_status", modelLogin.shipping_status);
                    editor.putString("shipping_cost", modelLogin.shipping_cost);
                    editor.putString("tax_label", modelLogin.tax_label);
                    editor.putString("message_count", modelLogin.message_count);
                    editor.putString("tax_percentage", modelLogin.tax_percentage);
                    editor.putString("customer_business", modelLogin.customer_business);
                    editor.commit();
                    log.add(modelLogin);
                    query.createLogin(modelLogin);
                } else {

                    JSONObject data_obj = obj.getJSONObject("Data");
                    result = data_obj.getString("result");


                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            // progressbar.setVisibility(View.GONE);
            if (status.equals("true")) {
                String product_image_id = "0", product_id = "0", category_id = "0", inventory_id = "0", tier_id = "0", order_info = "0", order_product = "0", template = "0";



                /*try {
                    category_id = query.Getlastsync_id(DatabaseQueries.TABLE_GETCATEGORIES);
                    product_id = query.Getlastsync_id(DatabaseQueries.TABLE_GET_PRODUCTS);
                    product_image_id = query.Getlastsync_id(DatabaseQueries.TABLE_GET_PRODUCT_IMAGE);
                    inventory_id = query.Getlastsync_id(DatabaseQueries.TABLE_GET_PRODUCT_INVENTORY);
                    tier_id = query.Getlastsync_id(DatabaseQueries.TABLE_TIER_PRICING);
                    order_info = query.Getlastsync_id(DatabaseQueries.TABLE_ORDER_INFO);
                    order_product = query.Getlastsync_id(DatabaseQueries.TABLE_ORDER_PRODUCT);
                    template = query.Getlastsync_id(DatabaseQueries.TABLE_ORDER_TEMPLATE);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

//***************************** Calling Async Task for Various web services*************************
                if (!b) {
                    Utils.conDialog(Add_Account.this);
                } else {
                    new multiple(category_id, product_id, product_image_id, inventory_id, tier_id, order_info, order_product, template).execute();
                }

            } else {
                progressbar.setVisibility(View.GONE);
                Utils.dialog(Add_Account.this, "Incorrect Credentials", "The Credentials entered are incorrect.\n"+"Please enter correct login credentials\n"+"to proceed.");
            }

        }


    }

    //*****************************Calling Multiple AsyncTasks******************************************
//**************************************************************************************************
    public class multiple extends AsyncTask<Void, Void, Void> {
        String status, data, category_id, product_id, product_image_id, inventory_id, tier_id, order_info, order_product, template;
        String last_sync_id, product_last_sync_id, productImage_last_sync_id, productInventory_last_sync_id, tierpriceing_last_sync_id, order_last_sync_id, order_product_last_sync_id, template_last_sync_id;

        //****Constructor****

        public multiple(String category_id, String product_id, String product_image_id, String inventory_id
                , String tier_id, String order_info, String order_product, String template) {
            this.product_id = product_id;
            this.category_id = category_id;
            this.product_image_id = product_image_id;
            this.inventory_id = inventory_id;
            this.tier_id = tier_id;
            this.order_info = order_info;
            this.order_product = order_product;
            this.template = template;
        }


        @Override
        public void onPreExecute() {
            super.onPreExecute();
//            flag=0;

//            tabLayout.setClickable(false);
            //  progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        public Void doInBackground(Void... params) {
            String responseGetCustomer = GetCustomerMethod(prefix, auth_code);
            String responseAddDetailCustomer = AddDetailMethod(prefix, auth_code);
            String responsegetcatagories = GetCategoriesMethod(prefix, auth_code, category_id);
            String responsegetpage = GetPageMethod(prefix);
            String responsegetproducts = GetProductsMethod(prefix, auth_code, product_id);
            String responsegetproductsimage = GetProductsImageMethod(prefix, auth_code, product_image_id);
            String responsegetproductsinventory = GetProductsInventoryMethod(prefix, auth_code, inventory_id);
            String responsetierpricing = GetTierPricingMethod(prefix, username, auth_code, tier_id);
            String responseorderinfo = GetOrderInfoMethod(prefix, username, auth_code, order_info);
            String responseorderproduct = GetOrderProductMethod(prefix, auth_code, order_product);
            String responsetemplate = GetTemplateMethod(prefix, auth_code, template, username);
            String responseGetnotificationsetting = GetNotificationSetting(prefix, auth_code);

            System.out.println("GetCustomer Response:::" + responseGetCustomer);
            System.out.println("AddDetailCustomer Response:::" + responseAddDetailCustomer);
            System.out.println("Getcatagories Response:::" + responsegetcatagories);
            System.out.println("GetProducts Response:::" + responsegetproducts);
            System.out.println("GetProductsImage Response:::" + responsegetproductsimage);
            System.out.println("GetProductsInventory Response:::" + responsegetproductsinventory);
            System.out.println("GetTierPricing Response:::" + responsetierpricing);

//*************************Parsing for Get_customer*************************************************
            try {
                JSONObject obj = new JSONObject(responseGetCustomer);
                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONObject data_obj = obj.getJSONObject("Data");
                    ModelgetCustomer modelgetCustomer = new ModelgetCustomer();
                    modelgetCustomer.ws_users_id = data_obj.getString("ws_users_id");
                    modelgetCustomer.business_name = data_obj.getString("business_name");
                    modelgetCustomer.business_abn = data_obj.getString("business_abn");
                    modelgetCustomer.billing_add_id = data_obj.getString("billing_add_id");
                    modelgetCustomer.default_shipping_add_id = data_obj.getString("default_shipping_add_id");
                    modelgetCustomer.external_record_id = data_obj.getString("external_record_id");
                    modelgetCustomer.credit_limit = data_obj.getString("credit_limit");
                    modelgetCustomer.customer_group_name = data_obj.getString("customer_group_name");
                    modelgetCustomer.prefix_name = prefix;
//*********calling Databasequries insert function for getcustomer***********************************
                    query.getcustomer(modelgetCustomer);

                } else {
                    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Add_Account.this, "Error!", data);
                        }
                    });
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//*************************Parsing for Add_detail_customer******************************************
            try {
                JSONObject obj = new JSONObject(responseAddDetailCustomer);
                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Add_Detail_Customer modeladddetail = new Model_Add_Detail_Customer();
                        modeladddetail.id = obj1.getString("id");
                        modeladddetail.ws_users_id = obj1.getString("ws_users_id");
                        modeladddetail.first_name = obj1.getString("first_name");
                        modeladddetail.last_name = obj1.getString("last_name");
                        modeladddetail.add_1 = obj1.getString("add_1");
                        modeladddetail.add_2 = obj1.getString("add_2");
                        modeladddetail.add_3 = obj1.getString("add_3");
                        modeladddetail.suburb = obj1.getString("suburb");
                        modeladddetail.postcode = obj1.getString("postcode");
                        modeladddetail.tel_1 = obj1.getString("tel_1");
                        modeladddetail.tel_2 = obj1.getString("tel_2");
                        modeladddetail.fax = obj1.getString("fax");
                        modeladddetail.email = obj1.getString("email");
                        modeladddetail.default_shipping_add = obj1.getString("default_shipping_add");
                        modeladddetail.default_billing_add = obj1.getString("default_billing_add");
                        modeladddetail.status = obj1.getString("status");
                        modeladddetail.created_date = obj1.getString("created_date");
                        modeladddetail.update_date = obj1.getString("update_date");
                        modeladddetail.updated_by = obj1.getString("updated_by");
                        modeladddetail.country = obj1.getString("country");
                        modeladddetail.state = obj1.getString("state");
                        modeladddetail.prefix_name = prefix;
//******************calling Databasequries insert function for add_detail_customer******************
                        query.add_detail_customer(modeladddetail);

                    }


                } else {
                    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Add_Account.this, "Error!", data);
                        }
                    });
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//******************************Parsing for Get-Categories******************************************
            try {
                JSONObject obj = new JSONObject(responsegetcatagories);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    last_sync_id = obj.getString("last_sync_id");

//****************************saving data to database through model class***************************
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_GetCategories modelgetcategoris = new Model_GetCategories();
                        modelgetcategoris.id = obj1.getString("id");
                        modelgetcategoris.category_name = obj1.getString("category_name");
                        modelgetcategoris.parent_cat_id = obj1.getString("parent_cat_id");
                        modelgetcategoris.fullname = obj1.getString("fullname");
                        modelgetcategoris.root_cat_id = obj1.getString("root_cat_id");
                        modelgetcategoris.level = obj1.getString("level");
                        modelgetcategoris.sort = obj1.getString("sort");
                        modelgetcategoris.status = obj1.getString("status");
                        modelgetcategoris.created_date = obj1.getString("created_date");
                        modelgetcategoris.update_date = obj1.getString("update_date");
                        modelgetcategoris.updated_by = obj1.getString("updated_by");
                        modelgetcategoris.sid = obj1.getString("sid");
                        modelgetcategoris.type = obj1.getString("type");
                        modelgetcategoris.view_type = obj1.getString("Typed");
                        modelgetcategoris.last_sync_id = last_sync_id;
                        modelgetcategoris.prefix_name = prefix;

//********************calling Databasequries insert function for getcategories**********************
                        query.getcategories(modelgetcategoris);

                    }


                } else {
                    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Add_Account.this, "Error!", data);
                        }
                    });
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//*******************************Parsing for Get-Page***********************************************
            try {
                JSONObject obj = new JSONObject(responsegetpage);
                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************************saving data to database through model class**************************

                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_GetPage modelGetPage = new Model_GetPage();
                        modelGetPage.id = obj1.getString("id");
                        modelGetPage.page_url = obj1.getString("page_url");
                        modelGetPage.status = obj1.getString("status");
                        modelGetPage.prefix_name = prefix;

//************************calling Databasequries insert function for getcategories******************

                        query.getpage(modelGetPage);

                    }


                } else {
                    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Add_Account.this, "Error!", data);
                        }
                    });
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//********************************Parsing for Get Products******************************************
            try {
                JSONObject obj = new JSONObject(responsegetproducts);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    product_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_GetProduct modelgetProduct = new Model_GetProduct();
                        modelgetProduct.order_unit_name = obj1.getString("order_unit_name");
                        modelgetProduct.base_unit_name = obj1.getString("base_unit_name");
                        modelgetProduct.id = obj1.getString("id");
                        modelgetProduct.sink_id = obj1.getString("sink_id");
                        modelgetProduct.cat_id = obj1.getString("cat_id");
                        modelgetProduct.product_code = obj1.getString("product_code");
                        modelgetProduct.name = obj1.getString("name");
                        modelgetProduct.desc = obj1.getString("desc");
                        modelgetProduct.status = obj1.getString("status");
                        modelgetProduct.cost_price = obj1.getString("cost_price");
                        modelgetProduct.min_order_qut = obj1.getString("min_order_qut");
                        modelgetProduct.max_order_qut = obj1.getString("max_order_qut");
                        modelgetProduct.taxable = obj1.getString("taxable");
                        modelgetProduct.base_unit_id = obj1.getString("base_unit_id");
                        modelgetProduct.unit_price = obj1.getString("unit_price");
                        modelgetProduct.order_unit_id = obj1.getString("order_unit_id");
                        modelgetProduct.inventory = obj1.getString("inventory");
                        modelgetProduct.spcl_price = obj1.getString("spcl_price");
                        modelgetProduct.warehouse_name = obj1.getString("warehouse_name");
                        modelgetProduct.warehouse_loc = obj1.getString("warehouse_loc");
                        modelgetProduct.stock_loc = obj1.getString("stock_loc");

                        modelgetProduct.img=obj1.getString("image");
                        modelgetProduct.thumb_image=obj1.getString("thumb_image");


                        modelgetProduct.created_date = obj1.getString("created_date");
                        modelgetProduct.update_date = obj1.getString("update_date");
                        modelgetProduct.updated_by = obj1.getString("updated_by");
                        modelgetProduct.type = obj1.getString("type");
                        modelgetProduct.view_type = obj1.getString("Typed");

                        //**Saving Array to a string in database
                        modelgetProduct.tier_price = obj1.getJSONArray("tier_price").toString();
                        modelgetProduct.last_sync_id = product_last_sync_id;
                        modelgetProduct.prefix_name = prefix;
                        //**calling Databasequries insert function for add_detail_customer
                        query.getproduct(modelgetProduct);

                    }


                } else {
                    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Add_Account.this, "Error!", data);
                        }
                    });
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//**************************Parsing for Get Products Image******************************************
//            try {
//                JSONObject obj = new JSONObject(responsegetproductsimage);
//                status = obj.getString("Status");
//
//                if (status.equals("true")) {
//                    productImage_last_sync_id = obj.getString("last_sync_id");
//                    //**saving data to database through model class
//                    JSONArray data_arr = obj.getJSONArray("Data");
//                    for (int i = 0; i < data_arr.length(); i++) {
//                        JSONObject obj1 = data_arr.getJSONObject(i);
//                        Model_Product_Image modelProductimage = new Model_Product_Image();
//                        modelProductimage.id = obj1.getString("id");
//                        modelProductimage.sink_id = obj1.getString("sink_id");
//                        modelProductimage.product_id = obj1.getString("product_id");
//                        modelProductimage.image = obj1.getString("image");
//                        modelProductimage.thumb_image = obj1.getString("thumb_image");
//                        modelProductimage.status = obj1.getString("status");
//                        modelProductimage.is_main = obj1.getString("is_main");
//                        modelProductimage.type = obj1.getString("type");
//                        modelProductimage.created_date = obj1.getString("created_date");
//                        modelProductimage.update_date = obj1.getString("update_date");
//                        modelProductimage.updated_by = obj1.getString("updated_by");
//                        modelProductimage.last_sync_id = productImage_last_sync_id;
//                        modelProductimage.prefix_name = prefix;
//                        //**calling Databasequries insert function for add_detail_customer
//                        query.getproductImage(modelProductimage);
//
//                    }
//
//
//                } else {
//                    data = obj.getString("error");
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Utils.dialog(Add_Account.this, "Error!", data);
//                        }
//                    });
//                }
//
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }

//**********************Parsing for Get Products Inventory******************************************
            try {
                JSONObject obj = new JSONObject(responsegetproductsinventory);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    productInventory_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Product_Inventory modelinventory = new Model_Product_Inventory();
                        modelinventory.id = obj1.getString("id");
                        modelinventory.product_id = obj1.getString("product_id");
                        modelinventory.sink_id = obj1.getString("sink_id");
                        modelinventory.aval_stock = obj1.getString("aval_stock");
                        modelinventory.reserve_qut = obj1.getString("reserve_qut");
                        modelinventory.bck_ord_aval = obj1.getString("bck_ord_aval");
                        modelinventory.status = obj1.getString("status");
                        modelinventory.warehouse_name = obj1.getString("warehouse_name");
                        modelinventory.warehouse_location = obj1.getString("warehouse_location");
                        modelinventory.stock_location = obj1.getString("stock_location");
                        modelinventory.created_date = obj1.getString("created_date");
                        modelinventory.update_date = obj1.getString("update_date");
                        modelinventory.updated_by = obj1.getString("updated_by");
                        modelinventory.type = obj1.getString("type");
                        modelinventory.last_sync_id = productInventory_last_sync_id;
                        modelinventory.prefix_name = prefix;
                        //**calling Databasequries insert function for getporductInventory
                        query.getproductInventory(modelinventory);

                    }


                } else {
                    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Add_Account.this, "Error!", data);
                        }
                    });
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//****************************Parsing for Get Tier Pricing******************************************
            try {
                JSONObject obj = new JSONObject(responsetierpricing);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    tierpriceing_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Tier_pricing modeltier = new Model_Tier_pricing();
                        modeltier.id = obj1.getString("id");
                        modeltier.product_id = obj1.getString("product_id");
                        modeltier.customer_group_id = obj1.getString("customer_group_id");
                        modeltier.quantity_upto = obj1.getString("quantity_upto");
                        modeltier.discount_type = obj1.getString("discount_type");
                        modeltier.discount = obj1.getString("discount");
                        modeltier.created_date = obj1.getString("created_date");
                        modeltier.update_date = obj1.getString("update_date");
                        modeltier.updated_by = obj1.getString("updated_by");
                        modeltier.sink_id = obj1.getString("sink_id");
                        modeltier.type = obj1.getString("type");
                        modeltier.last_sync_id = tierpriceing_last_sync_id;
                        modeltier.prefix_name = prefix;
                        //**calling Databasequries insert function for getporductInventory
                        query.gettierpricing(modeltier);

                    }


                } else {
                    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Add_Account.this, "Error!", data);
                        }
                    });
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//****************************Parsing for Get OrderInfo*********************************************
            try {
                JSONObject obj = new JSONObject(responseorderinfo);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    order_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Order_Info modelorder = new Model_Order_Info();
                        modelorder.order_id = obj1.getString("order_id");
                        modelorder.customer_id = obj1.getString("customer_id");
                        modelorder.billing_country = obj1.getString("billing_country");
                        modelorder.billing_state = obj1.getString("billing_state");
                        modelorder.billing_suburb = obj1.getString("billing_suburb");
                        modelorder.billing_add_1 = obj1.getString("billing_add_1");
                        modelorder.billing_add_2 = obj1.getString("billing_add_2");
                        modelorder.billing_add_3 = obj1.getString("billing_add_3");
                        modelorder.billing_postcode = obj1.getString("billing_postcode");
                        modelorder.shipping_country = obj1.getString("shipping_country");
                        modelorder.shipping_state = obj1.getString("shipping_state");
                        modelorder.shipping_suburb = obj1.getString("shipping_suburb");
                        modelorder.shipping_add_1 = obj1.getString("shipping_add_1");
                        modelorder.shipping_add_2 = obj1.getString("shipping_add_2");
                        modelorder.shipping_add_3 = obj1.getString("shipping_add_3");
                        modelorder.shipping_postcode = obj1.getString("shipping_postcode");
                        modelorder.shipping_cost = obj1.getString("shipping_cost");
                        modelorder.sub_total = obj1.getString("sub_total");
                        modelorder.tax_total = obj1.getString("tax_total");
                        modelorder.total = obj1.getString("total");
                        modelorder.order_notes = obj1.getString("order_notes");
                        modelorder.internal_notes = obj1.getString("internal_notes");
                        modelorder.order_status = obj1.getString("order_status");
                        modelorder.back_order = obj1.getString("back_order");
                        modelorder.main_order_id = obj1.getString("main_order_id");
                        modelorder.delivery_date = obj1.getString("delivery_date");
                        modelorder.delivery_notes = obj1.getString("delivery_notes");
                        modelorder.created_date = obj1.getString("created_date");
                        modelorder.created_by = obj1.getString("created_by");
                        modelorder.update_date = obj1.getString("update_date");
                        modelorder.updated_by = obj1.getString("updated_by");
                        modelorder.order_name = obj1.getString("order_name");
                        modelorder.order_by = obj1.getString("order_by");
                        modelorder.sink_id = obj1.getString("sink_id");
                        modelorder.type = obj1.getString("type");
                        modelorder.show_deliverydate_order = obj1.getString("show_deliverydate_order");
                        modelorder.mandetory_delivery_order = obj1.getString("mandetory_delivery_order");
                        modelorder.last_sync_id = order_last_sync_id;
                        modelorder.prefix_name = prefix;

                        //**calling Databasequries insert function for getporductInventory

                        query.getorderinfo(modelorder);

                    }


                } else {
                    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Add_Account.this, "Error!", data);
                        }
                    });
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//***************************Parsing for Get Order Product******************************************
            try {
                JSONObject obj = new JSONObject(responseorderproduct);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    order_product_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Order_Product modelOrderProduct = new Model_Order_Product();
                        modelOrderProduct.id = obj1.getString("id");
                        modelOrderProduct.order_id = obj1.getString("order_id");
                        modelOrderProduct.product_id = obj1.getString("product_id");
                        modelOrderProduct.product_name = obj1.getString("product_name");
                        modelOrderProduct.product_code = obj1.getString("product_code");
                        modelOrderProduct.qty = obj1.getString("qty");
                        modelOrderProduct.qty_new_order = obj1.getString("qty_new_order");
                        modelOrderProduct.act_qty = obj1.getString("act_qty");
                        modelOrderProduct.applied_price = obj1.getString("applied_price");
                        modelOrderProduct.unit_price = obj1.getString("unit_price");
                        modelOrderProduct.sub_total = obj1.getString("sub_total");
                        modelOrderProduct.sink_id = obj1.getString("sink_id");
                        modelOrderProduct.type = obj1.getString("type");
                        modelOrderProduct.created_date = obj1.getString("created_date");
                        modelOrderProduct.update_date = obj1.getString("update_date");
                        modelOrderProduct.updated_by = obj1.getString("updated_by");
                        modelOrderProduct.last_sync_id = order_product_last_sync_id;
                        modelOrderProduct.prefix_name = prefix;
                        //**calling Databasequries insert function for getorderProduct
                        query.getorderproduct(modelOrderProduct);

                    }


                } else {
                    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Add_Account.this, "Error!", data);
                        }
                    });
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//*************************Parsing for Get Order Templates******************************************
            try {
                JSONObject obj = new JSONObject(responsetemplate);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    template_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Template modelTemplate = new Model_Template();
                        modelTemplate.order_template_id = obj1.getString("order_template_id");
                        modelTemplate.customer_id = obj1.getString("customer_id");
                        modelTemplate.template_name = obj1.getString("template_name");
                        modelTemplate.created_date = obj1.getString("created_date");
                        modelTemplate.created_by = obj1.getString("created_by");
                        modelTemplate.sink_id = obj1.getString("sink_id");
                        modelTemplate.type = obj1.getString("type");
                        modelTemplate.prefix_name = prefix;
                        modelTemplate.last_sync_id = template_last_sync_id;
                        //**Saving Array to a string in database
                        modelTemplate.product_id = obj1.getJSONArray("product_id").toString();
                        modelTemplate.product_code = obj1.getJSONArray("product_code").toString();
                        modelTemplate.qty = obj1.getJSONArray("qty").toString();




                      /*  modelTemplate.product_id = new Gson().toJson((obj1.getJSONArray("product_id")));
                        modelTemplate.product_code = new Gson().toJson((obj1.getString("product_code")));
                        modelTemplate.qty = new Gson().toJson((obj1.getString("qty")));*/

                        //**calling Databasequries insert function for gettemplates
                        query.gettemplate(modelTemplate);

                    }


                } else {
                    data = obj.getString("error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.dialog(Add_Account.this, "Error!", data);
                        }
                    });
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


//---------------------parsing for getnotification settings------------
            try {
                JSONObject jsonObject = new JSONObject(responseGetnotificationsetting);
                String status = jsonObject.optString("Status");
                if (status.equals("true")) {
                    Model_get_notification item = new Model_get_notification();
                    item.Currency_code = jsonObject.optString("currency_code");
                    item.Currency_symbol = jsonObject.optString("currency_symbol");
                    item.show_deliverydate = jsonObject.optString("show_deliverydate");
                    item.mandetory_delivery = jsonObject.optString("mandetory_delivery");
                    item.time_zone = jsonObject.optString("timezone");
                    item.push_notification = jsonObject.optString("push_notification");
                    item.email_notification = jsonObject.optString("email_notification");
                    item.Prefix_name = prefix;


                    query.add_get_notification(item);

                }

            } catch (Exception e) {

            }
            return null;
        }

        @Override
        public void onPostExecute(Void o) {
            super.onPostExecute(o);
            progressbar.setVisibility(View.GONE);
            More_Setting_Accounts.flagrefresh = true;
            finish();
        }


    }

//*==*==*==*==*==*==*==**************Webservice parameter GetCustomerMethod*************************

    public String GetCustomerMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.getcustomer, builder,Add_Account.this);
        return res;
    }

//************************Webservice parameter GetCategories****************************************

    public String GetCategoriesMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getcategories, builder,Add_Account.this);
        return res;
    }

//******************************Webservice parameter GetPage****************************************

    public String GetPageMethod(String prefix_name) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name);
        res = parser.getJSONFromUrl(Utils.getpage, builder,Add_Account.this);
        return res;
    }

//******************************Webservice parameter AddDetailMethod********************************

    public String AddDetailMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.adddetailcustomer, builder,Add_Account.this);
        return res;
    }

//************************Webservice parameter GetProductsMethod ***********************************

    public String GetProductsMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproducts, builder,Add_Account.this);
        return res;
    }

//***************************Webservice parameter GetProductsImageMethod ***************************

    public String GetProductsImageMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproductsimage, builder,Add_Account.this);
        return res;
    }

//************************Webservice parameter GetProductsINVENTORYMethod **************************

    public String GetProductsInventoryMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproductsinventory, builder,Add_Account.this);
        return res;
    }

//************************Webservice parameter GetProductsINVENTORYMethod **************************

    public String GetTierPricingMethod(String prefix_name, String username, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", username)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproducttierpricing, builder,Add_Account.this);
        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderInfoMethod(String prefix_name, String username, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", username)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getorderinfo, builder,Add_Account.this);
        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderProductMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getorderproduct, builder,Add_Account.this);
        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetTemplateMethod(String prefix_name, String auth_code, String sid, String username) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid)
                .appendQueryParameter("username", username);
        res = parser.getJSONFromUrl(Utils.getordertemplate, builder,Add_Account.this);
        return res;
    }
    //*************************Webservice parameter getNotificationSetting *************************

    public String GetNotificationSetting(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.getNotification_setting, builder,Add_Account.this);
        return res;
    }
    //************************Webservice parameter LoginMethod*************************

    public String LoginMethod(String prefix_name, String username, String password, String device_id) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", username)
                .appendQueryParameter("password", password)
                .appendQueryParameter("device_id", device_id)
                .appendQueryParameter("type", "android");
        res = parser.getJSONFromUrl(Utils.login, builder,Add_Account.this);
        return res;
    }
}

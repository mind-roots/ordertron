package com.ordertron;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Abhijeet on 10/14/2016.
 */

public class FirstViewSplash extends Activity {
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firstviewsplash);
        login = (Button) findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(FirstViewSplash.this,LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}

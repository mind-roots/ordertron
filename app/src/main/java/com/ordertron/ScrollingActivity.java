package com.ordertron;

import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ScrollingActivity extends AppCompatActivity
        implements AppBarLayout.OnOffsetChangedListener {
    ImageView image;
    TextView product_name, product_code, txt_apl_price, txt_price, desc, max_qty, min_qty, avail_stock, category, txt_discount;
    SharedPreferences prefs;
    String currency_symbol;
    //ProgressBar progressBar;
    RelativeLayout lay_tier;
    NumberFormat formatter;
    CollapsingToolbarLayout collapsingToolbar;
    Toolbar toolbar;
    TextView txt_main;
    AppBarLayout mAppBarLayout;
    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    private boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

        toolbar.setTitle("");
        mAppBarLayout.addOnOffsetChangedListener(ScrollingActivity.this);

        setSupportActionBar(toolbar);
    }

    private void init() {

        formatter = new DecimalFormat("#0.00");

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        txt_main = (TextView) findViewById(R.id.main_textview_title);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar);

        //  progressBar = (ProgressBar) findViewById(R.id.progressBar);
        image = (ImageView) findViewById(R.id.image);
        product_name = (TextView) findViewById(R.id.product_name);
        product_code = (TextView) findViewById(R.id.product_code);
        txt_apl_price = (TextView) findViewById(R.id.txt_apl_price);
        txt_price = (TextView) findViewById(R.id.txt_price);
        category = (TextView) findViewById(R.id.category);
        avail_stock = (TextView) findViewById(R.id.avail_stock);
        min_qty = (TextView) findViewById(R.id.min_qty);
        max_qty = (TextView) findViewById(R.id.max_qty);
        desc = (TextView) findViewById(R.id.desc);
        txt_discount = (TextView) findViewById(R.id.txt_discount);
        lay_tier=(RelativeLayout)findViewById(R.id.lay_tier);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        currency_symbol = prefs.getString("currency_symbol", null);
//***************************************setting values*********************************************
        String url = Product_detail.image;
        if(url==null||url.equals(""))
        {
            url= String.valueOf(R.drawable.default_item_icon);
        }
        if (url != null) {
            url = url.replaceAll(" ", "%20"); // remove all whitespaces
        }

        // Picasso.with(View_Product_detail.this).load(url).placeholder(R.drawable.default_image).into(image);
        Glide.with(getApplicationContext())
                .load(url)
                .placeholder(R.drawable.default_item_icon)
                .crossFade()
                .thumbnail(0.1f)
                .into(image);
//        Picasso.with(getApplicationContext()).load(url).placeholder(R.drawable.default_item_icon)
//
//                .into(image, new Callback() {
//
//                    @Override
//                    public void onSuccess() {
//                        // TODO Auto-generated method stub
//                        //    progressBar.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onError() {
//                        // TODO Auto-generated method stub
//                        //     progressBar.setVisibility(View.GONE);
//                      //  image.setImageResource(R.drawable.default_item_icon);
//
//                    }
//                });

        product_name.setText(Product_detail.name);

        startAlphaAnimation(txt_main, 0, View.INVISIBLE);
        txt_main.setText(Product_detail.name);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            collapsingToolbar.setExpandedTitleTextAppearance(R.style.TransparentText);
        }

        product_code.setText(Product_detail.product_code);

        desc.setText(Product_detail.desc);
        min_qty.setText(Product_detail.min_order_qut + " " + Product_detail.Order_unit_name);
        if (Product_detail.max_order_qut.length() != 0) {
            max_qty.setText(Product_detail.max_order_qut + " " + Product_detail.Order_unit_name);
        } else {
            max_qty.setText("No Limit");
        }


        category.setText(Product_detail.fullcatname);
        if(Product_detail.avail_stock1.equals("No Limits")){
            avail_stock.setText("No Limit");
        }else{
            avail_stock.setText(Product_detail.avail_stock1);
        }


        if (Product_detail.spcl_price.equals("0")) {
            txt_apl_price.setText(" "+currency_symbol + formatter.format(Double.parseDouble(Product_detail.price)) + "/" + Product_detail.base_unit_name);
        } else {
            txt_price.setText(currency_symbol + formatter.format(Double.parseDouble(Product_detail.price)) + "/" + Product_detail.base_unit_name);

            //*************'stoke on text ****************************
            txt_price.setPaintFlags(txt_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            txt_apl_price.setText(" "+currency_symbol + formatter.format(Double.parseDouble(Product_detail.spcl_price)) + "/" + Product_detail.base_unit_name);
        }


        try {
            JSONArray jarr = new JSONArray(Product_detail.tier_price);
            // if (jarr!=null||)
            /*"quantity_upto":"5",
                    "discount_type":"Percentage",
                    "discount":"10"*/
            if (jarr != null) {
                String setdiscount = "";
                if (jarr.length() != 0) {
                    for (int i = 0; i < jarr.length(); i++) {
                        JSONObject obj = jarr.getJSONObject(i);
                        String quantity_upto = obj.optString("quantity_upto");
                        String discount = obj.optString("discount");
                        String discount_type = obj.optString("discount_type");
                        if (i == jarr.length() - 1) {
                            if(discount_type.equals("percentage")) {
                                setdiscount = setdiscount + "Buy " + quantity_upto + " or More - " + discount + "% Off";
                            }else{
                                setdiscount = setdiscount + "Buy " + quantity_upto + " or More - " + currency_symbol + discount  +"/"+ Product_detail.base_unit_name;
                            }
                        } else {
                            if(discount_type.equals("percentage")) {
                                setdiscount = setdiscount + "Buy " + quantity_upto + " or More - " + discount + "% Off\n";
                            }else{
                                setdiscount = setdiscount + "Buy " + quantity_upto + " or More - " +  currency_symbol + discount +"/"+ Product_detail.base_unit_name + "\n";
                            }

                        }


                    }
                    txt_discount.setText(setdiscount);
                }else{
                    lay_tier.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(txt_main, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(txt_main, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(image, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(image, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }
}

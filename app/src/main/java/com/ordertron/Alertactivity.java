package com.ordertron;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import Model.Utils;

/**
 * Created by user on 5/3/2016.
 */
public class Alertactivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        AlertDialog.Builder alert = new AlertDialog.Builder(
                this, Dashboard.style);

        alert.setTitle("order placed");
        alert.setMessage("Your order has been placed");
        alert.setIcon(android.R.drawable.ic_menu_save);
        alert.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                        Utils.startsync = true;
                    }
                });

        alert.show();
    }
}
package com.ordertron;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.onesignal.OneSignal;

import org.json.JSONObject;


public class ParseApplication extends Application {


	public static String objectId;
	public static boolean IS_APP_RUNNING = false;
	SharedPreferences prefs;
	public static Context context;
	String error;
	public static String additionalMessage = "";
	JSONObject obj;
	public static String type = "home";
	public static boolean isOpen;
	@Override
	public void onCreate() {
		super.onCreate();
		// appid+clientid
		// Add your initialization code here
		//Parse.initialize(this, "sqgYNtr4QsJcym955D76NB37ChGKqmiBdPmqOXXm", "dvavfhrZhy0DxuGkeCPmqJB6N8ByJgZgEqGW3J3B");


		try {
			OneSignal.startInit(this)
					.setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
					.setAutoPromptLocation(true)
					.init();
			OneSignal.enableNotificationsWhenActive(true);
		} catch (Exception e) {
			e.printStackTrace();
		}


		//*********************COMMENTED CODE OF PARSE BY ABHIJEET*****************************************************

//		Parse.initialize(this, "VcuXfcvwmp606t7O8AqEc00vYx4MjdzRMm2PckME", "1kxzpYSDPocKxmyjoAFn99UIczSc4e23EGHea98A");
//		ParseUser.enableAutomaticUser();
//
//		String android_id = Settings.Secure.getString(getApplicationContext()
//				.getContentResolver(), Settings.Secure.ANDROID_ID);
//	//	objectId=android_id;
//
//		ParseACL defaultACL = new ParseACL();
//
//		// If you would like all objects to be private by default, remove this
//		// line.
//		defaultACL.setPublicReadAccess(true);
//
//		ParseACL.setDefaultACL(defaultACL, true);
//
//		//PushService.setDefaultPushCallback(this, SplashActivity.class, R.mipmap.ic_launcher);
//		NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder( this).setSmallIcon(R.mipmap.ic_launcher);
//		//PushService.subscribe(this, "ChannelName", DashBoard.class, R.drawable.ic_add_b);
//		ParseInstallation installation = ParseInstallation
//				.getCurrentInstallation();
//		installation.put("UniqueId", android_id);
//		installation.saveInBackground();
//		final ParseObject po = new ParseObject("Installation");
//		po.saveInBackground(new SaveCallback() {
//			public void done(ParseException e) {
//				//objectid== 5d30faa04cdbd019
//
//				if (e == null) {
//					System.out.println("objectid== " + objectId);
//					// Success!
//					// objectId = po.getObjectId();
//					// String token=po.get
//					objectId = (String) ParseInstallation
//							.getCurrentInstallation().get("UniqueId");
//					System.out.println("objectid== " + objectId);
//					SharedPreferences.Editor editor = getSharedPreferences("login", MODE_PRIVATE).edit();
//					editor.putString("objectId", objectId);
//					editor.commit();
//				} else {
//					// Failure!
//				}
//			}
//		});

	}
	class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
		/**
		 * Callback to implement in your app to handle when a notification is opened from the Android status bar or
		 * a new one comes in while the app is running.
		 * This method is located in this Application class as an example, you may have any class you wish implement NotificationOpenedHandler and define this method.
		 *
		 * @param message        The message string the user seen/should see in the Android status bar.
		 * @param additionalData The additionalData key value pair section you entered in on onesignal.com.
		 * @param isActive       Was the app in the foreground when the notification was received.
		 */

		@Override
		public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {

			try {
				if (additionalData != null) {
					if (additionalData.has("actionSelected"))
						additionalMessage += "Pressed ButtonID: " + additionalData.getString("actionSelected");
					isOpen = isActive;
					obj = new JSONObject(additionalData.toString());
					type = obj.optString("type");
					System.out.println("message" + message);
					System.out.println("one signal data" + obj);
					//SplashScreen.push = true;
					System.out.println("values in one signal" + type);
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}


		}


	}
}

package com.ordertron;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import DataBaseUtils.DatabaseQueries;
import Model.JSONParser;
import Model.ModelLogin;
import Model.Utils;



public class LoginActivity extends Activity {
    JSONParser jsonParser;
    SharedPreferences prefs,prefs1;
    SharedPreferences.Editor editor,editor1;
    DatabaseQueries query;
    ProgressBar progressBar;
    RelativeLayout layoutsignin, layoutforgot;
    EditText edt_companycode, edt_Username, edt_Password;
    Button btn_Login, btn_Forgot, btn_Submit, btn_Cancel;
    EditText edt_forgot_Companycode, edt_forgot_Email1;
    Animation animSlideUp;
    // boolean b;
    String token_id;
    int style;
    // String Url[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (Build.VERSION.SDK_INT < 21) {
            //   style = Color.TRANSPARENT;
            style = R.style.DialogTheme;
        } else {
            style = R.style.DialogTheme1;
        }
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                token_id = userId;
                Log.d("debug", "User:" + userId);
                System.out.println("playerid in yourAppclass::" + token_id);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);
            }
        });
        init();
        clickevents();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    //******************Initilize UI components*******************************************************
    public void init() {

        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        jsonParser = new JSONParser();
        query = new DatabaseQueries(LoginActivity.this);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        prefs1 = getSharedPreferences("tutorial", MODE_PRIVATE);
        edt_companycode = (EditText) findViewById(R.id.edt_companycode);
        edt_Username = (EditText) findViewById(R.id.edt_Username);
        edt_Password = (EditText) findViewById(R.id.edt_Password);
        edt_forgot_Companycode = (EditText) findViewById(R.id.edt_forgot_Companycode);
        edt_forgot_Email1 = (EditText) findViewById(R.id.edt_forgot_Email1);
        btn_Forgot = (Button) findViewById(R.id.btn_Forgot);
        btn_Cancel = (Button) findViewById(R.id.btn_Cancel);
        btn_Submit = (Button) findViewById(R.id.btn_Submit);
        btn_Login = (Button) findViewById(R.id.btn_Login);
        layoutforgot = (RelativeLayout) findViewById(R.id.layoutforgot);
        layoutsignin = (RelativeLayout) findViewById(R.id.layoutsignin);
        animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.from_middle);
//        editor1=prefs1.edit();
//        editor1.putString("dontshowtutorial", "dontshowtutorial");
//        editor1.commit();
    }

    //********************************************Click Events*****************************************
    public void clickevents() {

        btn_Login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                hidekeyboard();
                if (edt_companycode.length() == 0) {
                    Toast.makeText(getApplicationContext(),
                            "Please enter Company Code!", Toast.LENGTH_SHORT).show();

                } else if (edt_Username.length() == 0) {
                    Toast.makeText(getApplicationContext(),
                            "Please enter Username!", Toast.LENGTH_SHORT).show();
                } else if (edt_Password.length() == 0) {
                    Toast.makeText(getApplicationContext(),
                            "Please enter Password!", Toast.LENGTH_SHORT).show();

                } else {
                    //**********************calling login webservice************************
                    if (!Utils.isNetworkConnected(LoginActivity.this)) {
                        Utils.conDialog(LoginActivity.this);
                    } else {
                        String company_code = edt_companycode.getText().toString().toLowerCase();
                        String username = edt_Username.getText().toString().toLowerCase();
                        String password = edt_Password.getText().toString().toLowerCase();
                        String device_id = token_id;

                        new login(company_code,username,password,device_id).execute();
                    }
//                    Intent intent = new Intent(LoginActivity.this, Dashboard.class);
//                    startActivity(intent);

                    //saving username in shared prefs..................
                    editor = prefs.edit();
                    editor.putString("username", edt_Username.getText().toString());
                    editor.commit();


                }
            }

        });
        btn_Forgot.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                hidekeyboard();
//                layoutforgot.setVisibility(View.VISIBLE);
//                layoutforgot.startAnimation(animSlideUp);
//                layoutsignin.setVisibility(View.GONE);
                edt_Username.setText("");
                edt_Password.setText("");
                edt_companycode.setText("");

                layoutforgot.animate().rotationX(90).setDuration(200).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        layoutsignin.setVisibility(View.GONE);
                        layoutforgot.setRotationX(-90);
                        layoutforgot.setVisibility(View.VISIBLE);
                        layoutforgot.animate().rotationX(0).setDuration(200).setListener(null);
                    }
                });
            }

        });
        btn_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidekeyboard();
                if (edt_forgot_Companycode.length() == 0) {
                    Toast.makeText(getApplicationContext(),
                            "Please enter Company Code!", Toast.LENGTH_SHORT).show();

                } else if (edt_forgot_Email1.length() == 0) {
                    Toast.makeText(getApplicationContext(),
                            "Please enter Username!", Toast.LENGTH_SHORT).show();
                } else {
                    if (!Utils.isNetworkConnected(LoginActivity.this)) {
                        Utils.conDialog(LoginActivity.this);
                    } else {
                        new forgetpass(edt_forgot_Companycode.getText().toString(), edt_forgot_Email1.getText().toString()).execute();
                    }
                }
            }
        });
        btn_Cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                hidekeyboard();

//                layoutsignin.setVisibility(View.VISIBLE);
//           //     layoutsignin.startAnimation(animSlideUp);
//                layoutforgot.setVisibility(View.GONE);
                edt_forgot_Companycode.setText("");
                edt_forgot_Email1.setText("");
                layoutsignin.animate().rotationX(90).setDuration(200).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        layoutforgot.setVisibility(View.GONE);
                        layoutsignin.setRotationX(-90);
                        layoutsignin.setVisibility(View.VISIBLE);
                        layoutsignin.animate().rotationX(0).setDuration(200).setListener(null);
                    }
                });
            }

        });
    }

    //88*******************forget password-*************//
    class forgetpass extends AsyncTask {
        String prefix_name, username, response;

        public forgetpass(String prefix, String username) {
            this.prefix_name = prefix;
            this.username = username;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {


            JSONParser parser = new JSONParser();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("prefix_name", prefix_name)
                    .appendQueryParameter("username", username)
                    .appendQueryParameter("auth_code", "");

            response = parser.getJSONFromUrl(Utils.logoutlink, builder,LoginActivity.this);


            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressBar.setVisibility(View.GONE);
            try {
                JSONObject jobk = new JSONObject(response);
                if (jobk.optString("Status").equals("true")) {
                    JSONObject jobj = jobk.optJSONObject("Data");

                    dialog(LoginActivity.this, "Success", jobj.optString("result"));
                } else {
                    JSONObject jobj = jobk.optJSONObject("Data");
                    new AlertDialog.Builder(LoginActivity.this, style)
                            .setTitle("Alert")
                            .setMessage(jobj.optString("result"))
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            try {
//                                                layoutforgot = (RelativeLayout) findViewById(R.id.layoutforgot);
//                                                layoutsignin = (RelativeLayout) findViewById(R.id.layoutsignin);
                                                //layoutsignin.setVisibility(View.VISIBLE);
                                                // layoutsignin.startAnimation(animSlideUp);
                                                // layoutforgot.setVisibility(View.GONE);
                                                edt_forgot_Companycode.setText("");
                                                edt_forgot_Email1.setText("");
                                                layoutsignin.animate().rotationX(90).setDuration(200).setListener(new AnimatorListenerAdapter() {
                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        layoutforgot.setVisibility(View.GONE);
                                                        layoutsignin.setRotationX(-90);
                                                        layoutsignin.setVisibility(View.VISIBLE);
                                                        layoutsignin.animate().rotationX(0).setDuration(200).setListener(null);
                                                    }
                                                });
                                                dialog.dismiss();
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }

                                        }
                                    })

                            .setIcon(android.R.drawable.ic_dialog_alert).show();

                    // dialog(LoginActivity.this, "Alert", jobj.optString("result"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void dialog(Context ctx, String title, String msg) {
        new AlertDialog.Builder(LoginActivity.this, style)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
//                                layoutsignin.setVisibility(View.VISIBLE);
//                                layoutsignin.startAnimation(animSlideUp);
//                                layoutforgot.setVisibility(View.GONE);
                                edt_forgot_Companycode.setText("");
                                edt_forgot_Email1.setText("");
                                layoutsignin.animate().rotationX(90).setDuration(200).setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        layoutforgot.setVisibility(View.GONE);
                                        layoutsignin.setRotationX(-90);
                                        layoutsignin.setVisibility(View.VISIBLE);
                                        layoutsignin.animate().rotationX(0).setDuration(200).setListener(null);
                                    }
                                });
                                dialog.dismiss();

                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();

    }


    //********************************************login parsing*************************************
    public class login extends AsyncTask {
        String status = "false", result,company_code,username,password,device_id;


        String type = "android";

        public login(String company_code, String username, String password, String device_id) {
            this.company_code=company_code;
            this.username=username;
            this.password=password;
            this.device_id=device_id;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            String response = LoginMethod(company_code, username,
                    password, device_id, type);
//            String response = LoginMethod("code", "rahul",
//                    "sharma", device_id);

            System.out.println("LOGIN RESPONCE:::" + response);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONObject data_obj = obj.getJSONObject("Data");
                    ModelLogin modelLogin = new ModelLogin();
                    modelLogin.auth_code = data_obj.getString("auth_code");
                    modelLogin.company_name = data_obj.getString("company_name");
                    modelLogin.company_logo = data_obj.getString("company_logo");
                    modelLogin.currency_code = data_obj.getString("currency_code");
                    modelLogin.currency_symbol = data_obj.getString("currency_symbol");
                    modelLogin.shipping_status = data_obj.getString("shipping_status");
                    modelLogin.shipping_cost = data_obj.getString("shipping_cost");
                    modelLogin.tax_label = data_obj.getString("tax_label");
                    modelLogin.message_count = data_obj.getString("message_count");
                    modelLogin.tax_percentage = data_obj.getString("tax_percentage");
                    modelLogin.prefix_name = data_obj.getString("company_prefix");
                    modelLogin.customer_business = data_obj.getString("customer_business");

//*******************Saving Login data to shared Prefrences*****************************************

                    System.out.println("Authcode="+data_obj.getString("auth_code"));
                    editor = prefs.edit();
                    editor.putString("prefix_name", modelLogin.prefix_name);
                    editor.putString("auth_code", modelLogin.auth_code);
                    editor.putString("company_name", modelLogin.company_name);
                    editor.putString("currency_symbol", modelLogin.currency_symbol);
                    editor.putString("shipping_status", modelLogin.shipping_status);
                    editor.putString("shipping_cost", modelLogin.shipping_cost);
                    editor.putString("tax_label", modelLogin.tax_label);
                    editor.putString("message_count", modelLogin.message_count);
                    editor.putString("tax_percentage", modelLogin.tax_percentage);
                    editor.putString("customer_business", modelLogin.customer_business);
                    editor.commit();
                    query.createLogin(modelLogin);
                } else {


                    JSONObject data_obj = obj.getJSONObject("Data");
                    result = data_obj.getString("result");


                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressBar.setVisibility(View.GONE);
            if (status.equals("true")) {

                SplashActivity.logdirect = false;
                Intent intent = new Intent(LoginActivity.this, Dashboard.class);
                startActivity(intent);
                finish();
            } else {

                Utils.dialog(LoginActivity.this, "Incorrect Credentials", "The Credentials entered are incorrect.\n"+"Please enter correct login credentials\n"+"to proceed.");
            }
        }
    }
    //************************Webservice parameter LoginMethod*************************

    public String LoginMethod(String prefix_name, String username, String password, String device_id, String type) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", username)
                .appendQueryParameter("password", password)
                .appendQueryParameter("device_id", device_id)
                .appendQueryParameter("type", "android");
        res = parser.getJSONFromUrl(Utils.login, builder,LoginActivity.this);
        return res;
    }
    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}



package com.ordertron;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;

import Adapters.More_settingAccount_Adapter;
import DataBaseUtils.DatabaseQueries;
import Model.ModelLogin;

/**
 * Created by Abhijeet on 12/21/2015.
 */
public class More_Setting_Accounts extends AppCompatActivity {
    RecyclerView list;

    RecyclerView.LayoutManager mLayoutManager;
    public static boolean flagrefresh = false;
    public static int pos = 0;
    SharedPreferences prefs;
    LinearLayout add_btn;
    SharedPreferences.Editor editor;
    DatabaseQueries query;
    More_settingAccount_Adapter adapter;
    public static int acc_edit = 0;
    ArrayList<ModelLogin> acounts = new ArrayList<ModelLogin>();
    Menu newmenu;
    public static ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_accounts);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        getvalues();
        clikevents();

    }

    public void init() {
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        query = new DatabaseQueries(More_Setting_Accounts.this);
        list = (RecyclerView) findViewById(R.id.acc_list);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        add_btn = (LinearLayout) findViewById(R.id.add_btn);
        mLayoutManager = new LinearLayoutManager(More_Setting_Accounts.this);
        list.setLayoutManager(mLayoutManager);
    }

    //********************************clikevents****************************************************
    private void clikevents() {
        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(More_Setting_Accounts.this, Add_Account.class);
                startActivity(i);
            }
        });
//        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Dashboard.item.clear();
//
//                query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
//
//                String new_prefix_name = list.getItemAtPosition(position).toString();
//                String dd = acounts.get(position).prefix_name;
//                try {
//                    new_prefix = query.GetLogin(acounts.get(position).prefix_name);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//
//                }
//                for (int i = 0; i < new_prefix.size(); i++) {
//                    ModelLogin modelLogin = new_prefix.get(i);
//                 //   Toast.makeText(getApplication(), "" + modelLogin.prefix_name, Toast.LENGTH_SHORT).show();
//
//                    editor = prefs.edit();
//                    editor.putString("prefix_name", modelLogin.prefix_name);
//                    editor.putString("auth_code", modelLogin.auth_code);
//                    editor.putString("company_name", modelLogin.company_name);
//                    editor.putString("currency_symbol", modelLogin.currency_symbol);
//                    editor.putString("shipping_status", modelLogin.shipping_status);
//                    editor.putString("shipping_cost", modelLogin.shipping_cost);
//                    editor.putString("tax_label", modelLogin.tax_label);
//                    editor.putString("message_count", modelLogin.message_count);
//                    editor.putString("tax_percentage", modelLogin.tax_percentage);
//                    editor.putString("customer_business", modelLogin.customer_business);
//                    editor.commit();
//                }
//
//                if ((acounts.get(position).prefix_name).equals(prefs.getString("prefix_name", null))) {
//                    list.setSelection(position);
//                    list.setSelected(true);
//                } else {
//                    list.setSelection(position);
//                    list.setSelected(false);
//                }
//
//
////                Intent i = new Intent(More_Setting_Accounts.this, Dashboard.class);
////                startActivity(i);
//            }
//        });

    }

    //********************************Getting values************************************************
    private void getvalues() {

        try {
            acounts = query.GetLogin_details();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //*************This code executes when customer is Inactive or deleted from backend*************
        String prefix = prefs.getString("prefix_name", "");
        if (prefix == null || prefix.equals("")) {
            for (int r = 0; r < 1; r++) {
                editor = prefs.edit();
                editor.putString("prefix_name", acounts.get(r).prefix_name);
                editor.putString("auth_code", acounts.get(r).auth_code);
                editor.putString("company_name", acounts.get(r).company_name);
                editor.putString("currency_symbol", acounts.get(r).currency_symbol);
                editor.putString("shipping_status", acounts.get(r).shipping_status);
                editor.putString("shipping_cost", acounts.get(r).shipping_cost);
                editor.putString("tax_label", acounts.get(r).tax_label);
                editor.putString("message_count", acounts.get(r).message_count);
                editor.putString("tax_percentage", acounts.get(r).tax_percentage);
                editor.putString("customer_business", acounts.get(r).customer_business);
                editor.commit();
                System.out.println("PPPPPrefix=" + acounts.get(r).prefix_name);

            }
        }
//        else{
////            for (int r = 0; r < 1; r++) {
////                editor = prefs.edit();
////                editor.putString("prefix_name", acounts.get(r).prefix_name);
////                editor.commit();
////            }
//        }
        adapter = new More_settingAccount_Adapter(More_Setting_Accounts.this, acounts);
        list.setAdapter(adapter);


    }

    //********************************onResume****************************************************
    @Override
    protected void onResume() {
        super.onResume();
        if (flagrefresh) {
            flagrefresh = false;
            getvalues();
        }
    }

    //********************************onCreateOptionsMenu****************************************************


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        this.newmenu = menu;
        inflater.inflate(R.menu.menu_actionbar, newmenu);
        acc_edit = 0;
        newmenu.findItem(R.id.edit).setVisible(true);
        newmenu.findItem(R.id.done).setVisible(false);
        return super.onCreateOptionsMenu(newmenu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // finish();
                return true;
            case R.id.edit:
                acc_edit = 1;
                newmenu.findItem(R.id.edit).setVisible(false);
                newmenu.findItem(R.id.done).setVisible(true);
                adapter.notifyDataSetChanged();
                return true;
            case R.id.done:
                acc_edit = 0;
                newmenu.findItem(R.id.edit).setVisible(true);
                newmenu.findItem(R.id.done).setVisible(false);
                adapter.notifyDataSetChanged();

                return true;
        }
        return false;
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
//    }
}

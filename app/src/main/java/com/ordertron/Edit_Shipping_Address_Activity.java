package com.ordertron;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Model.Model_Add_Detail_Customer;

/**
 * Created by admin on 1/8/2016.
 */
public class Edit_Shipping_Address_Activity extends AppCompatActivity {
    Intent i;
    public static ListView list;
    String shipping_address_id, prefix_name;
    ArrayList<String> arr = new ArrayList<>();
    Menu menu;
    public static String Country_name;
    public static String value_of_selected_list, addrId;
    DatabaseQueries databaseQueries;
    ArrayList<Model_Add_Detail_Customer> allOrder_arrayList = new ArrayList<Model_Add_Detail_Customer>();
    SharedPreferences prefs;
    int pos = -1;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Init();
        getadress();
        getvalues();
    }

    private void getadress() {
        allOrder_arrayList.clear();
        try {
            allOrder_arrayList = databaseQueries.get_all_address(prefix_name);


//            }
        } catch (Exception e) {
        }
    }

    private void getvalues() {
        arr.clear();




        for (int r = 0; r < allOrder_arrayList.size(); r++) {
            String Full_adress = allOrder_arrayList.get(r).add_1;
            if (shipping_address_id.equals(allOrder_arrayList.get(r).id)) {
                pos = r;
            }
            if (!allOrder_arrayList.get(r).add_2.equals("")) {
                Full_adress = Full_adress + ", " + allOrder_arrayList.get(r).add_2;
            }
            if (!allOrder_arrayList.get(r).add_3.equals("")) {
                Full_adress = Full_adress + ", " + allOrder_arrayList.get(r).add_3;
            }
            if (!allOrder_arrayList.get(r).suburb.equals("")) {
                Full_adress = Full_adress + ", " + allOrder_arrayList.get(r).suburb;
            }
            if (!allOrder_arrayList.get(r).state.equals("")) {
                Full_adress = Full_adress + ", " + allOrder_arrayList.get(r).state;
            }
            if (!allOrder_arrayList.get(r).country.equals("")) {
                Full_adress = Full_adress + ", " + allOrder_arrayList.get(r).country;
            }
            if (!allOrder_arrayList.get(r).postcode.equals("")) {
                Full_adress = Full_adress + ", " + allOrder_arrayList.get(r).postcode;
            }
            if (!allOrder_arrayList.get(r).fax.equals("")) {
                Full_adress = Full_adress + ", " + allOrder_arrayList.get(r).fax;
            }
            if (!allOrder_arrayList.get(r).tel_1.equals("")) {
                Full_adress = Full_adress + ", " + allOrder_arrayList.get(r).tel_1;
            }
            if (!allOrder_arrayList.get(r).tel_2.equals("")) {
                Full_adress = Full_adress + ", " + allOrder_arrayList.get(r).tel_2;
            }
            // if(allOrder_arrayList.get(r).id.equals())
            arr.add(Full_adress);

            Country_name = allOrder_arrayList.get(r).country;
        }
        ArrayList<String> al = new ArrayList<>();
// add elements to al, including duplicates
        // List<String> al = new ArrayList<>();
// add elements to al, including duplicates
        int  a= arr.size();
        int b=allOrder_arrayList.size();

        ArrayAdapter<String> adp = new ArrayAdapter<String>(Edit_Shipping_Address_Activity.this, android.R.layout.simple_list_item_checked, arr);
        adp.notifyDataSetChanged();
        list.setAdapter(adp);

        list.setItemChecked(pos, true);
        list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                value_of_selected_list = (String) parent.getItemAtPosition(position);
                addrId = allOrder_arrayList.get(position).id;
                PlaceOrderActivity.check = false;
                //  Toast.makeText(getApplicationContext(),allOrder_arrayList.get(position).id,Toast.LENGTH_LONG).show();
                editor = prefs.edit();
                editor.putString("shipping_address_id", allOrder_arrayList.get(position).id);
                editor.putString("shipping_address", (String) parent.getItemAtPosition(position));
                editor.commit();
                //       PlaceOrderActivity.pos = position;
                finish();
            }
        });

    }

    //********************************onCreateOptionsMenu****************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_add_address, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.add:
                // EITHER CALL THE METHOD HERE OR DO THE FUNCTION DIRECTLY

                Intent intent = new Intent(Edit_Shipping_Address_Activity.this, Add_Shipping_Address.class);
                intent.putExtra("country_name", Country_name);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void Init() {
        list = (ListView) findViewById(R.id.acc_list);
        i = getIntent();
        shipping_address_id = i.getStringExtra("shipping_address_id");
        databaseQueries = new DatabaseQueries(Edit_Shipping_Address_Activity.this);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        prefix_name = prefs.getString("prefix_name", null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getadress();
        getvalues();
      /* if (new_ad){
            new_ad=false;
            getadress();
            getvalues();
        }*/

    }
}

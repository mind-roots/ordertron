package com.ordertron;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import DataBaseUtils.DatabaseQueries;
import Model.JSONParser;
import Model.Methods;
import Model.ModelLogin;
import Model.Model_Add_Detail_Customer;
import Model.Model_GetCategories;
import Model.Model_GetPage;
import Model.Model_GetProduct;
import Model.Model_Order_Info;
import Model.Model_Order_Product;
import Model.Model_Product_Inventory;
import Model.Model_Template;
import Model.Model_Tier_pricing;
import Model.Model_get_notification;
import Model.ModelgetCustomer;
import Model.Utils;

/**
 * Created by abc on 12/8/2015.
 */
public class More_Settings extends AppCompatActivity {

    RelativeLayout rl_resetall_data, rl_prefrences, rl_orders;
    DatabaseQueries query;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    ProgressBar progressBar;
    boolean b;
    String product_image_id = "0", product_id = "0", category_id = "0", inventory_id = "0", tier_id = "0", order_info = "0", order_product = "0", template = "0";
    boolean movefurther = true;
    Methods methods;
    String responseGetCustomer, responseAddDetailCustomer, responsegetcatagories, responsegetpage, responsegetproducts, responsegetproductsimage,
            responsegetproductsinventory, responsetierpricing, responseorderinfo, responseorderproduct, responsetemplate, responseGetnotificationsetting;

    Intent intent;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //*******************************************************************************************
        //when account is inactive form server then user is passing form this acitivity to more setting acconts
        //so that on back press it will come to this activity
        intent=getIntent();
        try {
            String del = intent.getStringExtra("del_account");
            if (del.equals("del_account")) {
                Intent in = new Intent(More_Settings.this, More_Setting_Accounts.class);
                startActivity(in);
            }
        }catch (Exception e){

        }


        //******************************************************************************************


        methods = new Methods(More_Settings.this);
        init();
        clickevent();



    }

    //**************Initilize UI components***********
    public void init() {
        b = Utils.isNetworkConnected(More_Settings.this);
        rl_resetall_data = (RelativeLayout) findViewById(R.id.rl_resetall_data);
        rl_prefrences = (RelativeLayout) findViewById(R.id.rl_prefrences);
        rl_orders = (RelativeLayout) findViewById(R.id.rl_orders);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        query = new DatabaseQueries(More_Settings.this);
        prefs = getSharedPreferences("login", Context.MODE_PRIVATE);
    }

    //*******Click Events***********************
    public void clickevent() {

        rl_resetall_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogBox();

            }
        });

        rl_prefrences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(More_Settings.this, More_Setting_Prefrences.class);
                startActivity(i);

            }
        });

        rl_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(More_Settings.this, More_Setting_Accounts.class);
                startActivity(i);

            }
        });
    }

    //***********Function Showing AlertDialog Box****************************************
    public void dialogBox() {


        new android.support.v7.app.AlertDialog.Builder(More_Settings.this, Dashboard.style)
                .setTitle("Reset Data")
                .setMessage("Are you sure you want to sync all data?\n" +
                        "You may lose any un-submitted orders,unsaved templates or items in cart.")
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                if (!b) {
                                    Utils.conDialog(More_Settings.this);
                                } else {
                                    delete_data();
                                    hitthreads();
                                }
                            }
                        })
                .setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete

                            }
                        })

                .show();

       /* AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, Dashboard.style);

        alertDialogBuilder.setTitle("Sync All Data?");
        alertDialogBuilder.setMessage("Are you sure you want to sync all data?");
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        /*//********Deleting contents form All Tables*********
         delete_data();
         hitthreads();
         }
         });
         alertDialogBuilder.setNegativeButton("No",
         new DialogInterface.OnClickListener() {

        @Override public void onClick(DialogInterface arg0, int arg1) {


        }
        });

         AlertDialog alertDialog = alertDialogBuilder.create();
         // alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
         alertDialog.show();*/
    }

    //********************************************Delete Date from Tables***************************
    public void delete_data() {
        query.delete_table_data(DatabaseQueries.TABLE_GETCUSTOMER, "prefix_name", prefs.getString("prefix_name", null));
        query.delete_table_data(DatabaseQueries.TABLE_ADD_DETAIL_CUSTOMER, "prefix_name", prefs.getString("prefix_name", null));
        query.delete_table_data(DatabaseQueries.TABLE_GETCATEGORIES, "prefix_name", prefs.getString("prefix_name", null));
        query.delete_table_data(DatabaseQueries.TABLE_GETPAGE, "prefix_name", prefs.getString("prefix_name", null));
        query.delete_table_data(DatabaseQueries.TABLE_GET_PRODUCTS, "prefix_name", prefs.getString("prefix_name", null));
        query.delete_table_data(DatabaseQueries.TABLE_GET_PRODUCT_IMAGE, "prefix_name", prefs.getString("prefix_name", null));
        query.delete_table_data(DatabaseQueries.TABLE_GET_PRODUCT_INVENTORY, "prefix_name", prefs.getString("prefix_name", null));
        query.delete_table_data(DatabaseQueries.TABLE_TIER_PRICING, "prefix_name", prefs.getString("prefix_name", null));
        query.delete_table_data(DatabaseQueries.TABLE_ORDER_INFO, "prefix_name", prefs.getString("prefix_name", null));
        query.delete_table_data(DatabaseQueries.TABLE_ORDER_PRODUCT, "prefix_name", prefs.getString("prefix_name", null));
        query.delete_table_data(DatabaseQueries.TABLE_ORDER_TEMPLATE, "prefix_name", prefs.getString("prefix_name", null));
        query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
        query.delete_table_data(DatabaseQueries.TABLE_NOTIFICATION_SETTINGS, "prefix_name", prefs.getString("prefix_name", null));
    }

    //***************************** Calling Async Task for Various web services*************************

    private void hitthreads() {
        if (!b) {
            Utils.conDialog(More_Settings.this);
        } else {
            new multiple(category_id, product_id, product_image_id, inventory_id, tier_id, order_info, order_product, template).execute();
        }
    }

    //*****************************Calling Multiple AsyncTasks******************************************
//**************************************************************************************************
    public class multiple extends AsyncTask<Void, Void, Void> {
        ProgressDialog dia = new ProgressDialog(More_Settings.this);
        String status, data, category_id, product_id, product_image_id, inventory_id, tier_id, order_info, order_product, template;
        String last_sync_id, product_last_sync_id, productImage_last_sync_id, productInventory_last_sync_id, tierpriceing_last_sync_id, order_last_sync_id, order_product_last_sync_id, template_last_sync_id;

        //****Constructor****

        public multiple(String category_id, String product_id, String product_image_id, String inventory_id
                , String tier_id, String order_info, String order_product, String template) {
            this.product_id = product_id;
            this.category_id = category_id;
            this.product_image_id = product_image_id;
            this.inventory_id = inventory_id;
            this.tier_id = tier_id;
            this.order_info = order_info;
            this.order_product = order_product;
            this.template = template;
        }


        @Override
        public void onPreExecute() {
            super.onPreExecute();
            dia.setMessage("Resetting all data....");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        public Void doInBackground(Void... params) {
            String responseGetCustomer = GetCustomerMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));
            //  String responseAddDetailCustomer = AddDetailMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));
            // String responsegetcatagories = GetCategoriesMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), category_id);
            // String responsegetpage = GetPageMethod(prefs.getString("prefix_name", null));
            // String responsegetproducts = GetProductsMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), product_id);
            //    String responsegetproductsimage = GetProductsImageMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), product_image_id);
            // String responsegetproductsinventory = GetProductsInventoryMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), inventory_id);
            // String responsetierpricing = GetTierPricingMethod(prefs.getString("prefix_name", null), prefs.getString("username", null), prefs.getString("auth_code", null), tier_id);
            // String responseorderinfo = GetOrderInfoMethod(prefs.getString("prefix_name", null), prefs.getString("username", null), prefs.getString("auth_code", null), order_info);
            //String responseorderproduct = GetOrderProductMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), order_product);
            //   String responsetemplate = GetTemplateMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), template, prefs.getString("username", null));
            // String responseGetnotificationsetting = GetNotificationSetting(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));

//*************************Parsing for Get_customer*************************************************
            try {
                JSONObject obj = new JSONObject(responseGetCustomer);
                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONObject data_obj = obj.getJSONObject("Data");
                    ModelgetCustomer modelgetCustomer = new ModelgetCustomer();
                    modelgetCustomer.ws_users_id = data_obj.getString("ws_users_id");
                    modelgetCustomer.business_name = data_obj.getString("business_name");
                    modelgetCustomer.business_abn = data_obj.getString("business_abn");
                    modelgetCustomer.billing_add_id = data_obj.getString("billing_add_id");
                    modelgetCustomer.default_shipping_add_id = data_obj.getString("default_shipping_add_id");
                    modelgetCustomer.external_record_id = data_obj.getString("external_record_id");
                    modelgetCustomer.credit_limit = data_obj.getString("credit_limit");
                    modelgetCustomer.customer_group_name = data_obj.getString("customer_group_name");
                    modelgetCustomer.prefix_name = prefs.getString("prefix_name", null);
//*********calling Databasequries insert function for getcustomer***********************************
                    query.getcustomer(modelgetCustomer);

                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    } else {
                        data = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.dialog(More_Settings.this, "Error!", data);
                            }
                        });
                    }
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responseAddDetailCustomer = AddDetailMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));

            }
//*************************Parsing for Add_detail_customer******************************************
            try {
                JSONObject obj = new JSONObject(responseAddDetailCustomer);
                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Add_Detail_Customer modeladddetail = new Model_Add_Detail_Customer();
                        modeladddetail.id = obj1.getString("id");
                        modeladddetail.ws_users_id = obj1.getString("ws_users_id");
                        modeladddetail.first_name = obj1.getString("first_name");
                        modeladddetail.last_name = obj1.getString("last_name");
                        modeladddetail.add_1 = obj1.getString("add_1");
                        modeladddetail.add_2 = obj1.getString("add_2");
                        modeladddetail.add_3 = obj1.getString("add_3");
                        modeladddetail.suburb = obj1.getString("suburb");
                        modeladddetail.postcode = obj1.getString("postcode");
                        modeladddetail.tel_1 = obj1.getString("tel_1");
                        modeladddetail.tel_2 = obj1.getString("tel_2");
                        modeladddetail.fax = obj1.getString("fax");
                        modeladddetail.email = obj1.getString("email");
                        modeladddetail.default_shipping_add = obj1.getString("default_shipping_add");
                        modeladddetail.default_billing_add = obj1.getString("default_billing_add");
                        modeladddetail.status = obj1.getString("status");
                        modeladddetail.created_date = obj1.getString("created_date");
                        modeladddetail.update_date = obj1.getString("update_date");
                        modeladddetail.updated_by = obj1.getString("updated_by");
                        modeladddetail.country = obj1.getString("country");
                        modeladddetail.state = obj1.getString("state");

                        modeladddetail.prefix_name = prefs.getString("prefix_name", null);
//******************calling Databasequries insert function for add_detail_customer******************
                        query.add_detail_customer(modeladddetail);

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    } else {
                        data = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.dialog(More_Settings.this, "Error!", data);
                            }
                        });
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responsegetcatagories = GetCategoriesMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), category_id);

            }
//******************************Parsing for Get-Categories******************************************
            try {
                JSONObject obj = new JSONObject(responsegetcatagories);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    last_sync_id = obj.getString("last_sync_id");

//****************************saving data to database through model class***************************
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_GetCategories modelgetcategoris = new Model_GetCategories();
                        modelgetcategoris.id = obj1.getString("id");
                        modelgetcategoris.category_name = obj1.getString("category_name");
                        modelgetcategoris.fullname = obj1.getString("fullname");
                        modelgetcategoris.parent_cat_id = obj1.getString("parent_cat_id");
                        modelgetcategoris.root_cat_id = obj1.getString("root_cat_id");
                        modelgetcategoris.level = obj1.getString("level");
                        modelgetcategoris.sort = obj1.getString("sort");
                        modelgetcategoris.status = obj1.getString("status");
                        modelgetcategoris.created_date = obj1.getString("created_date");
                        modelgetcategoris.update_date = obj1.getString("update_date");
                        modelgetcategoris.updated_by = obj1.getString("updated_by");
                        modelgetcategoris.sid = obj1.getString("sid");
                        modelgetcategoris.type = obj1.getString("type");
                        modelgetcategoris.view_type = obj1.getString("Typed");
                        modelgetcategoris.last_sync_id = last_sync_id;

                        modelgetcategoris.prefix_name = prefs.getString("prefix_name", null);
//********************calling Databasequries insert function for getcategories**********************
                        query.getcategories(modelgetcategoris);

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    } else {
                        data = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.dialog(More_Settings.this, "Error!", data);
                            }
                        });
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (movefurther) {
                responsegetpage = GetPageMethod(prefs.getString("prefix_name", null));
            }
//*******************************Parsing for Get-Page***********************************************
            try {
                JSONObject obj = new JSONObject(responsegetpage);
                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************************saving data to database through model class**************************

                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_GetPage modelGetPage = new Model_GetPage();
                        modelGetPage.id = obj1.getString("id");
                        modelGetPage.page_url = obj1.getString("page_url");
                        modelGetPage.status = obj1.getString("status");

                        modelGetPage.prefix_name = prefs.getString("prefix_name", null);

//************************calling Databasequries insert function for getcategories******************

                        query.getpage(modelGetPage);

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    } else {
                        data = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.dialog(More_Settings.this, "Error!", data);
                            }
                        });
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responsegetproducts = GetProductsMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), product_id);

            }
//********************************Parsing for Get Products******************************************
            try {
                JSONObject obj = new JSONObject(responsegetproducts);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    product_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_GetProduct modelgetProduct = new Model_GetProduct();
                        modelgetProduct.order_unit_name = obj1.getString("order_unit_name");
                        modelgetProduct.base_unit_name = obj1.getString("base_unit_name");
                        modelgetProduct.id = obj1.getString("id");
                        modelgetProduct.sink_id = obj1.getString("sink_id");
                        modelgetProduct.cat_id = obj1.getString("cat_id");
                        modelgetProduct.product_code = obj1.getString("product_code");
                        modelgetProduct.name = obj1.getString("name");
                        modelgetProduct.desc = obj1.getString("desc");
                        modelgetProduct.status = obj1.getString("status");
                        modelgetProduct.cost_price = obj1.getString("cost_price");
                        modelgetProduct.min_order_qut = obj1.getString("min_order_qut");
                        modelgetProduct.max_order_qut = obj1.getString("max_order_qut");
                        modelgetProduct.taxable = obj1.getString("taxable");
                        modelgetProduct.base_unit_id = obj1.getString("base_unit_id");
                        modelgetProduct.unit_price = obj1.getString("unit_price");
                        modelgetProduct.order_unit_id = obj1.getString("order_unit_id");
                        modelgetProduct.inventory = obj1.getString("inventory");
                        modelgetProduct.spcl_price = obj1.getString("spcl_price");
                        modelgetProduct.warehouse_name = obj1.getString("warehouse_name");
                        modelgetProduct.warehouse_loc = obj1.getString("warehouse_loc");
                        modelgetProduct.stock_loc = obj1.getString("stock_loc");

                        modelgetProduct.img=obj1.getString("image");
                        modelgetProduct.thumb_image=obj1.getString("thumb_image");

                        modelgetProduct.created_date = obj1.getString("created_date");
                        modelgetProduct.update_date = obj1.getString("update_date");
                        modelgetProduct.updated_by = obj1.getString("updated_by");
                        modelgetProduct.type = obj1.getString("type");
                        modelgetProduct.view_type = obj1.getString("Typed");
                        //**Saving Array to a string in database

                        modelgetProduct.tier_price = obj1.getJSONArray("tier_price").toString();
                        modelgetProduct.prefix_name = prefs.getString("prefix_name", null);
                        modelgetProduct.last_sync_id = product_last_sync_id;
                        //**calling Databasequries insert function for add_detail_customer
                        query.getproduct(modelgetProduct);

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    } else {
                        data = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.dialog(More_Settings.this, "Error!", data);
                            }
                        });
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//            if (movefurther) {
//                responsegetproductsimage = GetProductsImageMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), product_image_id);
//            }
////**************************Parsing for Get Products Image******************************************
//            try {
//                JSONObject obj = new JSONObject(responsegetproductsimage);
//                status = obj.getString("Status");
//
//                if (status.equals("true")) {
//                    productImage_last_sync_id = obj.getString("last_sync_id");
//                    //**saving data to database through model class
//                    JSONArray data_arr = obj.getJSONArray("Data");
//                    for (int i = 0; i < data_arr.length(); i++) {
//                        JSONObject obj1 = data_arr.getJSONObject(i);
//                        Model_Product_Image modelProductimage = new Model_Product_Image();
//                        modelProductimage.id = obj1.getString("id");
//                        modelProductimage.sink_id = obj1.getString("sink_id");
//                        modelProductimage.product_id = obj1.getString("product_id");
//                        modelProductimage.image = obj1.getString("image");
//                        modelProductimage.thumb_image = obj1.getString("thumb_image");
//                        modelProductimage.status = obj1.getString("status");
//                        modelProductimage.is_main = obj1.getString("is_main");
//                        modelProductimage.type = obj1.getString("type");
//                        modelProductimage.created_date = obj1.getString("created_date");
//                        modelProductimage.update_date = obj1.getString("update_date");
//                        modelProductimage.updated_by = obj1.getString("updated_by");
//                        modelProductimage.last_sync_id = productImage_last_sync_id;
//
//                        modelProductimage.prefix_name = prefs.getString("prefix_name", null);
//                        //**calling Databasequries insert function for add_detail_customer
//                        query.getproductImage(modelProductimage);
//
//                    }
//
//
//                } else {
//                    if (obj.has("logout")) {
//                        String logout = obj.optString("logout");
//                        if (logout.equals("yes")) {
//                            JSONObject jobj = obj.optJSONObject("Data");
//                            final String result = jobj.optString("result");
//                            movefurther = false;
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                    methods.dialog(result);
//                                }
//                            });
//                        }
//                    } else {
//                        data = obj.getString("error");
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Utils.dialog(More_Settings.this, "Error!", data);
//                            }
//                        });
//                    }
//                }
//
//            } catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
            if (movefurther) {
                responsegetproductsinventory = GetProductsInventoryMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), inventory_id);

            }
//**********************Parsing for Get Products Inventory******************************************
            try {
                JSONObject obj = new JSONObject(responsegetproductsinventory);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    productInventory_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Product_Inventory modelinventory = new Model_Product_Inventory();
                        modelinventory.id = obj1.getString("id");
                        modelinventory.product_id = obj1.getString("product_id");
                        modelinventory.sink_id = obj1.getString("sink_id");
                        modelinventory.aval_stock = obj1.getString("aval_stock");
                        modelinventory.reserve_qut = obj1.getString("reserve_qut");
                        modelinventory.bck_ord_aval = obj1.getString("bck_ord_aval");
                        modelinventory.status = obj1.getString("status");
                        modelinventory.warehouse_name = obj1.getString("warehouse_name");
                        modelinventory.warehouse_location = obj1.getString("warehouse_location");
                        modelinventory.stock_location = obj1.getString("stock_location");
                        modelinventory.created_date = obj1.getString("created_date");
                        modelinventory.update_date = obj1.getString("update_date");
                        modelinventory.updated_by = obj1.getString("updated_by");
                        modelinventory.type = obj1.getString("type");
                        modelinventory.last_sync_id = productInventory_last_sync_id;

                        modelinventory.prefix_name = prefs.getString("prefix_name", null);
                        //**calling Databasequries insert function for getporductInventory


                        query.getproductInventory(modelinventory);

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    } else {
                        data = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.dialog(More_Settings.this, "Error!", data);
                            }
                        });
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responsetierpricing = GetTierPricingMethod(prefs.getString("prefix_name", null), prefs.getString("username", null), prefs.getString("auth_code", null), tier_id);

            }
//****************************Parsing for Get Tier Pricing******************************************
            try {
                JSONObject obj = new JSONObject(responsetierpricing);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    tierpriceing_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Tier_pricing modeltier = new Model_Tier_pricing();
                        modeltier.id = obj1.getString("id");
                        modeltier.product_id = obj1.getString("product_id");
                        modeltier.customer_group_id = obj1.getString("customer_group_id");
                        modeltier.quantity_upto = obj1.getString("quantity_upto");
                        modeltier.discount_type = obj1.getString("discount_type");
                        modeltier.discount = obj1.getString("discount");
                        modeltier.created_date = obj1.getString("created_date");
                        modeltier.update_date = obj1.getString("update_date");
                        modeltier.updated_by = obj1.getString("updated_by");
                        modeltier.sink_id = obj1.getString("sink_id");
                        modeltier.type = obj1.getString("type");
                        modeltier.last_sync_id = tierpriceing_last_sync_id;

                        modeltier.prefix_name = prefs.getString("prefix_name", null);
                        //**calling Databasequries insert function for getporductInventory
                        query.gettierpricing(modeltier);

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    } else {
                        data = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.dialog(More_Settings.this, "Error!", data);
                            }
                        });
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responseorderinfo = GetOrderInfoMethod(prefs.getString("prefix_name", null), prefs.getString("username", null), prefs.getString("auth_code", null), order_info);

            }
//****************************Parsing for Get OrderInfo*********************************************
            try {
                JSONObject obj = new JSONObject(responseorderinfo);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    order_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Order_Info modelorder = new Model_Order_Info();
                        modelorder.order_id = obj1.getString("order_id");
                        modelorder.customer_id = obj1.getString("customer_id");
                        modelorder.billing_country = obj1.getString("billing_country");
                        modelorder.billing_state = obj1.getString("billing_state");
                        modelorder.billing_suburb = obj1.getString("billing_suburb");
                        modelorder.billing_add_1 = obj1.getString("billing_add_1");
                        modelorder.billing_add_2 = obj1.getString("billing_add_2");
                        modelorder.billing_add_3 = obj1.getString("billing_add_3");
                        modelorder.billing_postcode = obj1.getString("billing_postcode");
                        modelorder.shipping_country = obj1.getString("shipping_country");
                        modelorder.shipping_state = obj1.getString("shipping_state");
                        modelorder.shipping_suburb = obj1.getString("shipping_suburb");
                        modelorder.shipping_add_1 = obj1.getString("shipping_add_1");
                        modelorder.shipping_add_2 = obj1.getString("shipping_add_2");
                        modelorder.shipping_add_3 = obj1.getString("shipping_add_3");
                        modelorder.shipping_postcode = obj1.getString("shipping_postcode");
                        modelorder.shipping_cost = obj1.getString("shipping_cost");
                        modelorder.sub_total = obj1.getString("sub_total");
                        modelorder.tax_total = obj1.getString("tax_total");
                        modelorder.total = obj1.getString("total");
                        modelorder.order_notes = obj1.getString("order_notes");
                        modelorder.internal_notes = obj1.getString("internal_notes");
                        modelorder.order_status = obj1.getString("order_status");
                        modelorder.back_order = obj1.getString("back_order");
                        modelorder.main_order_id = obj1.getString("main_order_id");
                        modelorder.delivery_date = obj1.getString("delivery_date");
                        modelorder.delivery_notes = obj1.getString("delivery_notes");
                        modelorder.created_date = obj1.getString("created_date");
                        modelorder.created_by = obj1.getString("created_by");
                        modelorder.update_date = obj1.getString("update_date");
                        modelorder.updated_by = obj1.getString("updated_by");
                        modelorder.order_name = obj1.getString("order_name");
                        modelorder.order_by = obj1.getString("order_by");
                        modelorder.sink_id = obj1.getString("sink_id");
                        modelorder.type = obj1.getString("type");
                        modelorder.last_sync_id = order_last_sync_id;
                        modelorder.show_deliverydate_order = obj1.getString("show_deliverydate_order");
                        modelorder.mandetory_delivery_order = obj1.getString("mandetory_delivery_order");
                        modelorder.prefix_name = prefs.getString("prefix_name", null);

                        //**calling Databasequries insert function for getporductInventory
                        query.getorderinfo(modelorder);

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    } else {
                        data = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.dialog(More_Settings.this, "Error!", data);
                            }
                        });
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responseorderproduct = GetOrderProductMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), order_product);

            }
//***************************Parsing for Get Order Product******************************************
            try {
                JSONObject obj = new JSONObject(responseorderproduct);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    order_product_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Order_Product modelOrderProduct = new Model_Order_Product();
                        modelOrderProduct.id = obj1.getString("id");
                        modelOrderProduct.order_id = obj1.getString("order_id");
                        modelOrderProduct.product_id = obj1.getString("product_id");
                        modelOrderProduct.product_name = obj1.getString("product_name");
                        modelOrderProduct.product_code = obj1.getString("product_code");
                        modelOrderProduct.qty = obj1.getString("qty");
                        modelOrderProduct.qty_new_order = obj1.getString("qty_new_order");
                        modelOrderProduct.act_qty = obj1.getString("act_qty");
                        modelOrderProduct.unit_price = obj1.getString("unit_price");
                        modelOrderProduct.applied_price = obj1.getString("applied_price");
                        modelOrderProduct.sub_total = obj1.getString("sub_total");
                        modelOrderProduct.sink_id = obj1.getString("sink_id");
                        modelOrderProduct.type = obj1.getString("type");
                        modelOrderProduct.created_date = obj1.getString("created_date");
                        modelOrderProduct.update_date = obj1.getString("update_date");
                        modelOrderProduct.updated_by = obj1.getString("updated_by");
                        modelOrderProduct.last_sync_id = order_product_last_sync_id;

                        modelOrderProduct.prefix_name = prefs.getString("prefix_name", null);
                        //**calling Databasequries insert function for getorderProduct
                        query.getorderproduct(modelOrderProduct);

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    } else {
                        data = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.dialog(More_Settings.this, "Error!", data);
                            }
                        });
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responsetemplate = GetTemplateMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), template, prefs.getString("username", null));

            }
//*************************Parsing for Get Order Templates******************************************
            try {
                JSONObject obj = new JSONObject(responsetemplate);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    template_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Template modelTemplate = new Model_Template();
                        modelTemplate.order_template_id = obj1.getString("order_template_id");
                        modelTemplate.customer_id = obj1.getString("customer_id");
                        modelTemplate.template_name = obj1.getString("template_name");
                        modelTemplate.created_date = obj1.getString("created_date");
                        modelTemplate.created_by = obj1.getString("created_by");
                        modelTemplate.sink_id = obj1.getString("sink_id");
                        modelTemplate.type = obj1.getString("type");

                        modelTemplate.prefix_name = prefs.getString("prefix_name", null);

                        //**Saving Array to a string in database

                        modelTemplate.product_id = obj1.getJSONArray("product_id").toString();
                        modelTemplate.product_code = obj1.getJSONArray("product_code").toString();
                        modelTemplate.qty = obj1.getJSONArray("qty").toString();
                        System.out.println("modelTemplate.product_id:" + modelTemplate.product_id);
                        System.out.println("modelTemplate.product_code:" + modelTemplate.product_code);
                        System.out.println("modelTemplate.qty:" + modelTemplate.qty);
                        //**calling Databasequries insert function for gettemplates
                        query.gettemplate(modelTemplate);

                    }


                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            movefurther = false;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    methods.dialog(result);
                                }
                            });
                        }
                    } else {
                        data = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.dialog(More_Settings.this, "Error!", data);
                            }
                        });
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (movefurther) {
                responseGetnotificationsetting = GetNotificationSetting(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));
            }
//---------------------parsing for getnotification settings------------
            try {
                JSONObject jsonObject = new JSONObject(responseGetnotificationsetting);
                String status = jsonObject.optString("Status");
                if (status.equals("true")) {
                    String currency_code = jsonObject.optString("currency_code");
                    String currency_symbol = jsonObject.optString("currency_symbol");
                    String show_deliverydate = jsonObject.optString("show_deliverydate");
                    String mandetory_delivery = jsonObject.optString("mandetory_delivery");
                    String time_zone = jsonObject.optString("timezone");
                    String push_noti = jsonObject.optString("push_notification");
                    String email_noti = jsonObject.optString("email_notification");
                    String prefix_name = prefs.getString("prefix_name", null);
                    Model_get_notification item = new Model_get_notification();
                    item.Prefix_name = prefix_name;
                    item.Currency_code = currency_code;
                    item.Currency_symbol = currency_symbol;
                    item.show_deliverydate = show_deliverydate;
                    item.mandetory_delivery = mandetory_delivery;
                    item.time_zone = time_zone;
                    item.push_notification = push_noti;
                    item.email_notification = email_noti;
                    query.add_get_notification(item);

                    ModelLogin modelLogin = new ModelLogin();
                    modelLogin.currency_symbol = jsonObject.optString("currency_symbol");
                    modelLogin.shipping_status = jsonObject.optString("shipping_status");
                    modelLogin.shipping_cost = jsonObject.optString("shipping_cost");
                    modelLogin.tax_label = jsonObject.optString("tax_label");
                    modelLogin.tax_percentage = jsonObject.optString("tax_percentage");
                    editor = prefs.edit();
                    editor.putString("currency_symbol", modelLogin.currency_symbol);
                    editor.putString("shipping_status", modelLogin.shipping_status);
                    editor.putString("shipping_cost", modelLogin.shipping_cost);
                    editor.putString("tax_label", modelLogin.tax_label);
                    editor.putString("tax_percentage", modelLogin.tax_percentage);
                    editor.commit();
                    query.updateLogin(modelLogin,prefs.getString("prefix_name", null));
                }

            } catch (Exception e) {

            }
            return null;
        }

        @Override
        public void onPostExecute(Void o) {
            super.onPostExecute(o);
            dia.dismiss();
        }
    }

//*==*==*==*==*==*==*==**************Webservice parameter GetCustomerMethod*************************

    public String GetCustomerMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.getcustomer, builder,More_Settings.this);
        return res;
    }

//************************Webservice parameter GetCategories****************************************

    public String GetCategoriesMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getcategories, builder,More_Settings.this);
        return res;
    }

//******************************Webservice parameter GetPage****************************************

    public String GetPageMethod(String prefix_name) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name);
        res = parser.getJSONFromUrl(Utils.getpage, builder,More_Settings.this);
        return res;
    }

//******************************Webservice parameter AddDetailMethod********************************

    public String AddDetailMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.adddetailcustomer, builder,More_Settings.this);
        return res;
    }

//************************Webservice parameter GetProductsMethod ***********************************

    public String GetProductsMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproducts, builder,More_Settings.this);
        return res;
    }

//***************************Webservice parameter GetProductsImageMethod ***************************

    public String GetProductsImageMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproductsimage, builder,More_Settings.this);
        return res;
    }

//************************Webservice parameter GetProductsINVENTORYMethod **************************

    public String GetProductsInventoryMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproductsinventory, builder,More_Settings.this);
        return res;
    }

//************************Webservice parameter GetProductsINVENTORYMethod **************************

    public String GetTierPricingMethod(String prefix_name, String username, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", username)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproducttierpricing, builder,More_Settings.this);
        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderInfoMethod(String prefix_name, String username, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", username)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getorderinfo, builder,More_Settings.this);
        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderProductMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getorderproduct, builder,More_Settings.this);
        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetTemplateMethod(String prefix_name, String auth_code, String sid, String username) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid)
                .appendQueryParameter("username", username);
        res = parser.getJSONFromUrl(Utils.getordertemplate, builder,More_Settings.this);
        return res;
    }
    //*************************Webservice parameter getNotificationSetting *************************

    public String GetNotificationSetting(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);
        res = parser.getJSONFromUrl(Utils.getNotification_setting, builder,More_Settings.this);
        return res;
    }
}

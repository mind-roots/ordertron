package com.ordertron;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import Tutorials.ViewPager_Fragment2;
import Tutorials.ViewPager_Fragment3;
import Tutorials.ViewPager_Fragment4;
import Tutorials.ViewPager_Fragment5;
import Tutorials.ViewPager_Fragment6;
import Tutorials.ViewPager_Fragment7;

/**
 * Created by Abhijeet on 9/16/2016.
 */

public class CustomPagerAdapterNew extends FragmentPagerAdapter {
int numboffrag;

    private List<Fragment> fragments;
    public CustomPagerAdapterNew(FragmentManager fm, int size) {
        super(fm);
        this.numboffrag=size;
        this.fragments = new ArrayList<Fragment>();
        if(numboffrag==1){
            fragments.add(new ViewPager_Fragment2());
            Tutorial_View.tutorial=1;
        }else if(numboffrag==2){
            Tutorial_View.tutorial=1;
            fragments.add(new ViewPager_Fragment3());
            fragments.add(new ViewPager_Fragment4());
        }else if(numboffrag==3){
            Tutorial_View.tutorial=1;
            fragments.add(new ViewPager_Fragment5());

        }else if(numboffrag==4){
            Tutorial_View.tutorial=1;
            fragments.add(new ViewPager_Fragment6());
        }else if(numboffrag==5){
            Tutorial_View.tutorial=1;
            fragments.add(new ViewPager_Fragment7());
        }



    }

    @Override
    public Fragment getItem(int position) {

        return fragments.get(position);

    }

    @Override
    public int getCount() {
         return fragments.size();
    }
}

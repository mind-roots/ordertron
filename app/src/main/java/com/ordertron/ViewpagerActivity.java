package com.ordertron;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import Tutorials.ActivityStack;
import Tutorials.CircleIndicator;
import Tutorials.CustomPagerAdapter;

/**
 * Created by Abhijeet on 9/15/2016.
 */

public class ViewpagerActivity extends AppCompatActivity {
    CustomPagerAdapter pagerAdapter;
    public static ViewPager defaultViewpager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_activity);

        setTitle("");
        ActivityStack.activity.add(ViewpagerActivity.this);
        defaultViewpager = (ViewPager) findViewById(R.id.viewpager_default);
        CircleIndicator defaultIndicator = (CircleIndicator) findViewById(R.id.indicator_default);
        pagerAdapter = new CustomPagerAdapter(getSupportFragmentManager());
        defaultViewpager.setAdapter(pagerAdapter);
        defaultIndicator.setViewPager(defaultViewpager);


    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
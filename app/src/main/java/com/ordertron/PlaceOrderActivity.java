package com.ordertron;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import Adapters.CartAdapter;
import DataBaseUtils.DatabaseQueries;
import Fragments.CartActivity;
import Model.JSONParser;
import Model.Methods;
import Model.Model_Add_Detail_Customer;
import Model.Model_Order_Info;
import Model.Model_Order_Product;
import Model.ModelgetCustomer;
import Model.Utils;

/**
 * Created by admin on 1/7/2016.
 */
public class PlaceOrderActivity extends AppCompatActivity {
    TextView order_total, billing_address, shipping_address, set_deliverydate, show_deliverydate;
    EditText delivery_notes, order_notes, order_by;
    String product_qty, product_code, total_cost, prefix_name, all_order_id,
            Full_adress_billing, Full_adress_shipping, username, Currency_symbol, authcode, order_by_string, delivery_notes_string, order_notes_string, id;
    ArrayList<Model_Add_Detail_Customer> allOrder_arrayList = new ArrayList<Model_Add_Detail_Customer>();
    public static String billing_address_id, shipping_address_id;
    ArrayList<ModelgetCustomer> addressId = new ArrayList<ModelgetCustomer>();
    // public static ArrayList<Model_billingqty> bill_q = new ArrayList<Model_billingqty>();
    DatabaseQueries databaseQueries;
    int position = 0;
    RelativeLayout showlay;
    SharedPreferences prefs;
    Menu menu;
    ProgressDialog dialog;
    ProgressDialog dialog1;

    public static boolean check = true;
    public static int pos = -1, cancel = 1;
    boolean b;
    SharedPreferences.Editor editor;
    NumberFormat formatter;
    Methods methods;
    float tt = 0;
    String tal = null;
    String showdeliverydate, mand_date, get_date = "", daa = "";

    private Calendar cal;
    private int day;
    private int month;
    private int year1;
    ImageView cross;
    public static final String DATE_DASH_FORMAT = "yyyy-MM-dd";

    public static final String DATE_FORMAT = "dd/MM/yyyy";

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_order);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();


        cal = Calendar.getInstance();
        year1 = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DAY_OF_MONTH);

        for (int i = 0; i < CartActivity.showdelivery.size(); i++) {
            showdeliverydate = CartActivity.showdelivery.get(i).show_deliverydate;
            mand_date = CartActivity.showdelivery.get(i).mandetory_delivery;
        }
        if (showdeliverydate == null) {
            showdeliverydate = "";
        }
        if (showdeliverydate.equals("yes")) {
            showlay.setVisibility(View.VISIBLE);
            if (mand_date.equals("yes")) {
                show_deliverydate.setText("Delivery Date*");
                cross.setVisibility(View.INVISIBLE);
            } else {
                show_deliverydate.setText("Delivery Date ");

            }
        } else {
            showlay.setVisibility(View.GONE);
        }

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set_deliverydate.setText("");
                cross.setVisibility(View.INVISIBLE);
            }
        });
        //   allOrder_arrayList.clear();
        methods = new Methods(PlaceOrderActivity.this);
        try {
            allOrder_arrayList = databaseQueries.get_all_address(prefix_name);
            addressId = databaseQueries.getShippingAndBillingID(prefix_name);
            billing_address_id = addressId.get(0).billing_add_id;
            shipping_address_id = addressId.get(0).default_shipping_add_id;
            editor = prefs.edit();
            editor.putString("shipping_address_id", shipping_address_id);
            editor.putString("billing_address_id", billing_address_id);
            editor.commit();
            for (int i = 0; i < allOrder_arrayList.size(); i++) {
                all_order_id = allOrder_arrayList.get(i).id;

                if (all_order_id.equals(prefs.getString("billing_address_id", null))) {

                    Full_adress_billing = allOrder_arrayList.get(i).add_1;

                    if (!allOrder_arrayList.get(i).add_2.equals("")) {
                        Full_adress_billing = Full_adress_billing + ", " + allOrder_arrayList.get(position).add_2;
                    }
                    if (!allOrder_arrayList.get(i).add_3.equals("")) {
                        Full_adress_billing = Full_adress_billing + ", " + allOrder_arrayList.get(i).add_3;
                    }
                    if (!allOrder_arrayList.get(i).suburb.equals("")) {
                        Full_adress_billing = Full_adress_billing + ", " + allOrder_arrayList.get(i).suburb;
                    }
                    if (!allOrder_arrayList.get(i).state.equals("")) {
                        Full_adress_billing = Full_adress_billing + ", " + allOrder_arrayList.get(i).state;
                    }
                    if (!allOrder_arrayList.get(i).country.equals("")) {
                        Full_adress_billing = Full_adress_billing + ", " + allOrder_arrayList.get(i).country;
                    }
                    if (!allOrder_arrayList.get(i).postcode.equals("")) {
                        Full_adress_billing = Full_adress_billing + ", " + allOrder_arrayList.get(i).postcode;
                    }


                }
                if (all_order_id.equals(prefs.getString("shipping_address_id", null))) {
                    Full_adress_shipping = allOrder_arrayList.get(i).add_1;
                    pos = i;
                    if (!allOrder_arrayList.get(i).add_2.equals("")) {
                        Full_adress_shipping = Full_adress_shipping + ", " + allOrder_arrayList.get(i).add_2;
                    }
                    if (!allOrder_arrayList.get(i).add_3.equals("")) {
                        Full_adress_shipping = Full_adress_shipping + ", " + allOrder_arrayList.get(i).add_3;
                    }
                    if (!allOrder_arrayList.get(i).suburb.equals("")) {
                        Full_adress_shipping = Full_adress_shipping + ", " + allOrder_arrayList.get(i).suburb;
                    }
                    if (!allOrder_arrayList.get(i).state.equals("")) {
                        Full_adress_shipping = Full_adress_shipping + ", " + allOrder_arrayList.get(i).state;
                    }
                    if (!allOrder_arrayList.get(i).country.equals("")) {
                        Full_adress_shipping = Full_adress_shipping + ", " + allOrder_arrayList.get(i).country;
                    }
                    if (!allOrder_arrayList.get(i).postcode.equals("")) {
                        Full_adress_shipping = Full_adress_shipping + ", " + allOrder_arrayList.get(i).postcode;
                    }


                }
            }
            editor = prefs.edit();
            editor.putString("shipping_address", Full_adress_shipping);

            editor.commit();
            billing_address.setText(Full_adress_billing);
            shipping_address.setText(Full_adress_shipping);
        } catch (Exception e) {
            e.printStackTrace();

        }
        click();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dialog.dismiss();
        dialog1.dismiss();
    }

    private void click() {
        shipping_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(PlaceOrderActivity.this, Edit_Shipping_Address_Activity.class);
                i.putExtra("shipping_address_id", prefs.getString("shipping_address_id", null));
                startActivity(i);
            }
        });
        set_deliverydate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateDialog();

            }
        });
    }

    public void DateDialog() {

        final DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {// Toast.makeText(getApplicationContext(), "Invalid Year", Toast.LENGTH_SHORT).show();
                int mont = monthOfYear + 1;
                if (cancel == 1) {

                } else {
                    cancel = 1;
                    if (year > year1) {
                        set_deliverydate.setText(dayOfMonth + "/" + mont + "/" + year);
                        if (mand_date.equals("yes")) {
                            cross.setVisibility(View.INVISIBLE);
                        }else{
                            cross.setVisibility(View.VISIBLE);
                        }
                        daa = dayOfMonth + "/" + mont + "/" + year;

                        String DateStr = daa;
                        SimpleDateFormat sim = new SimpleDateFormat(" EEE, dd MMM yyyy");
                        try {
                            Date d = new SimpleDateFormat("dd/MM/yyyy").parse(DateStr);
                            String convettdate = sim.format(d);
                            set_deliverydate.setText(convettdate);
                            if (mand_date.equals("yes")) {
                                cross.setVisibility(View.INVISIBLE);
                            }else{
                                cross.setVisibility(View.VISIBLE);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else {

                        if (year == year1) {
                            if (monthOfYear > month) {
                                //set_deliverydate.setText(dayOfMonth + "/" + mont + "/" + year);
                                daa = dayOfMonth + "/" + mont + "/" + year;

                                String DateStr = daa;
                                SimpleDateFormat sim = new SimpleDateFormat(" EEE, dd MMM yyyy");
                                try {
                                    Date d = new SimpleDateFormat("dd/MM/yyyy").parse(DateStr);
                                    String convettdate = sim.format(d);
                                    set_deliverydate.setText(convettdate);
                                    if (mand_date.equals("yes")) {
                                        cross.setVisibility(View.INVISIBLE);
                                    }else{
                                        cross.setVisibility(View.VISIBLE);
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (monthOfYear == month) {
                                    if (dayOfMonth >= day) {
                                        //set_deliverydate.setText(dayOfMonth + "/" + mont + "/" + year);

                                        daa = dayOfMonth + "/" + mont + "/" + year;

                                        String DateStr = daa;
                                        SimpleDateFormat sim = new SimpleDateFormat(" EEE, dd MMM yyyy");
                                        try {
                                            Date d = new SimpleDateFormat("dd/MM/yyyy").parse(DateStr);
                                            String convettdate = sim.format(d);
                                            set_deliverydate.setText(convettdate);
                                            if (mand_date.equals("yes")) {
                                                cross.setVisibility(View.INVISIBLE);
                                            }else{
                                                cross.setVisibility(View.VISIBLE);
                                            }
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Delivery date cannot be in the past", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Delivery date cannot be in the past", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Delivery date cannot be in the past", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            }
        };

        if (!set_deliverydate.getText().toString().equals("")) {
            if (mand_date.equals("yes")) {
                cross.setVisibility(View.INVISIBLE);
            }else{
                cross.setVisibility(View.VISIBLE);
            }
            try {
                String sss = set_deliverydate.getText().toString();
                SimpleDateFormat parserSDF = new SimpleDateFormat(" EEE, dd MMM yyyy", Locale.ENGLISH);
                Date date = parserSDF.parse(set_deliverydate.getText().toString());
                System.out.println("date: " + date.toString());
                String year = (String) android.text.format.DateFormat.format("yyyy", date);
                int yeare = Integer.parseInt(year);
                String moo = (String) android.text.format.DateFormat.format("MM", date);
                int moo1 = Integer.parseInt(moo);
                int moo2 = moo1 - 1;
                String ddd = (String) android.text.format.DateFormat.format("dd", date);
                int ddd1 = Integer.parseInt(ddd);
                // String s= String.valueOf(date.getYear());


                final DatePickerDialog dpDialog = new DatePickerDialog(this, listener, yeare, moo2, ddd1);
                dpDialog.setCancelable(false);
                dpDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dpDialog.setOnDismissListener(mOnDismissListener);
                            cancel = 1;

                        }
                    }
                });
                dpDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            cancel = 0;
                            DatePicker datePicker = dpDialog.getDatePicker();
                            listener.onDateSet(datePicker, datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                        }
                    }
                });
                dpDialog.show();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            cross.setVisibility(View.INVISIBLE);
            final DatePickerDialog dpDialog = new DatePickerDialog(this, listener, year1, month, day);
            dpDialog.setCancelable(false);
            dpDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (which == DialogInterface.BUTTON_NEGATIVE) {
                        dpDialog.setOnDismissListener(mOnDismissListener);
                    }
                }
            });
            dpDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        cancel = 0;
                        DatePicker datePicker = dpDialog.getDatePicker();
                        listener.onDateSet(datePicker, datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                    }
                }
            });
            dpDialog.show();

        }

    }

    public DialogInterface.OnDismissListener mOnDismissListener =
            new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                    cancel = 1;
                }
            };

    private void init() {
        order_total = (TextView) findViewById(R.id.order_total);
        billing_address = (TextView) findViewById(R.id.billing_address);
        dialog = new ProgressDialog(PlaceOrderActivity.this);
        dialog1 = new ProgressDialog(PlaceOrderActivity.this);
        set_deliverydate = (TextView) findViewById(R.id.set_deliverydate);
        show_deliverydate = (TextView) findViewById(R.id.show_deliverydate);
        showlay = (RelativeLayout) findViewById(R.id.showlay);

cross=(ImageView)findViewById(R.id.cross);
        shipping_address = (TextView) findViewById(R.id.shipping_address);
        delivery_notes = (EditText) findViewById(R.id.delivery_notes);
        order_notes = (EditText) findViewById(R.id.order_notes);
        order_by = (EditText) findViewById(R.id.order_by);
        b = Utils.isNetworkConnected(PlaceOrderActivity.this);
        databaseQueries = new DatabaseQueries(PlaceOrderActivity.this);
        prefs = getSharedPreferences("login", MODE_PRIVATE);
        prefix_name = prefs.getString("prefix_name", null);
        username = prefs.getString("username", null);
        authcode = prefs.getString("auth_code", null);
        Currency_symbol = prefs.getString("currency_symbol", null);
        formatter = new DecimalFormat("#0.00");
        Intent i = getIntent();
        total_cost = i.getStringExtra("total_cost");
        product_code = i.getStringExtra("pid");
        product_qty = i.getStringExtra("pqty");

        if (total_cost != null) {
            if (total_cost.equals("TBA")) {
                order_total.setText(total_cost);
            } else {
                order_total.setText(Currency_symbol + formatter.format(Double.parseDouble(total_cost)));
            }
        }


    }

    //********************************onCreateOptionsMenu****************************************************
    //@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.submitorder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.submit:
                // EITHER CALL THE METHOD HERE OR DO THE FUNCTION DIRECTLY

                if (order_by.getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please Fill all the requirement!", Toast.LENGTH_LONG).show();
                } else {
                    order_by_string = order_by.getText().toString();
                    //  delivery_notes_string = delivery_notes.getText().toString();
                    delivery_notes_string = "";
                    order_notes_string = order_notes.getText().toString();
                    if (!b) {
                        Utils.conDialog(PlaceOrderActivity.this);
                    } else {
                        hidekeyboard();

                        if (mand_date.equals("yes")) {
                            if (set_deliverydate.getText().toString() == null || set_deliverydate.getText().toString().equals("")) {
                                Toast.makeText(getApplicationContext(), "Delivery Date is required!", Toast.LENGTH_LONG).show();
                            } else {
                                get_date = daa;
                                try {
                                    Date date = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH).parse(get_date);

                                    DateFormat formatter = new SimpleDateFormat(DATE_DASH_FORMAT, Locale.getDefault());

                                    get_date = formatter.format(date.getTime());

                                } catch (ParseException e) {

                                    e.printStackTrace();

                                }
                                new Submit_order(get_date).execute();
                            }
                        } else {
                            get_date = set_deliverydate.getText().toString();
                            if (get_date == null || get_date.equals("")) {
                                get_date = "";
                            } else {
                                try {
                                    Date date = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH).parse(get_date);

                                    DateFormat formatter = new SimpleDateFormat(DATE_DASH_FORMAT, Locale.getDefault());

                                    get_date = formatter.format(date.getTime());

                                } catch (ParseException e) {

                                    e.printStackTrace();

                                }
                                SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd");
                                try {
                                    Date d = new SimpleDateFormat(" EEE, dd MMM yyyy").parse(get_date);
                                    get_date = sim.format(d);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    Date date = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH).parse(get_date);

                                    DateFormat formatter = new SimpleDateFormat(DATE_DASH_FORMAT, Locale.getDefault());

                                    get_date = formatter.format(date.getTime());

                                } catch (ParseException e) {

                                    e.printStackTrace();

                                }
                            }
                            new Submit_order(get_date).execute();
                        }


                    }
                }
                /*else if (order_notes.getText().toString().length() == 0)
                    Toast.makeText(getApplicationContext(), "Please Fill all the requirement!", Toast.LENGTH_LONG).show();
                else if (delivery_notes.getText().toString().length() == 0)
                    Toast.makeText(getApplicationContext(), "Please Fill all the requirement!", Toast.LENGTH_LONG).show();*/

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
    //--------------------ASYNC to submit order---------------

    public class Submit_order extends AsyncTask<Void, Void, Void> {

        String value_url, get_date, nedate;

        public Submit_order(String get_date) {
            this.get_date = get_date;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog1.setMessage("Submitting order.......");
            dialog1.setCancelable(false);
            dialog1.show();
            nedate = get_date;
        }

        @Override
        protected Void doInBackground(Void... params) {
            value_url = submit_order_method(prefix_name, username, authcode, product_code, product_qty, billing_address_id, prefs.getString("shipping_address_id", null), total_cost, order_by_string, order_notes_string, delivery_notes_string, nedate);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //  {"Status":"true","Data":{"order_id":"75","default_shipping":21,"default_billing":22,"order_product_id":[{"id":"121"},{"id":"122"}],"order_name":"02 Feb 2016 - 04:54 AM","sync_id":"63"}}
            try {

                JSONObject jsonObject = new JSONObject(value_url);
                String status = jsonObject.getString("Status");


                if (status.equals("true")) {
                    JSONObject jsonObject1 = jsonObject.getJSONObject("Data");
                    String order_id = jsonObject1.optString("order_id");
                    String default_shipping = jsonObject1.optString("default_shipping");
                    String default_billing = jsonObject1.optString("default_billing");
                    String act = jsonObject1.getJSONArray("act").toString();
//                    JSONArray act= jsonObject1.getJSONArray("act");
//                    for(int l=0;l<act.length();l++){
//                        JSONObject bi=act.getJSONObject(l);
//                        Model_billingqty bill= new Model_billingqty();
//                        bill.product_code=bi.getString("product_code");
//                        bill.act_qty=bi.getString("act_qty");
//                        bill_q.add(bill);
//
//                    }
                    databaseQueries.updateaadid(prefix_name, default_shipping, default_billing, act);
                    //  databaseQueries.updateaadid(prefix_name, default_shipping, default_billing);
                    JSONArray jsonArray = jsonObject1.getJSONArray("order_product_id");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                        String id = jsonObject2.optString("id");
                    }
                    String order_name = jsonObject1.optString("order_name");
                    String sync_id = jsonObject1.optString("sync_id");

                    updatestock(order_id);

                } else if (status.equals("false")) {

                    if (jsonObject.has("logout")) {
                        String logout = jsonObject.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = jsonObject.optJSONObject("Data");
                            final String result = jobj.optString("result");
                            methods = new Methods(PlaceOrderActivity.this);
                            Methods.placeorder = 1;
                            methods.dialog(result);


                        }
                    } else {
                        JSONObject jsonObject1 = jsonObject.optJSONObject("Data");
                        String result = jsonObject1.optString("result");
                        try {
                            dialog("Alert!", result, android.R.drawable.ic_dialog_alert);

                        } catch (Exception e) {
                            dialog("Alert!", "Your order has not been submitted due to some technical issue.Please try again later", android.R.drawable.ic_dialog_alert);
                        }
                    }
                }
            } catch (Exception e) {
                dialog("Alert!", "Your order has not been submitted due to some technical issue.Please try again later.", android.R.drawable.ic_dialog_alert);

            }

        }

        private void updatestock(String order_id) {
            databaseQueries.delete(DatabaseQueries.TABLE_ADD_TO_CART);
            Dashboard.cartupdate = true;
            if (!b) {
                Utils.conDialog(PlaceOrderActivity.this);
            } else {
                new stock().execute();
                new updatingorder(order_id).execute();
            }

        }

    }


//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderInfoMethod(String prefix_name, String username, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", username)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getorderinfo, builder, PlaceOrderActivity.this);
        return res;
    }

//*************************Webservice parameter GetProductsINVENTORYMethod *************************

    public String GetOrderProductMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getorderproduct, builder, PlaceOrderActivity.this);
        return res;
    }

    class updatingorder extends AsyncTask<String, Void, String> {
        String responseorderinfo, responseorderproduct, oid, order_product_last_sync_id, status, order_last_sync_id;
        String weboid, webpoid;

        public updatingorder(String order_id) {
            this.oid = order_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog.setMessage("Submitting order.......");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            responseorderinfo = GetOrderInfoMethod(prefs.getString("prefix_name", null), prefs.getString("username", null), prefs.getString("auth_code", null), "0");
            responseorderproduct = GetOrderProductMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), "0");


//****************************Parsing for Get OrderInfo*********************************************
            try {
                JSONObject obj = new JSONObject(responseorderinfo);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    order_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Order_Info modelorder = new Model_Order_Info();
                        weboid = obj1.getString("order_id");
                        modelorder.order_id = obj1.getString("order_id");
                        modelorder.customer_id = obj1.getString("customer_id");
                        modelorder.billing_country = obj1.getString("billing_country");
                        modelorder.billing_state = obj1.getString("billing_state");
                        modelorder.billing_suburb = obj1.getString("billing_suburb");
                        modelorder.billing_add_1 = obj1.getString("billing_add_1");
                        modelorder.billing_add_2 = obj1.getString("billing_add_2");
                        modelorder.billing_add_3 = obj1.getString("billing_add_3");
                        modelorder.billing_postcode = obj1.getString("billing_postcode");
                        modelorder.shipping_country = obj1.getString("shipping_country");
                        modelorder.shipping_state = obj1.getString("shipping_state");
                        modelorder.shipping_suburb = obj1.getString("shipping_suburb");
                        modelorder.shipping_add_1 = obj1.getString("shipping_add_1");
                        modelorder.shipping_add_2 = obj1.getString("shipping_add_2");
                        modelorder.shipping_add_3 = obj1.getString("shipping_add_3");
                        modelorder.shipping_postcode = obj1.getString("shipping_postcode");


                       // modelorder.shipping_cost=obj1.getString("shipping_cost");
                        String subt = obj1.getString("shipping_cost");
                        if (!subt.equals("TBA")) {
                            if(subt.equals("")){
                                subt="0";
                            }
                            tt = CartAdapter.round(Float.parseFloat(subt), 2);
                            modelorder.shipping_cost = subt;
                        } else {
                            modelorder.shipping_cost = String.valueOf(subt);
                        }

                        String taxt = obj1.getString("tax_total");
                        if (!taxt.equals("TBA")) {
                            tt = CartAdapter.round(Float.parseFloat(taxt), 2);
                            modelorder.tax_total = obj1.getString("tax_total");
                        } else {
                            modelorder.tax_total = String.valueOf(taxt);
                        }


                        String sot = obj1.getString("sub_total");
                        if (!sot.equals("TBA")) {
                            tt = CartAdapter.round(Float.parseFloat(sot), 2);
                            modelorder.sub_total = obj1.getString("sub_total");
                        } else {
                            modelorder.sub_total = String.valueOf(sot);
                        }


                        String tot = obj1.getString("total");
                        if (!tot.equals("TBA")) {
                            tt = CartAdapter.round(Float.parseFloat(tot), 2);
                            modelorder.total = String.valueOf(tt);
                        } else {
                            modelorder.total = String.valueOf(tot);
                        }

                        modelorder.order_notes = obj1.getString("order_notes");
                        modelorder.internal_notes = obj1.getString("internal_notes");
                        modelorder.order_status = obj1.getString("order_status");
                        modelorder.back_order = obj1.getString("back_order");
                        modelorder.main_order_id = obj1.getString("main_order_id");

                        String date = obj1.getString("delivery_date");
                        if (date == null || date.equals("")) {
                            modelorder.delivery_date = "";
                        } else {
                            SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd");
                            Date d = null;
                            try {
                                d = new SimpleDateFormat("yyyy-MM-dd").parse(date);
                                modelorder.delivery_date = sim.format(d);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }


                        modelorder.delivery_notes = obj1.getString("delivery_notes");
                        modelorder.created_date = obj1.getString("created_date");
                        modelorder.created_by = obj1.getString("created_by");
                        modelorder.update_date = obj1.getString("update_date");
                        modelorder.updated_by = obj1.getString("updated_by");
                        modelorder.order_name = obj1.getString("order_name");
                        modelorder.order_by = obj1.getString("order_by");
                        modelorder.sink_id = obj1.getString("sink_id");
                        modelorder.type = obj1.getString("type");
                        modelorder.last_sync_id = order_last_sync_id;
                        modelorder.mandetory_delivery_order = obj1.getString("mandetory_delivery_order");
                        modelorder.show_deliverydate_order = obj1.getString("show_deliverydate_order");
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("created_at", obj1.getString("order_name"));
                        editor.commit();

                        modelorder.prefix_name = prefs.getString("prefix_name", null);
                        //**calling Databasequries insert function for getporductInventory
                        if (weboid.equals(oid)) {
                            databaseQueries.getorderinfo(modelorder);

                        }

                    }


                } else {
                    //data = obj.getString("error");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//***************************Parsing for Get Order Product******************************************
            try {
                JSONObject obj = new JSONObject(responseorderproduct);
                status = obj.getString("Status");

                if (status.equals("true")) {
                    order_product_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    for (int i = 0; i < data_arr.length(); i++) {
                        JSONObject obj1 = data_arr.getJSONObject(i);
                        Model_Order_Product modelOrderProduct = new Model_Order_Product();
                        modelOrderProduct.id = obj1.getString("id");
                        webpoid = obj1.getString("order_id");
                        modelOrderProduct.order_id = obj1.getString("order_id");
                        modelOrderProduct.product_id = obj1.getString("product_id");
                        modelOrderProduct.product_name = obj1.getString("product_name");
                        modelOrderProduct.product_code = obj1.getString("product_code");
                        modelOrderProduct.qty = obj1.getString("qty");
                        modelOrderProduct.qty_new_order = obj1.getString("qty_new_order");
                        modelOrderProduct.act_qty = obj1.getString("act_qty");
                        modelOrderProduct.unit_price = obj1.getString("unit_price");
                        modelOrderProduct.applied_price = obj1.getString("applied_price");
                        modelOrderProduct.sub_total = obj1.getString("sub_total");
                        modelOrderProduct.sink_id = obj1.getString("sink_id");
                        modelOrderProduct.type = obj1.getString("type");
                        modelOrderProduct.created_date = obj1.getString("created_date");
                        modelOrderProduct.update_date = obj1.getString("update_date");
                        modelOrderProduct.updated_by = obj1.getString("updated_by");
                        modelOrderProduct.last_sync_id = order_product_last_sync_id;

                        modelOrderProduct.prefix_name = prefs.getString("prefix_name", null);
                        //**calling Databasequries insert function for getorderProduct
                        if (webpoid.equals(oid)) {
                            databaseQueries.getorderproduct(modelOrderProduct);

                        }

                    }


                } else {
                    //   data = obj.getString("error");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            dialog("Success", "Your order has been submitted.", android.R.drawable.ic_dialog_info);
        }
    }

    class stock extends AsyncTask<String, Void, String> {
        String responsegetproductsinventory;

        @Override
        protected String doInBackground(String... params) {
            responsegetproductsinventory = GetProductsInventoryMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null), "0");

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject obj = new JSONObject(responsegetproductsinventory);
                String status = obj.getString("Status");

                if (status.equals("true")) {
                    String productInventory_last_sync_id = obj.getString("last_sync_id");
                    //**saving data to database through model class
                    JSONArray data_arr = obj.getJSONArray("Data");
                    if (data_arr != null) {
                        if (data_arr.length() != 0) {
                            for (int i = 0; i < data_arr.length(); i++) {

                                JSONObject jobj = data_arr.getJSONObject(i);
                                String id = jobj.getString("id");
                                String product_id = jobj.getString("product_id");
                                String sink_id = jobj.getString("sink_id");
                                String aval_stock = jobj.getString("aval_stock");
                                String reserve_qut = jobj.getString("reserve_qut");
                                String bck_ord_aval = jobj.getString("bck_ord_aval");
                                String instatus = jobj.getString("status");
                                String type = jobj.getString("type");

                                databaseQueries.updateinventdb(id, product_id, sink_id, aval_stock, reserve_qut, bck_ord_aval, instatus, type, productInventory_last_sync_id, prefix_name);

                            }
                        }
                    }


                } else {

                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    public String GetProductsInventoryMethod(String prefix_name, String auth_code, String sid) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("sid", sid);
        res = parser.getJSONFromUrl(Utils.getproductsinventory, builder, PlaceOrderActivity.this);
        return res;
    }
    //*************************Webservice parameter Submit order *************************

    public String submit_order_method(String prefix_name, String user_name, String auth_code, String product_code
            , String qty, String bill_add_id, String shipp_add_id, String sub_total, String Order_by
            , String order_notes, String delivery_notes, String getdate) {
        String res = null;
        JSONParser parser = new JSONParser();
        //  String URL=Utils.order_save+"prefix_name="+prefix_name+"&username="+user_name+"&auth_code="+auth_code+"&product_code="+product_code+"&qty="+qty+"&billing_add_id=", bill_add_id;

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("username", user_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("product_code", product_code)
                .appendQueryParameter("qty", qty)
                .appendQueryParameter("billing_add_id", bill_add_id)
                .appendQueryParameter("default_shipping_add_id", shipp_add_id)
                .appendQueryParameter("sub_total", sub_total)
                .appendQueryParameter("order_by", Order_by)
                .appendQueryParameter("order_notes", order_notes)
                .appendQueryParameter("delivery_notes", delivery_notes)
                .appendQueryParameter("delivery_date", getdate);
//        Uri.Builder builder1 = new Uri.Builder()
//                .appendQueryParameter("", "");
//
//        String buill=Utils.order_save.concat("prefix_name="+prefix_name).concat("&username="+user_name).concat("&auth_code="+auth_code).concat("&product_code="+product_code)
//                .concat("&qty="+qty).concat("&billing_add_id="+bill_add_id)
//                .concat("&default_shipping_add_id="+shipp_add_id).concat("&sub_total="+sub_total).concat("&order_by="+Order_by)
//                .concat("&order_notes="+order_notes).concat("&delivery_notes="+delivery_notes).concat("&delivery_date="+getdate);
//        System.out.println("Buildresponce: "+buill);
//        res = parser.getJSONFromUrl(buill,builder1);

        System.out.print("sa=" + Utils.order_save + builder.toString());
        res = parser.getJSONFromUrl(Utils.order_save, builder, PlaceOrderActivity.this);
        return res;
    }

    public void dialog(String title, String msg, int icon) {
        new AlertDialog.Builder(PlaceOrderActivity.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CartActivity.cartonres = 1;
                        finish();
                    }
                }).show();

    }
//        new AlertDialog.Builder(PlaceOrderActivity.this, Dashboard.style)
//                .setTitle(title)
//                .setMessage(msg)
//                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        CartActivity.cartonres = 1;
//                        finish();
//
//                    }
//                })
//
//                .setIcon(icon)
//                .setCancelable(false)
//                .show();

    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (check == false) {
            shipping_address.setText(prefs.getString("shipping_address", null));
            // shipping_address_id = Edit_Shipping_Address_Activity.addrId;
        }


    }
}

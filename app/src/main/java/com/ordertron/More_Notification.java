package com.ordertron;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import Adapters.More_Info_adapter;
import Fragments.MoreActivity;
import Model.Model_getUrl;

/**
 * Created by Abhijeet on 1/6/2016.
 */
public class More_Notification extends AppCompatActivity {

    SharedPreferences prefs;
    WebView web;

    RelativeLayout lay1, lay2, lay3, laymain;
    String url = "";
    int lay1visible = 0, lay2visible = 0, lay3visible = 0;
    RecyclerView info_list;
    int size = 0;
    ArrayList<Model_getUrl> geturl = new ArrayList<>();
    More_Info_adapter info_adapter;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_information);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        if (MoreActivity.url.size() > 0) {
            geturl.clear();
            for (int i = 0; i < MoreActivity.url.size(); i++) {
                if (MoreActivity.url.get(i).status.equalsIgnoreCase("Active")) {
                    Model_getUrl g = MoreActivity.url.get(i);
                    geturl.add(g);
                }
            }
            if(geturl.size()==0) {
                laymain.setVisibility(View.VISIBLE);
            }else{
                laymain.setVisibility(View.GONE);
                info_adapter = new More_Info_adapter(this, geturl, size);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                info_list.setLayoutManager(mLayoutManager);
                info_list.setAdapter(info_adapter);
            }

        }


    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private void init() {

        info_list = (RecyclerView) findViewById(R.id.info_list);

        laymain = (RelativeLayout) findViewById(R.id.laymain);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


}

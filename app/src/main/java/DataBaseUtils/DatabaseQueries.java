package DataBaseUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import Model.ModelLogin;
import Model.Model_Add_Detail_Customer;
import Model.Model_Add_To_Cart;
import Model.Model_Cat_Prod;
import Model.Model_GetCategories;
import Model.Model_GetPage;
import Model.Model_GetProduct;
import Model.Model_Order_Info;
import Model.Model_Order_Product;
import Model.Model_Product_Image;
import Model.Model_Product_Inventory;
import Model.Model_Template;
import Model.Model_Tier_pricing;
import Model.Model_get_notification;
import Model.ModelgetCustomer;
import Model.Utils;

public class DatabaseQueries {


    private DataBaseHelper dataBaseHelper;
    //***************Table names************
    public static String TABLE_LOGIN = "login";
    public static String TABLE_GETCUSTOMER = "getcustomer";
    public static String TABLE_ADD_DETAIL_CUSTOMER = "add_detail_customer";
    public static String TABLE_GETCATEGORIES = "getcategories";
    public static String TABLE_GETPAGE = "getpage";
    public static String TABLE_GET_PRODUCTS = "getproducts";
    public static String TABLE_GET_PRODUCT_IMAGE = "get_product_image";
    public static String TABLE_GET_PRODUCT_INVENTORY = "getproduct_inventory";
    public static String TABLE_TIER_PRICING = "tier_pricing";
    public static String TABLE_ORDER_INFO = "order_info";
    public static String TABLE_ORDER_PRODUCT = "order_product";
    public static String TABLE_ORDER_TEMPLATE = "order_template";
    public static String TABLE_ADD_TO_CART = "cart_table";
    public static String TABLE_NOTIFICATION_SETTINGS = "notification_setting";
    //**********common fields**********
//    String ITEM = "item";
//    String STATUS = "status";

    //***********Login fields********
    String PREFIX_NAME = "prefix_name";
    String AUTH_CODE = "auth_code";
    String COMPANY_NAME = "company_name";
    String COMPANY_LOGO = "company_logo";
    String CURRENCY_CODE = "currency_code";
    String CURRENCY_SYMBOL = "currency_symbol";
    String SHIPPING_STATUS = "shipping_status";
    String SHIPPING_COST = "shipping_cost";
    String TAX_LABEL = "tax_label";
    String MESSAGE_COUNT = "message_count";
    String TAX_PERCENTAGE = "tax_percentage";
    String CUST_BUSINESS = "customer_business";

    //*************getcustomer fields************
    String PREFIX_GETCUSTOMER = " prefix_name";

    String USER_ID = "ws_users_id";
    String BUSINESS_NAME = "business_name";
    String BUSINESS_ABN = "business_abn";
    String BILLING_ADD_ID = "billing_add_id";
    String DEFAULT_SHIPPING_ADD_ID = "default_shipping_add_id";
    String EXTERNAL_RECORD_ID = "external_record_id";
    String CREDIT_LIMIT = "credit_limit";
    String CUSTOMER_GROUP_NAME = "customer_group_name";

    //*************Add_Detail_Customer fields************
    String PREFIX_ADD_DETAIL = "prefix_name";
    String ID = "id";
    String WS_USERS_ID = "ws_users_id";
    String FIRST_NAME = "first_name";
    String LAST_NAME = "last_name";
    String ADD_1 = "add_1";
    String ADD_2 = "add_2";
    String ADD_3 = "add_3";
    String SUBURB = "suburb";
    String POSTCODE = "postcode";
    String TEL_1 = "tel_1";
    String TEL_2 = "tel_2";
    String FAX = "fax";
    String EMAIL = "email";
    String DEFAULT_SHIPPING_ADD = "default_shipping_add";
    String DEFAULT_BILLING_ADD = "default_billing_add";
    String ACT = "act";
    String STATUS = "status";
    String CREATED_DATE = "created_date";
    String UPDATE_DATE = "update_date";
    String UPDATED_BY = "updated_by";
    String COUNTRY = "country";
    String STATE = "state";

    //*************GETCATEGORIES fields************
    String PREFIX_GETCATEGORIES = "prefix_name";
    String CAT_ID = "id";
    String CATETORY_NAME = "category_name";
    String FULLNAME = "fullname";
    String PARENT_CAT_ID = "parent_cat_id";
    String ROOT_CAT_ID = "root_cat_id";
    String LEVEL = "level";
    String SORT = "sort";
    String CAT_STATUS = "status";
    String CAT_CREATED_DATE = "created_date";
    String CAT_UPDATE_DATE = "update_date";
    String CAT_UPDATED_BY = "updated_by";
    String CAT_SID = "sid";
    String CAT_TYPE = "type";
    String VIEW_TYPE = "view_type";
    String LAST_SYNC_ID = "last_sync_id";
    //*************GETPAGE fields************
    String PREFIX_GETPAGE = " prefix_name";
    String PAGE_ID = "id";
    String PAGE_URL = "page_url";
    String PAGE_STATUS = "status";

    //*************GET PRODUCTS fields************
    String PREFIX_GET_PRODUCTS = "prefix_name";
    String ORDER_UNIT_NAME = "order_unit_name";
    String BASE_UNIT_NAME = "base_unit_name";
    String PRODUCT_ID = "id";
    String SINK_ID = "sink_id";
    String PRODUCT_CAT_ID = "cat_id";
    String PRODUCT_CODE = "product_code";
    String NAME = "name";
    String DESC = "desc";
    String PRODUCT_STATUS = "status";
    String COST_PRICE = "cost_price";
    String MIN_ORDER_QUT = "min_order_qut";
    String MAX_ORDER_QUT = "max_order_qut";
    String TAXABLE = "taxable";
    String BASE_UNIT_ID = "base_unit_id";
    String UNIT_PRICE = "unit_price";
    String ORDER_UNIT_ID = "order_unit_id";
    String INVENTORY = "inventory";
    String SPCL_PRICE = "spcl_price";
    String WAREHOUSE_NAME = "warehouse_name";
    String WAREHOUSE_LOC = "warehouse_loc";
    String STOCK_LOC = "stock_loc";
    String PRODUCT_CREATED_DATE = "created_date";
    String PRODUCT_UPDATE_DATE = "update_date";
    String PRODUCT_UPDATED_BY = "updated_by";
    String TYPE = "type";
    String TIER_PRICE = "tier_price";
    String PRODUCT_LAST_SYNC_ID = "last_sync_id";
    String THUMB_IMAGE = "thumb_image";

    //*************get_Product_Image fields************
    String PREFIX_PRODUCT_IMAGE = "prefix_name";
    String IMAGE_ID = "id";
    String IMAGE_SINK_ID = "sink_id";
    String IMAGE_PRODUCT_ID = "product_id";
    String IMAGE = "image";

    String IMAGE_STATUS = "status";
    String IS_MAIN = "is_main";
    String IMAGE_TYPE = "type";
    String IMAGE_CREATED_DATE = "created_date";
    String IMAGE_UPDATE_DATE = "update_date";
    String IMAGE_UPDATED_BY = "updated_by";
    String IMAGE_LAST_SYNC_ID = "last_sync_id";

    //*************get_Product_INVENTORY fields************
    String PREFIX_PRODUCT_INVENTORY = "prefix_name";
    String INVENTORY_ID = "id";
    String INVENTORY_PRODUCT_ID = "product_id";
    String INVENTORY_SINK_ID = "sink_id";
    String AVAL_STOCK = "aval_stock";
    String RESERVE_QUT = "reserve_qut";
    String BCK_ORD_AVAL = "bck_ord_aval";
    String INVENTORY_STATUS = "status";
    String INVENTORY_WAREHOUSE_NAME = "warehouse_name";
    String INVENTORY_WAREHOUSE_LOCATION = "warehouse_location";
    String STOCK_LOCATION = "stock_location";
    String INVENTORY_CREATED_DATE = "created_date";
    String INVENTORY_UPDATE_DATE = "update_date";
    String INVENTORY_UPDATE_BY = "updated_by";
    String INVENTORY_TYPE = "type";
    String INVENTORY_LAST_SYNC_ID = "last_sync_id";

    //*************get_TIER_PRICING fields************
    String PREFIX_TIER_PRICING = "prefix_name";
    String TIER_ID = "id";
    String TIER_PRODUCT_ID = "product_id";
    String CUSTOMER_GROUP_ID = "customer_group_id";
    String QUANTITY_UPTO = "quantity_upto";
    String TIER_DISCOUNT_TYPE = "discount_type";
    String TIER_DISCOUNT = "discount";
    String TIER_CREATED_DATE = "created_date";
    String TIER_UPDATE_DATE = "update_date";
    String TIER_UPDATE_BY = "updated_by";
    String TIER_SINK_ID = "sink_id";
    String TIER_TYEPE = "type";
    String TIER_LAST_SYNC_ID = "last_sync_id";

    //*************ORDER_INFO fields************
    String PREFIX_ORDER_INFO = "prefix_name";
    String ORDER_ID = "order_id";
    String CUSTOMER_ID = "customer_id";
    String BILLING_COUNTRY = "billing_country";
    String BILLING_STATE = "billing_state";
    String BILLING_SUBURB = "billing_suburb";
    String BILLING_ADD_1 = "billing_add_1";
    String BILLING_ADD_2 = "billing_add_2";
    String BILLING_ADD_3 = "billing_add_3";
    String BILLING_POSTCODE = "billing_postcode";
    String SHIPPING_COUNTRY = "shipping_country";
    String SHIPPING_STATE = "shipping_state";
    String SHIPPING_SUBURB = "shipping_suburb";
    String SHIPPING_ADD_1 = "shipping_add_1";
    String SHIPPING_ADD_2 = "shipping_add_2";
    String SHIPPING_ADD_3 = "shipping_add_3";
    String ORDER_SHIPPING_POSTCODE = "shipping_postcode";
    String ORDER_SHIPPING_COST = "shipping_cost";
    String SUB_TOTAL = "sub_total";
    String ORDER_TAX_TOTAL = "tax_total";
    String ORDER_TOTAL = "total";
    String ORDER_NOTES = "order_notes";
    String INTERNAL_NOTES = "internal_notes";
    String ORDER_STATUS = "order_status";
    String BACK_ORDER = "back_order";
    String MAIN_ORDER_ID = "main_order_id";
    String DELIVERY_DATE = "delivery_date";
    String DELIVERY_NOTES = "delivery_notes";
    String ORDER_CREATED_DATE = "created_date";
    String ORDER_CREATED_BY = "created_by";
    String ORDER_UPDATED_DATE = "update_date";
    String ORDER_UPDATED_BY = "updated_by";
    String ORDER_NAME = "order_name";
    String ORDER_BY = "order_by";
    String ORDER_SINK_ID = "sink_id";
    String ORDER_TYPE = "type";
    String SHOW_DELIVERYDATE_ORDER = "show_deliverydate_order";
    String MANDETORY_DELIVERY_ORDER = "mandetory_delivery_order";
    String ORDER_LAST_SYNC_ID = "last_sync_id";

    //*************ORDER_PRODUCTS fields************
    String PREFIX_ORDER_PRODUCTS = "prefix_name";
    String ORDER_PRODUCT_ID = "id";
    String PRODUCT_ORDER_ID = "order_id";
    String PRODUCT_PRODUCT_ID = "product_id";
    String PRODUCT_NAME = "product_name";
    String ORDER_PRODUCT_CODE = "product_code";
    String QTY = "qty";
    String QTY_NEW_ORDER = "qty_new_order";
    String ACT_QTY = "act_qty";
    String PRODUCT_UNIT_PRICE = "unit_price";
    String APL_PRICE = "applied_price";
    String PRODUCT_SUB_TOTAL = "sub_total";
    String PRODUCT_SINK_ID = "sink_id";
    String PRODUCT_TYPE = "type";
    String ORDER_PRODUCT_CREATED_DATE = "created_date";
    String ORDER_PRODUCT_UPDATE_DATE = "update_date";
    String ORDER_PRODUCT_UPDATED_BY = "updated_by";
    String ORDER_PRODUCT_LAST_SYNC_ID = "last_sync_id";

    //*************ORDER _TEMPLATE fields************
    String PREFIX_ORDER_TEMPLATE = "prefix_name";
    String ORDER_TEMPLATE_ID = "order_template_id";
    String TEMPLATE_ORDER_ID = "customer_id";
    String TEMPLATE_NAME = "template_name";
    String TEMPLATE_CREATED_DATE = "created_date";
    String TEMPLATE_CREATED_BY = "created_by";
    String TEMPLATE_SINK_ID = "sink_id";
    String TEMPLATE_TYPE = "type";
    String TEMPLATE_PRODUCT_ID = "product_id";
    String TEMPLATE_PRODUCT_CODE = "product_code";
    String TEMPLATE_QTY = "qty";
    String TEMPLATE_LAST_SYNC_ID = "last_sync_id";

    //*******************************************
//************ADD_TO_CART fields*************
    public static String PROD_ID = "prod_id";
    String PROD_NAME = "prod_name";
    public static String PROD_SKU = "prod_sku";
    String CART_QTY = "qty";
    String PRICE = "price";
    String CART_SPCL_PRICE = "spcl_price";
    String CART_MAX_QTY = "max_qty";
    String CART_MIN_QTY = "min_qty";
    String BASE_UNIT = "base_unit";
    String ORDER_UNIT = "order_unit";
    String CART_TAXABLE = "taxable";
    String PROD_TAX = "prod_tax";
    String PROD_TOTAL = "prod_total";
    String PROD_INVENT = "prod_inventory";
    double product_total, prod_tax;
    //------------------------NOTIFICATION_SETTINGS------------
    String PREFIX_NAME_NOTI = "prefix_name";
    String CURRENCY_CODE_NOTI = "currency_code";
    String CURRENCY_SYMBOL_NOTI = "currency_symbol";
    String TIME_ZONE_NOTI = "timezone";
    String PUSH_NOTIFICATION = "push_notification";
    String EMAIL_NOTIFICATION = "email_notification";
    String SHOW_DELIVERYDATE = "show_deliverydate";
    String MANDETORY_DELIVERY = "mandetory_delivery";

    public DatabaseQueries(Context context) {

        dataBaseHelper = new DataBaseHelper(context);
    }

    private String populateItem(Cursor c, String COLUMN) {

        String item = null;
        item = c.getString(c.getColumnIndex(COLUMN));
        return item;
    }

//    private byte[] populateImageItem(Cursor c, String COLUMN) {
//
//
//        byte[] item;
//        item = c.getBlob(c.getColumnIndex(COLUMN));
//        return item;
//    }

    //*****************Content Values for Login*************
    private ContentValues populateLogin(ModelLogin model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_NAME, model.prefix_name);
        values.put(AUTH_CODE, model.auth_code);
        values.put(COMPANY_NAME, model.company_name);
        values.put(COMPANY_LOGO, model.company_logo);
        values.put(CURRENCY_CODE, model.currency_code);
        values.put(CURRENCY_SYMBOL, model.currency_symbol);
        values.put(SHIPPING_STATUS, model.shipping_status);
        values.put(SHIPPING_COST, model.shipping_cost);
        values.put(TAX_LABEL, model.tax_label);
        values.put(MESSAGE_COUNT, model.message_count);
        values.put(TAX_PERCENTAGE, model.tax_percentage);
        values.put(CUST_BUSINESS, model.customer_business);
        return values;
    }

    //*****************Content Values for getCustomer*************
    private ContentValues populategetCustomer(ModelgetCustomer model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_GETCUSTOMER, model.prefix_name);
        values.put(USER_ID, model.ws_users_id);
        values.put(BUSINESS_NAME, model.business_name);
        values.put(BUSINESS_ABN, model.business_abn);
        values.put(BILLING_ADD_ID, model.billing_add_id);
        values.put(DEFAULT_SHIPPING_ADD_ID, model.default_shipping_add_id);
        values.put(EXTERNAL_RECORD_ID, model.external_record_id);
        values.put(CREDIT_LIMIT, model.credit_limit);
        values.put(CUSTOMER_GROUP_NAME, model.customer_group_name);

        return values;
    }

    //*****************Content Values for Add_detail_Customer*************
    private ContentValues populateAdd_detail_Customer(Model_Add_Detail_Customer model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_ADD_DETAIL, model.prefix_name);
        values.put(ID, model.id);
        values.put(WS_USERS_ID, model.ws_users_id);
        values.put(FIRST_NAME, model.first_name);
        values.put(LAST_NAME, model.last_name);
        values.put(ADD_1, model.add_1);
        values.put(ADD_2, model.add_2);
        values.put(ADD_3, model.add_3);
        values.put(SUBURB, model.suburb);
        values.put(POSTCODE, model.postcode);
        values.put(TEL_1, model.tel_1);
        values.put(TEL_2, model.tel_2);
        values.put(FAX, model.fax);
        values.put(EMAIL, model.email);
        values.put(DEFAULT_SHIPPING_ADD, model.default_shipping_add);
        values.put(DEFAULT_BILLING_ADD, model.default_billing_add);
        values.put(ACT, model.act);
        values.put(STATUS, model.status);
        values.put(CREATED_DATE, model.created_date);
        values.put(UPDATE_DATE, model.update_date);
        values.put(UPDATED_BY, model.updated_by);
        values.put(COUNTRY, model.country);
        values.put(STATE, model.state);

        return values;
    }

    //*****************Content Values for GetCategoties*************
    private ContentValues populateGetCategoties(Model_GetCategories model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_GETCATEGORIES, model.prefix_name);
        values.put(CAT_ID, model.id);
        values.put(CATETORY_NAME, model.category_name);
        values.put(FULLNAME, model.fullname);
        values.put(PARENT_CAT_ID, model.parent_cat_id);
        values.put(ROOT_CAT_ID, model.root_cat_id);
        values.put(LEVEL, model.level);
        values.put(SORT, model.sort);
        values.put(CAT_STATUS, model.status);
        values.put(CAT_CREATED_DATE, model.created_date);
        values.put(CAT_UPDATE_DATE, model.update_date);
        values.put(CAT_UPDATED_BY, model.updated_by);
        values.put(CAT_SID, model.sid);
        values.put(CAT_TYPE, model.type);
        values.put(VIEW_TYPE, model.view_type);
        values.put(LAST_SYNC_ID, model.last_sync_id);


        return values;
    }

    //*****************Content Values for GET-PAGE*************
    private ContentValues populateGetPage(Model_GetPage model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_GETPAGE, model.prefix_name);
        values.put(PAGE_ID, model.id);
        values.put(PAGE_URL, model.page_url);
        values.put(PAGE_STATUS, model.status);


        return values;
    }

    //*****************Content Values for Get Products*************
    private ContentValues populategetproduct(Model_GetProduct model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_GET_PRODUCTS, model.prefix_name);
        values.put(ORDER_UNIT_NAME, model.order_unit_name);
        values.put(BASE_UNIT_NAME, model.base_unit_name);
        values.put(PRODUCT_ID, model.id);
        values.put(SINK_ID, model.sink_id);
        values.put(PRODUCT_CAT_ID, model.cat_id);
        values.put(PRODUCT_CODE, model.product_code);
        values.put(NAME, model.name);
        values.put(DESC, model.desc);
        values.put(PRODUCT_STATUS, model.status);
        values.put(COST_PRICE, model.cost_price);
        values.put(MIN_ORDER_QUT, model.min_order_qut);
        values.put(MAX_ORDER_QUT, model.max_order_qut);
        values.put(TAXABLE, model.taxable);
        values.put(BASE_UNIT_ID, model.base_unit_id);
        values.put(UNIT_PRICE, model.unit_price);
        values.put(ORDER_UNIT_ID, model.order_unit_id);
        values.put(INVENTORY, model.inventory);
        values.put(SPCL_PRICE, model.spcl_price);
        values.put(WAREHOUSE_NAME, model.warehouse_name);
        values.put(WAREHOUSE_LOC, model.warehouse_loc);
        values.put(STOCK_LOC, model.stock_loc);
        values.put(PRODUCT_CREATED_DATE, model.created_date);
        values.put(PRODUCT_UPDATE_DATE, model.update_date);
        values.put(PRODUCT_UPDATED_BY, model.updated_by);
        values.put(TYPE, model.type);
        values.put(TIER_PRICE, model.tier_price);
        values.put(VIEW_TYPE, model.view_type);
        values.put(PRODUCT_LAST_SYNC_ID, model.last_sync_id);
        values.put(IMAGE, model.img);
        values.put(THUMB_IMAGE, model.thumb_image);

        return values;
    }

    //*****************Content Values for GetProductImage*************
    private ContentValues populateGetProductImage(Model_Product_Image model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_PRODUCT_IMAGE, model.prefix_name);
        values.put(IMAGE_ID, model.id);
        values.put(IMAGE_SINK_ID, model.sink_id);
        values.put(IMAGE_PRODUCT_ID, model.product_id);
        values.put(IMAGE, model.image);
        values.put(THUMB_IMAGE, model.thumb_image);
        values.put(IMAGE_STATUS, model.status);
        values.put(IS_MAIN, model.is_main);
        values.put(IMAGE_TYPE, model.type);
        values.put(IMAGE_CREATED_DATE, model.created_date);
        values.put(IMAGE_UPDATE_DATE, model.update_date);
        values.put(IMAGE_UPDATED_BY, model.updated_by);
        values.put(IMAGE_LAST_SYNC_ID, model.last_sync_id);


        return values;
    }


    //*****************Content Values for GetProductInventory*************
    private ContentValues populateGetProductInventory(Model_Product_Inventory model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_PRODUCT_INVENTORY, model.prefix_name);
        values.put(INVENTORY_ID, model.id);
        values.put(INVENTORY_PRODUCT_ID, model.product_id);
        values.put(INVENTORY_SINK_ID, model.sink_id);
        values.put(AVAL_STOCK, model.aval_stock);
        values.put(RESERVE_QUT, model.reserve_qut);
        values.put(BCK_ORD_AVAL, model.bck_ord_aval);
        values.put(INVENTORY_STATUS, model.status);
        values.put(INVENTORY_WAREHOUSE_NAME, model.warehouse_name);
        values.put(INVENTORY_WAREHOUSE_LOCATION, model.warehouse_location);
        values.put(STOCK_LOCATION, model.stock_location);
        values.put(INVENTORY_CREATED_DATE, model.created_date);
        values.put(INVENTORY_UPDATE_DATE, model.update_date);
        values.put(INVENTORY_UPDATE_BY, model.updated_by);
        values.put(INVENTORY_TYPE, model.type);
        values.put(INVENTORY_LAST_SYNC_ID, model.last_sync_id);


        return values;
    }

    //*****************Content Values for GetTierPricing*************
    private ContentValues populatetierpricing(Model_Tier_pricing model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_TIER_PRICING, model.prefix_name);
        values.put(TIER_ID, model.id);
        values.put(TIER_PRODUCT_ID, model.product_id);
        values.put(CUSTOMER_GROUP_ID, model.customer_group_id);
        values.put(QUANTITY_UPTO, model.quantity_upto);
        values.put(TIER_DISCOUNT_TYPE, model.discount_type);
        values.put(TIER_DISCOUNT, model.discount);
        values.put(TIER_CREATED_DATE, model.created_date);
        values.put(TIER_UPDATE_DATE, model.update_date);
        values.put(TIER_UPDATE_BY, model.updated_by);
        values.put(TIER_SINK_ID, model.sink_id);
        values.put(TIER_TYEPE, model.type);
        values.put(TIER_LAST_SYNC_ID, model.last_sync_id);


        return values;
    }


    //*****************Content Values for ORDER INFO*************
    private ContentValues populateorderinfo(Model_Order_Info model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_ORDER_INFO, model.prefix_name);
        values.put(ORDER_ID, model.order_id);
        values.put(CUSTOMER_ID, model.customer_id);
        values.put(BILLING_COUNTRY, model.billing_country);
        values.put(BILLING_STATE, model.billing_state);
        values.put(BILLING_SUBURB, model.billing_suburb);
        values.put(BILLING_ADD_1, model.billing_add_1);
        values.put(BILLING_ADD_2, model.billing_add_2);
        values.put(BILLING_ADD_3, model.billing_add_3);
        values.put(BILLING_POSTCODE, model.billing_postcode);
        values.put(SHIPPING_COUNTRY, model.shipping_country);
        values.put(SHIPPING_STATE, model.shipping_state);
        values.put(SHIPPING_SUBURB, model.shipping_suburb);
        values.put(SHIPPING_ADD_1, model.shipping_add_1);
        values.put(SHIPPING_ADD_2, model.shipping_add_2);
        values.put(SHIPPING_ADD_3, model.shipping_add_3);
        values.put(ORDER_SHIPPING_POSTCODE, model.shipping_postcode);
        values.put(ORDER_SHIPPING_COST, model.shipping_cost);
        values.put(SUB_TOTAL, model.sub_total);
        values.put(ORDER_TAX_TOTAL, model.tax_total);
        values.put(ORDER_TOTAL, model.total);
        values.put(ORDER_NOTES, model.order_notes);
        values.put(INTERNAL_NOTES, model.internal_notes);
        values.put(ORDER_STATUS, model.order_status);
        values.put(BACK_ORDER, model.back_order);
        values.put(MAIN_ORDER_ID, model.main_order_id);
        values.put(DELIVERY_DATE, model.delivery_date);
        values.put(DELIVERY_NOTES, model.delivery_notes);
        values.put(ORDER_CREATED_DATE, model.created_date);
        values.put(ORDER_CREATED_BY, model.created_by);
        values.put(ORDER_UPDATED_DATE, model.update_date);
        values.put(ORDER_UPDATED_BY, model.updated_by);
        values.put(ORDER_NAME, model.order_name);
        values.put(ORDER_BY, model.order_by);
        values.put(ORDER_SINK_ID, model.sink_id);
        values.put(ORDER_TYPE, model.type);
        values.put(SHOW_DELIVERYDATE_ORDER, model.show_deliverydate_order);
        values.put(MANDETORY_DELIVERY_ORDER, model.mandetory_delivery_order);
        values.put(ORDER_LAST_SYNC_ID, model.last_sync_id);
        return values;
    }

    //*****************Content Values for Get ORDER PRODUCT*************
    private ContentValues populateOrderProduct(Model_Order_Product model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_ORDER_PRODUCTS, model.prefix_name);
        values.put(ORDER_PRODUCT_ID, model.id);
        values.put(PRODUCT_ORDER_ID, model.order_id);
        values.put(PRODUCT_PRODUCT_ID, model.product_id);
        values.put(PRODUCT_NAME, model.product_name);
        values.put(ORDER_PRODUCT_CODE, model.product_code);
        values.put(QTY, model.qty);
        values.put(QTY_NEW_ORDER, model.qty_new_order);
        values.put(ACT_QTY, model.act_qty);
        values.put(PRODUCT_UNIT_PRICE, model.unit_price);
        values.put(APL_PRICE, model.applied_price);
        values.put(PRODUCT_SUB_TOTAL, model.sub_total);
        values.put(PRODUCT_SINK_ID, model.sink_id);
        values.put(PRODUCT_TYPE, model.type);
        values.put(ORDER_PRODUCT_CREATED_DATE, model.created_date);
        values.put(ORDER_PRODUCT_UPDATE_DATE, model.update_date);
        values.put(ORDER_PRODUCT_UPDATED_BY, model.updated_by);
        values.put(ORDER_PRODUCT_LAST_SYNC_ID, model.last_sync_id);


        return values;
    }

    //*****************Content Values for Get ORDER TEMPLATE*************

    private ContentValues populateOrderTemplate(Model_Template model) {

        ContentValues values = new ContentValues();
        values.put(PREFIX_ORDER_TEMPLATE, model.prefix_name);
        values.put(ORDER_TEMPLATE_ID, model.order_template_id);
        values.put(TEMPLATE_ORDER_ID, model.customer_id);
        values.put(TEMPLATE_NAME, model.template_name);
        values.put(TEMPLATE_CREATED_DATE, model.created_date);
        values.put(TEMPLATE_CREATED_BY, model.created_by);
        values.put(TEMPLATE_SINK_ID, model.sink_id);
        values.put(TEMPLATE_TYPE, model.type);
        values.put(TEMPLATE_PRODUCT_ID, model.product_id);
        values.put(TEMPLATE_PRODUCT_CODE, model.product_code);
        values.put(TEMPLATE_QTY, model.qty);
        values.put(TEMPLATE_LAST_SYNC_ID, model.last_sync_id);


        return values;
    }

    //**********************INSERT into Login Table*****************
    public void createLogin(ModelLogin model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateLogin(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_LOGIN, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//**********************UPDATE into Login Table*****************
    public void updateLogin(ModelLogin model, String prefix) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }




        ContentValues values = new ContentValues();


        values.put(CURRENCY_SYMBOL, model.currency_symbol);
        values.put(SHIPPING_STATUS, model.shipping_status);
        values.put(SHIPPING_COST, model.shipping_cost);
        values.put(TAX_LABEL, model.tax_label);
        values.put(TAX_PERCENTAGE, model.tax_percentage);


        dataBaseHelper.getWritableDatabase().update(TABLE_LOGIN, values,  PREFIX_NAME + "='" + prefix +"'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ModelLogin> getLogin() {

        ArrayList<ModelLogin> arr = new ArrayList<ModelLogin>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_LOGIN;

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {

                ModelLogin login = new ModelLogin();
                login.prefix_name = c.getString(c.getColumnIndex(PREFIX_NAME));
                login.auth_code = c.getString(c.getColumnIndex(AUTH_CODE));


                arr.add(login);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }

    //**********************INSERT into getcustomer Table*****************
    public void getcustomer(ModelgetCustomer model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populategetCustomer(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_GETCUSTOMER, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public void updatecustomer(ModelgetCustomer model) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();

        values.put(BUSINESS_NAME, model.business_name);
        values.put(BUSINESS_ABN, model.business_abn);
        values.put(BILLING_ADD_ID, model.billing_add_id);
        values.put(DEFAULT_SHIPPING_ADD_ID, model.default_shipping_add_id);
        values.put(EXTERNAL_RECORD_ID, model.external_record_id);
        values.put(CREDIT_LIMIT, model.credit_limit);
        values.put(CUSTOMER_GROUP_NAME, model.customer_group_name);
        dataBaseHelper.getWritableDatabase().update(TABLE_GETCUSTOMER, values, "prefix_name ='" + model.prefix_name + "' AND ws_users_id='" + model.ws_users_id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }
    //**********************INSERT into add_detail_customer Table*****************

    public void add_detail_customer(Model_Add_Detail_Customer model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateAdd_detail_Customer(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_ADD_DETAIL_CUSTOMER, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public void update_detail_customer(Model_Add_Detail_Customer model) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(WS_USERS_ID, model.ws_users_id);
        values.put(FIRST_NAME, model.first_name);
        values.put(LAST_NAME, model.last_name);
        values.put(ADD_1, model.add_1);
        values.put(ADD_2, model.add_2);
        values.put(ADD_3, model.add_3);
        values.put(SUBURB, model.suburb);
        values.put(POSTCODE, model.postcode);
        values.put(TEL_1, model.tel_1);
        values.put(TEL_2, model.tel_2);
        values.put(FAX, model.fax);
        values.put(EMAIL, model.email);
        values.put(DEFAULT_SHIPPING_ADD, model.default_shipping_add);
        values.put(DEFAULT_BILLING_ADD, model.default_billing_add);
        values.put(STATUS, model.status);
        values.put(CREATED_DATE, model.created_date);
        values.put(UPDATE_DATE, model.update_date);
        values.put(UPDATED_BY, model.updated_by);
        values.put(COUNTRY, model.country);
        values.put(STATE, model.state);
        values.put(ACT, model.act);
        dataBaseHelper.getWritableDatabase().update(TABLE_ADD_DETAIL_CUSTOMER, values, "prefix_name ='" + model.prefix_name + "' AND id='" + model.id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }
    //**********************INSERT into Getcategories Table*****************

    public void getcategories(Model_GetCategories model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateGetCategoties(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_GETCATEGORIES, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    //**********************INSERT into GetPage Table*****************

    public void getpage(Model_GetPage model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateGetPage(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_GETPAGE, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public void updatepage(Model_GetPage model) {

        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            // values.put(PAGE_ID, model.id);
            values.put(PAGE_URL, model.page_url);
            values.put(PAGE_STATUS, model.status);
            dataBaseHelper.getWritableDatabase().update(TABLE_GETPAGE, values, "prefix_name ='" + model.prefix_name + "' AND id='" + model.id + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    //**********************INSERT into GetProducts Table*****************

    public void getproduct(Model_GetProduct model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populategetproduct(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_GET_PRODUCTS, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    //**********************INSERT into GetProducts Table*****************

    public void getproductImage(Model_Product_Image model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateGetProductImage(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_GET_PRODUCT_IMAGE, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    //**********************INSERT into GetProducts Inventory*****************


    public void getproductInventory(Model_Product_Inventory model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateGetProductInventory(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_GET_PRODUCT_INVENTORY, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }
    //**********************INSERT into GetTier Pricing*****************

    public void gettierpricing(Model_Tier_pricing model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populatetierpricing(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_TIER_PRICING, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }
    //**********************INSERT into Order Info*****************

    public void getorderinfo(Model_Order_Info model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateorderinfo(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_ORDER_INFO, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }
    //**********************INSERT into Order Product*****************

    public void getorderproduct(Model_Order_Product model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateOrderProduct(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_ORDER_PRODUCT, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    //**********************INSERT into Order TEMPLATE*****************

    public void gettemplate(Model_Template model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateOrderTemplate(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_ORDER_TEMPLATE, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //**************************Get Get Categories***********************
    public String Getlastsync_id(String table, String prefix) throws Exception {
        String id = "0";
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + table + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                id = c.getString(c.getColumnIndex("last_sync_id"));
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }



    //**************************s Teplate present or not***********************
    public boolean isTemplateExist(String prefix, String temp_id) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_ORDER_TEMPLATE + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(ORDER_TEMPLATE_ID)).equals(temp_id)){
                        id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }



    //**************************Is order info present or not***********************
    public boolean isorderExist(String prefix, Model_Order_Info model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_ORDER_INFO + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(ORDER_ID)).equals(model.order_id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }


    //**************************Is order product present or not***********************
    public boolean isorderproductExist(String prefix, Model_Order_Product model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_ORDER_PRODUCT + " WHERE prefix_name='" + prefix + "' AND "+PRODUCT_ORDER_ID +"= "+model.order_id;

            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(PRODUCT_PRODUCT_ID)).equals(model.product_id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }

//**************************Is order product present or not***********************
    public boolean iscustomerExist(String prefix, ModelgetCustomer model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GETCUSTOMER + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(USER_ID)).equals(model.ws_users_id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }
//**************************Is order product present or not***********************
    public boolean isdetailcustomerExist(String prefix, Model_Add_Detail_Customer model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_ADD_DETAIL_CUSTOMER + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(WS_USERS_ID)).equals(model.ws_users_id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }

//**************************Is order product present or not***********************
    public boolean iscustomerdetailExist(String prefix, Model_Add_Detail_Customer model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_ADD_DETAIL_CUSTOMER + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(ID)).equals(model.id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }

//**************************Is order product present or not***********************
    public boolean isgetcategorisExist(String prefix, Model_GetCategories model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GETCATEGORIES + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(CAT_ID)).equals(model.id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }

//**************************Is order product present or not***********************
    public boolean isgetpageExist(String prefix, Model_GetPage model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GETPAGE + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(PAGE_ID)).equals(model.id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }

//**************************Is order product present or not***********************
    public boolean isgetproductExist(String prefix, Model_GetProduct model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(PRODUCT_ID)).equals(model.id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }
//**************************Is  order product status is active or not***********************
    public boolean isgetproductActive( String id) throws Exception {
        boolean id1 = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS  + " WHERE " + PRODUCT_ID + " = '" + id + "' AND " + PRODUCT_STATUS + "= 'Active'";
            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(PRODUCT_ID)).equals(id)){
                    id1= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id1;
    }

//**************************Is order product present or not***********************
    public boolean isgetproductImageExist(String prefix, Model_Product_Image model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GET_PRODUCT_IMAGE + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(IMAGE_ID)).equals(model.id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }

//**************************Is order product present or not***********************
    public boolean isgetproductInventExist(String prefix, Model_Product_Inventory model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GET_PRODUCT_INVENTORY + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(INVENTORY_ID)).equals(model.id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return id;
    }

//**************************Is order product present or not***********************
    public boolean isgetproducttierExist(String prefix, Model_Tier_pricing model) throws Exception {
        boolean id = false;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_TIER_PRICING + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                if(c.getString(c.getColumnIndex(TIER_ID)).equals(model.id)){
                    id= true;
                }
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
             e.printStackTrace();
        }
        return id;
    }


    //*****************Get Values from GetCatagories************************************************
/*
    public ArrayList<Model_GetCategories> Get_Product_Catagory(String prefix, String parent_cat_id) {

        ArrayList<Model_GetCategories> arr = new ArrayList<Model_GetCategories>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GETCATEGORIES + " WHERE " + PREFIX_GETCATEGORIES + " = '" + prefix + "'" + " AND " + PARENT_CAT_ID + " = '" + parent_cat_id + "'";

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {

                Model_GetCategories modelGetCategories = new Model_GetCategories();
                modelGetCategories.id = c.getString(c.getColumnIndex("id"));
                modelGetCategories.category_name = c.getString(c.getColumnIndex("category_name"));
                modelGetCategories.parent_cat_id = c.getString(c.getColumnIndex("parent_cat_id"));
                modelGetCategories.root_cat_id = c.getString(c.getColumnIndex("root_cat_id"));
                modelGetCategories.level = c.getString(c.getColumnIndex("level"));
                modelGetCategories.sort = c.getString(c.getColumnIndex("sort"));
                modelGetCategories.status = c.getString(c.getColumnIndex("status"));
                arr.add(modelGetCategories);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }*/
    public ArrayList<Model_Cat_Prod> Get_Prod_Cat(String prefix, String parent_cat_id) {

        ArrayList<Model_Cat_Prod> arr = new ArrayList<Model_Cat_Prod>();

        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GETCATEGORIES + " WHERE " + PREFIX_GETCATEGORIES + " = '" + prefix + "'" + " AND " + PARENT_CAT_ID + " = '" + parent_cat_id + "' AND " + CAT_STATUS + " = 'Active'"+ " ORDER BY LOWER(" + CATETORY_NAME + ")";

            Cursor c = db.rawQuery(select, null);
            int cantt = c.getCount();
            if (cantt > 0) {
                Model_Cat_Prod pmodel = new Model_Cat_Prod();
                pmodel.view_type = "CatHeader";
                arr.add(pmodel);
            }
            while (c.moveToNext()) {

                Model_Cat_Prod modelGetCategories = new Model_Cat_Prod();
                modelGetCategories.only_cat_id = c.getString(c.getColumnIndex("id"));
                modelGetCategories.fullname = c.getString(c.getColumnIndex("fullname"));
                modelGetCategories.category_name = c.getString(c.getColumnIndex("category_name"));
                modelGetCategories.parent_cat_id = c.getString(c.getColumnIndex("parent_cat_id"));
                modelGetCategories.root_cat_id = c.getString(c.getColumnIndex("root_cat_id"));
                modelGetCategories.level = c.getString(c.getColumnIndex("level"));
                modelGetCategories.sort = c.getString(c.getColumnIndex("sort"));
                modelGetCategories.cat_status = c.getString(c.getColumnIndex("status"));
                modelGetCategories.view_type = c.getString(c.getColumnIndex("view_type"));
                arr.add(modelGetCategories);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PRODUCT_CAT_ID + " = '" + parent_cat_id + "' AND prefix_name ='" + prefix + "' AND " + PRODUCT_STATUS + "= 'Active'"+ " ORDER BY LOWER(" + NAME + ")";

            Cursor c = db.rawQuery(select, null);
            int cntt = c.getCount();
            if (cntt > 0) {
                Model_Cat_Prod pmodel = new Model_Cat_Prod();
                pmodel.view_type = "ProdHeader";
                arr.add(pmodel);
            }


            while (c.moveToNext()) {

                Model_Cat_Prod Product = new Model_Cat_Prod();
                Product.prefix_name = c.getString(c.getColumnIndex("prefix_name"));
                Product.order_unit_name = c.getString(c.getColumnIndex("order_unit_name"));
                Product.base_unit_name = c.getString(c.getColumnIndex("base_unit_name"));
                Product.prod_id = c.getString(c.getColumnIndex("id"));
                Product.sink_id = c.getString(c.getColumnIndex("sink_id"));
                Product.cat_id = c.getString(c.getColumnIndex("cat_id"));
                Product.prod_cat_name = getcatname(Product.cat_id, prefix, 0);
                Product.fullname = getcatname(Product.cat_id, prefix, 1);
                Product.product_code = c.getString(c.getColumnIndex("product_code"));
                Product.name = c.getString(c.getColumnIndex("name"));
                Product.desc = c.getString(c.getColumnIndex("desc"));
                Product.prod_status = c.getString(c.getColumnIndex("status"));
                Product.cost_price = c.getString(c.getColumnIndex("cost_price"));
                Product.min_order_qut = c.getString(c.getColumnIndex("min_order_qut"));
                Product.max_order_qut = c.getString(c.getColumnIndex("max_order_qut"));
                Product.taxable = c.getString(c.getColumnIndex("taxable"));
                Product.base_unit_id = c.getString(c.getColumnIndex("base_unit_id"));
                Product.unit_price = c.getString(c.getColumnIndex("unit_price"));
                Product.order_unit_id = c.getString(c.getColumnIndex("order_unit_id"));
                Product.view_type = c.getString(c.getColumnIndex("view_type"));

                //******************gettng now image***********************************************
                Product.img=c.getString(c.getColumnIndex("image"));
              // Product.thumb_img=c.getString(c.getColumnIndex("thumb_image"));


               Product.thumb_image = c.getString(c.getColumnIndex("thumb_image"));
                //   Product.largeimg = getimage(Product.prod_id,prefix);

                Product.inventory = c.getString(c.getColumnIndex("inventory"));
                if (Product.inventory.equals("1")) {
                    Product.avail_stock = getinvent(Product.prod_id, prefix);
                } else {
                    Product.avail_stock = "No Limits";
                }

                Product.spcl_price = c.getString(c.getColumnIndex("spcl_price"));
                Product.warehouse_name = c.getString(c.getColumnIndex("warehouse_name"));
                Product.warehouse_loc = c.getString(c.getColumnIndex("warehouse_loc"));
                Product.stock_loc = c.getString(c.getColumnIndex("stock_loc"));
                Product.prod_created_date = c.getString(c.getColumnIndex("created_date"));
                Product.prod_update_date = c.getString(c.getColumnIndex("update_date"));
                Product.prod_updated_by = c.getString(c.getColumnIndex("updated_by"));
                Product.prod_type = c.getString(c.getColumnIndex("type"));
                Product.tier_price = c.getString(c.getColumnIndex("tier_price"));
                arr.add(Product);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


        return arr;
    }
    //*****************Get Values from Get_Product_id on the basis of ID************************************************

    public ArrayList<Model_GetProduct> Get_Product_id(String prefix) {

        ArrayList<Model_GetProduct> arr = new ArrayList<Model_GetProduct>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PREFIX_GET_PRODUCTS + " = '" + prefix + "' AND " + PRODUCT_STATUS + " ='Active'";

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {

                Model_GetProduct Product = new Model_GetProduct();
                Product.prefix_name = c.getString(c.getColumnIndex("prefix_name"));
                Product.order_unit_name = c.getString(c.getColumnIndex("order_unit_name"));
                Product.base_unit_name = c.getString(c.getColumnIndex("base_unit_name"));
                Product.id = c.getString(c.getColumnIndex("id"));
                Product.sink_id = c.getString(c.getColumnIndex("sink_id"));
                Product.cat_id = c.getString(c.getColumnIndex("cat_id"));
                Product.cat_name = getcatname(Product.cat_id, prefix, 0);
                Product.fullname = getcatname(Product.cat_id, prefix, 1);
                Product.product_code = c.getString(c.getColumnIndex("product_code"));
                Product.name = c.getString(c.getColumnIndex("name"));
                Product.desc = c.getString(c.getColumnIndex("desc"));
                Product.status = c.getString(c.getColumnIndex("status"));
                Product.cost_price = c.getString(c.getColumnIndex("cost_price"));
                Product.min_order_qut = c.getString(c.getColumnIndex("min_order_qut"));
                Product.max_order_qut = c.getString(c.getColumnIndex("max_order_qut"));
                Product.taxable = c.getString(c.getColumnIndex("taxable"));
                Product.base_unit_id = c.getString(c.getColumnIndex("base_unit_id"));
                Product.unit_price = c.getString(c.getColumnIndex("unit_price"));
                Product.order_unit_id = c.getString(c.getColumnIndex("order_unit_id"));
                Product.view_type = c.getString(c.getColumnIndex("view_type"));

                Product.img=c.getString(c.getColumnIndex("image"));
                Product.thumb_image=c.getString(c.getColumnIndex("thumb_image"));
                //Product.img = getimage3(Product.id, prefix, "image");
              //  Product.img = "SELECT * FROM " + TABLE_GET_PRODUCT_IMAGE + " WHERE " + IMAGE_PRODUCT_ID + " = '" +   Product.id + "' AND " + PREFIX_GET_PRODUCTS + "='" + prefix + "'";

                Product.inventory = c.getString(c.getColumnIndex("inventory"));
                if (Product.inventory.equals("1")) {
                    Product.avail_stock = getinvent(Product.id, prefix);
                } else {
                    Product.avail_stock = "No Limits";
                }

                Product.spcl_price = c.getString(c.getColumnIndex("spcl_price"));
                Product.warehouse_name = c.getString(c.getColumnIndex("warehouse_name"));
                Product.warehouse_loc = c.getString(c.getColumnIndex("warehouse_loc"));
                Product.stock_loc = c.getString(c.getColumnIndex("stock_loc"));
                Product.created_date = c.getString(c.getColumnIndex("created_date"));
                Product.update_date = c.getString(c.getColumnIndex("update_date"));
                Product.updated_by = c.getString(c.getColumnIndex("updated_by"));
                Product.type = c.getString(c.getColumnIndex("type"));
                Product.tier_price = c.getString(c.getColumnIndex("tier_price"));
                arr.add(Product);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }


    //**************************Get Get values***********************
    public Model_Add_Detail_Customer Getvalues(String table, String prefix) throws Exception {
        String fax, phone;
        Model_Add_Detail_Customer customer = new Model_Add_Detail_Customer();
        try {
            dataBaseHelper.createDataBase();


            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + table + " WHERE prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                customer.fax = c.getString(c.getColumnIndex("fax"));
                customer.tel_1 = c.getString(c.getColumnIndex("tel_1"));
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return customer;

    }

    //**************************Get GetTemplate_name***********************
    public ArrayList<String> GetTemplate_name(String prefix) throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //  e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_ORDER_TEMPLATE + " WHERE prefix_name='" + prefix + "'";
        Cursor c = db.rawQuery(select, null);
        ArrayList<String> arr = new ArrayList<String>();

        while (c.moveToNext()) {

            arr.add(populateItem(c, "template_name"));

        }
        try {
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }

    //**************************Get Get Image***********************
    public ArrayList<Model_Order_Product> GetProductWimage(String table, String id, String prefix) throws Exception {
        ArrayList<Model_Order_Product> arr = new ArrayList<Model_Order_Product>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + table + " WHERE " + PRODUCT_ORDER_ID + " = '" + id + "' AND prefix_name ='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {
                Model_Order_Product product = new Model_Order_Product();
                product.order_id = id;
                product.product_id = c.getString(c.getColumnIndex("product_id"));
                product.product_name = c.getString(c.getColumnIndex("product_name"));
                product.product_code = c.getString(c.getColumnIndex("product_code"));


                //product.image=c.getString(c.getColumnIndex("image"));
               // product.image = "SELECT * FROM " + TABLE_GET_PRODUCT_IMAGE + " WHERE " + IMAGE_PRODUCT_ID + " = '" +   product.product_id + "' AND " + PREFIX_GET_PRODUCTS + "='" + prefix + "'";
              product.image = getimage3(product.product_id, prefix, "image");//caling getimage function
              product.thumb_image =getimage2(product.product_id, prefix, "thumb_image");//caling getimage function

                product.unit_price = c.getString(c.getColumnIndex("unit_price"));
                product.qty = c.getString(c.getColumnIndex("qty"));
                product.act_qty = c.getString(c.getColumnIndex("act_qty"));
                product.sub_total = c.getString(c.getColumnIndex("sub_total"));
                product.applied_price = c.getString(c.getColumnIndex("applied_price"));
                product.taxable = getminmax(product.product_id, "taxable", prefix);
                product.min_qty = getminmax(product.product_id, "min_order_qut", prefix);
                product.max_qty = getminmax(product.product_id, "max_order_qut", prefix);
                product.order_unit = getminmax(product.product_id, "order_unit_name", prefix);
                product.base_unit = getminmax(product.product_id, "base_unit_name", prefix);
                product.tier_price = getminmax(product.product_id, "tier_price", prefix);
                product.spcl_price = getminmax(product.product_id, "spcl_price", prefix);
                product.inventory = getminmax(product.product_id, "inventory", prefix);


                arr.add(product);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }

    private String getminmax(String product_id, String column, String prefix) {
        String value = "";
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PRODUCT_ID + " = '" + product_id + "' AND prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                value = c.getString(c.getColumnIndex(column));

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return value;
    }

    //***************Function for getting Image****
    String getimage2(String product_id, String prefix, String column) throws Exception {
        System.out.println("stattime: "+System.currentTimeMillis());
        long st=System.currentTimeMillis();
        //   String img = "https://appsglory.com.au/uploads/default-app.png";
        String img = "default-app.png";

        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PRODUCT_ID + " = '" + product_id + "' AND " + PREFIX_GET_PRODUCTS + "='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                img = c.getString(c.getColumnIndex(column));
                String[] strArray = img.split("\\.");
                if (strArray.length <= 1) {
                    img = "default-app.png";
                    System.out.println("default-app.png");
                }


            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        long en=System.currentTimeMillis();
        System.out.println("Timetaken: "+(en-st));
        return img;
    }
    //***************Function for getting Image****
    String getimage3(String product_id, String prefix, String column) throws Exception {
        System.out.println("stattime: "+System.currentTimeMillis());
        long st=System.currentTimeMillis();
        //   String img = "https://appsglory.com.au/uploads/default-app.png";
        String img = "default-app.png";

        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT IMAGE FROM " + TABLE_GET_PRODUCTS + " WHERE " + PRODUCT_ID + " = '" + product_id + "' AND " + PREFIX_GET_PRODUCTS + "='" + prefix + "'";
            Cursor  cursor = db.rawQuery(select,null);

            if (cursor != null) {
                cursor.moveToFirst();
            }
            // return cursor;
            //Cursor c = db.rawQuery(select, null);
            img = cursor.getString(cursor.getColumnIndex(column));
           // img = "default-app.png";
            System.out.println("img: "+img);
//            while (c.moveToNext()) {
//
//                img = c.getString(c.getColumnIndex(column));
//                System.out.println("Imagename: "+img);
//                String[] strArray = img.split("\\.");
//                if (strArray.length <= 1) {
//                    img = "default-app.png";
//                  //  System.out.println("default-app.png");
//                }
//
//
//            }
            dataBaseHelper.close();
            db.close();
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        long en=System.currentTimeMillis();
        System.out.println("Timetaken: "+(en-st));
        return img;
    }

    //***************Function for getting category****
    String getcatname(String cat_id, String prefix, int type) throws Exception {
        String name = null;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GETCATEGORIES + " WHERE " + PREFIX_GETCATEGORIES + " = '" + prefix + "' AND id ='" + cat_id + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {
                if (type == 0) {
                    name = c.getString(c.getColumnIndex("category_name"));
                } else {
                    name = c.getString(c.getColumnIndex("fullname"));
                }


            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return name;
    }


    //***************Function for getting Inventory****
    public String getinvent(String product_id, String prefix) {
        float avail = 0, reserve = 0;
        String avail_stock = null;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GET_PRODUCT_INVENTORY + " WHERE " + INVENTORY_PRODUCT_ID + " = '" + product_id + "' AND prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);

            if (c != null) {
                while (c.moveToNext()) {

                    String availstock = c.getString(c.getColumnIndex("aval_stock"));
                    availstock.replaceAll(" ", "");
                    if (availstock != null || !availstock.equals("")
                            || availstock.length() != 0) {
                        try {
                            avail = Float.parseFloat(availstock);
                        } catch (Exception e) {
                            avail = 0;
                        }
                    }else{

                        avail_stock ="No Limits";
                    }
                    String resrvstock = c.getString(c.getColumnIndex("reserve_qut"));
                    resrvstock.replaceAll(" ", "");
                    if (resrvstock != null || !resrvstock.equals("")
                            || resrvstock.length() != 0) {
                        try {
                            reserve = Float.parseFloat(resrvstock);
                        } catch (Exception e) {
                            reserve = 0;
                        }
                    }
                    float av = avail - reserve;
                    av = Utils.round(av, 2);
                    if (av <= 0) {
                        avail_stock = "0";
                    } else {
                        avail_stock = String.valueOf(av);
                    }
                }
            } else {
                avail_stock = "0";
            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
            avail_stock = "0";
        }
        return avail_stock;

    }

    //    //**************************Get current Remedials Ground***********************
//    public ArrayList<String> GetRemedialCurrentGround(String item) throws Exception {
//        try {
//            dataBaseHelper.createDataBase();
//        } catch (IOException e) {
//
//
//            // e.printStackTrace();
//        }
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//        String select = "SELECT * FROM " + TABLE_GROUND_REMEDIAL + " WHERE " + ITEM + " = '" + item + "'";
//
//        Cursor c = db.rawQuery(select, null);
//        ArrayList<String> arr = new ArrayList<String>();
//
//        while (c.moveToNext()) {
//
//            arr.add(populateItem(c, CURRENT_GROUND));
//
//        }
//        try {
//            dataBaseHelper.close();
//            db.close();
//            c.close();
//
//        } catch (Exception e) {
//            //   e.printStackTrace();
//        }
//        return arr;
//    }
//
    //**************************Get Data from Template and add to Model_Templates ***********************
    public ArrayList<Model_Template> GetTemplate(String prefix, String gettype) throws Exception {
        String select = "";
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {


            //  e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        if (gettype.equals("all")) {
            select = "SELECT * FROM " + TABLE_ORDER_TEMPLATE + " WHERE " + PREFIX_ORDER_TEMPLATE + "='" + prefix + "'";
        } else {
            select = "SELECT * FROM " + TABLE_ORDER_TEMPLATE + " WHERE " + PREFIX_ORDER_TEMPLATE + "='" + prefix + "' AND " + TEMPLATE_TYPE + "='newupdate'";
        }
        if(gettype.equals("off")){
            select = "SELECT * FROM " + TABLE_ORDER_TEMPLATE + " WHERE " + LAST_SYNC_ID + "='" + gettype + "' AND " + PREFIX_ORDER_TEMPLATE +  "='" + prefix + "'";
        }
        Cursor c = db.rawQuery(select, null);
        ArrayList<Model_Template> arr = new ArrayList<Model_Template>();

        while (c.moveToNext()) {

            Model_Template item = new Model_Template();

            item.order_template_id = populateItem(c, "order_template_id");
            item.type = populateItem(c, "type");
            item.customer_id = populateItem(c, "customer_id");
            item.template_name = populateItem(c, "template_name");
            item.product_code = populateItem(c, "product_code");
            item.product_id = populateItem(c, "product_id");
            item.qty = populateItem(c, "qty");
            arr.add(item);
        }
        try {
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
             e.printStackTrace();
        }


        Collections.sort(arr, new Comparator<Model_Template>() {
            @Override
            public int compare(Model_Template lhs, Model_Template rhs) {
                return lhs.template_name.compareToIgnoreCase(rhs.template_name);
            }

//                    public int compare(ModelStore v1, ModelStore v2) {
//                        //v1.brand_name.equalsIgnoreCase(v2.brand_name);
//                        return v1.header_name.compareToIgnoreCase(v2.brand_name);
//                    }
        });

        return arr;
    }

    //**************************Get Data from Template and add to Model_Templates ***********************
    public ArrayList<Model_GetProduct> GetTemplate_product(JSONArray qtyarray, JSONArray productarray, String prefix) {
        ArrayList<Model_GetProduct> arr = new ArrayList<Model_GetProduct>();


        try {
            for (int i = 0; i < productarray.length(); i++) {
                dataBaseHelper.createDataBase();
                SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

                String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PRODUCT_ID + " = '" + productarray.get(i).toString() + "' AND " + PREFIX_NAME + " = '" + prefix + "'";
                System.out.print("selct=" + select);
                Cursor c = db.rawQuery(select, null);


                while (c.moveToNext()) {


                    Model_GetProduct item = new Model_GetProduct();
                    item.id = populateItem(c, "id");
                    item.unit_price = populateItem(c, "unit_price");
                    item.inventory = populateItem(c, "inventory");
                    if (item.inventory.equals("1")) {
                        item.avail_stock = getinvent(item.id, prefix);
                    } else {
                        item.avail_stock = "No Limits";
                    }
                    item.min_order_qut = populateItem(c, "min_order_qut");
                    item.status = populateItem(c, "status");
                    item.max_order_qut = populateItem(c, "max_order_qut");
                    item.order_unit_id = populateItem(c, "order_unit_id");
                    item.base_unit_id = populateItem(c, "base_unit_id");
                    item.order_unit_name = populateItem(c, "order_unit_name");
                    item.base_unit_name = populateItem(c, "base_unit_name");
                    item.spcl_price = populateItem(c, "spcl_price");
                    item.taxable = populateItem(c, "taxable");
                    item.product_code = populateItem(c, "product_code");
                    item.name = populateItem(c, "name");
                    item.qty = qtyarray.get(i).toString();
                   item.img = getimage3(productarray.get(i).toString(), prefix, "image");
                   item.thumb_image = getimage2(productarray.get(i).toString(), prefix, "thumb_image");
                   // item.img = populateItem(c, "image");

                    arr.add(item);
                }


                dataBaseHelper.close();
                db.close();
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return arr;
    }

    //**************************Get Data from Login and add to Model_Login ***********************
    public ArrayList<ModelLogin> GetLogin_details() throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {


            //  e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

        //***********************Sorted Alphabetically by using ORDER BY LOWER**********************

        String select = "SELECT * FROM " + TABLE_LOGIN + " ORDER BY LOWER(" + COMPANY_NAME + ")";

        Cursor c = db.rawQuery(select, null);
        ArrayList<ModelLogin> arr = new ArrayList<ModelLogin>();

        while (c.moveToNext()) {

            ModelLogin item = new ModelLogin();
            item.company_name = populateItem(c, "company_name");
            item.prefix_name = populateItem(c, "prefix_name");

            item.auth_code = populateItem(c, "auth_code");

            item.company_logo = populateItem(c, "company_logo");
            item.currency_code = populateItem(c, "currency_code");
            item.currency_symbol = populateItem(c, "currency_symbol");
            item.shipping_status = populateItem(c, "shipping_status");
            item.shipping_cost = populateItem(c, "shipping_cost");
            item.tax_label = populateItem(c, "tax_label");
            item.message_count = populateItem(c, "message_count");
            item.tax_percentage = populateItem(c, "tax_percentage");
            item.customer_business = populateItem(c, "customer_business");
            arr.add(item);
        }
        try {
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }

    //**************************Get Data from Login and add to Model_Login FOR OTHER USER***********************
    public ArrayList<ModelLogin> GetLogin(String prefix) throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {


            //  e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_LOGIN + " WHERE " + PREFIX_NAME + " = '" + prefix + "'";

        Cursor c = db.rawQuery(select, null);
        ArrayList<ModelLogin> arr = new ArrayList<ModelLogin>();

        while (c.moveToNext()) {

            ModelLogin item = new ModelLogin();
            item.prefix_name = populateItem(c, "prefix_name");

            item.auth_code = populateItem(c, "auth_code");
            item.company_name = populateItem(c, "company_name");
            item.company_logo = populateItem(c, "company_logo");
            item.currency_code = populateItem(c, "currency_code");
            item.currency_symbol = populateItem(c, "currency_symbol");
            item.shipping_status = populateItem(c, "shipping_status");
            item.shipping_cost = populateItem(c, "shipping_cost");
            item.tax_label = populateItem(c, "tax_label");
            item.message_count = populateItem(c, "message_count");
            item.tax_percentage = populateItem(c, "tax_percentage");
            item.customer_business = populateItem(c, "customer_business");
            arr.add(item);
        }
        try {
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }

//**********************submitted order info where order status is submitted*********

    public String Select_submittedorder_id(String prefix) throws Exception {
        String order_name = null;
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {

            e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();


        String select = "SELECT * FROM " + TABLE_ORDER_INFO + " WHERE  prefix_name='" + prefix + "'";

        Cursor c = db.rawQuery(select, null);

        while (c.moveToNext()) {

            order_name = populateItem(c, ORDER_NAME);


        }


        try {
            dataBaseHelper.close();
            db.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return order_name;


    }
    //**********************submitted order info where order status is Cancelled*********

    public ArrayList<Model_Order_Info> Select_cancelorder_id(String prefix) throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {

            e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();


        String select = "SELECT * FROM " + TABLE_ORDER_INFO + " WHERE " + ORDER_STATUS + " = 'Cancelled' AND prefix_name='" + prefix + "'";

        Cursor c = db.rawQuery(select, null);
        ArrayList<Model_Order_Info> array = new ArrayList<Model_Order_Info>();
        while (c.moveToNext()) {
            Model_Order_Info info = new Model_Order_Info();
            info.total = populateItem(c, ORDER_TOTAL);
            info.order_id = populateItem(c, ORDER_ID);
            info.order_by = populateItem(c, ORDER_BY);
            info.order_name = populateItem(c, ORDER_NAME);
            info.order_status = populateItem(c, ORDER_STATUS);
            info.billing_suburb = populateItem(c, BILLING_SUBURB);
            info.billing_add_1 = populateItem(c, BILLING_ADD_1);
            info.billing_add_2 = populateItem(c, BILLING_ADD_2);
            info.billing_add_3 = populateItem(c, BILLING_ADD_3);
            info.billing_postcode = populateItem(c, BILLING_POSTCODE);
            info.billing_country = populateItem(c, BILLING_COUNTRY);
            info.billing_state = populateItem(c, BILLING_STATE);
            info.shipping_country = populateItem(c, SHIPPING_COUNTRY);
            info.shipping_state = populateItem(c, SHIPPING_STATE);
            info.delivery_date = populateItem(c, DELIVERY_DATE);
            info.delivery_notes = populateItem(c, DELIVERY_NOTES);
            info.order_notes = populateItem(c, ORDER_NOTES);
            info.shipping_add_1 = populateItem(c, SHIPPING_ADD_1);
            info.shipping_add_2 = populateItem(c, SHIPPING_ADD_2);
            info.shipping_add_3 = populateItem(c, SHIPPING_ADD_3);
            info.shipping_suburb = populateItem(c, SHIPPING_SUBURB);
            info.shipping_postcode = populateItem(c, ORDER_SHIPPING_POSTCODE);
            info.sub_total = populateItem(c, SUB_TOTAL);
            info.tax_total = populateItem(c, ORDER_TAX_TOTAL);
            info.shipping_cost = populateItem(c, SHIPPING_COST);
            info.show_deliverydate_order = populateItem(c, SHOW_DELIVERYDATE_ORDER);
            info.mandetory_delivery_order = populateItem(c, MANDETORY_DELIVERY_ORDER);
            array.add(info);
        }


        try {
            dataBaseHelper.close();
            db.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return array;


    }

    //--------------------Get all address from add_detil_customer table----------------
    public ArrayList<Model_Add_Detail_Customer> get_all_address(String prefix_name) throws Exception {
        ArrayList<Model_Add_Detail_Customer> array = new ArrayList<Model_Add_Detail_Customer>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();


            String select = "SELECT * FROM " + TABLE_ADD_DETAIL_CUSTOMER + " WHERE " + PREFIX_NAME + " = '" + prefix_name + "'";

            Cursor c = db.rawQuery(select, null);

            while (c.moveToNext()) {
                Model_Add_Detail_Customer info = new Model_Add_Detail_Customer();
                info.id = populateItem(c, ID);
                info.ws_users_id = populateItem(c, WS_USERS_ID);
                info.first_name = populateItem(c, FIRST_NAME);
                info.last_name = populateItem(c, LAST_NAME);
                info.add_1 = populateItem(c, ADD_1);
                info.add_2 = populateItem(c, ADD_2);
                info.add_3 = populateItem(c, ADD_3);
                info.suburb = populateItem(c, SUBURB);
                info.postcode = populateItem(c, POSTCODE);
                info.tel_1 = populateItem(c, TEL_1);
                info.tel_2 = populateItem(c, TEL_2);
                info.fax = populateItem(c, FAX);
                info.email = populateItem(c, EMAIL);
                info.act = populateItem(c, ACT);
//            info.default_shipping_add = populateItem(c, DEFAULT_SHIPPING_ADD);
//            info.default_billing_add = populateItem(c, DEFAULT_BILLING_ADD);
//            info.status = populateItem(c, STATUS);
//            info.created_date = populateItem(c, CREATED_DATE);
//            info.update_date = populateItem(c, UPDATE_DATE);
//            info.updated_by = populateItem(c, UPDATED_BY);
                info.country = populateItem(c, COUNTRY);
                info.state = populateItem(c, STATE);
//if(!c.getString(c.getColumnIndex(ID)).equals(info.id)){
                    array.add(info);
              //  }

            }


            dataBaseHelper.close();
            db.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return array;


    }

    //--------------------Get shipping add_id and billing add_id from getcustomer table----------------
    public ArrayList<ModelgetCustomer> getShippingAndBillingID(String prefix_name) throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {

            e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();


        String select = "SELECT * FROM " + TABLE_GETCUSTOMER + " WHERE " + PREFIX_NAME + " = '" + prefix_name + "'";

        Cursor c = db.rawQuery(select, null);
        ArrayList<ModelgetCustomer> array = new ArrayList<ModelgetCustomer>();
        while (c.moveToNext()) {

            ModelgetCustomer info = new ModelgetCustomer();
            info.billing_add_id = populateItem(c, BILLING_ADD_ID);
            info.default_shipping_add_id = populateItem(c, DEFAULT_SHIPPING_ADD_ID);
            array.add(info);
        }


        try {
            dataBaseHelper.close();
            db.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return array;


    }

    //**********************INSERT into add_to_cart Table*****************
    public void add_to_cart(Model_Add_To_Cart model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateAddToCart(model);
        dataBaseHelper.getWritableDatabase().insert(TABLE_ADD_TO_CART, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public ArrayList<String> GetCart() throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {

            e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();


        String select = "SELECT * FROM " + TABLE_ADD_TO_CART;
        //String select = "SELECT * FROM " + TABLE_ADD_TO_CART+ " WHERE " + ORDER_TEMPLATE_ID + " = '" + tempid;

        Cursor c = db.rawQuery(select, null);
        ArrayList<String> array = new ArrayList<>();
        while (c.moveToNext()) {


            array.add(populateItem(c, PROD_ID));
        }


        try {
            dataBaseHelper.close();
            db.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return array;


    }

    //*****************Content Values for add_to_cart*************
    private ContentValues populateAddToCart(Model_Add_To_Cart model) {

        ContentValues values = new ContentValues();
        values.put(PROD_ID, model.prod_id);
        values.put(PROD_NAME, model.prod_name);
        values.put(PROD_SKU, model.prod_sku);
        values.put(CART_QTY, model.qty);
        values.put(PRICE, model.price);
        values.put(CART_SPCL_PRICE, model.spcl_price);
        values.put(CART_MAX_QTY, model.max_qty);
        values.put(CART_MIN_QTY, model.min_qty);
        values.put(BASE_UNIT, model.base_unit);
        values.put(ORDER_UNIT, model.order_unit);
        values.put(CART_TAXABLE, model.taxable);
        values.put(PROD_INVENT, model.inventory);


        return values;
    }

    //**************************Get products added to CartFragment ***********************
    public ArrayList<Model_Add_To_Cart> GetProductFromCart(String prefix) throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {


            //  e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_ADD_TO_CART;

        Cursor c = db.rawQuery(select, null);
        ArrayList<Model_Add_To_Cart> arr = new ArrayList<Model_Add_To_Cart>();

        while (c.moveToNext()) {

            Model_Add_To_Cart item = new Model_Add_To_Cart();
            item.prod_id = populateItem(c, "prod_id");
            item.prod_name = populateItem(c, "prod_name");
            item.prod_sku = populateItem(c, "prod_sku");
            item.qty = populateItem(c, "qty");
            item.price = populateItem(c, "price");
            item.spcl_price = populateItem(c, "spcl_price");
            item.max_qty = populateItem(c, "max_qty");
            item.min_qty = populateItem(c, "min_qty");
            item.base_unit = populateItem(c, "base_unit");
            item.order_unit = populateItem(c, "order_unit");
            item.taxable = populateItem(c, "taxable");
            item.inventory = populateItem(c, "prod_inventory");
            item.prod_image = getimage2(item.prod_id, prefix, "thumb_image");
            //item.prod_image = getimage3(item.prod_id, prefix, "image");
            item.tier_price = gettierprice(item.prod_id, prefix);

            arr.add(item);
        }
        try {
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }

    private String gettierprice(String prod_id, String prefix) {

        String tier = null;
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_GET_PRODUCTS + " WHERE " + PRODUCT_ID + " = '" + prod_id + "' AND prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                tier = c.getString(c.getColumnIndex("tier_price"));

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        System.out.print("tier=" + tier);
        return tier;


    }

    //    **********************Get data from Order_info table***********************
    public int getallcount(String prefix, String type) {
        String countQuery = "";
        if (type.equals("all")) {
            countQuery = "SELECT  * FROM " + TABLE_ORDER_INFO + " WHERE " + PREFIX_ORDER_INFO + " = '" + prefix + "'";
        } else {
            countQuery = "SELECT  * FROM " + TABLE_ORDER_INFO + " WHERE " + PREFIX_ORDER_INFO + " = '" + prefix + "' AND " + ORDER_STATUS + " = '" + type + "'";

        }
        int cnt;
        try {
        // String countQuery = "SELECT  * FROM " + TABLE_ORDER_INFO ;
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cnt = cursor.getCount();
        cursor.close();
        } catch (Exception e) {
            cnt = 0;
        }
        return cnt;
    }

    //    **********************Get data from Order_info table***********************
    public int getcartcount(String countQuery) {
        int cnt;
            try {
            // String countQuery = "SELECT  * FROM " + TABLE_ORDER_INFO ;
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(countQuery, null);
            cnt = cursor.getCount();
            cursor.close();
        } catch (Exception e) {
            cnt = 0;
        }
        return cnt;
    }


    public ArrayList<Model_Order_Info> Select_orders(String select) throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {

            // e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();


        Cursor c = db.rawQuery(select, null);
        ArrayList<Model_Order_Info> array = new ArrayList<Model_Order_Info>();
        while (c.moveToNext()) {
            Model_Order_Info info = new Model_Order_Info();
            info.total = populateItem(c, ORDER_TOTAL);
            info.created_date = populateItem(c, ORDER_CREATED_DATE);
            info.order_id = populateItem(c, ORDER_ID);
            info.order_by = populateItem(c, ORDER_BY);
            info.order_name = populateItem(c, ORDER_NAME);
            info.order_status = populateItem(c, ORDER_STATUS);
            info.billing_suburb = populateItem(c, BILLING_SUBURB);
            info.billing_add_1 = populateItem(c, BILLING_ADD_1);
            info.billing_add_2 = populateItem(c, BILLING_ADD_2);
            info.billing_add_3 = populateItem(c, BILLING_ADD_3);
            info.billing_postcode = populateItem(c, BILLING_POSTCODE);
            info.billing_country = populateItem(c, BILLING_COUNTRY);
            info.billing_state = populateItem(c, BILLING_STATE);
            info.shipping_country = populateItem(c, SHIPPING_COUNTRY);
            info.shipping_state = populateItem(c, SHIPPING_STATE);
            info.delivery_date = populateItem(c, DELIVERY_DATE);
            info.delivery_notes = populateItem(c, DELIVERY_NOTES);
            info.order_notes = populateItem(c, ORDER_NOTES);
            info.shipping_add_1 = populateItem(c, SHIPPING_ADD_1);
            info.shipping_add_2 = populateItem(c, SHIPPING_ADD_2);
            info.shipping_add_3 = populateItem(c, SHIPPING_ADD_3);
            info.shipping_suburb = populateItem(c, SHIPPING_SUBURB);
            info.shipping_postcode = populateItem(c, ORDER_SHIPPING_POSTCODE);
            info.sub_total = populateItem(c, SUB_TOTAL);
            info.tax_total = populateItem(c, ORDER_TAX_TOTAL);
            info.shipping_cost = populateItem(c, SHIPPING_COST);
            info.show_deliverydate_order = populateItem(c, SHOW_DELIVERYDATE_ORDER);
            info.mandetory_delivery_order = populateItem(c, MANDETORY_DELIVERY_ORDER);
            array.add(info);
        }


        try {
            dataBaseHelper.close();
            db.close();
            c.close();
        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return array;


    }


    //*******************Delete all Ground Data*******************
    public void deleteDataBase(String table) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //  e.printStackTrace();
        }
        dataBaseHelper.getWritableDatabase().delete(table, null, null);
        try {
            dataBaseHelper.close();
        } catch (Exception e) {
            //  e.printStackTrace();
        }
    }
    public void deleteTitle(String id,String type)
    {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
              e.printStackTrace();
        }
        dataBaseHelper.getWritableDatabase().delete(TABLE_ORDER_TEMPLATE, "order_template_id=? and last_sync_id=?", new String[] {id,type});
        try {
            dataBaseHelper.close();
        } catch (Exception e) {
              e.printStackTrace();
        }


    }


    public void deleteTitlepro(String id,String type)
    {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataBaseHelper.getWritableDatabase().delete(TABLE_GET_PRODUCTS, "id=? and order_unit_id=?", new String[] {id,type});
        try {
            dataBaseHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    public void deleteoneTable(String prefix,String tablename)
    {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
      //  dataBaseHelper.getWritableDatabase().delete(tablename, "prefix_name="+prefix,null);// new String[] {prefix});
        dataBaseHelper.getWritableDatabase().delete(tablename, "prefix_name=?", new String[]{(prefix)});
        try {
            dataBaseHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    //*******************Delete all tables*******************
    public void delete(String table) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            // e.printStackTrace();
        }
        dataBaseHelper.getWritableDatabase().delete(table, null, null);
        try {
            dataBaseHelper.close();
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public String Setvalues() {
        String returnvalue = null;
        boolean tba = false;
        product_total = 0;
        prod_tax = 0;
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {


            //  e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_ADD_TO_CART;

        Cursor c = db.rawQuery(select, null);
        while (c.moveToNext()) {
            try {
                double ptot = 0;
                String tott = c.getString(c.getColumnIndex("prod_total"));
                if (c.getString(c.getColumnIndex("prod_total")).equals("TBA")) {
                    tba = true;
                } else {
                    ptot = Double.parseDouble(c.getString(c.getColumnIndex("prod_total")));
                }
                double ptax = Double.parseDouble(c.getString(c.getColumnIndex("prod_tax")));

                product_total = product_total + ptot;
                prod_tax = prod_tax + ptax;
            } catch (Exception e) {
            }
        }
        if (tba) {
            returnvalue = "TBA";
        } else {
            returnvalue = String.valueOf(product_total) + ":" + String.valueOf(prod_tax);
        }
        try {
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnvalue;
    }
    public String Setvalues222() {
        String returnvalue = null;
        boolean tba = false;
        product_total = 0;
        prod_tax = 0;
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {


            //  e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_ADD_TO_CART;

        Cursor c = db.rawQuery(select, null);
        while (c.moveToNext()) {
            try {
                double ptot = 0;
                String tott = c.getString(c.getColumnIndex("prod_total"));
                //  String name = c.getString(c.getColumnIndex("name"));
                if (c.getString(c.getColumnIndex("taxable")).equals("1")&&c.getString(c.getColumnIndex("prod_total")).equals("TBA")) {
                    tba = true;
                }
                if(c.getString(c.getColumnIndex("taxable")).equals("1")&& !(c.getString(c.getColumnIndex("prod_total"))).equals("TBA")){
                    ptot = Double.parseDouble(c.getString(c.getColumnIndex("prod_total")));
                    product_total = product_total + ptot;
                }
                double ptax = Double.parseDouble(c.getString(c.getColumnIndex("prod_tax")));


                prod_tax = prod_tax + ptax;
            } catch (Exception e) {
            }
        }
        if (tba) {
            returnvalue = "TBA";
        } else {
            returnvalue = String.valueOf(product_total) + ":" + String.valueOf(prod_tax);
        }
        try {
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnvalue;
    }

    //-------------------update table_add_to_cart----------------
    public void updatecartprodt(String prod_id, String prodtax, String prodtotalprice) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(PROD_TAX, prodtax);
        values.put(PROD_TOTAL, prodtotalprice);

        dataBaseHelper.getWritableDatabase().update(TABLE_ADD_TO_CART, values, "prod_id ='" + prod_id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    //-------------------update table_add_to_cart----------------
    public void updateaadid(String prefix, String shipingid, String billingid, String act) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(DEFAULT_SHIPPING_ADD, shipingid);
        values.put(DEFAULT_BILLING_ADD, billingid);
        values.put(ACT, act);

        dataBaseHelper.getWritableDatabase().update(TABLE_ADD_DETAIL_CUSTOMER, values, "prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    public void updatecartqty(String prod_id, String qty) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();

        values.put(CART_QTY, qty);

        dataBaseHelper.getWritableDatabase().update(TABLE_ADD_TO_CART, values, "prod_id ='" + prod_id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    //**********************INSERT into TABLE_ORDER_TEMPLATE Table*****************
   public void newtemplate(String pcode, String prodid, String qty, String temp_order_id, String prefix_name, String type) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
       ContentValues values = new ContentValues();
       values.put(TEMPLATE_QTY, qty);
       values.put(TEMPLATE_PRODUCT_ID, prodid);
       values.put(TEMPLATE_PRODUCT_CODE, pcode);
       values.put(TEMPLATE_TYPE, type);
        dataBaseHelper.getWritableDatabase().insert(TABLE_ORDER_TEMPLATE, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    //*****************Content Values for add_customer_address*************
    private ContentValues populateAddAdress(Model_Add_Detail_Customer model) {
        ContentValues values = new ContentValues();
        values.put(PREFIX_ADD_DETAIL, model.prefix_name);
        values.put(ID, model.id);
        values.put(ADD_1, model.add_1);
        values.put(COUNTRY, model.country);
        values.put(STATE, model.state);
        values.put(SUBURB, model.suburb);
        values.put(POSTCODE, model.postcode);

        return values;
    }

    public void update_prouct_Temp(String pcode, String prodid, String qty, String temp_order_id, String prefix_name, String type) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            values.put(TEMPLATE_QTY, qty);
            values.put(TEMPLATE_PRODUCT_ID, prodid);
            values.put(TEMPLATE_PRODUCT_CODE, pcode);
            values.put(TEMPLATE_TYPE, type);

            dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, "order_template_id ='" + temp_order_id + "' AND " + PREFIX_NAME + " = '" + prefix_name + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //**************************Get Data from Template and add to Model_Templates ***********************
    public ArrayList<Model_Template> GetTemp_Detail(String prefix, String tempid) throws Exception {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {


            //  e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_ORDER_TEMPLATE + " WHERE " + ORDER_TEMPLATE_ID + " = '" + tempid + "' AND " + PREFIX_ORDER_TEMPLATE + " = '" + prefix + "'";

        Cursor c = db.rawQuery(select, null);
        ArrayList<Model_Template> arr = new ArrayList<>();

        while (c.moveToNext()) {

            Model_Template item = new Model_Template();
            item.order_template_id = populateItem(c, "order_template_id");
            item.type = populateItem(c, "type");
            item.customer_id = populateItem(c, "customer_id");
            item.template_name = populateItem(c, "template_name");
            item.product_code = populateItem(c, "product_code");
            item.product_id = populateItem(c, "product_id");
            item.qty = populateItem(c, "qty");
            arr.add(item);
        }
        try {
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }
    //*****************Get Values from get_notification************************************************

    public ArrayList<Model_get_notification> get_notification_setting(String prefix) {

        ArrayList<Model_get_notification> arr = new ArrayList<Model_get_notification>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_NOTIFICATION_SETTINGS + " WHERE " + PREFIX_NAME_NOTI + " = '" + prefix + "'";

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {

                Model_get_notification modelGetCategories = new Model_get_notification();
                modelGetCategories.push_notification = c.getString(c.getColumnIndex("push_notification"));
                modelGetCategories.email_notification = c.getString(c.getColumnIndex("email_notification"));
                modelGetCategories.time_zone = c.getString(c.getColumnIndex("timezone"));
                modelGetCategories.show_deliverydate = c.getString(c.getColumnIndex("show_deliverydate"));
                modelGetCategories.mandetory_delivery = c.getString(c.getColumnIndex("mandetory_delivery"));
                arr.add(modelGetCategories);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr;
    }


    //*****************Get Values from get_notification************************************************

    public String get_timezone_setting(String prefix) {

        ArrayList<Model_get_notification> arr = new ArrayList<Model_get_notification>();
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + TABLE_NOTIFICATION_SETTINGS + " WHERE " + PREFIX_NAME_NOTI + " = '" + prefix + "'";

            Cursor c = db.rawQuery(select, null);
            while (c.moveToNext()) {

                Model_get_notification modelGetCategories = new Model_get_notification();
                modelGetCategories.push_notification = c.getString(c.getColumnIndex("push_notification"));
                modelGetCategories.email_notification = c.getString(c.getColumnIndex("email_notification"));
                modelGetCategories.time_zone = c.getString(c.getColumnIndex("timezone"));
                arr.add(modelGetCategories);
            }

            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return arr.get(0).time_zone;
    }

    //-------------------update notification Setting----------------
    public void update_notification_setting(String email_notification_switch_value, String push_notification_switch_value
            , String timeZOne_value, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(TIME_ZONE_NOTI, timeZOne_value);
        values.put(PUSH_NOTIFICATION, push_notification_switch_value);
        values.put(EMAIL_NOTIFICATION, email_notification_switch_value);


        dataBaseHelper.getWritableDatabase().update(TABLE_NOTIFICATION_SETTINGS, values, "prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    //-----------------------insert in  notification setting
    public void add_get_notification(Model_get_notification item) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populategetNotification(item);
        dataBaseHelper.getWritableDatabase().insert(TABLE_NOTIFICATION_SETTINGS, null, values);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public void update_get_notification(Model_get_notification model) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();

        values.put(CURRENCY_CODE_NOTI, model.Currency_code);
        values.put(CURRENCY_SYMBOL_NOTI, model.Currency_symbol);
        values.put(TIME_ZONE_NOTI, model.time_zone);
        values.put(PUSH_NOTIFICATION, model.push_notification);
        values.put(EMAIL_NOTIFICATION, model.email_notification);
        values.put(SHOW_DELIVERYDATE, model.show_deliverydate);
        values.put(MANDETORY_DELIVERY, model.mandetory_delivery);
        dataBaseHelper.getWritableDatabase().update(TABLE_NOTIFICATION_SETTINGS, values, "prefix_name ='" + model.Prefix_name + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    //*****************Content Values for add_customer_address*************
    private ContentValues populategetNotification(Model_get_notification model) {
        ContentValues values = new ContentValues();
        values.put(PREFIX_NAME_NOTI, model.Prefix_name);
        values.put(CURRENCY_CODE_NOTI, model.Currency_code);
        values.put(CURRENCY_SYMBOL_NOTI, model.Currency_symbol);
        values.put(TIME_ZONE_NOTI, model.time_zone);
        values.put(PUSH_NOTIFICATION, model.push_notification);
        values.put(EMAIL_NOTIFICATION, model.email_notification);
        values.put(SHOW_DELIVERYDATE, model.show_deliverydate);
        values.put(MANDETORY_DELIVERY, model.mandetory_delivery);
        return values;
    }

    //*******************Delete data on reset*******************
    public void delete_table_data(String table, String column_name, String prefix) {
        try {
            dataBaseHelper.createDataBase();
            dataBaseHelper.getWritableDatabase().delete(table, column_name + " = ?", new String[]{prefix});
            dataBaseHelper.close();
        } catch (IOException e) {

        }
    }

    public void delete_temp(String name) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //  e.printStackTrace();
        }
        dataBaseHelper.getWritableDatabase().delete(TABLE_ORDER_TEMPLATE, ORDER_TEMPLATE_ID + " = ?", new String[]{name});
        try {
            dataBaseHelper.close();
        } catch (Exception e) {
            //  e.printStackTrace();
        }
    }
    public void delete_tempProduct(String name,String prefix) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //  e.printStackTrace();
        }
        dataBaseHelper.getWritableDatabase().delete(TABLE_GET_PRODUCTS, PRODUCT_ID + "=? AND " + PREFIX_NAME + " = ?", new String[]{name,prefix});
        try {
            dataBaseHelper.close();
        } catch (Exception e) {
              e.printStackTrace();
        }
    }
    public void delete_value(String tablename, String column_name, String value) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataBaseHelper.getWritableDatabase().delete(tablename, column_name + " = ?", new String[]{value});
        try {
            dataBaseHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //-------------------update table_Order Template----------------
    public void update_temp_name(String order_id, String tempname, String prefix, String type) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(TEMPLATE_NAME, tempname);
        values.put(TEMPLATE_TYPE, type);


        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, "order_template_id ='" + order_id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    public void updateinventdb(String id, String product_id, String sink_id, String aval_stock, String reserve_qut, String bck_ord_aval, String instatus, String type, String lastsyncid, String prefix) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();

            values.put("sink_id", sink_id);
            values.put("aval_stock", aval_stock);
            values.put("reserve_qut", reserve_qut);
            values.put("bck_ord_aval", bck_ord_aval);
            values.put("status", instatus);
            values.put("type", type);
            values.put("last_sync_id", lastsyncid);

            dataBaseHelper.getWritableDatabase().update(TABLE_GET_PRODUCT_INVENTORY, values, "product_id ='" + product_id + "' AND id ='" + id + "' AND prefix_name ='" + prefix + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    //-------------------update table_Order Template----------------
    public void update_temp_productId(String prodcode, String product_id, String qty, String template_id, String prefix,String type) {
//dasfasdfasfa
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put(TEMPLATE_PRODUCT_CODE, prodcode);
        values.put(TEMPLATE_PRODUCT_ID, product_id);
        values.put(TEMPLATE_QTY, qty);
        values.put(TYPE, type);

        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, "order_template_id ='" + template_id + "' AND prefix_name ='" + prefix + "'", null);
     //   dataBaseHelper.getWritableDatabase().delete(TABLE_ORDER_TEMPLATE, ORDER_TEMPLATE_ID + "=? AND " + PREFIX_NAME + " = ?", new String[]{template_id,prefix});
      //  dataBaseHelper.getWritableDatabase().delete(TABLE_ORDER_TEMPLATE,  "order_template_id=?", new String[] {(template_id)});


        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }


    //***********************************************************************
    //-------------------update table_Order Template----------------
    public void update_temp_qty(String pid, String qnty, String prefix, String type) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ContentValues values = new ContentValues();

        values.put(TEMPLATE_QTY, qnty);
        values.put(TEMPLATE_TYPE, type);

        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, "order_template_id ='" + pid + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //-------------------update table_Order Template----------------
    public void update_newtemp(String order_id_new, String sink_id, String prefix, String type) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();

        values.put(TEMPLATE_SINK_ID, sink_id);
        values.put(TEMPLATE_TYPE, type);

        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, ORDER_TEMPLATE_ID + " ='" + order_id_new + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    //-------------------Full update table_Order Template----------------
    public void update_newtemp2(Model_Template model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();


        values.put(PREFIX_ORDER_TEMPLATE, model.prefix_name);
        values.put(TEMPLATE_TYPE, model.type);
        values.put(TEMPLATE_ORDER_ID, model.order_template_id);
        values.put(TEMPLATE_NAME, model.template_name);
        values.put(TEMPLATE_CREATED_DATE, model.created_date);
        values.put(TEMPLATE_CREATED_BY, model.created_by);
        values.put(TEMPLATE_PRODUCT_ID, model.product_id);
        values.put(TEMPLATE_PRODUCT_CODE, model.product_code);
        values.put(TEMPLATE_QTY, model.qty);
        values.put(TEMPLATE_SINK_ID, model.last_sync_id);

        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, ORDER_TEMPLATE_ID + " ='" + model.order_template_id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }


 //-------------------Full update table_Order Template----------------
    public void update_neworder(Model_Order_Info model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();


        values.put(PREFIX_ORDER_INFO, model.prefix_name);
        values.put(ORDER_ID, model.order_id);
        values.put(CUSTOMER_ID, model.customer_id);
        values.put(BILLING_COUNTRY, model.billing_country);
        values.put(BILLING_STATE, model.billing_state);
        values.put(BILLING_SUBURB, model.billing_suburb);
        values.put(BILLING_ADD_1, model.billing_add_1);
        values.put(BILLING_ADD_2, model.billing_add_2);
        values.put(BILLING_ADD_3, model.billing_add_3);
        values.put(BILLING_POSTCODE, model.billing_postcode);
        values.put(SHIPPING_COUNTRY, model.shipping_country);
        values.put(SHIPPING_STATE, model.shipping_state);
        values.put(SHIPPING_SUBURB, model.shipping_suburb);
        values.put(SHIPPING_ADD_1, model.shipping_add_1);
        values.put(SHIPPING_ADD_2, model.shipping_add_2);
        values.put(SHIPPING_ADD_3, model.shipping_add_3);
        values.put(ORDER_SHIPPING_POSTCODE, model.shipping_postcode);
        values.put(ORDER_SHIPPING_COST, model.shipping_cost);
        values.put(SUB_TOTAL, model.sub_total);
        values.put(ORDER_TAX_TOTAL, model.tax_total);
        values.put(ORDER_TOTAL, model.total);
        values.put(ORDER_NOTES, model.order_notes);
        values.put(INTERNAL_NOTES, model.internal_notes);
        values.put(ORDER_STATUS, model.order_status);
        values.put(BACK_ORDER, model.back_order);
        values.put(MAIN_ORDER_ID, model.main_order_id);
        values.put(DELIVERY_DATE, model.delivery_date);
        values.put(DELIVERY_NOTES, model.delivery_notes);
        values.put(ORDER_CREATED_DATE, model.created_date);
        values.put(ORDER_CREATED_BY, model.created_by);
        values.put(ORDER_UPDATED_DATE, model.update_date);
        values.put(ORDER_UPDATED_BY, model.updated_by);
        values.put(ORDER_NAME, model.order_name);
        values.put(ORDER_BY, model.order_by);
        values.put(ORDER_SINK_ID, model.sink_id);
        values.put(ORDER_TYPE, model.type);
        values.put(MANDETORY_DELIVERY_ORDER, model.mandetory_delivery_order);
        values.put(SHOW_DELIVERYDATE_ORDER, model.show_deliverydate_order);
        values.put(ORDER_LAST_SYNC_ID, model.last_sync_id);
        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_INFO, values, ORDER_ID + " ='" + model.order_id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }


//-------------------Full update table_Order Product----------------
    public void update_neworderproduct(Model_Order_Product model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();


        values.put(PREFIX_ORDER_PRODUCTS, model.prefix_name);
        values.put(ORDER_PRODUCT_ID, model.id);
         values.put(PRODUCT_ORDER_ID, model.order_id);
        values.put(PRODUCT_PRODUCT_ID, model.product_id);
        values.put(PRODUCT_NAME, model.product_name);
        values.put(ORDER_PRODUCT_CODE, model.product_code);
      //  values.put(IMAGE, model.image);
        values.put(QTY, model.qty);
        values.put(QTY_NEW_ORDER, model.qty_new_order);
        values.put(ACT_QTY, model.act_qty);
        values.put(PRODUCT_UNIT_PRICE, model.unit_price);
        values.put(APL_PRICE, model.applied_price);
        values.put(PRODUCT_SUB_TOTAL, model.sub_total);
        values.put(PRODUCT_SINK_ID, model.sink_id);
        values.put(PRODUCT_TYPE, model.type);
        values.put(ORDER_PRODUCT_CREATED_DATE, model.created_date);
        values.put(ORDER_PRODUCT_UPDATE_DATE, model.update_date);
        values.put(ORDER_PRODUCT_UPDATED_BY, model.updated_by);
        values.put(ORDER_PRODUCT_LAST_SYNC_ID, model.last_sync_id);
       // dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_PRODUCT, values, PRODUCT_ORDER_ID + " ='" + model.order_id + "' AND prefix_name ='" + prefix + "' AND "+PRODUCT_PRODUCT_ID+"='"+model.product_id+"'", null);
        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_PRODUCT, values, PRODUCT_ORDER_ID + " ='" + model.order_id + "' AND prefix_name ='" + prefix + "' AND "+PRODUCT_PRODUCT_ID+"='"+model.product_id+"'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

//-------------------Full update table_Order Product----------------
    public void update_newcustomer(ModelgetCustomer model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();


        values.put(PREFIX_GETCUSTOMER, model.prefix_name);
        values.put(USER_ID, model.ws_users_id);
        values.put(BUSINESS_NAME, model.business_name);
        values.put(BUSINESS_ABN, model.business_abn);
        values.put(BILLING_ADD_ID, model.billing_add_id);
        values.put(DEFAULT_SHIPPING_ADD_ID, model.default_shipping_add_id);
        values.put(EXTERNAL_RECORD_ID, model.external_record_id);
        values.put(CREDIT_LIMIT, model.credit_limit);
        values.put(CUSTOMER_GROUP_NAME, model.customer_group_name);
        dataBaseHelper.getWritableDatabase().update(TABLE_GETCUSTOMER, values, USER_ID + " ='" + model.ws_users_id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

//-------------------Full update table_Order Product----------------
    public void update_detailcustomer(Model_Add_Detail_Customer model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();


        values.put(PREFIX_ADD_DETAIL, model.prefix_name);
        values.put(ID, model.id);
        values.put(WS_USERS_ID, model.ws_users_id);
        values.put(FIRST_NAME, model.first_name);
        values.put(LAST_NAME, model.last_name);
        values.put(ADD_1, model.add_1);
        values.put(ADD_2, model.add_2);
        values.put(ADD_3, model.add_3);
        values.put(SUBURB, model.suburb);
        values.put(POSTCODE, model.postcode);
        values.put(TEL_1, model.tel_1);
        values.put(TEL_2, model.tel_2);
        values.put(FAX, model.fax);
        values.put(EMAIL, model.email);
        values.put(DEFAULT_SHIPPING_ADD, model.default_shipping_add);
        values.put(DEFAULT_BILLING_ADD, model.default_billing_add);
        values.put(ACT, model.act);
        values.put(STATUS, model.status);
        values.put(CREATED_DATE, model.created_date);
        values.put(UPDATE_DATE, model.update_date);
        values.put(UPDATED_BY, model.updated_by);
        values.put(COUNTRY, model.country);
        values.put(STATE, model.state);
        dataBaseHelper.getWritableDatabase().update(TABLE_ADD_DETAIL_CUSTOMER, values, ID + " ='" + model.id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

//-------------------Full update table_Order Product----------------
    public void update_adddetail(Model_Add_Detail_Customer model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();


        values.put(PREFIX_ADD_DETAIL, model.prefix_name);
        values.put(ID, model.id);
        values.put(WS_USERS_ID, model.ws_users_id);
        values.put(FIRST_NAME, model.first_name);
        values.put(LAST_NAME, model.last_name);
        values.put(ADD_1, model.add_1);
        values.put(ADD_2, model.add_2);
        values.put(ADD_3, model.add_3);
        values.put(SUBURB, model.suburb);
        values.put(POSTCODE, model.postcode);
        values.put(TEL_1, model.tel_1);
        values.put(TEL_2, model.tel_2);
        values.put(FAX, model.fax);
        values.put(EMAIL, model.email);
        values.put(DEFAULT_SHIPPING_ADD, model.default_shipping_add);
        values.put(DEFAULT_BILLING_ADD, model.default_billing_add);
        values.put(ACT, model.act);
        values.put(STATUS, model.status);
        values.put(CREATED_DATE, model.created_date);
        values.put(UPDATE_DATE, model.update_date);
        values.put(UPDATED_BY, model.updated_by);
        values.put(COUNTRY, model.country);
        values.put(STATE, model.state);
        dataBaseHelper.getWritableDatabase().update(TABLE_ADD_DETAIL_CUSTOMER, values, ID + " ='" + model.id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

//-------------------Full update table_Order Product----------------
    public void update_getcutomer(Model_GetCategories model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();

        values.put(PREFIX_GETCATEGORIES, model.prefix_name);
        values.put(CAT_ID, model.id);
        values.put(CATETORY_NAME, model.category_name);
        values.put(FULLNAME, model.fullname);
        values.put(PARENT_CAT_ID, model.parent_cat_id);
        values.put(ROOT_CAT_ID, model.root_cat_id);
        values.put(LEVEL, model.level);
        values.put(SORT, model.sort);
        values.put(CAT_STATUS, model.status);
        values.put(CAT_CREATED_DATE, model.created_date);
        values.put(CAT_UPDATE_DATE, model.update_date);
        values.put(CAT_UPDATED_BY, model.updated_by);
        values.put(CAT_SID, model.sid);
        values.put(CAT_TYPE, model.type);
        values.put(VIEW_TYPE, model.view_type);
        values.put(LAST_SYNC_ID, model.last_sync_id);
        dataBaseHelper.getWritableDatabase().update(TABLE_GETCATEGORIES, values, CAT_ID + " ='" + model.id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

//-------------------Full update table_Order Product----------------
    public void update_getpage(Model_GetPage model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();

        values.put(PREFIX_GETPAGE, model.prefix_name);
        values.put(PAGE_ID, model.id);
        values.put(PAGE_URL, model.page_url);
        values.put(PAGE_STATUS, model.status);
        dataBaseHelper.getWritableDatabase().update(TABLE_GETPAGE, values, PAGE_ID + " ='" + model.id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }



//-------------------Full update table_Order Product----------------
    public void update_getproduct(Model_GetProduct model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();

        values.put(PREFIX_GET_PRODUCTS, model.prefix_name);
        values.put(ORDER_UNIT_NAME, model.order_unit_name);
        values.put(BASE_UNIT_NAME, model.base_unit_name);
        values.put(PRODUCT_ID, model.id);
        values.put(SINK_ID, model.sink_id);
        values.put(PRODUCT_CAT_ID, model.cat_id);
        values.put(PRODUCT_CODE, model.product_code);
        values.put(NAME, model.name);
        values.put(DESC, model.desc);
        values.put(PRODUCT_STATUS, model.status);
        values.put(COST_PRICE, model.cost_price);
        values.put(MIN_ORDER_QUT, model.min_order_qut);
        values.put(MAX_ORDER_QUT, model.max_order_qut);
        values.put(TAXABLE, model.taxable);
        values.put(BASE_UNIT_ID, model.base_unit_id);
        values.put(UNIT_PRICE, model.unit_price);
        values.put(ORDER_UNIT_ID, model.order_unit_id);
        values.put(INVENTORY, model.inventory);
        values.put(SPCL_PRICE, model.spcl_price);
        values.put(WAREHOUSE_NAME, model.warehouse_name);
        values.put(WAREHOUSE_LOC, model.warehouse_loc);
        values.put(STOCK_LOC, model.stock_loc);
        values.put(PRODUCT_CREATED_DATE, model.created_date);
        values.put(PRODUCT_UPDATE_DATE, model.update_date);
        values.put(PRODUCT_UPDATED_BY, model.updated_by);
        values.put(TYPE, model.type);
        values.put(TIER_PRICE, model.tier_price);
        values.put(VIEW_TYPE, model.view_type);
        values.put(PRODUCT_LAST_SYNC_ID, model.last_sync_id);
        values.put(IMAGE, model.img);
        values.put(THUMB_IMAGE, model.thumb_image);

        dataBaseHelper.getWritableDatabase().update(TABLE_GET_PRODUCTS, values, PRODUCT_ID + " ='" + model.id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }


//-------------------Full update table_Order Product----------------
    public void update_getproductImage(Model_Product_Image model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();

        values.put(PREFIX_PRODUCT_IMAGE, model.prefix_name);
        values.put(IMAGE_ID, model.id);
        values.put(IMAGE_SINK_ID, model.sink_id);
        values.put(IMAGE_PRODUCT_ID, model.product_id);
        values.put(IMAGE, model.image);
        values.put(THUMB_IMAGE, model.thumb_image);
        values.put(IMAGE_STATUS, model.status);
        values.put(IS_MAIN, model.is_main);
        values.put(IMAGE_TYPE, model.type);
        values.put(IMAGE_CREATED_DATE, model.created_date);
        values.put(IMAGE_UPDATE_DATE, model.update_date);
        values.put(IMAGE_UPDATED_BY, model.updated_by);
        values.put(IMAGE_LAST_SYNC_ID, model.last_sync_id);

        dataBaseHelper.getWritableDatabase().update(TABLE_GET_PRODUCT_IMAGE, values, IMAGE_ID + " ='" + model.id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

//-------------------Full update table_Order Product----------------
    public void update_getproductinventory(Model_Product_Inventory model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();

        values.put(PREFIX_PRODUCT_INVENTORY, model.prefix_name);
        values.put(INVENTORY_ID, model.id);
        values.put(INVENTORY_PRODUCT_ID, model.product_id);
        values.put(INVENTORY_SINK_ID, model.sink_id);
        values.put(AVAL_STOCK, model.aval_stock);
        values.put(RESERVE_QUT, model.reserve_qut);
        values.put(BCK_ORD_AVAL, model.bck_ord_aval);
        values.put(INVENTORY_STATUS, model.status);
        values.put(INVENTORY_WAREHOUSE_NAME, model.warehouse_name);
        values.put(INVENTORY_WAREHOUSE_LOCATION, model.warehouse_location);
        values.put(STOCK_LOCATION, model.stock_location);
        values.put(INVENTORY_CREATED_DATE, model.created_date);
        values.put(INVENTORY_UPDATE_DATE, model.update_date);
        values.put(INVENTORY_UPDATE_BY, model.updated_by);
        values.put(INVENTORY_TYPE, model.type);
        values.put(INVENTORY_LAST_SYNC_ID, model.last_sync_id);

        dataBaseHelper.getWritableDatabase().update(TABLE_GET_PRODUCT_INVENTORY, values, INVENTORY_ID + " ='" + model.id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

//-------------------Full update table_Order Product----------------
    public void update_getproducttier(Model_Tier_pricing model, String prefix) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }


        ContentValues values = new ContentValues();

        values.put(PREFIX_TIER_PRICING, model.prefix_name);
        values.put(TIER_ID, model.id);
        values.put(TIER_PRODUCT_ID, model.product_id);
        values.put(CUSTOMER_GROUP_ID, model.customer_group_id);
        values.put(QUANTITY_UPTO, model.quantity_upto);
        values.put(TIER_DISCOUNT_TYPE, model.discount_type);
        values.put(TIER_DISCOUNT, model.discount);
        values.put(TIER_CREATED_DATE, model.created_date);
        values.put(TIER_UPDATE_DATE, model.update_date);
        values.put(TIER_UPDATE_BY, model.updated_by);
        values.put(TIER_SINK_ID, model.sink_id);
        values.put(TIER_TYEPE, model.type);
        values.put(TIER_LAST_SYNC_ID, model.last_sync_id);

        dataBaseHelper.getWritableDatabase().update(TABLE_TIER_PRICING, values, TIER_ID + " ='" + model.id + "' AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }




    //-------------------------Update off template-------------------------------------------------

    public void update_offtemp(String order_id_old,String order_id_new, String sink_id, String prefix, String type) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = new ContentValues();

        values.put(LAST_SYNC_ID, sink_id);
        values.put(ORDER_TEMPLATE_ID, order_id_new);
        values.put(TEMPLATE_TYPE, type);

        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, LAST_SYNC_ID + " = 'off' AND "+ORDER_TEMPLATE_ID+" = "+order_id_old+" AND prefix_name ='" + prefix + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }
    //*********************update order_cancel_info**********************
    public void update_cancel_info(String orderId, String prefix) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            values.put(ORDER_STATUS, "Cancelled");

            dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_INFO, values, "order_id ='" + orderId + "' AND " + PREFIX_ORDER_INFO + " = '" + prefix + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //-------------------------updateorderinfo------------------//
    public void updateorderinfo(String prefix_name, String order_id, String shipping_cost, String total, String tax_total, String sub_total) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            values.put(ORDER_SHIPPING_COST, shipping_cost);
            values.put(SUB_TOTAL, sub_total);
            values.put(ORDER_TAX_TOTAL, tax_total);
            values.put(ORDER_TOTAL, total);

            dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_INFO, values, ORDER_ID + " = '" + order_id + "' AND " + PREFIX_ORDER_INFO + " = '" + prefix_name + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //-----------------------updating order adress info-----//
    public void updata_orderadd_info(String order_id, String prefix_name, String billing_country, String billing_state, String billing_suburb
            , String billing_add1, String billing_add2, String billing_add3, String billing_postcode, String shipping_country, String shipping_state, String shipping_suburb
            , String shipping_add1, String shipping_add2, String shipping_add3, String shipping_postcode, String text_order_notes, String text_order_by, String text_delivery_notes,String delivery_date) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            values.put(BILLING_COUNTRY, billing_country);
            values.put(BILLING_STATE, billing_state);
            values.put(BILLING_SUBURB, billing_suburb);
            values.put(BILLING_ADD_1, billing_add1);
            values.put(BILLING_ADD_2, billing_add2);
            values.put(BILLING_ADD_3, billing_add3);
            values.put(BILLING_POSTCODE, billing_postcode);
            values.put(SHIPPING_COUNTRY, shipping_country);
            values.put(SHIPPING_STATE, shipping_state);
            values.put(SHIPPING_SUBURB, shipping_suburb);
            values.put(SHIPPING_ADD_1, shipping_add1);
            values.put(SHIPPING_ADD_2, shipping_add2);
            values.put(SHIPPING_ADD_3, shipping_add3);
            values.put(ORDER_SHIPPING_POSTCODE, shipping_postcode);
            values.put(ORDER_BY, text_order_by);
            values.put(ORDER_NOTES, text_order_notes);
            values.put(DELIVERY_NOTES, text_delivery_notes);
            values.put(DELIVERY_DATE, delivery_date);


            dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_INFO, values, ORDER_ID + " = '" + order_id + "' AND " + PREFIX_ORDER_INFO + " = '" + prefix_name + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //-------------------------updateprodorder------------------//
    public void updateprodorder(String prefix_name, String order_product_id, String product_id, String qty, String prod_sub_total, String unit_price, String applied_price) {
        try {
            dataBaseHelper.createDataBase();

            ContentValues values = new ContentValues();
            values.put(APL_PRICE, applied_price);
            values.put(PRODUCT_UNIT_PRICE, unit_price);
            values.put(PRODUCT_SUB_TOTAL, prod_sub_total);
            values.put(QTY, qty);

            dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_PRODUCT, values, ORDER_PRODUCT_ID + " = '" + order_product_id + "' AND " + PRODUCT_PRODUCT_ID + " = '" + product_id + "' AND " + PREFIX_ORDER_PRODUCTS + " = '" + prefix_name + "'", null);

            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //*******************Delete data on reset*******************
    public void delete_order_prod(String orderid, String prefix) {
        try {
            dataBaseHelper.createDataBase();
            dataBaseHelper.getWritableDatabase().delete(TABLE_ORDER_PRODUCT, PREFIX_ORDER_PRODUCTS + "=? AND " + PRODUCT_ORDER_ID + " = ?", new String[]{prefix, orderid});

            dataBaseHelper.close();
        } catch (IOException e) {

        }
    }

    public void updateproduct(Model_GetProduct model) {

        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ContentValues values = populategetproduct(model);
        dataBaseHelper.getWritableDatabase().update(TABLE_GET_PRODUCTS, values, "prefix_name ='" + model.prefix_name + "' AND id='" + model.id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void updatecategory(Model_GetCategories model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateGetCategoties(model);
        dataBaseHelper.getWritableDatabase().update(TABLE_GETCATEGORIES, values, "prefix_name ='" + model.prefix_name + "' AND id='" + model.id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public void updateproductImage(Model_Product_Image model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateGetProductImage(model);
        dataBaseHelper.getWritableDatabase().update(TABLE_GET_PRODUCT_IMAGE, values, "prefix_name ='" + model.prefix_name + "' AND id='" + model.id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public void updateproductInventory(Model_Product_Inventory model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateGetProductInventory(model);
        dataBaseHelper.getWritableDatabase().update(TABLE_GET_PRODUCT_INVENTORY, values, "prefix_name ='" + model.prefix_name + "' AND id='" + model.id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public void updatetierpricing(Model_Tier_pricing model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populatetierpricing(model);
        dataBaseHelper.getWritableDatabase().update(TABLE_TIER_PRICING, values, "prefix_name ='" + model.prefix_name + "' AND id='" + model.id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public void updateorderinfosync(Model_Order_Info model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateorderinfo(model);
        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_INFO, values, "prefix_name ='" + model.prefix_name + "' AND order_id='" + model.order_id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public void updateorderproduct(Model_Order_Product model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateOrderProduct(model);
        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_PRODUCT, values, "prefix_name ='" + model.prefix_name + "' AND order_id='" + model.order_id + "' AND id='" + model.id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public void updatetemplate(Model_Template model) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        ContentValues values = populateOrderTemplate(model);
        dataBaseHelper.getWritableDatabase().update(TABLE_ORDER_TEMPLATE, values, "prefix_name ='" + model.prefix_name + "' AND order_template_id='" + model.order_template_id + "'", null);
        try {
            dataBaseHelper.close();
            values = null;
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    public boolean isexist(String table, String key, String value, String prefix) {
        try {
            dataBaseHelper.createDataBase();

            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String select = "SELECT * FROM " + table + " WHERE " + key + " = '" + value + "' AND prefix_name='" + prefix + "'";

            Cursor c = db.rawQuery(select, null);


            while (c.moveToNext()) {

                return true;

            }
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            //  e.printStackTrace();
        }
        return false;
    }


    public ArrayList<Model_Order_Info> getmand_date(String prefix, String orderid) {
        try {
            dataBaseHelper.createDataBase();
        } catch (IOException e) {


            //  e.printStackTrace();
        }
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_ORDER_INFO + " WHERE " + ORDER_ID + " = '" + orderid + "' AND " + PREFIX_ORDER_INFO + " = '" + prefix + "'";

        Cursor c = db.rawQuery(select, null);
        ArrayList<Model_Order_Info> arr = new ArrayList<>();

        while (c.moveToNext()) {

            Model_Order_Info item = new Model_Order_Info();
            item.show_deliverydate_order = populateItem(c, "show_deliverydate_order");
            item.mandetory_delivery_order = populateItem(c, "mandetory_delivery_order");

            arr.add(item);
        }
        try {
            dataBaseHelper.close();
            db.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }
}

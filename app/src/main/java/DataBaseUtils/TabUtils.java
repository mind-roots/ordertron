package DataBaseUtils;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ordertron.R;

import Fragments.CartActivity;


public class TabUtils {

	/* This code snippet is 
	 * to add the counter badge on tabbar 
	 * on the main screen
	 */
	
	public static View renderTabView(Context context, int titleResource, int backgroundResource, String string) {
	    FrameLayout view = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.tab_badge, null);
	    // We need to manually set the LayoutParams here because we don't have a view root
	    view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
	    ((ImageView) view.findViewById(R.id.tab_text)).setImageResource(titleResource);
	    view.findViewById(R.id.tab_text).setBackgroundResource(backgroundResource);
	    //updateTabBadge((TextView) view.findViewById(R.id.tab_badge), string);
	    return view;
	}
	 
	@SuppressWarnings("deprecation")
	public static void updateTabBadge(TabLayout.Tab tab, int badgeNumber) {
	    updateTabBadge((TextView) tab.getCustomView().findViewById(R.id.tab_badge), badgeNumber);
	}
	 
	private static void updateTabBadge(TextView view, int badgeNumber) {
	    if (badgeNumber > 0) {
	        view.setVisibility(View.VISIBLE);
	        view.setText(Integer.toString(badgeNumber));
	    }
	    else {
	        view.setVisibility(View.GONE);
	    }
	}

	
	public static Object renderTabView(CartActivity ios, int icCartW,
			int backgroundResource, int count_cart) {
		// TODO Auto-generated method stub
		return null;
	}
}

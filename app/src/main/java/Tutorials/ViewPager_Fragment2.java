package Tutorials;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ordertron.R;
import com.ordertron.Tutorial_View;


/**
 * Created by admin on 11/21/2015.
 */
public class ViewPager_Fragment2 extends Fragment {

    RelativeLayout rl_two;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.layout_two, container, false);

        rl_two=(RelativeLayout)v.findViewById(R.id.rl_two);
//        next = (ImageView)v.findViewById(R.id.next);
//        back = (ImageView)v.findViewById(R.id.back);
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ViewpagerActivity.defaultViewpager.setCurrentItem(0);
//            }
//        });
//        next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ViewpagerActivity.defaultViewpager.setCurrentItem(2);
//            }
//        });

        if( Tutorial_View.tutorial==1){
            rl_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Tutorial_View.tutorial=0;
                    getActivity().finish();
                }
            });
        }

        return v;
    }
}

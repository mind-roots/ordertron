package Tutorials;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ordertron.R;
import com.ordertron.Tutorial_View;


/**
 * Created by Balvinder on 7/21/2016.
 */
public class ViewPager_Fragment5  extends Fragment{
    RelativeLayout rl_five;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_five, container, false);
        rl_five=(RelativeLayout)view.findViewById(R.id.rl_five);
//        start = (TextView) view.findViewById(R.id.start);
//
//        start.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent i = new Intent(getActivity(), RegisterThird.class);
//                startActivity(i);
//
//            }
//        });
        if( Tutorial_View.tutorial==1){
            rl_five.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Tutorial_View.tutorial=0;
                    getActivity().finish();
                }
            });
        }
        return view;
    }
}

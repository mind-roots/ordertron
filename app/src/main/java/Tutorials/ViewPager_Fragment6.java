package Tutorials;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ordertron.R;
import com.ordertron.Tutorial_View;

/**
 * Created by Balvinder on 7/22/2016.
 */
public class ViewPager_Fragment6 extends Fragment {

    Button btn;
    ListView listView;
    TextView sign_with;
    Boolean isInternetPresent = false;
    RelativeLayout rl_six;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_six, container, false);

        Init(view);
        if( Tutorial_View.tutorial==1){
            rl_six.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Tutorial_View.tutorial=0;
                    getActivity().finish();
                }
            });
        }
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    // ***********************initialize the UI
    // components*************************************
    private void Init(View view) {
        rl_six=(RelativeLayout)view.findViewById(R.id.rl_six);
    }




    // *************************************************************************************
}

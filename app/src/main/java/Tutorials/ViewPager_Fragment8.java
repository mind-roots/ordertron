package Tutorials;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

import com.ordertron.R;

/**
 * Created by Abhijeet on 9/15/2016.
 */

public class ViewPager_Fragment8 extends Fragment {

   Button login,regi;
    WebView webView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_eight, container, false);


        init(v);
        click();
//        next = (ImageView)v.findViewById(R.id.next);
//        next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ViewpagerActivity.defaultViewpager.setCurrentItem(1);
//            }
//        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void click() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i= new Intent(getActivity(), LoginActivity.class);
//                getActivity().startActivity(i);
                getActivity().finish();
            }
        });
//
//        regi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //openWebPage("www.google.com");
////                Intent i= new Intent(getActivity(), Web_view1.class);
////                getActivity().startActivity(i);
//
//                String url = "http://www.google.com";
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);
//            }
//        });
    }
    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }
    private void init(View v) {

        login=(Button)v.findViewById(R.id.login);
       // regi=(Button)v.findViewById(R.id.regi);
        webView = (WebView)v.findViewById(R.id.help_webview);
    }
}

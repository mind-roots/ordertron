package Tutorials;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 11/20/2015.
 */
public class CustomPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;
    public CustomPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fragments = new ArrayList<Fragment>();
        fragments.add(new ViewPager_Fragment1());
        fragments.add(new ViewPager_Fragment2());
        fragments.add(new ViewPager_Fragment3());
        fragments.add(new ViewPager_Fragment4());
         fragments.add(new ViewPager_Fragment5());
        fragments.add(new ViewPager_Fragment6());
        fragments.add(new ViewPager_Fragment7());
        fragments.add(new ViewPager_Fragment8());
    }

  @Override
    public Fragment getItem(int position) {

                return fragments.get(position);

    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}

package Tutorials;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ordertron.R;
import com.ordertron.Tutorial_View;

/**
 * Created by Abhijeet on 9/15/2016.
 */

public class ViewPager_Fragment7 extends Fragment {



    RelativeLayout rl_seven;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_seven, container, false);

        Init(view);
        if( Tutorial_View.tutorial==1){
            rl_seven.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Tutorial_View.tutorial=0;
                    getActivity().finish();
                }
            });
        }
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    // ***********************initialize the UI
    // components*************************************
    private void Init(View view) {
        rl_seven=(RelativeLayout)view.findViewById(R.id.rl_seven);
    }




    // *************************************************************************************
}

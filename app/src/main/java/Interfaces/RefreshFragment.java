package Interfaces;

/**
 * Created by Abhijeet on 8/1/2016.
 */
public interface RefreshFragment {

    public void refresh();
}

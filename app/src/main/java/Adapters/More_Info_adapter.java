package Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ordertron.R;
import com.ordertron.Web_View;

import java.util.ArrayList;

import Model.Model_getUrl;

/**
 * Created by Abhijeet on 8/4/2016.
 */
public class More_Info_adapter extends RecyclerView.Adapter<More_Info_adapter.MyViewHolder> {

    private ArrayList<Model_getUrl> infoList=new ArrayList<>();
Context context;
    int size;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        LinearLayout click_lay;
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.page_title);
            click_lay = (LinearLayout) view.findViewById(R.id.click_lay);

        }
    }


    public More_Info_adapter(Context c, ArrayList<Model_getUrl> infoList, int size) {
        this.infoList = infoList;
        this.context = c;
        this.size = size;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.more_information_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

                holder.title.setText(infoList.get(position).page_title);
                holder.click_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, Web_View.class);
                        intent.putExtra("url", infoList.get(position).page_url);
                        context.startActivity(intent);
                    }
                });

    }

    @Override
    public int getItemCount() {
        return infoList.size();
    }
}
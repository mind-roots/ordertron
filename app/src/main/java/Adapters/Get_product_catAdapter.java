package Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ordertron.Add_Product;
import com.ordertron.Dashboard;
import com.ordertron.Product_detail;
import com.ordertron.R;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import Model.Model_GetProduct;
import Model.Model_Product_Image;
import Model.Utils;

/**
 * Created by Abhijeet on 12/31/2015.
 */
public class Get_product_catAdapter extends RecyclerView.Adapter<Get_product_catAdapter.ViewHolder> {

    SharedPreferences prefs;
    public Context context;
    private LayoutInflater inflater;
    public ArrayList<Model_GetProduct> data = new ArrayList<Model_GetProduct>();
    public ArrayList<Model_Product_Image> imagea = new ArrayList<Model_Product_Image>();
    private ArrayList<Model_GetProduct> arraylist;
    String Type,avs=null;
    NumberFormat formatter;

    public Get_product_catAdapter(Context context, ArrayList<Model_GetProduct> arrayList, String type) {
        this.context = context;
        this.data = arrayList;
        this.Type = type;
        this.arraylist = new ArrayList<Model_GetProduct>();
        this.arraylist.addAll(data);
        prefs = context.getSharedPreferences("login", context.MODE_PRIVATE);
        //AllOrdersAdapter.time_order = time_order;
        formatter = new DecimalFormat("#0.00");
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {

        String name = "";
        final Model_GetProduct ci = data.get(i);
        name = ci.name;

        try {
            JSONArray tier = new JSONArray(ci.tier_price);
            if (tier.length() > 0) {
                name = ci.name + " *";
            } else {
                name = ci.name;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        viewHolder.product_name.setText(name);
        viewHolder.product_code.setText(ci.product_code);
        if (ci.spcl_price.equals("0") || ci.spcl_price == null || ci.spcl_price.equals(null)) {
            viewHolder.txt_spcl_price.setVisibility(View.GONE);
            viewHolder.price.setText(prefs.getString("currency_symbol", null) +  formatter.format(Double.parseDouble(ci.unit_price)));
        } else {
            viewHolder.txt_spcl_price.setVisibility(View.VISIBLE);
            viewHolder.txt_spcl_price.setText(prefs.getString("currency_symbol", null) +  formatter.format(Double.parseDouble(ci.unit_price)));
            viewHolder.price.setText(prefs.getString("currency_symbol", null) +  formatter.format(Double.parseDouble(ci.spcl_price)));
            viewHolder.txt_spcl_price.setPaintFlags(viewHolder.txt_spcl_price
                    .getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        final String url =  ci.img;


        String q = ci.thumb_image;
        if (q != null) {
            q = q.replaceAll(" ", "%20"); // remove all whitespaces
        }
        try {
            if (Dashboard.width == 1440 && Dashboard.height == 2560) {
                Glide.with(context)
                        .load(q)
                        .placeholder(R.drawable.default_item_icon)
                        .crossFade()
                        .thumbnail(0.1f)
                        .into(viewHolder.del_icon);
               // Picasso.with(context).load(q).placeholder(R.drawable.default_item_icon).resize(150, 150).into(viewHolder.del_icon);
            } else {
                Glide.with(context)
                        .load(q)
                        .placeholder(R.drawable.default_item_icon)
                        .crossFade()
                        .thumbnail(0.1f)
                        .into(viewHolder.del_icon);
               // Picasso.with(context).load(q).placeholder(R.drawable.default_item_icon).resize(100, 100).into(viewHolder.del_icon);
            }
        } catch (Exception e) {

        }
        viewHolder.liner_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!data.get(i).avail_stock.equals("No Limits")) {
                    float s1 = Float.parseFloat(data.get(i).avail_stock);
                    avs = String.format("%.2f", s1);
                }else {
                    avs ="No Limits";
                }
              String s= data.get(i).status;
                Intent intent = new Intent(context, Product_detail.class);
                intent.putExtra("Intenttype", Type);
                intent.putExtra("name", data.get(i).name);
                intent.putExtra("catname", data.get(i).cat_name);
                intent.putExtra("fullcatname", data.get(i).fullname);
                intent.putExtra("image", url);
                intent.putExtra("product_code", data.get(i).product_code);
                intent.putExtra("price", data.get(i).unit_price);
                intent.putExtra("desc", data.get(i).desc);
                intent.putExtra("min_order_qut", data.get(i).min_order_qut);
                intent.putExtra("max_order_qut", data.get(i).max_order_qut);
                intent.putExtra("order_unit_name", data.get(i).order_unit_name);
                intent.putExtra("avail_stock",avs);
                intent.putExtra("spcl_price", data.get(i).spcl_price);
                intent.putExtra("productid", data.get(i).id);
                intent.putExtra("base_unit_name", data.get(i).base_unit_name);
                intent.putExtra("taxable", data.get(i).taxable);
                intent.putExtra("inventory", data.get(i).inventory);
                if (Type.equals("template")) {
                    intent.putExtra("tempid", Add_Product.tempid);
//                intent.putExtra("products", Add_Product.products);
//                intent.putExtra("qty", Add_Product.qty);
//                intent.putExtra("productscode", Add_Product.productscode);
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("tier_price", data.get(i).tier_price);
                context.startActivity(intent);
                Utils.hidekeyboard(context);
                //Toast.makeText(context,"Get_product_catAdapter hello"+i,Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.get_product_cat, viewGroup, false);

        return new ViewHolder(itemView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected LinearLayout liner_lay;
        protected ImageView del_icon;
        protected TextView product_name;
        protected TextView product_code;
        protected TextView price, txt_spcl_price;

        // protected TextView product_code;
        protected LinearLayout lay;

        public ViewHolder(View v) {
            super(v);
            del_icon = (ImageView) v.findViewById(R.id.del_icon);
            product_name = (TextView) v.findViewById(R.id.product_name);
            product_code = (TextView) v.findViewById(R.id.product_code);
            price = (TextView) v.findViewById(R.id.price);

            txt_spcl_price = (TextView) v.findViewById(R.id.txt_spcl_price);
            liner_lay = (LinearLayout) v.findViewById(R.id.liner_lay);
        }
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(arraylist);
        } else if (charText.length() > 2){
            for (Model_GetProduct wp : arraylist) {
                if (wp.name.toLowerCase(Locale.getDefault())
                        .contains(charText)||wp.product_code.toLowerCase(Locale.getDefault()).contains(charText)) {
                    data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }



}


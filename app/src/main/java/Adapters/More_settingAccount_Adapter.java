package Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ordertron.Dashboard;
import com.ordertron.More_Setting_Accounts;
import com.ordertron.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import DataBaseUtils.DatabaseQueries;
import Model.JSONParser;
import Model.ModelLogin;
import Model.Utils;

/**
 * Created by Abhijeet on 2/3/2016.
 */
public class More_settingAccount_Adapter extends RecyclerView.Adapter<More_settingAccount_Adapter.ViewHolder> {
    ArrayList<ModelLogin> acc = new ArrayList<ModelLogin>();
    Context context;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    DatabaseQueries query;
    boolean[] checked;

    int pos = -1;


    public More_settingAccount_Adapter(Context context, ArrayList<ModelLogin> mDataset) {
        this.acc = mDataset;
        checked = new boolean[mDataset.size()];
        Arrays.fill(checked, Boolean.FALSE);
        this.context = context;
        query = new DatabaseQueries(context);
        prefs = context.getSharedPreferences("login", context.MODE_PRIVATE);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.account_item, parent, false);

        return new ViewHolder(v);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public CheckBox checkBox;
        public TextView txt_title;
        public RelativeLayout btn_edit, rl_cat_layout;
        public LinearLayout click;

        public ViewHolder(View view) {
            super(view);
            this.rl_cat_layout = (RelativeLayout) view.findViewById(R.id.rl_cat_layout);
            this.checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            this.txt_title = (TextView) view.findViewById(R.id.txt_title);
            this.btn_edit = (RelativeLayout) view.findViewById(R.id.btn_edit);
            this.click = (LinearLayout) view.findViewById(R.id.click);
        }

    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.txt_title.setText(acc.get(position).company_name);
        if ((acc.get(position).prefix_name).equals(prefs.getString("prefix_name", null))) {

            checked[position] = true;
        }
        if (checked[position]) {
            holder.checkBox.setChecked(true);
            try {

                Dashboard.last_order_created = query.Select_submittedorder_id(acc.get(position).prefix_name);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("created_at", Dashboard.last_order_created);
                editor.putString("prefix_name",acc.get(position).prefix_name);
                editor.putString("shipping_cost",acc.get(position).shipping_cost);
                editor.putString("auth_code",acc.get(position).auth_code);
                editor.commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder.checkBox.setChecked(false);
        }
        if (More_Setting_Accounts.acc_edit == 1) {
            holder.btn_edit.setVisibility(View.VISIBLE);
        } else {
            holder.btn_edit.setVisibility(View.GONE);
        }
        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (acc.size() > 1) {
                    if (holder.checkBox.isChecked()) {
                        dialog("Error", "You cannot delete an account while logged in.", android.R.drawable.ic_dialog_alert, "no", "no", position);

                    } else {

                        dialog2("Confirm Account delete", "Are you you want to delete this account?", android.R.drawable.ic_dialog_alert, acc.get(position).auth_code, acc.get(position).prefix_name, position);

                    }
                } else {
                    dialog("Error", "You cannot delete an account while logged in.", android.R.drawable.ic_dialog_alert, "no", "no", position);

                }
            }
        });
        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (More_Setting_Accounts.acc_edit == 1) {

                } else {
                    Arrays.fill(checked, Boolean.FALSE);
                    Dashboard.item.clear();
                    Dashboard.categorystack.clear();
                    holder.checkBox.setChecked(true);
                    query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
//Toast.makeText(context,acc.get(position).prefix_name,Toast.LENGTH_SHORT).show();
                    editor = prefs.edit();
                    editor.putString("prefix_name", acc.get(position).prefix_name);
                    editor.putString("auth_code", acc.get(position).auth_code);
                    editor.putString("company_name", acc.get(position).company_name);
                    editor.putString("currency_symbol", acc.get(position).currency_symbol);
                    editor.putString("shipping_status", acc.get(position).shipping_status);
                    editor.putString("shipping_cost", acc.get(position).shipping_cost);
                    editor.putString("tax_label", acc.get(position).tax_label);
                    editor.putString("message_count", acc.get(position).message_count);
                    editor.putString("tax_percentage", acc.get(position).tax_percentage);
                    editor.putString("customer_business", acc.get(position).customer_business);
                    editor.commit();
                    //Dashboard.arrayList.add(0,acc.get(position).company_name);

                    notifyDataSetChanged();
                }

            }
        });
    }

    public class logout extends AsyncTask<Void, Void, Void> {
        String status = "false";
        String auth, prefix;
        int pos;

        public logout(String auth_code, String prefix_name, int position) {
            this.auth = auth_code;
            this.prefix = prefix_name;
            this.pos = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            More_Setting_Accounts.progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String responseLogOut = LogOutMethod(prefix, auth);

            System.out.println("LOGOUT Response:::" + responseLogOut);
//*************************Parsing for Logout******************************************
            try {
                JSONObject obj = new JSONObject(responseLogOut);
                status = obj.getString("Status");


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            More_Setting_Accounts.progressbar.setVisibility(View.GONE);
            if (status.equals("true")) {
                acc.remove(pos);
                Dashboard.item.clear();

                query.delete_table_data(DatabaseQueries.TABLE_LOGIN, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_GETCUSTOMER, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_ADD_DETAIL_CUSTOMER, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_GETCATEGORIES, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_GETPAGE, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_GET_PRODUCTS, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_GET_PRODUCT_IMAGE, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_GET_PRODUCT_INVENTORY, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_TIER_PRICING, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_ORDER_INFO, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_ORDER_PRODUCT, "prefix_name", prefix);
                query.delete_table_data(DatabaseQueries.TABLE_ORDER_TEMPLATE, "prefix_name", prefix);
                query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
                query.delete_table_data(DatabaseQueries.TABLE_NOTIFICATION_SETTINGS, "prefix_name", prefix);



                notifyDataSetChanged();

            } else {
                Toast.makeText(context, "Network Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //************************Webservice parameter LogOutMethod *************************

    public String LogOutMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);

        res = parser.getJSONFromUrl(Utils.logout, builder,context);
        return res;
    }

    @Override
    public int getItemCount() {
        return acc.size();
    }

    public void dialog(String title, String msg, int icon, final String auth_code, final String prefix_name, final int position) {

        new android.support.v7.app.AlertDialog.Builder(context, Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                                if (auth_code.equals("no")) {
                                } else {
                                   /* Toast.makeText(context, "logout", Toast.LENGTH_SHORT).show();
                                    acc.remove(position);
                                    notifyDataSetChanged();*/
                                    new logout(auth_code, prefix_name, position).execute();
                                }
                            }
                        }).show();
    }

    public void dialog2(String title, String msg, int icon, final String auth_code, final String prefix_name, final int position) {
        new android.support.v7.app.AlertDialog.Builder(context, Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                                if (auth_code.equals("no")) {
                                } else {
                                   /* Toast.makeText(context, "logout", Toast.LENGTH_SHORT).show();
                                    acc.remove(position);
                                    notifyDataSetChanged();*/
                                    new logout(auth_code, prefix_name, position).execute();
                                }


                            }
                        })
                .setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();


                            }
                        })

                .setIcon(icon).show();
    }
}
package Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ordertron.Add_Product;
import com.ordertron.Dashboard;
import com.ordertron.Product_detail;
import com.ordertron.R;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Model.Model_Cat_Prod;
import Model.Model_Message;
import Model.Utils;

/**
 * Created by user on 1/27/2016.
 */
public class Prod_Cat_Template_Adpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context context;
    private LayoutInflater inflater;
    public ArrayList<Model_Cat_Prod> data = new ArrayList<Model_Cat_Prod>();
    DatabaseQueries databaseQueries;
    SharedPreferences prefs;
    String avs=null;
    private static final int TYPE_CAT_HEADER = 1;
    private static final int TYPE_CAT = 2;
    private static final int TYPE_PROD_HEADER = 3;
    private static final int TYPE_PROD = 4;
    NumberFormat formatter;

    public Prod_Cat_Template_Adpter(Context context, ArrayList<Model_Cat_Prod> arrayList) {
        this.context = context;
        this.data = arrayList;
        formatter = new DecimalFormat("#0.00");
        //AllOrdersAdapter.time_order = time_order;
        databaseQueries = new DatabaseQueries(context);
        prefs = context.getSharedPreferences("login", context.MODE_PRIVATE);

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).view_type.equals("CatHeader")) {
            return TYPE_CAT_HEADER;
        } else if (data.get(position).view_type.equals("category")) {
            return TYPE_CAT;
        } else if (data.get(position).view_type.equals("ProdHeader")) {
            return TYPE_PROD_HEADER;
        } else if (data.get(position).view_type.equals("product")) {
            return TYPE_PROD;
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_PROD) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.get_product_cat, parent, false);
            return new ProdViewHolder(v);
        } else if (viewType == TYPE_CAT) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_adapter_template, parent, false);
            return new ViewHolder(v);
        } else if (viewType == TYPE_CAT_HEADER || viewType == TYPE_PROD_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            return new HeaderViewHolder(v);
        }
        return null;

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof HeaderViewHolder) {
            final HeaderViewHolder hold = (HeaderViewHolder) holder;
            if (data.get(position).view_type.equals("CatHeader")) {
                hold.txtheader.setText("Categories");
            } else if (data.get(position).view_type.equals("ProdHeader")) {
                hold.txtheader.setText("Products");
            }
        } else if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.product_cat.setText(data.get(position).category_name);

            viewHolder.lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Toast.makeText(context, "Recycle Click" + i, Toast.LENGTH_SHORT).show();
                    Utils.hidekeyboard(context);
                    String catid = data.get(position).only_cat_id;
                    String catname = data.get(position).category_name;
                    String parentcatid = data.get(position).parent_cat_id;


                    try {
                        Add_Product.mixlistt = databaseQueries.Get_Prod_Cat(prefs.getString("prefix_name", null), catid);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    if (Add_Product.mixlistt.size() != 0) {
                        Add_Product.actionbar.setTitle(catname);
                        Model_Message model = new Model_Message();
                        model.parentcatid = parentcatid;
                        model.catname = catname;
                        Add_Product.categorystack.add(model);
                        Add_Product.actionbar.setHomeButtonEnabled(true);
                        Add_Product.actionbar.setDisplayHomeAsUpEnabled(true);
                        Add_Product.getcatsame = true;
                        Add_Product.mixAdapter = new Prod_Cat_Template_Adpter(context, Add_Product.mixlistt);
                        Add_Product.recyclerview.setAdapter(Add_Product.mixAdapter);
                    } else {
                        Toast.makeText(context, "There are no products in this category.", Toast.LENGTH_SHORT).show();

                    }


                }
            });
        } else if (holder instanceof ProdViewHolder) {
            final ProdViewHolder prodHolder = (ProdViewHolder) holder;
            String name = "";
            final Model_Cat_Prod ci = data.get(position);
            name = ci.name;
            try {
                JSONArray tier = new JSONArray(ci.tier_price);
                if (tier.length() > 0) {
                    name = ci.name + " *";
                } else {
                    name = ci.name;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            prodHolder.product_name.setText(name);
            prodHolder.product_code.setText(ci.product_code);
            if (ci.spcl_price.equals("0") || ci.spcl_price == null || ci.spcl_price.equals(null)) {
                prodHolder.txt_spcl_price.setVisibility(View.GONE);
                prodHolder.price.setText(prefs.getString("currency_symbol", null)+formatter.format(Double.parseDouble(ci.unit_price)));
            } else {
                prodHolder.txt_spcl_price.setVisibility(View.VISIBLE);
                prodHolder.txt_spcl_price.setText(prefs.getString("currency_symbol", null)+formatter.format(Double.parseDouble(ci.unit_price)));
                prodHolder.price.setText(prefs.getString("currency_symbol", null)+formatter.format(Double.parseDouble(ci.spcl_price)));
                prodHolder.txt_spcl_price.setPaintFlags(prodHolder.txt_spcl_price
                        .getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }

            final String url =  ci.img;
      /*  final String product_code=ci.product_code;
        final String price=ci.unit_price;
        final String desc=ci.desc;
        final String min_order_qut=ci.min_order_qut;
        final String max_order_qut=ci.max_order_qut;
        final String order_unit_name=ci.order_unit_name;
        final String avail_stock=ci.avail_stock;
        final String spcl_price=ci.spcl_price;
        final String base_unit_name = ci.base_unit_name;
        final String taxable = ci.taxable;*/

            String q = ci.thumb_image;
            if (q != null) {
                if(!q.equals("")) {
                    q = q.replaceAll(" ", "%20"); // remove all whitespaces
                    try {
                        if (Dashboard.width == 1440 && Dashboard.height == 2560) {
                            Glide.with(context)
                                    .load(q)
                                    .placeholder(R.drawable.default_item_icon)
                                    .crossFade()
                                    .thumbnail(0.1f)
                                    .into(prodHolder.del_icon);
                           // Picasso.with(context).load(q).placeholder(R.drawable.default_item_icon).resize(150, 150).into(prodHolder.del_icon);
                        } else {
                            Glide.with(context)
                                    .load(q)
                                    .placeholder(R.drawable.default_item_icon)
                                    .crossFade()
                                    .thumbnail(0.1f)
                                    .into(prodHolder.del_icon);
                            //Picasso.with(context).load(q).placeholder(R.drawable.default_item_icon).resize(100, 100).into(prodHolder.del_icon);
                        }
                    } catch (Exception e) {

                    }
                }else{
                    Glide.with(context)
                            .load(R.drawable.default_item_icon)
                            .crossFade()
                            .thumbnail(0.1f)
                            .into(prodHolder.del_icon);
                    //Picasso.with(context).load(R.drawable.default_item_icon).resize(100, 100).into(prodHolder.del_icon);

                }
            }

            prodHolder.liner_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.hidekeyboard(context);
                    if(! ci.avail_stock.equals("No Limits")) {
                        float s1 = Float.parseFloat( ci.avail_stock);
                        avs = String.format("%.2f", s1);
                    }else {
                        avs ="No Limits";
                    }
                    Intent intent = new Intent(context, Product_detail.class);
                    intent.putExtra("Intenttype", "template");
                    intent.putExtra("name", data.get(position).name);
                    intent.putExtra("catname", data.get(position).prod_cat_name);
                    intent.putExtra("fullcatname", data.get(position).fullname);
                    intent.putExtra("image", url);
                    intent.putExtra("product_code", ci.product_code);
                    intent.putExtra("price", ci.unit_price);
                    intent.putExtra("desc", ci.desc);
                    intent.putExtra("min_order_qut", ci.min_order_qut);
                    intent.putExtra("max_order_qut", ci.max_order_qut);
                    intent.putExtra("order_unit_name", ci.order_unit_name);
                    intent.putExtra("avail_stock", avs);
                    intent.putExtra("spcl_price", ci.spcl_price);
                    intent.putExtra("productid", data.get(position).prod_id);
                    intent.putExtra("base_unit_name", ci.base_unit_name);
                    intent.putExtra("taxable", ci.taxable);
                    intent.putExtra("inventory", ci.inventory);
//                    intent.putExtra("products", Add_Product.products);
//                    intent.putExtra("qty", Add_Product.qty);
//                    intent.putExtra("productscode", Add_Product.productscode);
                    intent.putExtra("tempid", Add_Product.tempid);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra("tier_price", ci.tier_price);
                    context.startActivity(intent);

                    //Toast.makeText(context,"Get_product_catAdapter hello"+i,Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView product_cat;
        protected LinearLayout lay;

        public ViewHolder(View v) {
            super(v);

            product_cat = (TextView) v.findViewById(R.id.txt_title);
            lay = (LinearLayout) v.findViewById(R.id.lay);
        }
    }

    class ProdViewHolder extends RecyclerView.ViewHolder {
        protected LinearLayout liner_lay;
        protected ImageView del_icon;
        protected TextView product_name;
        protected TextView product_code;
        protected TextView price;
        protected TextView txt_spcl_price;
        // protected TextView product_code;
        protected LinearLayout lay;

        public ProdViewHolder(View v) {
            super(v);
            this.del_icon = (ImageView) v.findViewById(R.id.del_icon);
            this.product_name = (TextView) v.findViewById(R.id.product_name);
            this.product_code = (TextView) v.findViewById(R.id.product_code);
            this.price = (TextView) v.findViewById(R.id.price);

            this.liner_lay = (LinearLayout) v.findViewById(R.id.liner_lay);
            this.txt_spcl_price = (TextView) v.findViewById(R.id.txt_spcl_price);

        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        protected TextView txtheader;

        // protected TextView product_code;
        protected LinearLayout lay;

        public HeaderViewHolder(View v) {
            super(v);
            this.txtheader = (TextView) v.findViewById(R.id.txtheader);

        }
    }
}









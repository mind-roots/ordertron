package Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ordertron.Dashboard;
import com.ordertron.Product_detail;
import com.ordertron.R;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Fragments.ProductCategories;
import Model.Model_Cat_Prod;
import Model.Model_Message;
import Model.Utils;

/**
 * Created by user on 1/25/2016.
 */
public class Product_Cat_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context context;
    private LayoutInflater inflater;
    public ArrayList<Model_Cat_Prod> data = new ArrayList<Model_Cat_Prod>();
    DatabaseQueries databaseQueries;
    SharedPreferences prefs;
    String argument;
    private static final int TYPE_CAT_HEADER = 1;
    private static final int TYPE_CAT = 2;
    private static final int TYPE_PROD_HEADER = 3;
    private static final int TYPE_PROD = 4;
    NumberFormat formatter;
    String q;
    String avs=null;
    public static  String catid;

    public Product_Cat_Adapter(Context context, ArrayList<Model_Cat_Prod> arrayList) {
        this.context = context;
        this.data = arrayList;
        formatter = new DecimalFormat("#0.00");
        //AllOrdersAdapter.time_order = time_order;
        databaseQueries = new DatabaseQueries(context);
        prefs = context.getSharedPreferences("login", context.MODE_PRIVATE);



// for(int i=0;i<data.size();i++){
//
//
//            try {
//                if (data.get(i).name != null) {
//                    Collections.sort(data, new Comparator<Model_Cat_Prod>() {
//                        public int compare(Model_Cat_Prod v1, Model_Cat_Prod v2) {
//                            return v1.name.compareToIgnoreCase(v2.name);
//                        }
//                    });
//                }
//            }catch (Exception e){
//
//            }
//
//            System.out.println("NameADDA=="+Dashboard.mixlist.get(i).name);
//        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).view_type.equals("CatHeader")) {
            return TYPE_CAT_HEADER;
        } else if (data.get(position).view_type.equals("category")) {
            return TYPE_CAT;
        } else if (data.get(position).view_type.equals("ProdHeader")) {
            return TYPE_PROD_HEADER;
        } else if (data.get(position).view_type.equals("product")) {
            return TYPE_PROD;
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_PROD) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.get_product_cat, parent, false);
            return new ProdViewHolder(v);
        } else if (viewType == TYPE_CAT) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_adapter_template, parent, false);
            return new ViewHolder(v);
        } else if (viewType == TYPE_CAT_HEADER || viewType == TYPE_PROD_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            return new HeaderViewHolder(v);
        }
        return null;

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof HeaderViewHolder) {
            final HeaderViewHolder hold = (HeaderViewHolder) holder;
            if (data.get(position).view_type.equals("CatHeader")) {
                hold.txtheader.setText("Categories");
            } else if (data.get(position).view_type.equals("ProdHeader")) {
                hold.txtheader.setText("Products");
            }
        } else if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.product_cat.setText(data.get(position).category_name);

            viewHolder.lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Toast.makeText(context, "Recycle Click" + i, Toast.LENGTH_SHORT).show();



                    try {
                        Utils.hidekeyboard(context);
                        String catname = data.get(position).category_name;
                        String parentcatid = data.get(position).parent_cat_id;

                        catid = data.get(position).only_cat_id;
//                        if(Dashboard.mixlist.size()==0){
//
//                        }
//                        Dashboard.mixlist22=databaseQueries.Get_Prod_Cat(prefs.getString("prefix_name", null), catid);
//                        if (Dashboard.mixlist22.size()>0) {
                            Dashboard.mixlist = databaseQueries.Get_Prod_Cat(prefs.getString("prefix_name", null), catid);
//                        }else{
//                            Toast.makeText(context, "There are no products in this category.", Toast.LENGTH_SHORT).show();
//
//                        }

                    if (Dashboard.mixlist.size() != 0) {
                        ProductCategories.recyclerview.setVisibility(View.VISIBLE);
                        ProductCategories.noproducts.setVisibility(View.GONE);
                        Dashboard.actionbar.setTitle(catname);
                        Model_Message model = new Model_Message();
                        model.parentcatid = parentcatid;
                        model.catname = catname;
                        Dashboard.categorystack.add(model);
                        Dashboard.actionbar.setHomeButtonEnabled(true);
                        Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);
                        Dashboard.getcatsame = true;
                        ProductCategories.get_catagoryAdapter = new Product_Cat_Adapter(context, Dashboard.mixlist);
                        ProductCategories.recyclerview.setAdapter(ProductCategories.get_catagoryAdapter);
                    } else {
                        Toast.makeText(context, "There are no products in this category.", Toast.LENGTH_SHORT).show();
                        //String dd=data.get(position-1).only_cat_id;
                        catid=parentcatid;

//                        ProductCategories.recyclerview.setVisibility(View.GONE);
//                        ProductCategories.noproducts.setVisibility(View.VISIBLE);
             /* Intent intent = new Intent(context, Product_Cat.class);
                        intent.putExtra("categoryid", catid);
                        intent.putExtra("catagory_name", catname);
                        context.startActivity(intent);*/
                    }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } else if (holder instanceof ProdViewHolder) {
            final ProdViewHolder prodHolder = (ProdViewHolder) holder;
            String name = "";

            final Model_Cat_Prod ci = data.get(position);
            name = ci.name;
            try {
                //JSONArray tier = new JSONArray(ci.tier_price);
                JSONArray tier = new JSONArray(data.get(position).tier_price);


                if (tier.length() > 0) {
                    name = ci.name + " *";
                } else {
                    name = ci.name;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            prodHolder.product_name.setText(name);
            prodHolder.product_code.setText(ci.product_code);
            if (ci.spcl_price.equals(" ")||ci.spcl_price.equals("")||ci.spcl_price.equals("0") || ci.spcl_price == null || ci.spcl_price.equals(null)) {
                prodHolder.txt_spcl_price.setVisibility(View.GONE);
                prodHolder.price.setText(prefs.getString("currency_symbol", null)+formatter.format(Double.parseDouble(ci.unit_price)));
            } else {
                prodHolder.txt_spcl_price.setVisibility(View.VISIBLE);
                prodHolder.txt_spcl_price.setText(prefs.getString("currency_symbol", null)+formatter.format(Double.parseDouble(ci.unit_price)));
                prodHolder.price.setText(prefs.getString("currency_symbol", null)+formatter.format(Double.parseDouble(ci.spcl_price)));
                prodHolder.txt_spcl_price.setPaintFlags(prodHolder.txt_spcl_price
                        .getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }

            final String url =  ci.img;
      /*  final String product_code=ci.product_code;
        final String price=ci.unit_price;
        final String desc=ci.desc;
        final String min_order_qut=ci.min_order_qut;
        final String max_order_qut=ci.max_order_qut;
        final String order_unit_name=ci.order_unit_name;
        final String avail_stock=ci.avail_stock;
        final String spcl_price=ci.spcl_price;
        final String base_unit_name = ci.base_unit_name;
        final String taxable = ci.taxable;*/

             q = ci.thumb_image;
            System.out.println("Imageddd  :"+q);
            if (q != null) {
                if (!q.equals("")) {
                    q = q.replaceAll(" ", "%20"); // remove all whitespaces

                    try {
                        if (Dashboard.width == 1440 && Dashboard.height == 2560) {
                            Glide.with(context)
                                    .load(q)
                                    .placeholder(R.drawable.default_item_icon)
                                    .crossFade()
                                    .thumbnail(0.1f)
                                    .into(prodHolder.del_icon);
                            // Picasso.with(context).load(q).placeholder(R.drawable.default_item_icon).resize(100,100).centerCrop().into(prodHolder.del_icon);
                        } else {

                            Glide.with(context)
                                    .load(q)
                                    .placeholder(R.drawable.default_item_icon)
                                    .crossFade()
                                    .thumbnail(0.1f)
                                    .into(prodHolder.del_icon);
                            // Picasso.with(context).load(q).placeholder(R.drawable.default_item_icon).resize(100,100).into(prodHolder.del_icon);
                        }
                    } catch (Exception e) {

                    }
                }
            }else{
                Glide.with(context)
                        .load(R.drawable.default_item_icon)
                        .crossFade()
                        .thumbnail(0.1f)
                        .into(prodHolder.del_icon);
            }

         /*   Glide.with(context)
                    .load("http://appsglory.com.au/uploads/" + q)
                    .placeholder(R.drawable.default_item_icon)
                    .crossFade()
                    .thumbnail(0.1f)
                    .into(prodHolder.del_icon);*/
            prodHolder.liner_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!data.get(position).avail_stock.equals("No Limits")) {
                        float s1 = Float.parseFloat(data.get(position).avail_stock);
                         avs = String.format("%.2f", s1);
                    }else {
                         avs ="No Limits";
                    }
                    Utils.hidekeyboard(context);
                    Intent intent = new Intent(context, Product_detail.class);
                    intent.putExtra("Intenttype", "products");
                    intent.putExtra("catname", data.get(position).prod_cat_name);
                    intent.putExtra("fullcatname", data.get(position).fullname);
                    intent.putExtra("name", data.get(position).name);
                    intent.putExtra("image", url);
                    //intent.putExtra("thumb_image", q);
                    intent.putExtra("product_code", data.get(position).product_code);
                    intent.putExtra("price", data.get(position).unit_price);
                    intent.putExtra("desc", data.get(position).desc);
                    intent.putExtra("min_order_qut", data.get(position).min_order_qut);
                    intent.putExtra("max_order_qut", data.get(position).max_order_qut);
                    intent.putExtra("order_unit_name", data.get(position).order_unit_name);
                    intent.putExtra("avail_stock", avs);
                    intent.putExtra("spcl_price", data.get(position).spcl_price);
                    intent.putExtra("productid", data.get(position).prod_id);
                    intent.putExtra("base_unit_name", data.get(position).base_unit_name);
                    intent.putExtra("taxable", data.get(position).taxable);
                    intent.putExtra("inventory", data.get(position).inventory);
//                    intent.putExtra("products", Product_Cat.products);
//                    intent.putExtra("qty", Product_Cat.qty);
//                    intent.putExtra("productscode", Product_Cat.productscode);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra("tier_price", data.get(position).tier_price);
                    context.startActivity(intent);

                    //Toast.makeText(context,"Get_product_catAdapter hello"+i,Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView product_cat;
        protected LinearLayout lay;

        public ViewHolder(View v) {
            super(v);

            product_cat = (TextView) v.findViewById(R.id.txt_title);
            lay = (LinearLayout) v.findViewById(R.id.lay);
        }
    }

    class ProdViewHolder extends RecyclerView.ViewHolder {
        protected LinearLayout liner_lay;
        protected ImageView del_icon;
        protected TextView product_name;
        protected TextView product_code;
        protected TextView price;
        protected TextView txt_spcl_price;
        // protected TextView product_code;
        protected LinearLayout lay;

        public ProdViewHolder(View v) {
            super(v);
            this.del_icon = (ImageView) v.findViewById(R.id.del_icon);
            this.product_name = (TextView) v.findViewById(R.id.product_name);
            this.product_code = (TextView) v.findViewById(R.id.product_code);
            this.price = (TextView) v.findViewById(R.id.price);

            this.txt_spcl_price = (TextView) v.findViewById(R.id.txt_spcl_price);
            this.liner_lay = (LinearLayout) v.findViewById(R.id.liner_lay);

        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        protected TextView txtheader;

        // protected TextView product_code;
        protected LinearLayout lay;

        public HeaderViewHolder(View v) {
            super(v);
            this.txtheader = (TextView) v.findViewById(R.id.txtheader);

        }
    }
}








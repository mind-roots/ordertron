package Adapters;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ordertron.R;

import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Model.Model_Message;
import Model.Utils;

/**
 * Created by Abhijeet on 1/29/2016.
 */
public class MessageAdap extends RecyclerView.Adapter<MessageAdap.ViewHolder> {

    private Context activity;
    SharedPreferences prefs;
    String prefix_name;
    String timezone;
    DatabaseQueries databaseQueries;
    public ArrayList<Model_Message> itemmsg = new ArrayList<Model_Message>();

    public MessageAdap(Context context, ArrayList<Model_Message> arrayList) {
        this.activity = context;
        this.itemmsg = arrayList;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        prefs = activity.getSharedPreferences("login", activity.MODE_PRIVATE);
        prefix_name = prefs.getString("prefix_name", null);
        databaseQueries = new DatabaseQueries(activity);
    }


    @Override
    public int getItemCount() {
        return itemmsg.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
       // Model_Order_Info template = AllOrdersApapter.list.get(i);
        prefix_name = prefs.getString("prefix_name", null);
        timezone = databaseQueries.get_timezone_setting(prefix_name);
        viewHolder.tmessage_text.setText(itemmsg.get(i).message_text);
      //viewHolder.tsent_date.setText(itemmsg.get(i).sent_date);
        String  s=itemmsg.get(i).sent_date;
        String t=timezone;
       viewHolder.tsent_date.setText(Utils.timezone(s,timezone,"MMM dd, yyyy hh:mm a"));
        viewHolder.twholesaler_name.setText("From: "+itemmsg.get(i).wholesaler_name);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.message_adapter, viewGroup, false);

        return new ViewHolder(itemView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView tmessage_text, tsent_date, twholesaler_name;
        // protected TextView product_code;
        protected LinearLayout lay;

        public ViewHolder(View v) {
            super(v);
            tmessage_text = (TextView) v.findViewById(R.id.message_text);
            tsent_date = (TextView) v.findViewById(R.id.sent_date);
            twholesaler_name = (TextView) v.findViewById(R.id.wholesaler_name);
        }
    }


}


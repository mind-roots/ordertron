package Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ordertron.Dashboard;
import com.ordertron.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import DataBaseUtils.TabUtils;
import Fragments.CartActivity;
import Model.ModelLogin;
import Model.Model_Add_To_Cart;
import Model.Utils;

import static Fragments.CartActivity.newmenu;

/**
 * Created by admin on 1/6/2016.
 */
public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    public static ArrayList<Model_Add_To_Cart> products;
     ArrayList<ModelLogin> login11=new ArrayList<>();
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    String Currency_symbol, tax_percentage, prefix_name;
    String prodtax;
    double prodtotalprice, original_total;
    public static ArrayList<Model_Add_To_Cart> price_tier = new ArrayList<>();
    private static final int TYPE_ITEM = 1;
    public static int TYPE_FOOTER = 2;
    DatabaseQueries query;
    String Tax_label, Shipping_cost;
    public static String sendtotal, Shipping_Status;
    double apl = 0, baseprice = 0, tiervalue = 0, spclvalue = 0,gstvalue;
    float min, max, enter,gstvvalue;
    NumberFormat formatter;
    EditText input;
    float ordertotal1 = 0;
    ViewHolder viewHolder;

    public CartAdapter(Context con, ArrayList<Model_Add_To_Cart> products) {
        this.context = con;
        this.products = products;
        query = new DatabaseQueries(con);
        prefs = con.getSharedPreferences("login", con.MODE_PRIVATE);
        Currency_symbol = prefs.getString("currency_symbol", null);
        tax_percentage = prefs.getString("tax_percentage", null);
        Tax_label = prefs.getString("tax_label", null);
     //  Shipping_cost = prefs.getString("shipping_cost", null);

        Shipping_Status = prefs.getString("shipping_status", null);
        prefix_name = prefs.getString("prefix_name", null);
        try {
            login11 =query.GetLogin(prefix_name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(int i=0;i<login11.size();i++){
            Shipping_cost=login11.get(i).shipping_cost;
        }
        formatter = new DecimalFormat("#0.00");
        if (Dashboard.cartupdate) {
            for (int position = 0; position < products.size(); position++) {

                tierLogic(products.get(position).prod_id, products.get(position).qty, products.get(position).spcl_price, products.get(position).price, products.get(position).tier_price, products.get(position).taxable, products.get(position).order_unit, products.get(position).base_unit, position);
//            Model_Add_To_Cart product_total=new Model_Add_To_Cart();
//           product_total.product_total= String.valueOf(prodtotalprice);
//           product_total.original_totl= String.valueOf(original_total);
            }
            Dashboard.cartupdate = false;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_footer, parent, false);


            return new FooterViewHolder(view);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item_view, parent, false);
            return new ViewHolder(v);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        //       int  pos = position;
        //       int  sixe = products.size();
        //String valrrtt=products.get(position-1).tier_price;

        if (holder instanceof ViewHolder) {
            viewHolder = (ViewHolder) holder;
            if (CartActivity.showedit == 0) {
                viewHolder.rl_delete.setVisibility(View.GONE);
                if (Dashboard.tabLayout.getSelectedTabPosition() == 2) {
                    try {
                        newmenu.findItem(R.id.edit).setVisible(true);
                        newmenu.findItem(R.id.clear).setVisible(true);
                        newmenu.findItem(R.id.done).setVisible(false);
                    } catch (Exception e) {

                    }
                }
            } else {
                viewHolder.rl_delete.setVisibility(View.VISIBLE);
            }
            final Model_Add_To_Cart item = products.get(position);
            viewHolder.product_name.setText(item.prod_name);
            viewHolder.product_id.setText(item.prod_sku);
//            if (item.spcl_price.equals("0") || item.spcl_price.equals(null) || item.spcl_price == "0") {
//                viewHolder.product_unit_price.setText("Unit price:  "+Currency_symbol + item.price + "/" + item.base_unit);
//            } else {
//                viewHolder.product_unit_price.setText("Unit price:  "+Currency_symbol + item.spcl_price + "/" + item.base_unit);
//            }
            viewHolder.product_qty.setText("Qty:  " + formatter.format(Double.parseDouble(item.qty)) + " " + item.order_unit);
            String prodimage = item.prod_image;
            System.out.println("image value" + item.prod_image);
            tierLogic(products.get(position).prod_id, products.get(position).qty, products.get(position).spcl_price, products.get(position).price, products.get(position).tier_price, products.get(position).taxable, products.get(position).order_unit, products.get(position).base_unit, position);

            viewHolder.product_unit_price.setText("Unit price:  " + Currency_symbol + formatter.format(Double.parseDouble(item.price)) + "/" + item.base_unit);
            viewHolder.product_apl_price.setText("Apl. price:  " + Currency_symbol + formatter.format(apl) + "/" + item.base_unit);
            Log.e("image value", "" + prodimage);
            if (prodimage != null) {
                if (!prodimage.equals("")) {
                    prodimage = prodimage.replaceAll(" ", "%20"); // remove all whitespaces
                    try {
                        //  Picasso.with(context).load(prodimage).placeholder(R.drawable.default_item_icon).resize(144, 144).into(viewHolder.product_image);

                        if (Dashboard.width == 1440 && Dashboard.height == 2560) {
                            Glide.with(context)
                                    .load(prodimage)
                                    .placeholder(R.drawable.default_item_icon)
                                    .crossFade()
                                    .thumbnail(0.1f)
                                    .into(viewHolder.product_image);
                            // Picasso.with(context).load(prodimage).placeholder(R.drawable.default_item_icon).resize(192, 192).into(viewHolder.product_image);
                        } else {
                            Glide.with(context)
                                    .load(prodimage)
                                    .placeholder(R.drawable.default_item_icon)
                                    .crossFade()
                                    .thumbnail(0.1f)
                                    .into(viewHolder.product_image);
                            //  Picasso.with(context).load(prodimage).placeholder(R.drawable.default_item_icon).resize(100, 100).into(viewHolder.product_image);

                        }

                    } catch (Exception e) {

                    }
                } else {

                    Glide.with(context)
                            .load(R.drawable.default_item_icon)
                            .crossFade()
                            .thumbnail(0.1f)
                            .into(viewHolder.product_image);
                    // Picasso.with(context).load(R.drawable.default_item_icon).resize(144, 144).into(viewHolder.product_image);

                }
            }


            //-----------------set total values----------------------//
            if (!(item.order_unit.equals(item.base_unit))) {

                viewHolder.product_total_price.setText("TBA");
                viewHolder.product_deprected_price.setText("");
                viewHolder.product_apl_price.setText("Apl. price:  TBA");
            } else {

                viewHolder.product_apl_price.setText("Apl. price:  " + Currency_symbol + formatter.format(apl) + "/" + item.base_unit);
                if (prodtotalprice == original_total) {
                    viewHolder.product_total_price.setText(Currency_symbol + formatter.format(prodtotalprice));

                    viewHolder.product_deprected_price.setText("");
                } else {
                    viewHolder.product_total_price.setText(Currency_symbol + formatter.format(prodtotalprice));
                    viewHolder.product_deprected_price.setText(Currency_symbol + formatter.format(original_total));
                    viewHolder.product_deprected_price.setPaintFlags(viewHolder.product_deprected_price
                            .getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }


            }

            viewHolder.rl_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog(item.prod_id, position);
                }
            });
            viewHolder.ll_mainclk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CartActivity.showedit == 0) {
                        //  viewHolder.rl_delete.setVisibility(View.GONE);

                        //  popdialog(products.get(pos - 1).qty, products.get(pos - 1).prod_id, products.get(pos - 1).max_qty, products.get(pos - 1).min_qty, products.get(pos - 1).inventory, pos - 1);
                        //  popdialog(products.get(position).qty, products.get(position).prod_id, products.get(position).max_qty, products.get(position).min_qty, products.get(position).inventory);


                    } else {

                        popdialog(products.get(position).qty, products.get(position).prod_id, products.get(position).max_qty, products.get(position).min_qty, products.get(position).inventory);

/*
                        Intent intent = new Intent(con, Pop_Window_Quantity.class);
                        intent.putExtra("type", "cart");
                        intent.putExtra("qty", item.qty);
                        intent.putExtra("pid", item.prod_id);
                        intent.putExtra("maxqty", item.max_qty);
                        intent.putExtra("minqty", item.min_qty);
                        intent.putExtra("inventory", item.inventory);

                        con.startActivity(intent);*/
                    }


                }
            });
        } else if (holder instanceof FooterViewHolder) {
            float ordertotal = 0;
            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            if (Shipping_Status.equals("2")) {
                footerHolder.gst_text.setText(Tax_label);
                footerHolder.shippingCost_text.setText("TBA");

            } else {
                footerHolder.gst_text.setText(Tax_label);
                footerHolder.shippingCost_text.setText(Currency_symbol + formatter.format(Double.parseDouble(Shipping_cost)));

            }

            String total = query.Setvalues();
            String taxorgst = query.Setvalues222();

    //*************Appling Gst or Tax only on those products who are taxable and have total************
            // ***********Also gst is TBA if shipping status is '2' i.e. specify on order process*******
            try {
           //if(taxorgst.equals("TBA")|| taxorgst.equals("0.0:0.0")){
            if(taxorgst.equals("TBA")){
                footerHolder.gst_value_txt.setText("TBA");
            }else{
                String[] str22 = taxorgst.split(":");
                float sub22 = round((Float.parseFloat(str22[0])), 2);
                String s122 = String.format("%.2f", sub22);

                if (Shipping_Status.equals("2")) {
                    footerHolder.gst_value_txt.setText("TBA");
                } else {
                    if(Shipping_cost.equals("")){
                        Shipping_cost="0";
                    }
                    //footerHolder.gst_value_txt.setText(Currency_symbol + formatter.format(Double.parseDouble(stt)));
                    footerHolder.gst_value_txt.setText(Currency_symbol + formatter.format( round1(((Double.parseDouble(s122) + Double.parseDouble(Shipping_cost)) * Double
                            .parseDouble(tax_percentage)) / 100, 2)));
                    gstvalue=round1(((Double.parseDouble(s122) + Double.parseDouble(Shipping_cost)) * Double
                            .parseDouble(tax_percentage)) / 100, 2);
                    //gstvvalue=Float.parseFloat(gstvalue);
                }
            }} catch (Exception e) {
                e.printStackTrace();
            }
    //**********************************************************************************************

            try {
                footerHolder.txt_prod_count.setText(String.valueOf(products.size()));
                if (total.equals("TBA") || total.equals("0.0:0.0")) {
                    footerHolder.sub_total_text.setText("TBA");

                    footerHolder.total_order_text.setText("TBA");

                    sendtotal = "TBA";
                } else {

                    String[] str = total.split(":");

                    float sub = round((Float.parseFloat(str[0])), 2);
                    String s1 = String.format("%.2f", sub);




                    footerHolder.sub_total_text.setText(Currency_symbol + s1);


                    float g = round((Float.parseFloat(str[1])), 2);

                    String stt = String.format("%.2f", g);


                    String[] str22 = taxorgst.split(":");
                    float sub22 = round((Float.parseFloat(str22[0])), 2);
                    String s122 = String.format("%.2f", sub22);

                    //Status based;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


                    // double a= Math.floor(Float.parseFloat(str[0]) * 100) / 100;
                    float a = round((Float.parseFloat(str[0])), 2);
                    //double b=Math.floor(Float.parseFloat(str[1]) * 100) / 100;

                    float b = round((Float.parseFloat(str[1])), 2);
                    // double c=Math.floor(Float.parseFloat(Shipping_cost) * 100) / 100;
                    float c = round(Float.parseFloat(Shipping_cost), 2);

                    String p = String.format("%.2f", a);
                    String q = String.format("%.2f", b);
                    String r = String.format("%.2f", c);

                   // ordertotal = Float.parseFloat(p) + Float.parseFloat(s122) + Float.parseFloat(r);
                    ordertotal = Float.parseFloat(s1) +  Float.parseFloat(Shipping_cost) +round((( Float.parseFloat(s122) +  Float.parseFloat(Shipping_cost)) *  Float.parseFloat(tax_percentage)) / 100, 2);
                    String st = String.format("%.2f", ordertotal);
                    sendtotal = st;
                    //Status based;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    if (Shipping_Status.equals("2")) {
                        footerHolder.total_order_text.setText("TBA");
                    } else {
                        footerHolder.total_order_text.setText(Currency_symbol + formatter.format(Double.parseDouble(sendtotal)));

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    private void popdialog(final String qty, final String prod_id, final String max_qty, final String min_qty, final String inventory) {
        min = 0;
        max = 0;
        enter = 0;

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, Dashboard.style);
        builder.setTitle("Edit Quantity");
        builder.setMessage("Edit the Quantity and tap on Update");
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.edittext, null);
        input = (EditText) view
                .findViewById(R.id.etCategory);
        input.requestFocus();

        input.setHint("Enter Quantity");
        input.setText(formatter.format(Double.parseDouble(qty)));


        // input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        // input.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
        // input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
        input.setSelection(qty.length());

        input.setTextColor(Color.BLACK);
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String enterqty = input.getText().toString();
                if (inventory.equals("1")) {
                    String availstock = query.getinvent(prod_id, prefix_name);
                    if (availstock != null && (Float.parseFloat(enterqty) > Float.parseFloat(availstock))) {
                        Toast.makeText(context,
                                "The ordered product quantity is more than the available stock.", Toast.LENGTH_SHORT).show();
                    } else {
                        updatecart(prod_id, enterqty, min_qty, max_qty);

                    }
                } else {
                    updatecart(prod_id, enterqty, min_qty, max_qty);
                }
                hidekeyboard();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                hidekeyboard();
                dialog.dismiss();
            }
        });
        builder.show();

    }

    public static double round1(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static float round(float value, int places) {
        //here 2 means 2 places after decimal
        long factor = (long) Math.pow(10, 2);
        value = value * factor;
        long tmp = Math.round(value);
        return (float) tmp / factor;
    }

    private void updatecart(String prod_id, String enterqty, String min_qty, String max_qty) {

        try {
            min = Float.parseFloat(min_qty);
            if (max_qty.length() != 0) {
                max = Float.parseFloat(max_qty);
            }
            enter = Float.parseFloat(enterqty);
            enter = Utils.round(enter, 2);
            enterqty = String.valueOf(enter);
        } catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }

        if ((enter < min)) {
            Toast.makeText(context, "The ordered product quantity is less than the minimum allowed cart quantity.", Toast.LENGTH_LONG).show();
        } else if (enter > max && max != 0) {
            Toast.makeText(context, "The ordered product quantity is more than the maximum allowed cart quantity.", Toast.LENGTH_LONG).show();

        } else {
            Dashboard.cartupdate = true;
            CartActivity.qtyupdate = true;
            query.updatecartqty(prod_id, enterqty);


            try {
                products = query.GetProductFromCart(prefs.getString("prefix_name", null));
            } catch (Exception e) {
                e.printStackTrace();
            }
            notifyDataSetChanged();

        }
    }

    private void dialog(final String psku, final int position) {
        new AlertDialog.Builder(context, Dashboard.style)
                .setTitle("Delete entry")
                .setMessage("Are you sure you want to delete this entry?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                        if (products.size() > 0) {
                            notifyDataSetChanged();
                            query.delete_value(DatabaseQueries.TABLE_ADD_TO_CART, DatabaseQueries.PROD_ID, psku);
                            products.remove(position);
                            TabUtils.updateTabBadge(Dashboard.cart, products.size());
                            if (products.size() == 0) {
                                newmenu.findItem(R.id.done).setVisible(false);
                                newmenu.findItem(R.id.clear).setVisible(false);
                                CartActivity.nocart.setVisibility(View.VISIBLE);
                                CartActivity.ll_main_layout.setVisibility(View.GONE);
                            }
                        }

                        //  query.deletecartitem(pid);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public Model_Add_To_Cart tierLogic(String prod_id, String qty, String spcl_price, String price, String tier_price, String taxable, String order_unit, String base_unit, int position) {
        prodtotalprice = 0;
        original_total = 0;
        tiervalue = 0;
        spclvalue = 0;
        baseprice = 0;
        apl = 0;
        prodtax = "0";
        price_tier.clear();
        try {
            apl = 0;
            if (tier_price != null) {
                if (tier_price.length() != 0) {
                    JSONArray array5 = new JSONArray(tier_price);
                    for (int i = 0; i < array5.length(); i++) {

                        JSONObject obj2 = array5.getJSONObject(i);
                        String quantity_upto = obj2.getString("quantity_upto");
                        String discount_type = obj2.getString("discount_type");
                        String discount = obj2.getString("discount");

                        Model_Add_To_Cart model = new Model_Add_To_Cart();

                        model.quantity_upto = quantity_upto;
                        model.discount_type = discount_type;
                        model.discount = discount;

                        price_tier.add(model);
                    }
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // -----------------calculation-----------------//
//        =0;
//        if(price_tier.get(position).discount_type.equals("percentage")) {
//
//        }else  if(price_tier.get(position).discount_type.equals("fixed")){
//
//            calcicost = Double.parseDouble(price_tier.get(position).discount);
//            baseprice = Double.parseDouble(price_tier.get(position).discount);
//        }

        double calcicost = Double.parseDouble(price);
        baseprice = Double.parseDouble(price);
        if (spcl_price.equals("")) {
            spcl_price = "0";
        }
        if (price_tier.size() > 0 && spcl_price.equals(null) || (spcl_price.equals("0"))) {
            for (int i = 0; i < price_tier.size(); i++) {

                double qty_upto = Double.parseDouble(price_tier.get(i).quantity_upto);

                if (Double.parseDouble(qty) >= qty_upto) {
                    if (price_tier.get(i).discount_type.equals("percentage")) {
                        double discount = (calcicost * (Double.parseDouble(price_tier.get(i).discount))) / 100;
                        calcicost = calcicost - discount;
                        tiervalue = calcicost;
                    } else {
                        double discount = Double.parseDouble(price_tier.get(i).discount) * Double.parseDouble(qty);
                        calcicost = Double.parseDouble(price_tier.get(i).discount);
                        tiervalue = calcicost;

                    }
                    break;

                } else {

                }
            }
        } else {
            //***********Calculate tier price if not empty*****************************
            if (price_tier.size() > 0 && spcl_price != null || (!spcl_price.equals("0"))) {
                for (int i = 0; i < price_tier.size(); i++) {

                    double qty_upto = Double.parseDouble(price_tier.get(i).quantity_upto);

                    if (Double.parseDouble(qty) >= qty_upto) {
                        if (price_tier.get(i).discount_type.equals("percentage")) {
                            double discount = (calcicost * (Double.parseDouble(price_tier.get(i).discount))) / 100;
                            calcicost = calcicost - discount;
                            tiervalue = calcicost;
                        } else {
                            double discount = Double.parseDouble(price_tier.get(i).discount) * Double.parseDouble(qty);
                            calcicost = discount;
                            tiervalue = Double.parseDouble(price_tier.get(i).discount);

                        }
                        break;

                    } else {

                    }
                }
            }


            // price.setText(dSpclprice);
            if (spcl_price.equals(" ") || spcl_price.equals("0") || spcl_price.equals("0") || spcl_price.equals(null) || spcl_price == "0" || spcl_price.length() == 0) {
                calcicost = Double.parseDouble(price);
                spclvalue = calcicost;
            } else {
                calcicost = Double.parseDouble(spcl_price);
                spclvalue = calcicost;
            }
        }

        //***********find minimum among Base , Tier and special value
        if (tiervalue != 0 & spclvalue != 0) {
            calcicost = min(baseprice, tiervalue, spclvalue);
        } else if (tiervalue == 0 && spclvalue != 0) {
            if (baseprice < spclvalue) {
                calcicost = baseprice;
            } else {
                calcicost = spclvalue;
            }
        } else if (tiervalue != 0 && spclvalue == 0) {
            if (baseprice < tiervalue) {
                calcicost = baseprice;
            } else {
                calcicost = tiervalue;
            }
        } else {
            calcicost = baseprice;
        }


        //     prod_calci_cost = String.valueOf(calcicost);


        prodtotalprice = (Double
                .parseDouble(qty) * calcicost);
        original_total = (Double
                .parseDouble(qty) * Double
                .parseDouble(price));
        //
        apl = prodtotalprice / Double
                .parseDouble(qty);

        String prod_cal_price = String.valueOf(prodtotalprice);

        if (taxable.equals("1")) {
            try {
                double shipcost = Double.parseDouble(Shipping_cost);
                double taxcal = ((prodtotalprice + shipcost) * Double
                        .parseDouble(tax_percentage)) / 100;
//                double taxcal = (prodtotalprice * Double
//                        .parseDouble(tax_percentage)) / 100;

                prodtax = String.valueOf(taxcal);
            } catch (Exception e) {

            }
            if (order_unit.equals(base_unit)) {
                try {
                    query.updatecartprodt(prod_id, prodtax, prod_cal_price);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    query.updatecartprodt(prod_id, prodtax, "TBA");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // CartActivity.subtax.add(cprodtax);

        } else {
            if (order_unit.equals(base_unit)) {
                prodtax = "0";
                try {

                    query.updatecartprodt(prod_id, prodtax, prod_cal_price);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //  CartActivity.subtax.add(cprodtax);

            } else {

                prodtax = "0";
                prod_cal_price = "TBA";
                try {
                    query.updatecartprodt(prod_id, prodtax, prod_cal_price);
                    // query.updatecartprodt(prod_id, "1", "22");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //CartActivity.subtax.add(cprodtax);

            }
        }
        return null;
    }

    //******************Function to find Min value among three values *****************************

    public static double min(double a, double b, double c) {
        return Math.min(Math.min(a, b), c);
    }


    @Override
    public int getItemCount() {
        return products.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionFooter(int position) {
        return position == products.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView product_name, product_id, product_unit_price, product_qty, product_deprected_price, product_total_price, product_apl_price;
        ImageView product_image;
        RelativeLayout rl_delete;
        LinearLayout ll_mainclk;

        public ViewHolder(View view) {
            super(view);
            ll_mainclk = (LinearLayout) view.findViewById(R.id.ll_mainclk);
            rl_delete = (RelativeLayout) view.findViewById(R.id.rl_delete);
            product_name = (TextView) view.findViewById(R.id.product_name);
            product_id = (TextView) view.findViewById(R.id.product_id);
            product_unit_price = (TextView) view.findViewById(R.id.product_unit_price);
            product_qty = (TextView) view.findViewById(R.id.product_qty);
            product_deprected_price = (TextView) view.findViewById(R.id.product_deprected_price);
            product_total_price = (TextView) view.findViewById(R.id.product_total_price);
            product_image = (ImageView) view.findViewById(R.id.product_image);
            product_apl_price = (TextView) view.findViewById(R.id.product_apl_price);
        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {
        public static TextView sub_total_text, gst_text, shippingCost_text, total_order_text, gst_value_txt, txt_prod_count;

        public FooterViewHolder(View itemView) {
            super(itemView);

            this.sub_total_text = (TextView) itemView.findViewById(R.id.sub_total_text);
            this.gst_text = (TextView) itemView.findViewById(R.id.gst_text);
            this.gst_value_txt = (TextView) itemView.findViewById(R.id.gst_value_txt);
            this.shippingCost_text = (TextView) itemView.findViewById(R.id.shippingCost_text);
            this.total_order_text = (TextView) itemView.findViewById(R.id.total_order_text);
            this.txt_prod_count = (TextView) itemView.findViewById(R.id.txt_prod_count);
        }
    }

    private void hidekeyboard() {
        // TODO Auto-generated method stub

        if (input != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
        }
    }
}

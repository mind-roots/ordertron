package Adapters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import Fragments.CartActivity;
import Fragments.HomeActivity;
import Fragments.MoreActivity;
import Fragments.ProductCategories;
import Fragments.TemplatesActivity;

/**
 * Created by android on 12/4/2015.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Context context;

    public PagerAdapter(FragmentManager fm, int NumOfTabs, Context con) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.context = con;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {

        switch (position) {
            case 0:
                HomeActivity homeActivity = new HomeActivity();
                return homeActivity;
            case 1:
                ProductCategories ProductCategories = new ProductCategories();
                return ProductCategories;
            case 2:
                CartActivity cartActivity = new CartActivity();
                return cartActivity;
            case 3:
                TemplatesActivity templatesActivity = new TemplatesActivity();
                return templatesActivity;
            case 4:
                MoreActivity moreActivity = new MoreActivity();
                return moreActivity;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}



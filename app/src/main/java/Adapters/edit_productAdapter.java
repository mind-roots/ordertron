package Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ordertron.Dashboard;
import com.ordertron.R;

import java.util.ArrayList;

import Model.Model_Order_Product;
import Model.Utils;

/**
 * Created by Abhijeet on 12/18/2015.
 */
public class edit_productAdapter extends BaseAdapter {

    SharedPreferences prefs;
    public Context context;
    private LayoutInflater inflater;
    public ArrayList<Model_Order_Product> data = new ArrayList<Model_Order_Product>();
    static ArrayList<String> order = new ArrayList<String>();
    ImageView img_icon;
    ArrayList<String> u_name = new ArrayList<String>();
    ArrayList<String> status = new ArrayList<String>();
    public static ImageView icon;
    String id_pos, visibility;

public edit_productAdapter(Context context, ArrayList<Model_Order_Product> arrayList, String gone) {
        this.context = context;
        this.data = arrayList;
        this.visibility = gone;
        prefs = context.getSharedPreferences("login", context.MODE_PRIVATE);
        //AllOrdersAdapter.time_order = time_order;
        this.u_name = u_name;
        this.status = status;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        if (inflater == null)
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.edit_productadapter, null);

        TextView product_name = (TextView) convertView.findViewById(R.id.product_name);
        TextView product_code = (TextView) convertView.findViewById(R.id.product_code);
        TextView unit_price = (TextView) convertView.findViewById(R.id.unit_price);
        TextView quantity = (TextView) convertView.findViewById(R.id.quantity);
        TextView price = (TextView) convertView.findViewById(R.id.price);

        ImageView del_icon = (ImageView) convertView.findViewById(R.id.del_icon);

        //  del_icon.setVisibility(View.VISIBLE);
        if (visibility.equals("gone")) {
            del_icon.setVisibility(View.GONE);
        } else {
            del_icon.setVisibility(View.VISIBLE);
        }
        img_icon = (ImageView) convertView.findViewById(R.id.image_icon);
        product_name.setText("" + data.get(position).product_name);
        product_code.setText("" + data.get(position).product_code);
        unit_price.setText("" +prefs.getString("currency_symbol", null) + data.get(position).unit_price);
        quantity.setText("" + data.get(position).qty);
        price.setText("" +prefs.getString("currency_symbol", null) + data.get(position).sub_total);
        product_name.setText("" + data.get(position).product_name);

        final String im = Utils.Imagepath.concat(data.get(position).image);
        try {
            if (Dashboard.width == 1440 && Dashboard.height == 2560) {

                Glide.with(context)
                        .load(Utils.Imagepath.concat(data.get(position).image))
                        .placeholder(R.drawable.default_item_icon)
                        .crossFade()
                        .thumbnail(0.1f)
                        .into(img_icon);

              //  Picasso.with(context).load(Utils.Imagepath.concat(data.get(position).image)).resize(150, 150).into(img_icon);
            } else {
                Glide.with(context)
                        .load(Utils.Imagepath.concat(data.get(position).image))
                        .placeholder(R.drawable.default_item_icon)
                        .crossFade()
                        .thumbnail(0.1f)
                        .into(img_icon);
               // Picasso.with(context).load(Utils.Imagepath.concat(data.get(position).image)).resize(100, 100).into(img_icon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return convertView;
    }


}

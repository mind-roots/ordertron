package Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ordertron.Dashboard;
import com.ordertron.Order_Edit;
import com.ordertron.Order_id;
import com.ordertron.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Model.ModelLogin;
import Model.Model_Add_Detail_Customer;
import Model.Model_Add_To_Cart;
import Model.Model_Order_Product;

/**
 * Created by admin on 1/13/2016.
 */
public class Product_order_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    public static ArrayList<Model_Order_Product> products = new ArrayList<>();
    public static ArrayList<Model_Add_Detail_Customer> bills = new ArrayList<>();
    public static ArrayList<String> bi = new ArrayList<>();

    JSONArray j;
    SharedPreferences prefs;
    String Currency_symbol, tax_percentage;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    DatabaseQueries query;
    String Tax_label, Shipping_cost, prefix_name;
    public static ArrayList<Double> total = new ArrayList<>();
    public static ArrayList<String> removeids = new ArrayList<>();
    Model_Order_Product item;
    //Model_billingqty bill;
    int del;
    double  baseprice=0, tiervalue=0, spclvalue=0;
    boolean tba = false;
    NumberFormat formatter;
    EditText input;
    ArrayList<ModelLogin> login11=new ArrayList<>();
    public Product_order_adapter(Context con, ArrayList<Model_Order_Product> products, int layout) {
        this.context = con;
        this.products = products;
        this.del = layout;
        query = new DatabaseQueries(context);
        prefs = con.getSharedPreferences("login", con.MODE_PRIVATE);
        Currency_symbol = prefs.getString("currency_symbol", null);
        tax_percentage = prefs.getString("tax_percentage", null);
        Tax_label = prefs.getString("tax_label", null);
        //Shipping_cost = prefs.getString("shipping_cost", null);

        prefix_name = prefs.getString("prefix_name", null);
        formatter = new DecimalFormat("#0.00");
        total.clear();
        removeids.clear();
        try {
            login11 =query.GetLogin(prefix_name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(int i=0;i<login11.size();i++){
            Shipping_cost=login11.get(i).shipping_cost;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_footer, parent, false);
            return new FooterViewHolder(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item_view, parent, false);
            return new ViewHolder(v);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            item = products.get(position);
//            try {
//                bills=query.get_all_address(prefs.getString("prefix_name", null));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            String acq=bills.get(position).act;
//            try {
//             j=new JSONArray(acq);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//bi.clear();
//            for (int i = 0; i < j.length(); i++) {
//                try {
//                    JSONObject ob=j.getJSONObject(i);
//                    String act=ob.getString("act_qty");
//                    bi.add(act);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
            viewHolder.product_name.setText(item.product_name);
            viewHolder.product_id.setText(item.product_code);

            viewHolder.b_qty.setText(item.product_code);
            if (item.act_qty.equals("0") || item.act_qty.equals("") || item.act_qty == null) {
                viewHolder.product_qty.setText("Order Qty: "+ formatter.format(Double.parseDouble(item.qty)) + " " + item.order_unit);
                viewHolder.b_qty.setVisibility(View.VISIBLE);
                viewHolder.b_qty.setText("Billing Qty:  TBA");
                viewHolder.product_apl_price.setText("Apl. price:  TBA");
                viewHolder.product_deprected_price.setVisibility(View.GONE);
            } else {
                viewHolder.product_qty.setText("Order Qty:  " +formatter.format(Double.parseDouble(item.qty)) + " " + item.order_unit);
                viewHolder.b_qty.setVisibility(View.VISIBLE);
                viewHolder.b_qty.setText("Billing Qty: "+ formatter.format(Double.parseDouble(item.act_qty)) + " " + item.base_unit);
                viewHolder.product_deprected_price.setVisibility(View.VISIBLE);
                viewHolder.product_apl_price.setText("Apl. price:  " + Currency_symbol + formatter.format(Double.parseDouble(item.applied_price)) + "/" + item.base_unit);
            }

         if(!item.unit_price.equals("")) {
             viewHolder.product_unit_price.setText("Unit price:  " + Currency_symbol + formatter.format(Double.parseDouble(item.unit_price)) + "/" + item.base_unit);
         }



            if (del == 2) {
                viewHolder.rl_delete.setVisibility(View.VISIBLE);
            } else {
                viewHolder.rl_delete.setVisibility(View.GONE);
            }
      //      try {
                if (item.act_qty.equals("0") || item.act_qty.equals("") || item.act_qty == null) {
                    viewHolder.product_qty.setText("Order Qty: "+formatter.format(Double.parseDouble(item.qty)) + " " + item.order_unit);
                    viewHolder.b_qty.setVisibility(View.VISIBLE);
                    viewHolder.b_qty.setText("Billing Qty:  TBA");

                    tba = true;
                    viewHolder.product_total_price.setText("TBA");

                } else {
                    double dqty = Double.parseDouble(item.act_qty);
                    double dunit = Double.parseDouble(item.unit_price);
                    double dapl = Double.parseDouble(item.applied_price);

                    double original = dqty * dunit;
                    double apl = dqty * dapl;

                    if (item.sub_total.equals("TBA")) {
                        //  viewHolder.product_deprected_price.setText(Currency_symbol + " " + String.valueOf(original));
                        tba = true;
                        viewHolder.product_total_price.setText("TBA");
                        viewHolder.product_deprected_price.setVisibility(View.GONE);
                    } else {

                        // if (item.order_unit.equals(item.base_unit)) {
                        if (original == apl) {
                            viewHolder.product_total_price.setText(Currency_symbol + formatter.format(apl));
                            viewHolder.product_deprected_price.setVisibility(View.GONE);
                        } else {
                            viewHolder.product_total_price.setText(Currency_symbol + formatter.format(apl));
                            Double up= Double.valueOf(formatter.format(Double.parseDouble(item.unit_price)));
                            Double app= Double.valueOf(formatter.format(Double.parseDouble(item.applied_price)));
                            viewHolder.product_deprected_price.setVisibility(View.VISIBLE);
                            viewHolder.product_deprected_price.setText(Currency_symbol + formatter.format(original));

                            viewHolder.product_deprected_price.setPaintFlags( Paint.STRIKE_THRU_TEXT_FLAG);
                        }
//                        } else {
//                            viewHolder.product_total_price.setText(Currency_symbol + "TBA");
//                        }
                    }
                    total.add(apl);
               }
//            } catch (Exception e) {
//                e.printStackTrace();
//                viewHolder.product_total_price.setText(Currency_symbol + formatter.format(Double.parseDouble(item.sub_total)));
//                // viewHolder.product_apl_price.setText("Apl. price:  " + Currency_symbol + " " + item.applied_price);
//            }
            String prodimage = item.thumb_image;
            // tierLogic(item.prod_id, item.qty, item.spcl_price, item.price, item.tier_price, item.taxable, item.order_unit, item.base_unit);
            Log.e("image value", "" + prodimage);
            if (prodimage != null) {
                if (!prodimage.equals("")) {
                    prodimage = prodimage.replaceAll(" ", "%20"); // remove all whitespaces

                try {
                    Glide.with(context)
                            .load(prodimage)
                            .placeholder(R.drawable.default_item_icon)
                            .crossFade()
                            .thumbnail(0.1f)
                            .into(viewHolder.product_image);
                   // Picasso.with(context).load(prodimage).placeholder(R.drawable.default_item_icon).resize(100, 100).into(viewHolder.product_image);
                } catch (Exception e) {

                }
            }else{
                    Glide.with(context)
                            .load(R.drawable.default_item_icon)
                            .crossFade()
                            .thumbnail(0.1f)
                            .into(viewHolder.product_image);
               // Picasso.with(context).load(R.drawable.default_item_icon).resize(100, 100).into(viewHolder.product_image);

            }
            }
            viewHolder.rl_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (products.size() > 1) {
                        dialog(products.get(position).product_code, products.get(position).order_id, position);

                    } else {
                        dialog2(android.R.drawable.ic_dialog_alert, "Alert!", "This is the last product of order.So you can not delete this product.");

                    }
                }
            });
            viewHolder.ll_mainclk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (del == 2) {
                        //  String str = "position=" + String.valueOf(position) + "/qty=" + products.get(position).qty + "/pid=" + products.get(position).product_id;
                        //   Toast.makeText(con,str,Toast.LENGTH_LONG).show();
                       /* Intent intent = new Intent(con, Pop_Window_Quantity.class);
                        intent.putExtra("type", "orderedit");
                        intent.putExtra("qty", products.get(position).qty);
                        intent.putExtra("pid", products.get(position).product_id);
                        intent.putExtra("maxqty", products.get(position).max_qty);
                        intent.putExtra("minqty", products.get(position).min_qty);
                        intent.putExtra("inventory", products.get(position).inventory);
                        intent.putExtra("position", position);
                        con.startActivity(intent);*/
                        popdialog(products.get(position).qty, products.get(position).product_id, products.get(position).max_qty, products.get(position).min_qty, products.get(position).inventory, position);


/*
                        intent.putExtra("product_name", item.product_name);
                        intent.putExtra("product_name", item.product_code);
                        intent.putExtra("position", item.product_id);
                        intent.putExtra("position", item.order_id);

                        intent.putExtra("position", item.image);
                        intent.putExtra("position", item.unit_price);
                        intent.putExtra("position", item.applied_price);
                        intent.putExtra("position", item.sub_total);*/

                    }

                }
            });
        } else if (holder instanceof FooterViewHolder) {

            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            if (del == 2) {
                footerHolder.one.setVisibility(View.GONE);
                footerHolder.three.setVisibility(View.GONE);
                footerHolder.two.setVisibility(View.GONE);
                double settotal = 0;
                footerHolder.txt_prod_count.setText(String.valueOf(products.size()));
                if (tba) {
                    tba = false;
                    footerHolder.total_order_text.setText("TBA");
                } else {
                    for (int i = 0; i < total.size(); i++) {
                        settotal = settotal + total.get(i);
                    }

                    footerHolder.total_order_text.setText(Currency_symbol + formatter.format(settotal));
                }

            } else {
                footerHolder.one.setVisibility(View.VISIBLE);
                footerHolder.three.setVisibility(View.VISIBLE);
                footerHolder.two.setVisibility(View.VISIBLE);
                footerHolder.gst_text.setText(Tax_label);
                footerHolder.txt_prod_count.setText(String.valueOf(products.size()));
                if (Order_id.order_infolist.get(0).sub_total.equals("TBA") || Order_id.order_infolist.get(0).tax_total.equals("TBA") || Order_id.order_infolist.get(0).total.equals("TBA")) {
                    if(Order_id.order_infolist.get(0).shipping_cost.equals("TBA")){
                        footerHolder.shippingCost_text.setText( (Order_id.order_infolist.get(0).shipping_cost));

                    }else {
                        footerHolder.shippingCost_text.setText(Currency_symbol + formatter.format(Double.parseDouble(Order_id.order_infolist.get(0).shipping_cost)));
                    }
                   if(Order_id.order_infolist.get(0).sub_total.equals("TBA")){
                       footerHolder.sub_total_text.setText(Order_id.order_infolist.get(0).sub_total);

                   }else{
                       footerHolder.sub_total_text.setText(Currency_symbol +formatter.format(Double.parseDouble(Order_id.order_infolist.get(0).sub_total)));

                   }
                    if(Order_id.order_infolist.get(0).tax_total.equals("TBA")){
                        footerHolder.gst_value_txt.setText( Order_id.order_infolist.get(0).tax_total);
                    }else{
                        footerHolder.gst_value_txt.setText(Currency_symbol + Order_id.order_infolist.get(0).tax_total);
                    }
                     if(Order_id.order_infolist.get(0).total.equals("TBA")){
                         footerHolder.total_order_text.setText( Order_id.order_infolist.get(0).total);
                    }else{
                         footerHolder.total_order_text.setText( Currency_symbol +Order_id.order_infolist.get(0).total);
                    }



                } else {
                  if(Order_id.order_infolist.get(0).shipping_cost.equals("")){
                      Order_id.order_infolist.get(0).shipping_cost="0";
                  }

                        footerHolder.shippingCost_text.setText(Currency_symbol + formatter.format(Double.parseDouble(Order_id.order_infolist.get(0).shipping_cost)));
                        footerHolder.sub_total_text.setText(Currency_symbol + formatter.format(Double.parseDouble(Order_id.order_infolist.get(0).sub_total)));
                        footerHolder.gst_value_txt.setText(Currency_symbol + formatter.format(Double.parseDouble(Order_id.order_infolist.get(0).tax_total)));
                        footerHolder.total_order_text.setText(Currency_symbol + formatter.format(Double.parseDouble(Order_id.order_infolist.get(0).total)));

                }
            }


        }
    }

    private void popdialog(final String qty, final String prod_id, final String max_qty, final String min_qty, final String inventory, final int position) {


        final AlertDialog.Builder builder = new AlertDialog.Builder(context, Dashboard.style);
        builder.setTitle("Edit Quantity");
        builder.setMessage("Edit Ordered Quantity and tap on Update");
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.edittext, null);
        input = (EditText) view
                .findViewById(R.id.etCategory);
        input.requestFocus();
        input.setHint("Enter Quantity");
        input.setText(qty);
        input.setSelection(qty.length());
        input.setTextColor(Color.BLACK);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String enterqty = input.getText().toString();
                if (inventory.equals("1")) {
                    String availstock = query.getinvent(prod_id, prefix_name);
                    if (availstock != null && (Float.parseFloat(enterqty) > Float.parseFloat(availstock))) {
                        Toast.makeText(context,
                                "The ordered product quantity is more than the available stock.", Toast.LENGTH_LONG).show();
                    } else {

                        editorder(position, enterqty, min_qty, max_qty, inventory);


                    }
                } else {
                    editorder(position, enterqty, min_qty, max_qty, inventory);

                }
                hidekeyboard();


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                hidekeyboard();
            }
        });
        builder.show();

    }


    private void editorder(int opos, String enterqty, String min_qty, String max_qty, String inventory) {
        Order_Edit.edit = true;
        Order_Edit.prodedit = true;
        float min = 0, max = 0, enter = 0;

        try {
            min = Float.parseFloat(min_qty);
            if (max_qty.length() != 0) {
                max = Float.parseFloat(max_qty);
            }
            enter = Float.parseFloat(enterqty);
        } catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }

        if ((enter < min)) {
            Toast.makeText(context, "The ordered product quantity is less than the minimum allowed cart quantity.", Toast.LENGTH_LONG).show();
        } else if (enter > max && max != 0) {
            Toast.makeText(context, "The ordered product quantity is more than the maximum allowed cart quantity.", Toast.LENGTH_LONG).show();

        } else {
            Model_Order_Product item = new Model_Order_Product();
            item.qty = enterqty;
            item.act_qty = Order_Edit.ordrprod.get(opos).act_qty;
            item.order_id = Order_Edit.ordrprod.get(opos).order_id;
            item.product_id = Order_Edit.ordrprod.get(opos).product_id;
            item.product_code = Order_Edit.ordrprod.get(opos).product_code;
            item.product_name = Order_Edit.ordrprod.get(opos).product_name;
            item.image = Order_Edit.ordrprod.get(opos).image;
            item.unit_price = Order_Edit.ordrprod.get(opos).unit_price;
            item.applied_price = tier(Order_Edit.ordrprod.get(opos).tier_price, Order_Edit.ordrprod.get(opos).spcl_price, Order_Edit.ordrprod.get(opos).unit_price, enterqty);
            item.sub_total = Order_Edit.ordrprod.get(opos).sub_total;
            item.order_unit = Order_Edit.ordrprod.get(opos).order_unit;
            item.base_unit = Order_Edit.ordrprod.get(opos).base_unit;
            item.tier_price = Order_Edit.ordrprod.get(opos).tier_price;
            item.spcl_price = Order_Edit.ordrprod.get(opos).spcl_price;
            item.min_qty = min_qty;
            item.max_qty = max_qty;
            item.inventory = inventory;


            Order_Edit.ordrprod.set(opos, item);
            products.set(opos, item);

            total.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return products.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionFooter(int position) {
        return position == products.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView product_name, b_qty,product_id, product_unit_price, product_qty, product_deprected_price, product_apl_price, product_total_price;
        ImageView product_image;
        RelativeLayout rl_delete;
        LinearLayout ll_mainclk;

        public ViewHolder(View view) {
            super(view);
            this.ll_mainclk = (LinearLayout) view.findViewById(R.id.ll_mainclk);
            this.product_name = (TextView) view.findViewById(R.id.product_name);
            this.product_id = (TextView) view.findViewById(R.id.product_id);
            this.rl_delete = (RelativeLayout) view.findViewById(R.id.rl_delete);
            this.product_unit_price = (TextView) view.findViewById(R.id.product_unit_price);
            this.product_qty = (TextView) view.findViewById(R.id.product_qty);
            this.product_apl_price = (TextView) view.findViewById(R.id.product_apl_price);
            this.product_deprected_price = (TextView) view.findViewById(R.id.product_deprected_price);
            this.product_total_price = (TextView) view.findViewById(R.id.product_total_price);
            this.b_qty = (TextView) view.findViewById(R.id.b_qty);
            this.product_image = (ImageView) view.findViewById(R.id.product_image);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView sub_total_text, gst_text, shippingCost_text, total_order_text, gst_value_txt, txt_prod_count;
        LinearLayout one, two, three;

        public FooterViewHolder(View itemView) {
            super(itemView);

            this.sub_total_text = (TextView) itemView.findViewById(R.id.sub_total_text);
            this.gst_text = (TextView) itemView.findViewById(R.id.gst_text);
            this.gst_value_txt = (TextView) itemView.findViewById(R.id.gst_value_txt);
            this.shippingCost_text = (TextView) itemView.findViewById(R.id.shippingCost_text);
            this.total_order_text = (TextView) itemView.findViewById(R.id.total_order_text);
            this.txt_prod_count = (TextView) itemView.findViewById(R.id.txt_prod_count);
            this.one = (LinearLayout) itemView.findViewById(R.id.one);
            this.two = (LinearLayout) itemView.findViewById(R.id.two);
            this.three = (LinearLayout) itemView.findViewById(R.id.three);

        }
    }

    public String tier(String tier_price, String spcl_price, String unitp, String qnty) {
        ArrayList<Model_Add_To_Cart> price_tier = new ArrayList<>();
        tiervalue = 0;
        spclvalue = 0;
        baseprice = 0;

        try {
            if (tier_price != null) {
                if (tier_price.length() != 0) {
                    JSONArray array5 = new JSONArray(tier_price);
                    for (int i = 0; i < array5.length(); i++) {

                        JSONObject obj2 = array5.getJSONObject(i);
                        String quantity_upto = obj2.getString("quantity_upto");
                        String discount_type = obj2.getString("discount_type");
                        String discount = obj2.getString("discount");

                        Model_Add_To_Cart model = new Model_Add_To_Cart();

                        model.quantity_upto = quantity_upto;
                        model.discount_type = discount_type;
                        model.discount = discount;

                        price_tier.add(model);
                    }
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // -----------------calculation-----------------//
        double calcicost = Double.parseDouble(unitp);

        baseprice = Double.parseDouble(unitp);
        if (spcl_price.equals("")) {
            spcl_price = "0";
        }
        if (price_tier.size() > 0 && spcl_price.equals("0") || spcl_price.equals(null)) {
            for (int i = 0; i < price_tier.size(); i++) {

                double qty_upto = Double.parseDouble(price_tier.get(i).quantity_upto);

                if (Double.parseDouble(qnty) >= qty_upto) {
                    if(price_tier.get(i).discount_type.equals("percentage")) {
                        double discount = (calcicost * (Double.parseDouble(price_tier.get(i).discount))) / 100;
                        calcicost = calcicost - discount;
                        tiervalue = calcicost;
                    }else{
                        double discount =Double.parseDouble(price_tier.get(i).discount)*Double.parseDouble(qnty);
                        calcicost =  Double.parseDouble(price_tier.get(i).discount);
                        tiervalue = calcicost;

                    }
                    break;

                } else {

                }
            }
        } else {
            //***********Calculate tier price if not empty*****************************
            if (price_tier.size() > 0 && spcl_price != null || (!spcl_price.equals("0"))) {
                for (int i = 0; i < price_tier.size(); i++) {

                    double qty_upto = Double.parseDouble(price_tier.get(i).quantity_upto);

                    if (Double.parseDouble(qnty) >= qty_upto) {
                        if(price_tier.get(i).discount_type.equals("percentage")) {
                            double discount = (calcicost * (Double.parseDouble(price_tier.get(i).discount))) / 100;
                            calcicost = calcicost - discount;
                            tiervalue = calcicost;
                        }else{
                            double discount =Double.parseDouble(price_tier.get(i).discount)*Double.parseDouble(qnty);
                            calcicost =  discount;
                            tiervalue  =Double.parseDouble(price_tier.get(i).discount);

                        }
                        break;

                    } else {

                    }
                }
            }


            // price.setText(dSpclprice);
            if (spcl_price.equals(" ") || spcl_price.equals("0") || spcl_price.equals("0") || spcl_price.equals(null) || spcl_price == "0" || spcl_price.length() == 0) {
                calcicost = Double.parseDouble(unitp);
                spclvalue = calcicost;
            } else {
                calcicost = Double.parseDouble(spcl_price);
                spclvalue = calcicost;
            }
        }
        //     prod_calci_cost = String.valueOf(calcicost);
        //***********find minimum among Base , Tier and special value
        if (tiervalue != 0 & spclvalue != 0) {
            calcicost = min(baseprice, tiervalue, spclvalue);
        } else if (tiervalue == 0 && spclvalue != 0) {
            if (baseprice < spclvalue) {
                calcicost = baseprice;
            } else {
                calcicost = spclvalue;
            }
        } else if (tiervalue != 0 && spclvalue == 0) {
            if (baseprice < tiervalue) {
                calcicost = baseprice;
            } else {
                calcicost = tiervalue;
            }
        } else {
            calcicost = baseprice;
        }

        double prodtotalprice = (Double
                .parseDouble(qnty) * calcicost);
        double apl = prodtotalprice / Double
                .parseDouble(qnty);


        return String.valueOf(apl);
    }


    //******************Function to find Min value among three values *****************************

    public static double min(double a, double b, double c) {
        return Math.min(Math.min(a, b), c);
    }



    private void dialog(final String prodid, final String orderid, final int pos) {
        new AlertDialog.Builder(context, Dashboard.style)
                .setTitle("Delete entry")
                .setMessage("Are you sure you want to delete this entry?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Order_Edit.prodedit = true;
                        removeids.add(prodid);
                        total.clear();
                        Order_Edit.ordrprod.remove(pos);
                        notifyDataSetChanged();
                        //  query.deletecartitem(pid);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void dialog2(int icon, String title, String msg) {
        new AlertDialog.Builder(context, Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })

                .setIcon(icon)
                .show();
    }

    private void hidekeyboard() {
        // TODO Auto-generated method stub

        if (input != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
        }
    }
}

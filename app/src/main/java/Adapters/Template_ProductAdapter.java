package Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ordertron.Dashboard;
import com.ordertron.R;
import com.ordertron.Template_Products;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Model.Model_GetProduct;
import Model.Model_Template;
import Model.Utils;

/**
 * Created by Abhijeet on 12/7/2016.
 */
public class Template_ProductAdapter extends
        RecyclerView.Adapter<Template_ProductAdapter.ViewHolder> {
    Context context;
    public static ArrayList<Model_GetProduct> data = new ArrayList<>();
    DatabaseQueries query;
    SharedPreferences prefs;
    String prefix, Currency_symbol;
    float min, max, enter;
    ArrayList<Model_Template> temparr = new ArrayList<>();
    NumberFormat formatter;
    EditText input;
    String products, qty, productscode;
int itempos;
    public Template_ProductAdapter(Context activity, ArrayList<Model_GetProduct> name) {
        this.context = activity;
        this.data = name;
        query = new DatabaseQueries(context);
        prefs = activity.getSharedPreferences("login", activity.MODE_PRIVATE);
        prefix = prefs.getString("prefix_name", "");
        formatter = new DecimalFormat("#0.00");
        Currency_symbol = prefs.getString("currency_symbol", null);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.template_productadapter, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);

        viewHolder.product_name = (TextView) contactView.findViewById(R.id.name);
        viewHolder.product_code = (TextView) contactView.findViewById(R.id.code);
        viewHolder.price = (TextView) contactView.findViewById(R.id.price);
        viewHolder.quantity = (TextView) contactView.findViewById(R.id.quantity);
        viewHolder.product_image = (ImageView) contactView.findViewById(R.id.img);
        viewHolder.rl_delete = (RelativeLayout) contactView.findViewById(R.id.rl_delete);
        viewHolder.ll_mainclk = (LinearLayout) contactView.findViewById(R.id.ll_mainclk);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // Model_Template name=new Model_Template();
        if (Template_Products.temp_pr_edit == 1) {
            holder.rl_delete.setVisibility(View.VISIBLE);

        } else {
            holder.rl_delete.setVisibility(View.GONE);
        }
        holder.product_name.setText(data.get(position).name);
        holder.product_code.setText(data.get(position).product_code);
       // holder.price.setText(Currency_symbol + formatter.format(Double.parseDouble(data.get(position).unit_price)));
        if(data.get(position).qty==null||data.get(position).qty.equals("")){
            data.get(position).qty="0";
        }
        holder.quantity.setText("Qty: " + formatter.format(Double.parseDouble(data.get(position).qty)));
        String prodimage = data.get(position).thumb_image;


        if (prodimage != null) {
            prodimage = prodimage.replaceAll(" ", "%20"); // remove all whitespaces
        }
        try {
            if (Dashboard.width == 1440 && Dashboard.height == 2560) {
                Glide.with(context)
                        .load(prodimage)
                        .placeholder(R.drawable.default_item_icon)
                        .crossFade()
                        .thumbnail(0.1f)
                        .into(holder.product_image);
               // Picasso.with(context).load(prodimage).placeholder(R.drawable.default_item_icon).resize(200, 200).into(holder.product_image);
            } else {
                Glide.with(context)
                        .load(prodimage)
                        .placeholder(R.drawable.default_item_icon)
                        .crossFade()
                        .thumbnail(0.1f)
                        .into(holder.product_image);
               // Picasso.with(context).load(prodimage).placeholder(R.drawable.default_item_icon).resize(100, 100).into(holder.product_image);
            }
        } catch (Exception e) {

        }
        holder.rl_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog(position);


            }
        });
        holder.ll_mainclk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Model_GetProduct a = name.get(position);
                if (Template_Products.temp_pr_edit == 0) {

                } else {
                    popdialog(Template_Products.tempid, data.get(position).qty, data.get(position).id, data.get(position).inventory, data.get(position).max_order_qut, data.get(position).min_order_qut, position, data.get(position).avail_stock);
                /*    Intent i = new Intent(c, Pop_Window_Quantity.class);
                    i.putExtra("tempid", Template_Products.tempid);
                    i.putExtra("qty", data.get(position).qty);
                    i.putExtra("type", "invent");
                    i.putExtra("pid", data.get(position).id);
                    i.putExtra("inventory", data.get(position).inventory);
                    i.putExtra("maxqty", data.get(position).max_order_qut);
                    i.putExtra("minqty", data.get(position).min_order_qut);
                    i.putExtra("position", position);
                    c.startActivity(i);*/
                }
            }
        });

    }

    private void popdialog(final String tempid, String qty, final String pid, final String inventory, final String maxqty, final String minqty, final int position, final String availstock) {
        min = 0;
        max = 0;
        enter = 0;
        temparr.clear();
        try {
            temparr = query.GetTemp_Detail(prefix, tempid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context, Dashboard.style);
        builder.setTitle("Edit Quantity");
        builder.setMessage("Edit the Quantity and tap on Update");
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.edittext, null);
        input = (EditText) view
                .findViewById(R.id.etCategory);
        input.requestFocus();
        input.setHint("Enter Quantity");
        input.setText(qty);
        input.setSelection(qty.length());
        input.setTextColor(Color.BLACK);
       // input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        input.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String enterqty = input.getText().toString();
                if(enterqty==null||enterqty.equals("")){
                    enterqty="0";
                }
                double valu= Double.parseDouble((enterqty));
                if (inventory.equals("1")) {
//                   String availstock = query.getinvent(pid, prefix);
//                    if (availstock != null && (Float.parseFloat(enterqty) > Float.parseFloat(availstock))) {
//                        Toast.makeText(context,
//                                "The ordered product quantity is more than the available stock.", Toast.LENGTH_SHORT).show();
//                    } else {
                 //   if(valu>=1) {
                        updatetemp(tempid, position, enterqty, maxqty, minqty, availstock);
//                    }else{
//                        Toast.makeText(context, "The quantity of product  can't be 0.", Toast.LENGTH_LONG).show();
//
//                    }

//                    }
                } else {
//                    if(valu>=1) {
                        updatetemp(tempid, position, enterqty, maxqty, minqty, availstock);
//                    }else{
//                        Toast.makeText(context, "The quantity of product  can't be 0.", Toast.LENGTH_LONG).show();
//
//                    }
                }
                hidekeyboard();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                hidekeyboard();
            }
        });
        builder.show();

    }

    private void updatetemp(String tempid, int pos, String enterqty, String maxqty, String minqty, String availstock) {
        float stock = 0;
        try {
            min = Float.parseFloat(minqty);
            if (maxqty.length() != 0) {
                max = Float.parseFloat(maxqty);
            }
            enter = Float.parseFloat(enterqty);
            enter= Utils.round(enter, 2);
            enterqty=String.valueOf(enter);
        } catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }
        try {
            stock = Float.parseFloat(availstock);
        } catch (NumberFormatException e) {
        }
//        if (availstock != null && (enter > stock) && !availstock.equals("No Limits")) {
//            Toast.makeText(context,
//                    "The ordered product quantity is more than the available stock.", Toast.LENGTH_LONG).show();
//        } else if ((enter < min)) {
//            Toast.makeText(context, "The ordered product quantity is less than the minimum allowed cart quantity.", Toast.LENGTH_LONG).show();
//
//        } else if (enter > max && max != 0) {
//            Toast.makeText(context, "The ordered product quantity is more than the maximum allowed cart quantity.", Toast.LENGTH_LONG).show();
//        } else {


            try {

                JSONArray productarray = new JSONArray(temparr.get(0).product_id);
                JSONArray qtyarray = new JSONArray(temparr.get(0).qty);
                for (int k = 0; k < productarray.length(); k++) {

                    if (pos == k) {
                        qtyarray.put(k, enterqty);
                        query.update_temp_qty(tempid, qtyarray.toString(), prefix, "newupdate");

                        data = query.GetTemplate_product(qtyarray, productarray, prefix);
                        notifyDataSetChanged();

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
          }
            Dashboard.tempserverupdate = true;
            Dashboard.tempupdate = true;
//        }
    }

    private void dialog(final int pos) {


        ArrayList<Model_Template> arr = new ArrayList<>();
        try {
            arr = query.GetTemp_Detail(prefix, Template_Products.tempid);

            products = arr.get(0).product_id;
            qty = arr.get(0).qty;
            productscode = arr.get(0).product_code;

        } catch (Exception e) {
            e.printStackTrace();
        }
        final ArrayList<Model_Template> finalArr = arr;
        final ArrayList<Model_Template> finalArr1 = arr;
        new AlertDialog.Builder(context, Dashboard.style)
                .setTitle("Delete entry")
                .setMessage("Are you sure you want to delete this entry?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {

                            JSONArray list = new JSONArray();
                            JSONArray list_q = new JSONArray();
                            JSONArray list_cq = new JSONArray();

                            JSONArray productarray = new JSONArray(products);
                            JSONArray productqty = new JSONArray(qty);
                            JSONArray pcodeqty = new JSONArray(productscode);






                            for (int k = 0; k < productarray.length(); k++) {

                                if (!productarray.getString(k).equals(data.get(pos).id)) {
                                    list.put(productarray.get(k));
                                    list_q.put(productqty.get(k));
                                    list_cq.put(pcodeqty.get(k));
//                                    query.update_temp_productId(list_cq.toString(), list.toString(), list_q.toString(), Template_Products.tempid, prefix,"newupdate");

                                }else{
                                    itempos=pos;
                                    //query.update_temp_productId(list_cq.toString(), list.toString(), list_q.toString(), Template_Products.tempid, prefix,"newupdate");

                                    if (data.size() > 0) {
                                        Template_Products.no_order_product.setVisibility(View.GONE);
                                        notifyDataSetChanged();
                                    } else {
                                        Template_Products.newmenu.findItem(R.id.edit).setVisible(false);
                                        Template_Products.newmenu.findItem(R.id.done).setVisible(false);
                                        Template_Products.no_order_product.setVisibility(View.VISIBLE);
                                        notifyDataSetChanged();
                                    }
                                }
                            }
                            data.remove(itempos);
                            notifyDataSetChanged();
                            query.update_temp_productId(list_cq.toString(), list.toString(), list_q.toString(), Template_Products.tempid, prefix,"newupdate");
                            notifyDataSetChanged();
                            if (data.size() > 0) {
                                Template_Products.no_order_product.setVisibility(View.GONE);
                                notifyDataSetChanged();
                            } else {
                                Template_Products.newmenu.findItem(R.id.edit).setVisible(false);
                                Template_Products.newmenu.findItem(R.id.done).setVisible(false);
                                Template_Products.no_order_product.setVisibility(View.VISIBLE);
                                notifyDataSetChanged();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }



                    }

                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        public TextView product_name, product_code, price, quantity;
        public ImageView product_image;
        Menu menu;
        RelativeLayout rl_delete;

        LinearLayout ll_mainclk;

        public ViewHolder(View itemView) {

            super(itemView);

        }
    }

    private void hidekeyboard() {
        // TODO Auto-generated method stub

        if (input != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
        }
    }
}

package Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ordertron.Order_id;
import com.ordertron.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Model.Model_Order_Info;
import Model.Utils;

/**
 * Created by android on 12/14/2015.
 */
public class AllOrdersApapter extends RecyclerView.Adapter<AllOrdersApapter.ViewHolder> {
    Context context;
   public static ArrayList<Model_Order_Info> list = new ArrayList<>();

    String mTimezone;

    public AllOrdersApapter(Context activity, ArrayList<Model_Order_Info> name, String timezone) {
        this.context = activity;
        this.list = name;
        this.mTimezone = timezone;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.all_order_item_adapter, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        viewHolder.date = (TextView) contactView.findViewById(R.id.date);
        viewHolder.order_id = (TextView) contactView.findViewById(R.id.orderid);
        viewHolder.c_name = (TextView) contactView.findViewById(R.id.name);
        viewHolder.icon = (ImageView) contactView.findViewById(R.id.img_orders);
        viewHolder.all = (RelativeLayout) contactView.findViewById(R.id.all);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Model_Order_Info template = list.get(position);
 String date=template.order_name;
 String time=mTimezone;
        holder.date.setText(Utils.timezone(template.order_name,mTimezone,"dd MMM yyyy - hh:mm a"));
        holder.order_id.setText(template.order_id);
        holder.c_name.setText(template.order_by);

        String status_pos = template.order_status;
        if (status_pos.equals("Completed")) {
            holder.icon.setImageResource(R.drawable.completed_orders);
        } else if (status_pos.equals("Processing")) {
            holder.icon.setImageResource(R.drawable.processing_orders);
        } else if (status_pos.equals("Cancelled")) {
            holder.icon.setImageResource(R.drawable.cancel_orders);
        } else if (status_pos.equals("Submitted")) {
            holder.icon.setImageResource(R.drawable.submitted_order);
        } else if (status_pos.equals("Back Order")) {
            holder.icon.setImageResource(R.drawable.back_orders);
        }


        holder.all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Order_id.class);
                i.putExtra("order_status", list.get(position).order_status);
                i.putExtra("order_id", list.get(position).order_id);
                context.startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView date, order_id, c_name;
        public ImageView icon;
        public RelativeLayout all;


        public ViewHolder(View itemView) {
            super(itemView);

        }
    }

    public String savedateformatter(String senddate) {

        // TODO Auto-generated method stub
        String setdate = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = format1.parse(senddate);
            format1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            setdate = format1.format(newDate);
        } catch (Exception e) {
            setdate = "NA";
        }
        return setdate;

    }
}

package Adapters;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ordertron.Dashboard;
import com.ordertron.Edit_address;
import com.ordertron.Order_Edit;
import com.ordertron.Orders_listing;
import com.ordertron.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import DataBaseUtils.DatabaseQueries;
import Fragments.OrderDetail_Fragment;
import Model.JSONParser;
import Model.Methods;
import Model.ModelLogin;
import Model.Model_Add_To_Cart;
import Model.Model_Order_Product;
import Model.Utils;

/**
 * Created by user on 5/5/2016.
 */
public class Order_edit_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    public static ArrayList<Model_Order_Product> products = new ArrayList<>();
    SharedPreferences prefs;
     ArrayList<ModelLogin> login11=new ArrayList<>();
    String Currency_symbol, tax_percentage;
    public static EditText edt_order_notes, edt_order_by;
    public static TextView edt_delivery_notes, setdiv;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    double baseprice = 0, tiervalue = 0, spclvalue = 0;
    DatabaseQueries query;
    String Tax_label, Shipping_cost, prefix_name, auth_code, username;
    public static ArrayList<Double> total = new ArrayList<>();
    public static ArrayList<Double> totalss = new ArrayList<>();
    public static ArrayList<String> removeids = new ArrayList<>();
    public static Model_Order_Product item;
    Methods methods;
    public static String daa = "";
    boolean tba = false;
    NumberFormat formatter;
    EditText input;
    boolean b;
    private Calendar cal;
    private int day, cancel = 1, rem = 0;
    private int month;
    private int year1;
    double settotaldd = 0;
    String convettdate, orderplacedby, ordernotes;
    DatePickerDialog dpDialog;

    public Order_edit_adapter(Context con, ArrayList<Model_Order_Product> products) {
        this.context = con;
        this.products = products;

        cal = Calendar.getInstance();
        year1 = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DAY_OF_MONTH);

        query = new DatabaseQueries(context);
        methods = new Methods(context);
        b = Utils.isNetworkConnected(context);
        prefs = con.getSharedPreferences("login", con.MODE_PRIVATE);
        Currency_symbol = prefs.getString("currency_symbol", null);
        auth_code = prefs.getString("auth_code", null);
        username = prefs.getString("username", null);
        tax_percentage = prefs.getString("tax_percentage", null);
        Tax_label = prefs.getString("tax_label", null);
        //Shipping_cost = prefs.getString("shipping_cost", null);
        try {
            login11 =query.GetLogin(prefix_name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(int i=0;i<login11.size();i++){
            Shipping_cost=login11.get(i).shipping_cost;
        }
        prefix_name = prefs.getString("prefix_name", null);
        formatter = new DecimalFormat("#0.00");
        total.clear();
        removeids.clear();
        //*****************Total for all products***********************
        totalss.clear();
        settotaldd = 0;
        for (int opos = 0; opos < products.size(); opos++) {
            Order_edit_adapter.item = products.get(opos);
            if (Order_edit_adapter.item.act_qty == null || Order_edit_adapter.item.act_qty.equals("")) {
                Order_edit_adapter.item.act_qty = "0";
            }
            if (Order_edit_adapter.item.unit_price == null || Order_edit_adapter.item.unit_price.equals("")) {
                Order_edit_adapter.item.unit_price = "0";
            }
            if (Order_edit_adapter.item.applied_price == null || Order_edit_adapter.item.applied_price.equals("")) {
                Order_edit_adapter.item.applied_price = "0";
            }
            double dqty = Double.parseDouble(Order_edit_adapter.item.act_qty);
            double dunit = Double.parseDouble(Order_edit_adapter.item.unit_price);
            double dapl = Double.parseDouble(Order_edit_adapter.item.applied_price);

            double original = dqty * dunit;
            double apl = dqty * dapl;
            if (original == apl) {
                Order_edit_adapter.totalss.add(apl);

            } else {
                Order_edit_adapter.totalss.add(apl);
            }
        }

        for (int i = 0; i < Order_edit_adapter.totalss.size(); i++) {
            if (Order_edit_adapter.totalss.get(i) == 0) {
                settotaldd = 0;

                break;
            } else {
                settotaldd = settotaldd + Order_edit_adapter.totalss.get(i);
            }
        }


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ordert_edit_header, parent, false);
            return new HeaderViewHolder(v);
        } else if (viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_footer, parent, false);
            return new FooterViewHolder(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item_view, parent, false);
            return new ViewHolder(v);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof HeaderViewHolder) {
            final HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            headerHolder.billing_add.setText(Order_Edit.Full_billing_adress);
            headerHolder.shipping_add.setText(Order_Edit.Full_shipping_adress);
            headerHolder.cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setdiv.setText("");
                    Order_Edit.delivery_date="";
                   daa=  Order_Edit.delivery_date;
                    headerHolder.cross.setVisibility(View.INVISIBLE);
                }
            });

            if (ordernotes == null || ordernotes.equals("")) {
                edt_order_notes.setText(Order_Edit.order_notes);
            } else {
                edt_order_notes.setText(ordernotes);
            }
            edt_order_notes.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    ordernotes = edt_order_notes.getText().toString();
                }
            });

            String lineSep = System.getProperty("line.separator");
            String yourString = Order_Edit.delivery_notes;
            yourString = yourString.replaceAll("<br />", lineSep);
            edt_delivery_notes.setText(yourString);
            if (orderplacedby == null || orderplacedby.equals("")) {
                edt_order_by.setText(Order_Edit.order_by);
            } else {
                edt_order_by.setText(orderplacedby);
            }
            edt_order_by.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    orderplacedby = edt_order_by.getText().toString();
                }
            });
            // setdiv.setText(Order_Edit.delivery_date);
            //if (OrderDetail_Fragment.showdeliverydate.equals("yes")) {
            if (OrderDetail_Fragment.show_deliverydate_order.equals("yes")) {
                daa = Order_Edit.delivery_date;
                headerHolder.divlay.setVisibility(View.VISIBLE);
                //if (OrderDetail_Fragment.mand_date.equals("yes")) {
                if (OrderDetail_Fragment.mandetory_delivery_order.equals("yes")) {
                    headerHolder.dliv.setText("Delivery Date *");
                    headerHolder.cross.setVisibility(View.INVISIBLE);
                } else {
                    headerHolder.dliv.setText("Delivery Date");

                }
                String DateStr = daa;
                if (convettdate == null || convettdate.equals("")) {
                    if (DateStr == null || DateStr.equals("")) {

                        setdiv.setText("");
                        if (OrderDetail_Fragment.mandetory_delivery_order.equals("yes")) {
                            headerHolder.cross.setVisibility(View.INVISIBLE);
                        }
                    } else {

                        SimpleDateFormat sim = new SimpleDateFormat(" EEE, dd MMM yyyy");
                        try {
                            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(daa);
                            String convettdate = sim.format(d);
                            setdiv.setText(convettdate);
                            if (OrderDetail_Fragment.mandetory_delivery_order.equals("yes")) {
                                headerHolder.cross.setVisibility(View.INVISIBLE);
                            }else {
                                headerHolder.cross.setVisibility(View.VISIBLE);

                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    setdiv.setText(convettdate);
                    if (OrderDetail_Fragment.mandetory_delivery_order.equals("yes")) {
                        headerHolder.cross.setVisibility(View.INVISIBLE);
                    }else {
                        headerHolder.cross.setVisibility(View.VISIBLE);

                    }
                }

            } else {
                headerHolder.divlay.setVisibility(View.GONE);
            }
            setdiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {// Toast.makeText(getApplicationContext(), "Invalid Year", Toast.LENGTH_SHORT).show();
                            int mont = monthOfYear + 1;

                            if (cancel == 1) {
                                //cancel=0;
                            } else {
                                cancel = 1;
                                if (year > year1) {
                                    //setdiv.setText(dayOfMonth + "/" + mont + "/" + year);

                                    daa = dayOfMonth + "/" + mont + "/" + year;

                                    String DateStr = daa;
                                    SimpleDateFormat sim = new SimpleDateFormat(" EEE, dd MMM yyyy");
                                    try {
                                        Date d = new SimpleDateFormat("dd/MM/yyyy").parse(DateStr);
                                        convettdate = sim.format(d);
                                        setdiv.setText(convettdate);
                                        if (OrderDetail_Fragment.mandetory_delivery_order.equals("yes")) {
                                            headerHolder.cross.setVisibility(View.INVISIBLE);
                                        }else {
                                            headerHolder.cross.setVisibility(View.VISIBLE);

                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                } else {

                                    if (year == year1) {
                                        if (monthOfYear > month) {
                                            //setdiv.setText(dayOfMonth + "/" + mont + "/" + year);

                                            daa = dayOfMonth + "/" + mont + "/" + year;

                                            String DateStr = daa;
                                            SimpleDateFormat sim = new SimpleDateFormat(" EEE, dd MMM yyyy");
                                            try {
                                                Date d = new SimpleDateFormat("dd/MM/yyyy").parse(DateStr);
                                                convettdate = sim.format(d);
                                                setdiv.setText(convettdate);
                                                if (OrderDetail_Fragment.mandetory_delivery_order.equals("yes")) {
                                                    headerHolder.cross.setVisibility(View.INVISIBLE);
                                                }else {
                                                    headerHolder.cross.setVisibility(View.VISIBLE);

                                                }
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            if (monthOfYear == month) {
                                                if (dayOfMonth >= day) {
                                                    // setdiv.setText(dayOfMonth + "/" + mont + "/" + year);

                                                    daa = dayOfMonth + "/" + mont + "/" + year;
                                                    String DateStr = daa;
                                                    SimpleDateFormat sim = new SimpleDateFormat(" EEE, dd MMM yyyy");
                                                    try {
                                                        Date d = new SimpleDateFormat("dd/MM/yyyy").parse(DateStr);
                                                        convettdate = sim.format(d);
                                                        setdiv.setText(convettdate);
                                                        if (OrderDetail_Fragment.mandetory_delivery_order.equals("yes")) {
                                                            headerHolder.cross.setVisibility(View.INVISIBLE);
                                                        }else {
                                                            headerHolder.cross.setVisibility(View.VISIBLE);

                                                        }
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {
                                                    Toast.makeText(context, "Delivery date cannot be in the past", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                Toast.makeText(context, "Delivery date cannot be in the past", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    } else {
                                        Toast.makeText(context, "Delivery date cannot be  in the past", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }
                        }

                    };
                    if (!setdiv.getText().toString().equals("")) {

                        if (OrderDetail_Fragment.mandetory_delivery_order.equals("yes")) {
                            headerHolder.cross.setVisibility(View.INVISIBLE);
                        }else {
                            headerHolder.cross.setVisibility(View.VISIBLE);

                        }
                        try {
                            SimpleDateFormat parserSDF = new SimpleDateFormat(" EEE, dd MMM yyyy", Locale.ENGLISH);
                            Date date = parserSDF.parse(setdiv.getText().toString());
                            System.out.println("date: " + date.toString());
                            String year = (String) android.text.format.DateFormat.format("yyyy", date);
                            int yeare = Integer.parseInt(year);
                            String moo = (String) android.text.format.DateFormat.format("MM", date);
                            int moo1 = Integer.parseInt(moo);
                            int moo2 = moo1 - 1;
                            String ddd = (String) android.text.format.DateFormat.format("dd", date);
                            int ddd1 = Integer.parseInt(ddd);

                            final DatePickerDialog dpDialog = new DatePickerDialog(context, listener, yeare, moo2, ddd1);
                            dpDialog.setCancelable(false);

                            dpDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == DialogInterface.BUTTON_NEGATIVE) {
                                        dpDialog.setOnDismissListener(mOnDismissListener);
                                    }
                                }
                            });
                            dpDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == DialogInterface.BUTTON_POSITIVE) {
                                        cancel = 0;
                                        DatePicker datePicker = dpDialog.getDatePicker();
                                        listener.onDateSet(datePicker, datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                                    }
                                }
                            });
                            dpDialog.show();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    } else {
                        headerHolder.cross.setVisibility(View.INVISIBLE);
                        dpDialog = new DatePickerDialog(context, listener, year1, month, day);
                        dpDialog.setCancelable(false);
                        dpDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == DialogInterface.BUTTON_NEGATIVE) {
                                    dpDialog.setOnDismissListener(mOnDismissListener);
                                }
                            }
                        });
                        dpDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == DialogInterface.BUTTON_POSITIVE) {
                                    cancel = 0;
                                    DatePicker datePicker = dpDialog.getDatePicker();
                                    listener.onDateSet(datePicker, datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                                }
                            }
                        });

                        dpDialog.show();
                    }
                }

                public DialogInterface.OnDismissListener mOnDismissListener =
                        new DialogInterface.OnDismissListener() {
                            public void onDismiss(DialogInterface dialog) {
                                dialog.dismiss();
                                cancel = 1;
                            }
                        };
            });

            headerHolder.billing_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, Edit_address.class);
                    intent.putExtra("type", "billing");
                    intent.putExtra("billing_suburb", Order_Edit.billing_suburb);
                    intent.putExtra("billing_add1", Order_Edit.billing_add_1);
                    intent.putExtra("billing_add2", Order_Edit.billing_add_2);
                    intent.putExtra("billing_add3", Order_Edit.billing_add_3);
                    intent.putExtra("billing_state", Order_Edit.billing_state);
                    intent.putExtra("billing_country", Order_Edit.billing_country);
                    intent.putExtra("billing_postcode", Order_Edit.billing_postcode);

                    context.startActivity(intent);
                }
            });

            headerHolder.shipping_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, Edit_address.class);
                    intent.putExtra("type", "shipping");
                    intent.putExtra("shipping_add1", Order_Edit.shipping_add_1);
                    intent.putExtra("shipping_add2", Order_Edit.shipping_add_2);
                    intent.putExtra("shipping_add3", Order_Edit.shipping_add_3);
                    intent.putExtra("shipping_suburb", Order_Edit.shipping_suburb);
                    intent.putExtra("shipping_state", Order_Edit.shipping_state);
                    intent.putExtra("shipping_country", Order_Edit.shipping_country);
                    intent.putExtra("shipping_postcode", Order_Edit.shipping_postcode);

                    context.startActivity(intent);
                }
            });

            edt_order_notes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    edt_order_notes.setCursorVisible(true);
                }
            });
//            edt_delivery_notes.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    edt_delivery_notes.setCursorVisible(true);
//                }
//            });
            edt_order_by.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    edt_order_by.setCursorVisible(true);
                }
            });
            headerHolder.cancel_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  Toast.makeText(context, "cancel", Toast.LENGTH_SHORT).show();
                    if (!b) {
                        Utils.conDialog(context);
                    } else {
                        new cancelOrder().execute();
                    }


                }
            });


        } else if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;

            item = products.get(position - 1);


            viewHolder.product_name.setText(item.product_name);
            viewHolder.product_id.setText(item.product_code);
            if (item.act_qty.equals("0") || item.act_qty.equals("") || item.act_qty == null) {
                viewHolder.b_qty.setVisibility(View.VISIBLE);
                viewHolder.b_qty.setText("Billing Qty:  TBA");
                viewHolder.product_apl_price.setText("Apl. price:  TBA");
            } else {
                viewHolder.b_qty.setVisibility(View.VISIBLE);
                viewHolder.b_qty.setText("Billing Qty:  " + formatter.format(Double.parseDouble(item.act_qty)) + " "+item.base_unit);
                viewHolder.product_apl_price.setText("Apl. price:  " + Currency_symbol + formatter.format(Double.parseDouble(item.applied_price)) + "/" + item.base_unit);

            }

            viewHolder.product_unit_price.setText("Unit price:  " + Currency_symbol + formatter.format(Double.parseDouble(item.unit_price)) + "/" + item.base_unit);

            viewHolder.rl_delete.setVisibility(View.VISIBLE);


            viewHolder.product_qty.setText("Qty: " + formatter.format(Double.parseDouble(item.qty)) + " " + item.order_unit);

            try {
                if (item.act_qty.equals("0") || item.act_qty.equals("") || item.act_qty == null) {
                    viewHolder.b_qty.setVisibility(View.VISIBLE);
                    viewHolder.b_qty.setText("Billing Qty:  TBA");
                    tba = true;
                    viewHolder.product_total_price.setText("TBA");
                    viewHolder.product_deprected_price.setVisibility(View.GONE);
                } else {
                    double dqty = Double.parseDouble(item.act_qty);
                    double dunit = Double.parseDouble(item.unit_price);
                    double dapl = Double.parseDouble(item.applied_price);

                    double original = dqty * dunit;
                    double apl = dqty * dapl;

                    if (item.sub_total.equals("TBA")) {
                        //  viewHolder.product_deprected_price.setText(Currency_symbol + " " + String.valueOf(original));
                        tba = true;
                        viewHolder.product_total_price.setText("TBA");
                        viewHolder.product_deprected_price.setVisibility(View.GONE);
                    } else {
                        // if (item.order_unit.equals(item.base_unit)) {
                        if (original == apl) {
                            viewHolder.product_total_price.setText(Currency_symbol + formatter.format(original));
                            viewHolder.product_deprected_price.setVisibility(View.GONE);
                        } else {
                            viewHolder.product_total_price.setText(Currency_symbol + formatter.format(apl));
                            viewHolder.product_deprected_price.setVisibility(View.VISIBLE);
                            viewHolder.product_deprected_price.setText(Currency_symbol + formatter.format(original));
                            viewHolder.product_deprected_price.setPaintFlags(viewHolder.product_deprected_price
                                    .getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        }
//                        } else {
//                            viewHolder.product_total_price.setText(Currency_symbol + "TBA");
//                        }
                    }
                    total.add(apl);
                }
            } catch (Exception e) {
                e.printStackTrace();
                viewHolder.product_total_price.setText(Currency_symbol + formatter.format(Double.parseDouble(item.sub_total)));
                // viewHolder.product_apl_price.setText("Apl. price:  " + Currency_symbol + " " + item.applied_price);
            }
            String prodimage = item.thumb_image;
            // tierLogic(item.prod_id, item.qty, item.spcl_price, item.price, item.tier_price, item.taxable, item.order_unit, item.base_unit);
            Log.e("image value", "" + prodimage);
            if (prodimage != null) {
                if (!prodimage.equals("")) {
                    prodimage = prodimage.replaceAll(" ", "%20"); // remove all whitespaces
                    try {
                        Glide.with(context)
                                .load(prodimage)
                                .placeholder(R.drawable.default_item_icon)
                                .crossFade()
                                .thumbnail(0.1f)
                                .into(viewHolder.product_image);
                        // Picasso.with(context).load(prodimage).placeholder(R.drawable.default_item_icon).resize(150, 150).into(viewHolder.product_image);
                    } catch (Exception e) {

                    }
                } else {
                    Glide.with(context)
                            .load(R.drawable.default_item_icon)
                            .crossFade()
                            .thumbnail(0.1f)
                            .into(viewHolder.product_image);
                    //  Picasso.with(context).load(R.drawable.default_item_icon).resize(150, 150).into(viewHolder.product_image);

                }
            }


            viewHolder.rl_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (products.size() > 1) {
                        dialog(products.get(position - 1).product_code, products.get(position - 1).order_id, position - 1);

                    } else {
                        dialog2(android.R.drawable.ic_dialog_alert, "Alert!", "This is the last product of order.So you can not delete this product.", 0);

                    }
                }
            });
            viewHolder.ll_mainclk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    popdialog(products.get(position - 1).qty, products.get(position - 1).product_id, products.get(position - 1).max_qty, products.get(position - 1).min_qty, products.get(position - 1).inventory, position - 1);


                }
            });
        } else if (holder instanceof FooterViewHolder) {

            FooterViewHolder footerHolder = (FooterViewHolder) holder;

            footerHolder.one.setVisibility(View.GONE);
            footerHolder.three.setVisibility(View.GONE);
            footerHolder.two.setVisibility(View.GONE);
            double settotal = 0;
            // double settotaldd = 0;
            footerHolder.txt_prod_count.setText(String.valueOf(products.size()));
            //total.clear();
            for (int i = 0; i < products.size(); i++) {

                if (products.get(i).act_qty.equals("") || products.get(i).act_qty.equals("0")) {
                    tba = true;
                } else {
                    tba = false;

                    //  total.add(aq*apri);
                }
            }
            if (tba) {
                tba = false;
                footerHolder.total_order_text.setText("TBA");
                settotaldd = 0;
            } else {
                if (rem == 1) {
                    settotal = 0;
                    for (int i = 0; i < products.size(); i++) {

                        if (products.get(i).act_qty.equals("") || products.get(i).act_qty.equals("0")) {
                            settotaldd = 0;
                            settotal = 0;
                            rem = 0;
                            break;
                        } else {

                            //*********Calculating Total of Products************
                            double aq=0;
                            aq= Double.parseDouble(products.get(i).act_qty);
                            double apri=0;
                            apri= Double.parseDouble(products.get(i).applied_price);
                            double apl=0;
                            apl=aq*apri;
                            settotal = settotal + apl;
                        }
                        // settotaldd = settotaldd + totalss.get(i);
                    }
                }
                //settotaldd=settotal;
                if (rem == 1) {
                    rem = 0;
                    settotaldd = settotal;
                }
                if (settotaldd == 0 ) {
                    footerHolder.total_order_text.setText("TBA");

                } else {
                    footerHolder.total_order_text.setText(Currency_symbol + formatter.format(settotaldd));
                }

            }


        }
    }

    private void popdialog(final String qty, final String prod_id, final String max_qty, final String min_qty, final String inventory, final int position) {


        final AlertDialog.Builder builder = new AlertDialog.Builder(context, Dashboard.style);
        builder.setTitle("Edit Quantity");
        builder.setMessage("Edit Ordered Quantity and tap on Update");
        builder.setCancelable(false);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.edittext, null);
        input = (EditText) view
                .findViewById(R.id.etCategory);

        input.setHint("Enter Quantity");
        input.setText(formatter.format(Double.parseDouble(qty)));
        input.setSelection(formatter.format(Double.parseDouble(qty)).length());
       // input.setSelection(qty.length());
        input.requestFocus();
        input.setTextColor(Color.BLACK);
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String enterqty = input.getText().toString();
                if (inventory.equals("1")) {
                    String availstock = query.getinvent(prod_id, prefix_name);
                    if (availstock != null && (Float.parseFloat(enterqty) > Float.parseFloat(availstock))) {
                        Toast.makeText(context,
                                "The ordered product quantity is more than the available stock.", Toast.LENGTH_LONG).show();
                    } else {

                        editorder(position, enterqty, min_qty, max_qty, inventory);


                    }
                } else {
                    editorder(position, enterqty, min_qty, max_qty, inventory);

                }
                hidekeyboard();


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                hidekeyboard();
            }
        });
        builder.show();

    }


    private void editorder(int opos, String enterqty, String min_qty, String max_qty, String inventory) {
        Order_Edit.edit = true;
        Order_Edit.prodedit = true;
        float min = 0, max = 0, enter = 0;

        try {
            min = Float.parseFloat(min_qty);
            if (max_qty.length() != 0) {
                max = Float.parseFloat(max_qty);
            }
            enter = Float.parseFloat(enterqty);
            enter = Utils.round(enter, 2);
            enterqty = String.valueOf(enter);
        } catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }

        if ((enter < min)) {
            Toast.makeText(context, "The ordered product quantity is less than the minimum allowed cart quantity.", Toast.LENGTH_LONG).show();
        } else if (enter > max && max != 0) {
            Toast.makeText(context, "The ordered product quantity is more than the maximum allowed cart quantity.", Toast.LENGTH_LONG).show();

        } else {
            Model_Order_Product item = new Model_Order_Product();
            item.qty = enterqty;
            if (Order_Edit.ordrprod.get(opos).act_qty.equals("0") || Order_Edit.ordrprod.get(opos).act_qty.equals("") || Order_Edit.ordrprod.get(opos).act_qty == null) {
                item.act_qty = Order_Edit.ordrprod.get(opos).act_qty;
            } else {
                item.act_qty = enterqty;
            }

            item.order_id = Order_Edit.ordrprod.get(opos).order_id;
            item.product_id = Order_Edit.ordrprod.get(opos).product_id;
            item.product_code = Order_Edit.ordrprod.get(opos).product_code;
            item.product_name = Order_Edit.ordrprod.get(opos).product_name;
            item.thumb_image = Order_Edit.ordrprod.get(opos).thumb_image;
            item.unit_price = Order_Edit.ordrprod.get(opos).unit_price;
            item.applied_price = tier(Order_Edit.ordrprod.get(opos).tier_price, Order_Edit.ordrprod.get(opos).spcl_price, Order_Edit.ordrprod.get(opos).unit_price, enterqty);
            item.sub_total = Order_Edit.ordrprod.get(opos).sub_total;
            item.order_unit = Order_Edit.ordrprod.get(opos).order_unit;
            item.base_unit = Order_Edit.ordrprod.get(opos).base_unit;
            item.tier_price = Order_Edit.ordrprod.get(opos).tier_price;
            item.spcl_price = Order_Edit.ordrprod.get(opos).spcl_price;
            item.min_qty = min_qty;
            item.max_qty = max_qty;
            item.inventory = inventory;


            Order_Edit.ordrprod.set(opos, item);
            products.set(opos, item);

            total.clear();
            create_total();

//            Order_Edit.productAdapter = new Order_edit_adapter(context, Order_Edit.ordrprod);
//            Order_Edit.product_list.setAdapter(Order_Edit.productAdapter);
        }
    }

    private void create_total() {
        //*****************Total for all products***********************
        settotaldd = 0;
//        totalss.clear();
//        for (int opos = 0; opos < products.size(); opos++) {
//            Order_edit_adapter.item = products.get(opos);
//            double dqty = Double.parseDouble(Order_edit_adapter.item.act_qty);
//            double dunit = Double.parseDouble(Order_edit_adapter.item.unit_price);
//            double dapl = Double.parseDouble(Order_edit_adapter.item.applied_price);
//
//            double original = dqty * dunit;
//            double apl = dqty * dapl;
//        for (int i = 0; i <products.size(); i++) {
//
//            if (products.get(i).act_qty.equals("0") || products.get(i).act_qty.equals("")) {
//                tba = true;
//                settotaldd = 0;
//                break;
//            } else {
//                double dqty = 0;
//                 dqty = Double.parseDouble(Order_edit_adapter.item.act_qty);
//                double dunit = 0;
//               dunit = Double.parseDouble(Order_edit_adapter.item.unit_price);
//                double dapl = 0;
//               dapl = Double.parseDouble(Order_edit_adapter.item.applied_price);
//
//                double original = dqty * dunit;
//                double apl = 0;
//                 apl = dqty * dapl;
//                settotaldd = settotaldd +apl;
//            }
//
//       }


//            if (original == apl) {
//                Order_edit_adapter.totalss.add(apl);
//
//            } else {
//                Order_edit_adapter.totalss.add(apl);
//            }
//        }

        rem = 1;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        return products.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == products.size() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView product_name, product_id, product_unit_price, product_qty, b_qty, product_deprected_price, product_apl_price, product_total_price;
        ImageView product_image;
        RelativeLayout rl_delete;
        LinearLayout ll_mainclk;

        public ViewHolder(View view) {
            super(view);
            this.ll_mainclk = (LinearLayout) view.findViewById(R.id.ll_mainclk);
            this.product_name = (TextView) view.findViewById(R.id.product_name);
            this.product_id = (TextView) view.findViewById(R.id.product_id);
            this.rl_delete = (RelativeLayout) view.findViewById(R.id.rl_delete);
            this.product_unit_price = (TextView) view.findViewById(R.id.product_unit_price);
            this.b_qty = (TextView) view.findViewById(R.id.b_qty);
            this.product_qty = (TextView) view.findViewById(R.id.product_qty);
            this.product_apl_price = (TextView) view.findViewById(R.id.product_apl_price);
            this.product_deprected_price = (TextView) view.findViewById(R.id.product_deprected_price);
            this.product_total_price = (TextView) view.findViewById(R.id.product_total_price);
            this.product_image = (ImageView) view.findViewById(R.id.product_image);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView sub_total_text, gst_text, shippingCost_text, total_order_text, gst_value_txt, txt_prod_count;
        LinearLayout one, two, three;

        public FooterViewHolder(View itemView) {
            super(itemView);

            this.sub_total_text = (TextView) itemView.findViewById(R.id.sub_total_text);
            this.gst_text = (TextView) itemView.findViewById(R.id.gst_text);
            this.gst_value_txt = (TextView) itemView.findViewById(R.id.gst_value_txt);
            this.shippingCost_text = (TextView) itemView.findViewById(R.id.shippingCost_text);
            this.total_order_text = (TextView) itemView.findViewById(R.id.total_order_text);
            this.txt_prod_count = (TextView) itemView.findViewById(R.id.txt_prod_count);
            this.one = (LinearLayout) itemView.findViewById(R.id.one);
            this.two = (LinearLayout) itemView.findViewById(R.id.two);
            this.three = (LinearLayout) itemView.findViewById(R.id.three);

        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mainlayout;
        TextView billing_add, shipping_add, dliv;
        RelativeLayout divlay;
        LinearLayout cancel_order;
        ImageView cross;
        public HeaderViewHolder(View itemView) {
            super(itemView);

            this.divlay = (RelativeLayout) itemView.findViewById(R.id.divlay);
            setdiv = (TextView) itemView.findViewById(R.id.setdiv);
            cross = (ImageView) itemView.findViewById(R.id.cross);
            dliv = (TextView) itemView.findViewById(R.id.dliv);
            this.mainlayout = (LinearLayout) itemView.findViewById(R.id.mainlayout);
            this.cancel_order = (LinearLayout) itemView.findViewById(R.id.canceloreder_ll);
            this.billing_add = (TextView) itemView.findViewById(R.id.billing_address);
            this.shipping_add = (TextView) itemView.findViewById(R.id.shipping_address);
            edt_order_notes = (EditText) itemView.findViewById(R.id.order_notes);
            edt_delivery_notes = (TextView) itemView.findViewById(R.id.delivery_notes);
            edt_order_by = (EditText) itemView.findViewById(R.id.placed_by);

        }
    }

    public String tier(String tier_price, String spcl_price, String unitp, String qnty) {
        ArrayList<Model_Add_To_Cart> price_tier = new ArrayList<>();
        tiervalue = 0;
        spclvalue = 0;
        baseprice = 0;
        try {
            if (tier_price != null) {
                if (tier_price.length() != 0) {
                    JSONArray array5 = new JSONArray(tier_price);
                    for (int i = 0; i < array5.length(); i++) {

                        JSONObject obj2 = array5.getJSONObject(i);
                        String quantity_upto = obj2.getString("quantity_upto");
                        String discount_type = obj2.getString("discount_type");
                        String discount = obj2.getString("discount");

                        Model_Add_To_Cart model = new Model_Add_To_Cart();

                        model.quantity_upto = quantity_upto;
                        model.discount_type = discount_type;
                        model.discount = discount;

                        price_tier.add(model);
                    }
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // -----------------calculation-----------------//
        double calcicost = Double.parseDouble(unitp);
        baseprice = Double.parseDouble(unitp);
        if (spcl_price.equals("")) {
            spcl_price = "0";
        }
        if (price_tier.size() > 0 && spcl_price.equals("0") || spcl_price.equals(null)) {
            for (int i = 0; i < price_tier.size(); i++) {

                double qty_upto = Double
                        .parseDouble(price_tier.get(i).quantity_upto);

                if (Double.parseDouble(qnty) >= qty_upto) {
                    if (price_tier.get(i).discount_type.equals("percentage")) {
                        double discount = (calcicost * (Double.parseDouble(price_tier.get(i).discount))) / 100;
                        calcicost = calcicost - discount;
                        tiervalue = calcicost;
                    } else {
                        double discount = Double.parseDouble(price_tier.get(i).discount) * Double.parseDouble(qnty);
                        calcicost = Double.parseDouble(price_tier.get(i).discount);
                        tiervalue = calcicost;

                    }
                    break;

                } else {

                }
            }
        } else {
            //***********Calculate tier price if not empty*****************************
            if (price_tier.size() > 0 && spcl_price != null || (!spcl_price.equals("0"))) {
                for (int i = 0; i < price_tier.size(); i++) {

                    double qty_upto = Double.parseDouble(price_tier.get(i).quantity_upto);

                    if (Double.parseDouble(qnty) >= qty_upto) {
                        if (price_tier.get(i).discount_type.equals("percentage")) {
                            double discount = (calcicost * (Double.parseDouble(price_tier.get(i).discount))) / 100;
                            calcicost = calcicost - discount;
                            tiervalue = calcicost;
                        } else {
                            double discount = Double.parseDouble(price_tier.get(i).discount) * Double.parseDouble(qnty);
                            calcicost = discount;
                            tiervalue = Double.parseDouble(price_tier.get(i).discount);

                        }
                        break;

                    } else {

                    }
                }
            }


            // price.setText(dSpclprice);
            if (spcl_price.equals(" ") || spcl_price.equals("0") || spcl_price.equals("0") || spcl_price.equals(null) || spcl_price == "0" || spcl_price.length() == 0) {
                calcicost = Double.parseDouble(unitp);
                spclvalue = calcicost;
            } else {
                calcicost = Double.parseDouble(spcl_price);
                spclvalue = calcicost;
            }
        }

        //***********find minimum among Base , Tier and special value
        if (tiervalue != 0 & spclvalue != 0) {
            calcicost = min(baseprice, tiervalue, spclvalue);
        } else if (tiervalue == 0 && spclvalue != 0) {
            if (baseprice < spclvalue) {
                calcicost = baseprice;
            } else {
                calcicost = spclvalue;
            }
        } else if (tiervalue != 0 && spclvalue == 0) {
            if (baseprice < tiervalue) {
                calcicost = baseprice;
            } else {
                calcicost = tiervalue;
            }
        } else {
            calcicost = baseprice;
        }

        double prodtotalprice = (Double
                .parseDouble(qnty) * calcicost);
        double apl = prodtotalprice / Double
                .parseDouble(qnty);


        return String.valueOf(apl);
    }

    //******************Function to find Min value among three values *****************************

    public static double min(double a, double b, double c) {
        return Math.min(Math.min(a, b), c);
    }


    private void dialog(final String prodid, final String orderid, final int pos) {
        new AlertDialog.Builder(context, Dashboard.style)
                .setTitle("Delete entry")
                .setMessage("Are you sure you want to delete this entry?")
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Order_Edit.prodedit = true;
                        removeids.add(prodid);
                        total.clear();
                        Order_Edit.ordrprod.remove(pos);

                        rem = 1;

                        notifyDataSetChanged();
                        //  query.deletecartitem(pid);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void dialog2(int icon, String title, String msg, final int type) {
        new AlertDialog.Builder(context, Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if (type == 1) {
                            ((Activity) context).finish();
                        }
                    }
                })

                .setIcon(icon)
                .show();
    }

    private void hidekeyboard() {
        // TODO Auto-generated method stub

        if (input != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
        }
    }
    //---------------------------Async to cancel_order----------

    public class cancelOrder extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog;
        String response;
        String status;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setCancelable(false);
            dialog.setMessage("cancelling order....");
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            response = cancel_order(prefix_name, auth_code, username, Order_Edit.order_id);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            try {

                JSONObject jsonObject = new JSONObject(response);
                status = jsonObject.getString("Status");
                if (status.equals("true")) {
                    query.update_cancel_info(Order_Edit.order_id, prefix_name);
                    Orders_listing.cancelorder = true;
                    dialog2(android.R.drawable.ic_dialog_info, "Success", "Your order has been cancelled.", 1);
                } else {
                    if (jsonObject.has("logout")) {
                        String logout = jsonObject.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = jsonObject.optJSONObject("Data");
                            final String result = jobj.optString("result");

                            methods.dialog(result);

                        }
                    } else {
                        dialog2(android.R.drawable.ic_dialog_alert, "Alert!", "Your order has not been cancelled.Please try again later.", 1);
                    }
                }

            } catch (Exception e) {
                dialog2(android.R.drawable.ic_dialog_alert, "Alert!", "Your order has not been cancelled.Please try again later.", 1);
            }

        }
    }
    //*************************Webservice parameter cancel_order  *************************

    public String cancel_order(String prefix_name, String auth_code, String user_name, String orderId) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("username", user_name)
                .appendQueryParameter("order_id", orderId);
        res = parser.getJSONFromUrl(Utils.cancel_order, builder,context);
        return res;
    }


}

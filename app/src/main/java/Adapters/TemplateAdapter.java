package Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ordertron.Dashboard;
import com.ordertron.R;
import com.ordertron.Template_Products;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Fragments.TemplatesActivity;
import Model.JSONParser;
import Model.Model_Template;
import Model.Utils;

/**
 * Created by abc on 12/11/2015.
 */
public class TemplateAdapter extends RecyclerView.Adapter<TemplateAdapter.ViewHolder> {
    Context context;
    ArrayList<Model_Template> name = new ArrayList<>();
    public static String sendtempid;
    String prefix, data;
    DatabaseQueries query;
    SharedPreferences prefs;
    ArrayList<String> arr = new ArrayList<>();
    boolean b;

    public TemplateAdapter(Context activity, ArrayList<Model_Template> name) {
        this.context = activity;
        this.name = name;
        b = Utils.isNetworkConnected(context);
        query = new DatabaseQueries(context);
        prefs = context.getSharedPreferences("login", Context.MODE_PRIVATE);
        prefix = prefs.getString("prefix_name", "");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.add_adapter_template, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        viewHolder.relativeLayout = (LinearLayout) contactView.findViewById(R.id.lay);
        viewHolder.textView = (TextView) contactView.findViewById(R.id.txt_title);
        viewHolder.btn_edit = (RelativeLayout) contactView.findViewById(R.id.btn_edit);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Model_Template model = name.get(position);

        holder.textView.setText(model.template_name);

        if (TemplatesActivity.tempedit == 1) {
            holder.btn_edit.setVisibility(View.VISIBLE);
        } else if (TemplatesActivity.tempedit == 0) {
            holder.btn_edit.setVisibility(View.GONE);

        }
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendtempid = name.get(position).order_template_id;
                if (TemplatesActivity.tempedit == 1) {
                    popdialog(name.get(position).order_template_id, name.get(position).template_name);

                } else {
                    Intent i = new Intent(context, Template_Products.class);
                    i.putExtra("tempid", model.order_template_id);
                    i.putExtra("name", name.get(position).template_name);
                    i.putExtra("products", name.get(position).product_id);
                    i.putExtra("productscode", name.get(position).product_code);
                    i.putExtra("qty", name.get(position).qty);
                    context.startActivity(i);
                }

            }
        });
        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog(position);
            }
        });
    }

    private void popdialog(final String order_template_id, String template_name) {

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context, Dashboard.style);
        builder.setTitle("Edit Template");
        builder.setMessage("Edit the Template Name and tap on Update");
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.edittext, null);
        final EditText input = (EditText) view
                .findViewById(R.id.etCategory);
        input.requestFocus();
        input.setHint("Enter Template Name");
        input.setText(template_name);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        input.setSelection(template_name.length());
        input.setTextColor(Color.BLACK);

        builder.setView(view);
        builder.setCancelable(false);
//                i = getActivity().getIntent();
//                intentdata_popupwindow = i.getStringExtra("edit");
//                order_id = i.getStringExtra("sendtempid");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                data = input.getText().toString();
                try {
                    arr = query.GetTemplate_name(prefix);
                } catch (Exception e) {

                }
                String avial = "true";
                for (int h = 0; h < arr.size(); h++) {
                    System.out.println("TEMP_name::" + arr.get(h));
                    if (arr.get(h).equalsIgnoreCase(data.trim())) {
                        avial = "false";

                    }
//                    else {
//                        avial = "true";
//
//                    }
                }
                if (avial.equals("false")) {
                    Toast.makeText(context, "Template name already Exists", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Template name updated", Toast.LENGTH_LONG).show();
                    Dashboard.tempserverupdate = true;
                    Dashboard.tempupdate = true;
                    query.update_temp_name(order_template_id, data, prefs.getString("prefix_name", ""), "newupdate");
                    try {
                        name = query.GetTemplate(prefix, "all");
                        notifyDataSetChanged();
                    } catch (Exception e) {

                    }
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }

    private void dialog(final int  position) {
        new AlertDialog.Builder(context, Dashboard.style)
                .setTitle("Delete Template")
                .setMessage("Are you sure you want to remove the template?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete


                        //*********Deleting Row from table Template**********
                        if (!Utils.isNetworkConnected(context)) {
                            Utils.conDialog(context);
                           // deletefromdb(name.get(position).order_template_id);
                        } else {
                            new delete(name.get(position).order_template_id, position,name.get(position).type).execute();
                        }


                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    @Override
    public int getItemCount() {
        return name.size();
    }

    //********************************************delete parsing*************************************
    public class delete extends AsyncTask<String, Void, String> {
        String status = "false", result, order_template_id,type;
        int position;
        ProgressDialog dialog;
        JSONObject obj;

        public delete(String sendtempid, int position,String type) {
            this.order_template_id = sendtempid;
            this.position = position;
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setMessage("Deleting Template...");
            dialog.show();

        }

        @Override
        protected String doInBackground(String[] params) {
            String response = DeleteMethod(prefix, prefs.getString("auth_code", null),
                    order_template_id);


            try {
                obj = new JSONObject(response);

                status = obj.getString("Status");


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);
            dialog.dismiss();
            try {
                if (status.equals("true")) {

                    deletefromdb(order_template_id);
                } else {
                    if (obj.has("logout")) {
                        String logout = obj.optString("logout");
                        if (logout.equals("yes")) {
                            JSONObject jobj = obj.optJSONObject("Data");
                            final String result = jobj.optString("result");


                            TemplatesActivity.methods.dialog(result);

                        }
                    } else {
                        if(type.equals("off")||status.equals("false")) {
                            deletefromdb(order_template_id);
                        }else {
                            Utils.dialog(context, "Error!", "Problem occurred while performing deletion.");
                        }
                    }
                    //  Utils.dialog(context, "Error!", "Problem occurred while performing deletion.");
                }
            }catch (Exception e){
            e.printStackTrace();
            }

        }


    }

    private void deletefromdb(String tempid) {
        query.delete_temp(tempid);
        TemplatesActivity.item.clear();
        try {
            TemplatesActivity.item = query.GetTemplate(prefix, "all");
        } catch (Exception e) {

        }
        if (TemplatesActivity.item.size() > 0) {

            TemplatesActivity.noordertemp.setVisibility(View.GONE);
            TemplatesActivity.temp_list.setVisibility(View.VISIBLE);
            TemplatesActivity.tadapter.notifyDataSetChanged();
            TemplatesActivity.tadapter = new TemplateAdapter(context, TemplatesActivity.item);
            TemplatesActivity.temp_list.setAdapter(TemplatesActivity.tadapter);
        } else {
            TemplatesActivity.temp_list.setVisibility(View.GONE);
            TemplatesActivity.noordertemp.setVisibility(View.VISIBLE);
            TemplatesActivity.newmenu.findItem(R.id.edit).setVisible(false);
            TemplatesActivity.newmenu.findItem(R.id.done).setVisible(false);
            TemplatesActivity.newmenu.findItem(R.id.add).setVisible(true);
        }
    }
    //************************Webservice parameter LoginMethod*************************

    public String DeleteMethod(String prefix_name, String auth_code, String order_template_id) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("order_template_id", order_template_id);
        res = parser.getJSONFromUrl(Utils.delete, builder,context);
        return res;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout relativeLayout;
        public TextView textView;
        public RelativeLayout btn_edit;
        Menu menu;

        public ViewHolder(View itemView) {
            super(itemView);

        }
    }
}

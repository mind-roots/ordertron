package Model;

public class Model_GetCategories {
    public String prefix_name;
    public String id;
    public String category_name;
    public String parent_cat_id;
    public String root_cat_id;
    public String level;
    public String sort;
    public String status;
    public String fullname;
    public String created_date;
    public String update_date;
    public String updated_by;
    public String sid;
    public String type;
    public String last_sync_id;
    public String view_type;

}

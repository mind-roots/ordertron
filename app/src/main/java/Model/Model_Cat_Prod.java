package Model;


public class Model_Cat_Prod {
    public String only_cat_id;
    public String category_name;
    public String parent_cat_id;
    public String root_cat_id;
    public String level;
    public String sort;
    public String cat_status;
    public String fullname;
    public String cat_created_date;
    public String cat_update_date;
    public String cat_updated_by;
    public String sid;
    public String cat_type;
    public String cat_last_sync_id;
    public String view_type;


    public String prefix_name;
    public String order_unit_name;
    public String base_unit_name;
    public String prod_id;
    public String sink_id;
    public String cat_id;
    public String product_code;
    public String name;
    public String desc;
    public String prod_status;
    public String cost_price;
    public String min_order_qut;
    public String max_order_qut;
    public String prod_cat_name;
    public String taxable;
    public String base_unit_id;
    public String unit_price;
    public String order_unit_id;
    public String inventory;
    public String spcl_price;
    public String warehouse_name;
    public String warehouse_loc;
    public String stock_loc;
    public String prod_created_date;
    public String prod_update_date;
    public String prod_updated_by;
    public String prod_type;
    public String tier_price;
    public String prod_last_sync_id;
    public String avail_stock;
    public String img;
    public String thumb_image;
    public String qty;
}


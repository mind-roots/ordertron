package Model;

/**
 * Created by abc on 12/7/2015.
 */
public class Model_Product_Image {
    public String prefix_name;
    public String id;
    public String sink_id;
    public String product_id;
    public String image;
    public String thumb_image;
    public String status;
    public String is_main;
    public String type;
    public String created_date;
    public String update_date;
    public String updated_by;
    public String last_sync_id;

}

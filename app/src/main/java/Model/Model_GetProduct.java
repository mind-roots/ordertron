package Model;


public class Model_GetProduct {
    public String prefix_name;
    public String order_unit_name;
    public String base_unit_name;
    public String id;
    public String sink_id;
    public String cat_id;
    public String product_code;
    public String name;
    public String desc;
    public String status;
    public String fullname;
    public String cost_price;
    public String min_order_qut;
    public String max_order_qut;
    public String taxable;
    public String base_unit_id;
    public String unit_price;
    public String order_unit_id;
    public String inventory;
    public String spcl_price;
    public String warehouse_name;
    public String warehouse_loc;
    public String stock_loc;
    public String created_date;
    public String update_date;
    public String updated_by;
    public String type;
    public String tier_price;
    public String last_sync_id;
    public String view_type;
    public String avail_stock;
    public String img;
    public String thumb_image;

    public String qty;
    public String cat_name;

}

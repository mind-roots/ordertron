package Model;

/**
 * Created by admin on 1/5/2016.
 */
public class Model_Add_To_Cart {
    public String prod_id;
    public String prod_name;
    public String prod_sku;
    public String qty;
    public String price;
    public String spcl_price;
    public String max_qty;
    public String min_qty;
    public String base_unit;
    public String order_unit;
    public String taxable;
    public String prod_image;
    public String tier_price;
    public String quantity_upto;
    public String discount_type;
    public String discount;
    public String inventory;
    public String product_total;
    public String original_totl;
    public String apl;

}

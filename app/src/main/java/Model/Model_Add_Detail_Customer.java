package Model;

/**
 * Created by abc on 12/5/2015.
 */
public class Model_Add_Detail_Customer {
    public String prefix_name;
    public String id;
    public String ws_users_id;
    public String first_name;
    public String last_name;
    public String add_1;
    public String add_2;
    public String add_3;
    public String suburb;
    public String postcode;
    public String tel_1;
    public String tel_2;
    public String fax;
    public String email;
    public String default_shipping_add;
    public String default_billing_add;
    public String status;
    public String created_date;
    public String update_date;
    public String updated_by;
    public String country;
    public String state;
    public String act;


}

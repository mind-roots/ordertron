package Model;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.security.ProviderInstaller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class JSONParser {
    // constructor
    public JSONParser() {

    }

/*             key value pairs
            builder = new Uri.Builder()
                    .appendQueryParameter("auth_code", auth_code);*/
    public String getJSONFromUrl(String urlAddress, Uri.Builder builder, Context context) {
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
       try {
           try {
            ProviderInstaller.installIfNeeded(context);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
////        try {
//            SSLContext sslcontext = SSLContext.getInstance("TLSv1.2");
//
//            sslcontext.init(null,
//                    null,
//                    null);
//            SSLSocketFactory NoSSLv3Factory = new NoSSLv3SocketFactory(sslcontext.getSocketFactory());
//
//            HttpsURLConnection.setDefaultSSLSocketFactory(NoSSLv3Factory);
//            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
//            sslContext.init(null, null, null);
//
//            SSLEngine engine = sslContext.createSSLEngine();
//
//            engine.getSupportedProtocols();




           SSLContext sslcontext = SSLContext.getInstance("TLSv1");

           sslcontext.init(null,
                   null,
                   null);
           SSLSocketFactory NoSSLv3Factory = new NoSSLv3SocketFactory(sslcontext.getSocketFactory());

           HttpsURLConnection.setDefaultSSLSocketFactory(NoSSLv3Factory);




           HttpsURLConnection.setDefaultSSLSocketFactory(NoSSLv3Factory);
           URL url = new URL(urlAddress);
           HttpURLConnection con = (HttpURLConnection) url
                   .openConnection();

            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            con.setRequestProperty("Content-Type", "application/json");
            con.setReadTimeout(15000);
            con.setConnectTimeout(15000);
            con.setRequestMethod("POST");
            con.setDoInput(true);
            con.setDoOutput(true);

            String query = builder.build().getEncodedQuery();
           con.connect();
            //adds key value pair to your request
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            // use this response code to get status codes like 200 is for
            // success,500 for error on server side etc
            int responseCode = con.getResponseCode();
            System.out.println(" code hh : "+responseCode);

            in = con.getInputStream();
            // in = url.openStream(); remove this line
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    in, "iso-8859-1"), 8);
            String line;
            while ((line = reader.readLine()) != null) {
                tempData.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return tempData.toString();
    }


    /*             key value pairs
                builder = new Uri.Builder()
                        .appendQueryParameter("auth_code", auth_code);*/
    public String getresFromUrl(String urlAddress, Uri.Builder builder) {
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;

        try {
            URL url = new URL(urlAddress);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            con.setRequestProperty("Content-Type", "application/json");
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            String query = builder.build().getEncodedQuery();

            //adds key value pair to your request
            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            // use this response code to get status codes like 200 is for
            // success,500 for error on server side etc
            int responseCode = connection.getResponseCode();
            System.out.println(" code hh : "+responseCode);
            if(responseCode == 200) {
                // response code is OK
                in = connection.getInputStream();
            }else{
                // response code is not OK
            }

            // in = url.openStream(); remove this line
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(in,"iso-8859-1"),8);
            String line;
            while ((line = reader.readLine()) != null) {
                tempData.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return tempData.toString();
    }
}
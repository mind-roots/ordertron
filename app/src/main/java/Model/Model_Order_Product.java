package Model;

/**
 * Created by abc on 12/7/2015.
 */
public class Model_Order_Product {
    public String prefix_name;
    public String id;
    public String order_id;
    public String product_id;
    public String product_name;
    public String product_code;
    public String qty;
    public String qty_new_order;
    public String act_qty;
    public String unit_price;
    public String sub_total;
    public String sink_id;
    public String type;
    public String created_date;
    public String update_date;
    public String updated_by;
    public String last_sync_id;
    public String min_qty;
    public String max_qty;
    public String inventory;
    public String applied_price;
    public String order_unit;
    public String base_unit;
    public String tier_price;
    public String spcl_price;
    public String image;
    public String thumb_image;
    public String taxable;
}

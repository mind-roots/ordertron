package Model;

/**
 * Created by abc on 12/7/2015.
 */
public class Model_Order_Info {
    public String prefix_name;
    public String order_id;
    public String customer_id;
    public String billing_country;
    public String billing_state;
    public String billing_suburb;
    public String billing_add_1;
    public String billing_add_2;
    public String billing_add_3;
    public String billing_postcode;
    public String shipping_country;
    public String shipping_state;
    public String shipping_suburb;
    public String shipping_add_1;
    public String shipping_add_2;
    public String shipping_add_3;
    public String shipping_postcode;
    public String shipping_cost;
    public String sub_total;
    public String tax_total;
    public String total;
    public String order_notes;
    public String internal_notes;
    public String order_status;
    public String back_order;
    public String main_order_id;
    public String delivery_date;
    public String delivery_notes;
    public String created_date;
    public String created_by;
    public String update_date;
    public String updated_by;
    public String order_name;
    public String order_by;
    public String sink_id;
    public String type;
    public String last_sync_id;
    public String mandetory_delivery_order;
    public String show_deliverydate_order;
}

package Model;

/**
 * Created by abc on 12/7/2015.
 */
public class Model_Product_Inventory {
    public String prefix_name;
    public String id;
    public String product_id;
    public String sink_id;
    public String aval_stock;
    public String reserve_qut;
    public String bck_ord_aval;
    public String status;
    public String warehouse_name;
    public String warehouse_location;
    public String stock_location;
    public String created_date;
    public String update_date;
    public String updated_by;
    public String last_sync_id;
    public String type;
}

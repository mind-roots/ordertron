package Model;

/**
 * Created by ASUS PC on 9/22/2015.
 */
public class ModelLogin {

    public String prefix_name;

    public String auth_code;
    public String company_name;
    public String company_logo;
    public String currency_code;
    public String currency_symbol;
    public String shipping_status;
    public String shipping_cost;
    public String tax_label;
    public String message_count;
    public String tax_percentage;
    public String customer_business;
}

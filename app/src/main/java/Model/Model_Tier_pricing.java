package Model;

/**
 * Created by abc on 12/7/2015.
 */
public class Model_Tier_pricing {
    public String prefix_name;
    public String id;
    public String product_id;
    public String customer_group_id;
    public String quantity_upto;
    public String discount_type;
    public String discount;
    public String created_date;
    public String update_date;
    public String updated_by;
    public String sink_id;
    public String type;
    public String last_sync_id;

}

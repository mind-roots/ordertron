package Model;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.ordertron.Dashboard;
import com.ordertron.R;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by abc on 12/4/2015.
 */
public class Utils {

    // public static String login = "http://appsglory.com.au/webservices/index.php/data/getlogin?";
    public static boolean startsync = false;
  //  public static String base_url = "http://appsglory.com.au/app/webservices/index.php/data/";
    //public static String base_url = "http://miinex.com/app/webservices/index.php/data/";
   // public static String base_url = "https://app.miinex.com/webservices/index.php/data/";
  //  public static String base_url = "https://app.appsglory.com.au/webservices/index.php/data/";


 //public static String base_url = "https://app.miinex.com/webservices/index.php/data/";
  //public static String base_url = "https://app.appsglory.com.au/webservices/index.php/data/";

   //***********************Url for live******************************
public static String base_url = "https://app.ordertron.com/webservices/index.php/data/";

    public static String login = base_url + "getlogin?";
    public static String logoutlink = base_url + "forgot?";
    public static String getcustomer = base_url + "getcustomer?";
    public static String adddetailcustomer = base_url + "add_detail_customer?";
    public static String getcategories = base_url + "getcategories?";
    public static String getpage = base_url + "getpage?";
    public static String getproducts = base_url + "getproduct?";
    public static String getproductsimage = base_url + "getproductimage?";
    public static String getproductsinventory = base_url + "getproductinventory?";
    public static String getproducttierpricing = base_url + "getproducttier_pricing?";
    public static String getorderinfo = base_url + "getorder_info?";
    public static String getorderproduct = base_url + "getorder_product?";
    public static String getordertemplate = base_url + "getorder_template?";
    public static String logout = base_url + "logout?";
    public static String timezone = base_url + "gettimezones?";
    public static String get_messages = base_url + "get_messages?";
    public static String Imagepath = "http://appsglory.com.au/app/uploads/";
    public static String geturl = base_url + "getpage?";
    public static String order_save = base_url + "ordersave?";
    public static String add_customer_address = base_url + "add_customer_address?";
    public static String notification_setting = base_url + "notificationsetting?";
    public static String getNotification_setting = base_url + "getnotificationsetting?";
    public static String order_edit = base_url + "orderedit?";
    public static String cancel_order = base_url + "order_cancel?";
    public static String orderproduct_edit = base_url + "orderproduct_edit?";
    public static String delete = base_url + "ordertemplate_delete?";
    public static String insert_temp = base_url + "insertorder_template?";
    public static String update = base_url + "updateorder_template?";

    public static AlertDialog.Builder builder;
    public static String setcountry;
    public static int pos = -1;


    // **************************************Connection check**************************************
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    // **************************************Connection Dialog**************************************
    public static void conDialog(final Context c) {
        new AlertDialog.Builder(c, Dashboard.style)
                .setTitle("Internet connection unavailable.")
                .setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.")
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                c.startActivity(new Intent(
                                        Settings.ACTION_WIRELESS_SETTINGS));
                            }
                        })

                .show();

    }

    public static void simDialog(final Context c, String title, String msg) {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                c, Dashboard.style);

        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setIcon(android.R.drawable.ic_menu_save);
        alert.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                        Utils.startsync = true;
                    }
                });

        alert.show();
    }


    // *******************************************Alert Dialog**************************************
    public static void dialog(Context ctx, String title, String msg) {

        new AlertDialog.Builder(ctx, Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete

                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }


    public static void hidekeyboard(Context ctx) {
        // TODO Auto-generated method stub
        View view = ((Activity) ctx).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) ((Activity) ctx).getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    //----------------time zone--------------------//
    public static String timezone(String datevalue, String timezone, String format) {
        String sDateInAmerica = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format);

          String dateInString = "July 7, 2016 3:00 PM";
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = formatter.parse(datevalue);
            SimpleDateFormat sdfAmerica = new SimpleDateFormat("MMM d, yyyy h:mm a");
            sdfAmerica.setTimeZone(TimeZone.getTimeZone(timezone));
            sDateInAmerica = sdfAmerica.format(date);
        } catch (Exception e) {
            sDateInAmerica = datevalue;
        }
        return sDateInAmerica;
    }
  //----------------time zone--------------------//
    public static String timezone1(String datevalue, String timezone, String format) {
        String sDateInAmerica = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format);

          String dateInString = "July 7, 2016 3:00 PM";
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = formatter.parse(datevalue);
            SimpleDateFormat sdfAmerica = new SimpleDateFormat("dd MMM yyyy - hh:mm a");
            sdfAmerica.setTimeZone(TimeZone.getTimeZone(timezone));
            sDateInAmerica = sdfAmerica.format(date);
        } catch (Exception e) {
            sDateInAmerica = datevalue;
        }
        return sDateInAmerica;
    }

    //---------------------country Selectore---------------------//
    public static void MyDialogSingle(final Activity activity, final TextView country_txt, String countryget) {
        setcountry = "";
        final CharSequence[] c = activity.getResources().getStringArray(R.array.countries);
        for (int i = 0; i < c.length; i++) {
            if (countryget.equals(c[i].toString())) {
                pos = i;
                break;
            } else {
                pos = -1;
            }
        }

        builder = new AlertDialog.Builder(activity, R.style.DialogTheme);
        builder.setTitle("Select Country");

        builder.setSingleChoiceItems(c, pos,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setcountry = c[which].toString();


                    }
                });

        builder.setPositiveButton("DONE",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        country_txt.setTextColor(activity.getResources().getColor(android.R.color.black));
                        country_txt.setText(setcountry);
                        dialog.dismiss();

                    }
                });

        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

}


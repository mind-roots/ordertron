package Model;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import com.ordertron.Dashboard;
import com.ordertron.LoginActivity;
import com.ordertron.More_Setting_Accounts;
import com.ordertron.More_Settings;

import org.json.JSONException;
import org.json.JSONObject;

import DataBaseUtils.DatabaseQueries;

/**
 * Created by user on 5/2/2016.
 */

public class Methods {
    DatabaseQueries query;
    SharedPreferences prefs;
    String prefix;
    Context mContext;
    public static  int count;
    public static  int placeorder=0;
    public Methods(Context con) {
        mContext = con;
        query = new DatabaseQueries(mContext);
        prefs = mContext.getSharedPreferences("login", mContext.MODE_PRIVATE);
        prefix = prefs.getString("prefix_name", "");
    }

    public void dialog(String msg) {
        String countQuery = "SELECT  * FROM login ";
        count = query.getcartcount(countQuery);
        if (count > 1) {
            //  singledialog("There was a problem in your logged in session. Please switch your account in settings to a different wholesaler or contact wholesaler for further information.","Go to settings",count);
            singledialog("" + msg, "Go to settings", count);
        } else {
            // singledialog("There was a problem in your logged in session. Please try to login again or contact wholesaler for further information.","OK",count);
            singledialog("" + msg, "OK", count);
        }


    }

    private void singledialog(String msg, String btn, final int count) {

        new AlertDialog.Builder(mContext, Dashboard.style)
                .setTitle("Logout")
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(btn,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                                if (count > 1) {
                                    Dashboard.item.clear();
                                    prefs = mContext.getSharedPreferences("login", mContext.MODE_PRIVATE);
                                    prefix = prefs.getString("prefix_name", "");
                                    //Toast.makeText(mContext,prefix,Toast.LENGTH_SHORT).show();
                                    query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_LOGIN);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_GETCUSTOMER);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_ADD_DETAIL_CUSTOMER);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_GETCATEGORIES);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_GETPAGE);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_GET_PRODUCTS);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_GET_PRODUCT_IMAGE);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_GET_PRODUCT_INVENTORY);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_TIER_PRICING);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_ORDER_INFO);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_ORDER_PRODUCT);
                                    query.deleteoneTable(prefix, DatabaseQueries.TABLE_ORDER_TEMPLATE);
                                    SharedPreferences preferences = mContext.getSharedPreferences("login", 0);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.clear();
                                    editor.commit();
                                    try {
                                        More_Setting_Accounts.pos = 1;
                                        More_Setting_Accounts.flagrefresh = true;
                                        Intent intent = new Intent(mContext, More_Settings.class);
                                        intent.putExtra("del_account","del_account");
                                        mContext.startActivity(intent);
                                        Dashboard.categorystack.clear();
                                        if(placeorder==1) {
                                            placeorder=0;
                                            ((Activity) mContext).finish();
                                        }
                                    } catch (Exception e) {

                                    }

                                } else {
                                    new logout().execute();
                                }

                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    //*****************************Calling LogOut AsyncTasks*****************************************
    //*************************************************************************************************
    public class logout extends AsyncTask<Void, Void, Void> {
        String status = "false", lgout, data_desc, data;
        ProgressDialog dia;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(mContext);
            dia.setMessage("Logging out");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String responseLogOut = LogOutMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));

            System.out.println("LOGOUT Response:::" + responseLogOut);
//*************************Parsing for Logout******************************************
            try {
                JSONObject obj = new JSONObject(responseLogOut);
                status = obj.getString("Status");
                if (status.equals("false")) {
                    lgout = obj.getString("logout");

                    JSONObject datades = obj.getJSONObject("Data");
                    data_desc = datades.getString("result");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dia.dismiss();
            if (status.equals("true")) {
                Dashboard.item.clear();

                query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
                query.delete(DatabaseQueries.TABLE_LOGIN);
                query.delete(DatabaseQueries.TABLE_GETCUSTOMER);
                query.delete(DatabaseQueries.TABLE_ADD_DETAIL_CUSTOMER);
                query.delete(DatabaseQueries.TABLE_GETCATEGORIES);
                query.delete(DatabaseQueries.TABLE_GETPAGE);
                query.delete(DatabaseQueries.TABLE_GET_PRODUCTS);
                query.delete(DatabaseQueries.TABLE_GET_PRODUCT_IMAGE);
                query.delete(DatabaseQueries.TABLE_GET_PRODUCT_INVENTORY);
                query.delete(DatabaseQueries.TABLE_TIER_PRICING);
                query.delete(DatabaseQueries.TABLE_ORDER_INFO);
                query.delete(DatabaseQueries.TABLE_ORDER_PRODUCT);
                query.delete(DatabaseQueries.TABLE_ORDER_TEMPLATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();
                try {

                    Intent i = new Intent(mContext, LoginActivity.class);
                    mContext.startActivity(i);
                    ((Activity) mContext).finish();
                    Dashboard.page = 0;
                } catch (Exception e) {

                }
            } else {
                if (lgout.equals("yes")) {
                    Dashboard.item.clear();

                    query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
                    query.delete(DatabaseQueries.TABLE_LOGIN);
                    query.delete(DatabaseQueries.TABLE_GETCUSTOMER);
                    query.delete(DatabaseQueries.TABLE_ADD_DETAIL_CUSTOMER);
                    query.delete(DatabaseQueries.TABLE_GETCATEGORIES);
                    query.delete(DatabaseQueries.TABLE_GETPAGE);
                    query.delete(DatabaseQueries.TABLE_GET_PRODUCTS);
                    query.delete(DatabaseQueries.TABLE_GET_PRODUCT_IMAGE);
                    query.delete(DatabaseQueries.TABLE_GET_PRODUCT_INVENTORY);
                    query.delete(DatabaseQueries.TABLE_TIER_PRICING);
                    query.delete(DatabaseQueries.TABLE_ORDER_INFO);
                    query.delete(DatabaseQueries.TABLE_ORDER_PRODUCT);
                    query.delete(DatabaseQueries.TABLE_ORDER_TEMPLATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.clear();
                    editor.commit();
                    try {

                        Intent i = new Intent(mContext, LoginActivity.class);
                        mContext.startActivity(i);
                        ((Activity) mContext).finish();
                        Dashboard.page = 0;
                    } catch (Exception e) {

                    }
                }
                // Toast.makeText(mContext, "Network Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
    //************************Webservice parameter LogOutMethod *************************

    public String LogOutMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);

        res = parser.getJSONFromUrl(Utils.logout, builder,mContext);
        return res;
    }
}

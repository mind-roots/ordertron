package Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ordertron.Dashboard;
import com.ordertron.Order_id;
import com.ordertron.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import DataBaseUtils.DatabaseQueries;
import Model.Model_Add_Detail_Customer;
import Model.Model_Order_Info;
import Model.Model_get_notification;
import Model.Utils;

/**
 * Created by Abhijeet on 12/16/2015.
 */
public class OrderDetail_Fragment extends Fragment {
    public static TextView delivery, order_total, billing_add, shipping_add, order_notes, delivery_notes, orderPlaced_by, order_status, submission_date;
    //public static String  ordernotes, deliverynotes, orderplacedby,  state, country, phone, fax, postcode;
    View root;
    Intent intent;
    DatabaseQueries databaseQueries;
    SharedPreferences prefs;
    public static String convettdate = "";
    Menu menu;
    int secondformat = 0;
    NumberFormat formatter;
    RelativeLayout d_lay;
    public static String showdeliverydate, mand_date, get_date,show_deliverydate_order,mandetory_delivery_order;
    ArrayList<Model_get_notification> showdelivery = new ArrayList<>();
    public static  ArrayList<Model_Order_Info> orderinfo = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.orderdetail_fragment, container, false);

        init();
        setHasOptionsMenu(true);
        Dashboard.isVisible = false;
        setdata();

        try {
            showdelivery = databaseQueries.get_notification_setting(prefs.getString("prefix_name", null));
            orderinfo=databaseQueries.getmand_date(prefs.getString("prefix_name", null), Order_id.order_id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < showdelivery.size(); i++) {
            showdeliverydate = showdelivery.get(i).show_deliverydate;
            mand_date = showdelivery.get(i).mandetory_delivery;

//**********************getting particular order delivery date ************************************

            show_deliverydate_order=orderinfo.get(0).show_deliverydate_order;
            mandetory_delivery_order=orderinfo.get(0).mandetory_delivery_order;
        }
        if (show_deliverydate_order.equals("yes")) {
            d_lay.setVisibility(View.VISIBLE);
            // delivery.setText(Order_id.order_infolist.get(0).delivery_date);
            String daa = Order_id.order_infolist.get(0).delivery_date;

            String DateStr = daa;
            if (DateStr == null || DateStr.equals("")) {
                delivery.setText("");
            } else {
                SimpleDateFormat sim = new SimpleDateFormat(" EEE, dd MMM yyyy");
                try {
                    Date d = new SimpleDateFormat("yyyy-MM-dd").parse(DateStr);
                    String convettdate = sim.format(d);
                    delivery.setText(convettdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                    secondformat = 1;
                }
                if (secondformat == 1) {

                    delivery.setText(DateStr);

                }
            }
        } else {
            d_lay.setVisibility(View.GONE);
        }
        return root;
    }


    public void init() {
        formatter = new DecimalFormat("#0.00");
        databaseQueries = new DatabaseQueries(getActivity());
        intent = getActivity().getIntent();
        delivery = (TextView) root.findViewById(R.id.delivery);
        order_total = (TextView) root.findViewById(R.id.order_total);
        billing_add = (TextView) root.findViewById(R.id.billing_address);
        shipping_add = (TextView) root.findViewById(R.id.shipping_address);
        order_notes = (TextView) root.findViewById(R.id.order_notes);
        delivery_notes = (TextView) root.findViewById(R.id.delivery_notes);
        orderPlaced_by = (TextView) root.findViewById(R.id.placed_by);
        order_status = (TextView) root.findViewById(R.id.status);
        submission_date = (TextView) root.findViewById(R.id.submission);
        d_lay = (RelativeLayout) root.findViewById(R.id.d_lay);
        prefs = getActivity().getSharedPreferences("login", getActivity().MODE_PRIVATE);
        try {

            Model_Add_Detail_Customer c = databaseQueries.Getvalues("add_detail_customer", Order_id.prefix_name);
//            phone = c.tel_1;
//            fax = c.fax;

        } catch (Exception e) {

        }


        //*****************Setting UI values for Order Details from Order_id list position*******************************


    }

    private void setdata() {try {
        if (Order_id.order_infolist.get(0).total.equals("TBA")) {
            order_total.setText(Order_id.order_infolist.get(0).total);
        } else {
            order_total.setText("$" + formatter.format(Double.parseDouble(Order_id.order_infolist.get(0).total)));
        }
        String Full_billing_adress = Order_id.order_infolist.get(0).billing_add_1;

        if (!Order_id.order_infolist.get(0).billing_add_2.equals("")) {
            Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_add_2;
        }
        if (!Order_id.order_infolist.get(0).billing_add_3.equals("")) {
            Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_add_3;
        }
        if (!Order_id.order_infolist.get(0).billing_suburb.equals("")) {
            Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_suburb;
        }
        if (!Order_id.order_infolist.get(0).billing_state.equals("")) {
            Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_state;
        }
        if (!Order_id.order_infolist.get(0).billing_country.equals("")) {
            Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_country;
        }
        if (!Order_id.order_infolist.get(0).billing_postcode.equals("")) {
            Full_billing_adress = Full_billing_adress + ", " + Order_id.order_infolist.get(0).billing_postcode;
        }

        String Full_shipping_adress = Order_id.order_infolist.get(0).shipping_add_1;

        if (!Order_id.order_infolist.get(0).shipping_add_2.equals("")) {
            Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_add_2;
        }
        if (!Order_id.order_infolist.get(0).shipping_add_3.equals("")) {
            Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_add_3;
        }
        if (!Order_id.order_infolist.get(0).shipping_suburb.equals("")) {
            Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_suburb;
        }
        if (!Order_id.order_infolist.get(0).shipping_state.equals("")) {
            Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_state;
        }
        if (!Order_id.order_infolist.get(0).shipping_country.equals("")) {
            Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_country;
        }
        if (!Order_id.order_infolist.get(0).shipping_postcode.equals("")) {
            Full_shipping_adress = Full_shipping_adress + ", " + Order_id.order_infolist.get(0).shipping_postcode;
        }

        billing_add.setText(Full_billing_adress);
        shipping_add.setText(Full_shipping_adress);
        order_notes.setText(Order_id.order_infolist.get(0).order_notes);

        //****************************Ignore Break text ****************************************

        String lineSep = System.getProperty("line.separator");
        String yourString = Order_id.order_infolist.get(0).delivery_notes;
        yourString = yourString.replaceAll("<br />", lineSep);
        delivery_notes.setText(yourString);

        orderPlaced_by.setText(Order_id.order_infolist.get(0).order_by);
        order_status.setText(Order_id.order_infolist.get(0).order_status);
        String subdate = Utils.timezone(Order_id.order_infolist.get(0).order_name, databaseQueries.get_timezone_setting(Order_id.prefix_name), "dd MMM yyyy - hh:mm a");
        SimpleDateFormat sim2 = new SimpleDateFormat(" EEE, dd MMM yyyy");// converted format
        try {
            Date d = new SimpleDateFormat("MMM d, yyyy h:mm a").parse(subdate);// actual format
            String convettdate = sim2.format(d);
            submission_date.setText(convettdate);
        } catch (ParseException e) {
            e.printStackTrace();

        }
    }catch (Exception e)
    {
        e.printStackTrace();

    }

    }

    @Override
    public void onResume() {
        super.onResume();
        setdata();
//        Toast.makeText(getActivity(),"hello",Toast.LENGTH_SHORT).show();
    /*    if(Order_Edit.check==false) {
            billing_add.setText(Order_Edit.billingAdress);
            shipping_add.setText(Order_Edit.shippingAdress);
            order_notes.setText(Order_Edit.order);
            delivery_notes.setText(Order_Edit.delivry);
            orderPlaced_by.setText(Order_Edit.order);
            System.out.println("billing adress :: " + billing_add.getText().toString());
            System.out.println("shipping adress :: " + OrderDetail_Fragment.shipping_add.getText().toString());
            System.out.println("order_notes adress :: " + OrderDetail_Fragment.order_notes.getText().toString());
            System.out.println("delivery_notes adress :: " + OrderDetail_Fragment.delivery_notes.getText().toString());
            System.out.println("orderPlaced_by adress :: " + OrderDetail_Fragment.orderPlaced_by.getText().toString());
        }*/

    }
}

package Fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ordertron.Dashboard;
import com.ordertron.LoginActivity;
import com.ordertron.MessageActivity;
import com.ordertron.More_Notification;
import com.ordertron.More_Settings;
import com.ordertron.OrdersActivity;
import com.ordertron.R;
import com.ordertron.Tutorial;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import DataBaseUtils.DatabaseQueries;
import Model.JSONParser;
import Model.Model_getUrl;
import Model.Utils;


/**
 * Created by android on 12/4/2015.
 */
public class MoreActivity extends Fragment {
    RelativeLayout rl_orders, rl_messages, rl_information, rl_settings, rl_logout,rl_share,rl_feedback,rl_tutorial;
    public static ArrayList<Model_getUrl> url = new ArrayList<>();
    ProgressBar progressBar;
    DatabaseQueries queries;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    boolean b;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.more, container, false);
        init(root);

        if (Dashboard.flag == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);

        }
        Dashboard.isVisible = false;
        clickevent();
        Dashboard.actionbar.setHomeButtonEnabled(false);
        Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);

        return root;

    }



    @Override
    public void onResume() {
        super.onResume();
        Dashboard.actionbar.setHomeButtonEnabled(false);
        Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        //   Dashboard.page=4;
    }




    private void init(View root) {
        b = Utils.isNetworkConnected(getActivity());
        queries = new DatabaseQueries(getActivity());
        rl_orders = (RelativeLayout) root.findViewById(R.id.rl_orders);
        rl_messages = (RelativeLayout) root.findViewById(R.id.rl_messages);
        rl_information = (RelativeLayout) root.findViewById(R.id.rl_information);
        rl_settings = (RelativeLayout) root.findViewById(R.id.rl_settings);
        rl_share = (RelativeLayout) root.findViewById(R.id.rl_share);
        rl_feedback = (RelativeLayout) root.findViewById(R.id.rl_feedback);
        rl_tutorial = (RelativeLayout) root.findViewById(R.id.rl_tutorial);
        rl_logout = (RelativeLayout) root.findViewById(R.id.rl_logout);
        prefs = getActivity().getSharedPreferences("login", getActivity().MODE_PRIVATE);

        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);

    }

    public void clickevent() {

        rl_orders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), OrdersActivity.class);
                startActivity(intent);

            }

        });


        rl_messages.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MessageActivity.class);
                startActivity(intent);
            }

        });
        rl_information.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                rl_information.setClickable(false);
                if (!b) {
                    Utils.conDialog(getActivity());
                } else {
                    new get_url().execute();

                }

            }

        });

        rl_settings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), More_Settings.class);
                startActivity(i);


            }

        });
        rl_tutorial.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Tutorial.class);
                startActivity(i);


            }

        });

        rl_feedback.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

//Toast.makeText(getActivity(),"Still to implement in MoreActivity",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {"feedback@ordertron.com"};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Customer feedback");
                intent.setType("message/rfc822");
                startActivity(Intent.createChooser(intent, "Send mail"));
            }

        });
        rl_share.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TEXT,
//                        "Hey! I’m using a great app for wholesale ordering. Check it out today at www.ordertron.com");
//                sendIntent.setType("text/plain");
//                startActivity(sendIntent);
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hey! I’m using a great app for wholesale ordering. Check it out today at www.ordertron.com");
                startActivity(shareIntent);

//                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                sharingIntent.setType("text/html");
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, ("Hey! I’m using a great app for wholesale ordering. Check it out today at www.ordertron.com"));
//                startActivity(Intent.createChooser(sharingIntent,"Share using"));

            }

        });
        rl_logout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(getActivity(), "jjjjj", Toast.LENGTH_SHORT).show();
                //calling logout webservice
                if (!b) {
                    Utils.conDialog(getActivity());
                } else {
                    String countQuery = "SELECT  * FROM login ";
                    int count = queries.getcartcount(countQuery);
                    if (count > 1) {
                        logdialog(android.R.drawable.ic_dialog_alert, "Multiple Accounts Detected", "You cannot log out while logged into multiple accounts\n" +
                                "Go to Settings > Accounts & Delete the account to wish to exit from.", count);

                    } else {
                        logdialog(android.R.drawable.ic_dialog_alert, "Alert!", "Are you you want to logout this account?", count);
                    }
                }
            }

        });
    }

    //********************************************Get URl parsing*************************************
    public class get_url extends AsyncTask {
        String status = "false", result;
        String a=prefs.getString("prefix_name", null);
        String prefix_name = prefs.getString("prefix_name", null);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);
            url.clear();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            String response = get_urlMethod(prefix_name);


            System.out.println("LOGIN RESPONCE:::" + response);
            try {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONArray data_obj = obj.getJSONArray("Data");

                    for (int i = 0; i < data_obj.length(); i++) {
                        JSONObject obj1 = data_obj.getJSONObject(i);
                        Model_getUrl geturl = new Model_getUrl();
                        geturl.id = obj1.getString("id");
                        geturl.page_url = obj1.getString("page_url");
                        geturl.status = obj1.getString("status");
                        geturl.page_title = obj1.getString("page_title");
                        url.add(geturl);
                    }


                } else {
                    status = obj.getString("Status");
                    if (status.equals("false")) {
                        JSONArray data_obj = obj.getJSONArray("Data");
                        for (int i = 0; i < data_obj.length(); i++) {
                            JSONObject obj1 = data_obj.getJSONObject(i);
                            result = obj1.getString("result");
                        }


                    }
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            //  progressBar.setVisibility(View.GONE);
//            if(status.equals("true")) {
//                Model_getUrl geturl = new Model_getUrl();
//                geturl.id = obj1.getString("id");
//                geturl.page_url = obj1.getString("page_url");
//                geturl.status= obj1.getString("status");
//\

            Intent intent = new Intent(getActivity(), More_Notification.class);
            startActivity(intent);
rl_information.setClickable(true);
//            }else
//                Toast.makeText(LoginActivity.this, "Network Error", Toast.LENGTH_SHORT);
//
//
//        }


        }
    }

    //*****************************Calling LogOut AsyncTasks*****************************************
    //*************************************************************************************************
    public class logout extends AsyncTask<Void, Void, Void> {
        String status = "false", data;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override

        protected Void doInBackground(Void... params) {
            String responseLogOut = LogOutMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));

            System.out.println("LOGOUT Response:::" + responseLogOut);
//*************************Parsing for Logout******************************************
            try {
                JSONObject obj = new JSONObject(responseLogOut);
                status = obj.getString("Status");


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            if (status.equals("true")) {
                Dashboard.item.clear();

                queries.delete(DatabaseQueries.TABLE_ADD_TO_CART);
                queries.delete(DatabaseQueries.TABLE_LOGIN);
                queries.delete(DatabaseQueries.TABLE_GETCUSTOMER);
                queries.delete(DatabaseQueries.TABLE_ADD_DETAIL_CUSTOMER);
                queries.delete(DatabaseQueries.TABLE_GETCATEGORIES);
                queries.delete(DatabaseQueries.TABLE_GETPAGE);
                queries.delete(DatabaseQueries.TABLE_GET_PRODUCTS);
                queries.delete(DatabaseQueries.TABLE_GET_PRODUCT_IMAGE);
                queries.delete(DatabaseQueries.TABLE_GET_PRODUCT_INVENTORY);
                queries.delete(DatabaseQueries.TABLE_TIER_PRICING);
                queries.delete(DatabaseQueries.TABLE_ORDER_INFO);
                queries.delete(DatabaseQueries.TABLE_ORDER_PRODUCT);
                queries.delete(DatabaseQueries.TABLE_ORDER_TEMPLATE);
                editor = prefs.edit();
                editor.clear();
                editor.commit();
                System.out.println("LOGOUT KA ON POST");
                try {

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                    Dashboard.page = 0;
                } catch (Exception e) {

                }
            } else {
                Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
    //************************Webservice parameter get_urlMethod*************************

    public String get_urlMethod(String prefix_name) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name);
        res = parser.getJSONFromUrl(Utils.geturl, builder,getActivity().getApplicationContext());
        return res;
    }


    //************************Webservice parameter LogOutMethod *************************

    public String LogOutMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);

        res = parser.getJSONFromUrl(Utils.logout, builder,getActivity().getApplicationContext());
        return res;
    }

    private void logdialog(int ic_menu_save, String title, String msg, final int count) {

        new AlertDialog.Builder(getActivity(),Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                                if (count > 1) {
                                   // Toast.makeText(getActivity(), "More Accounts", Toast.LENGTH_LONG).show();
                                } else {
                                    new logout().execute();

                                }


                            }
                        })
                .setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();


                            }
                        })
                .setIcon(ic_menu_save).show();


    }
}

package Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ordertron.Dashboard;
import com.ordertron.LoginActivity;
import com.ordertron.MessageActivity;
import com.ordertron.More_Settings;
import com.ordertron.OrdersActivity;
import com.ordertron.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import DataBaseUtils.DatabaseQueries;
import DataBaseUtils.TabUtils;
import Interfaces.RefreshFragment;
import Model.JSONParser;
import Model.Utils;

/**
 * Created by android on 12/4/2015.
 */
public class HomeActivity extends Fragment implements RefreshFragment {


    public static ImageView img_Update, img_template, img_neworder, img_messages, img_orderhistory;
    public static TextView txt_business, txt_orderplaced, txt_Update, txt_msg_count;
    public static ProgressBar progressbar;
    View root;
    String mTimezone;
    DatabaseQueries queries;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    RelativeLayout message;
    LinearLayout lay_orderhistory, lay_template, lay_neworder;
    Menu newmenu;
    boolean b;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.home, container, false);
        setHasOptionsMenu(true);

        init(root);
        clickevents();


        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.newmenu = menu;
        super.onCreateOptionsMenu(newmenu, inflater);

        newmenu.findItem(R.id.add).setVisible(false);
        newmenu.findItem(R.id.edit).setVisible(false);
        newmenu.findItem(R.id.logout).setVisible(true);
        newmenu.findItem(R.id.setting).setVisible(true);
        newmenu.findItem(R.id.search).setVisible(false);
        newmenu.findItem(R.id.done).setVisible(false);
        newmenu.findItem(R.id.clear).setVisible(false);
        Dashboard.actionbar.setHomeButtonEnabled(false);
        Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.logout:
                if (Dashboard.flag == 0) {
                    dialog(android.R.drawable.ic_dialog_alert, "Sync in Progress", "Please wait. The latest data is being fetched from the server.");
                } else {
                    if (!b) {
                        Utils.conDialog(getActivity());
                    } else {
                        String countQuery = "SELECT  * FROM login ";
                        int count = queries.getcartcount(countQuery);
                        if (count > 1) {
                            logdialog(android.R.drawable.ic_dialog_alert, "Multiple Accounts Detected", "You cannot log out while logged into multiple accounts\n" +
                                    "Go to Settings > Accounts & Delete the account to wish to exit from.", count);

                        } else {
                            b = Utils.isNetworkConnected(getActivity());
                            if (!b) {
                                Utils.conDialog(getActivity());
                            }else {
                                logdialog(android.R.drawable.ic_dialog_alert, "Alert!", "Are you sure you want to logout from app?", count);
                            }
                        }


                    }

                }


                return true;
            case R.id.setting:
                if (Dashboard.flag == 0) {
                    dialog(android.R.drawable.ic_dialog_alert, "Sync in Progress", "Please wait. The latest data is being fetched from the server.");
                } else {
                    startActivity(new Intent(getActivity(), More_Settings.class));
                    Dashboard.setting=1;
                }

                return true;

        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(Dashboard.tabLayout.getSelectedTabPosition()==0) {
            Dashboard.actionbar.setTitle(prefs.getString("company_name", null));
            Dashboard.actionbar.setHomeButtonEnabled(false);
            Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
            setHasOptionsMenu(true);
        }
        if(Dashboard.categorystack.size()==0) {
            Dashboard.actionbar.setHomeButtonEnabled(false);
            Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
        }
        Dashboard.isVisible = true;

        String countQuery = "SELECT  * FROM cart_table";
        int count = queries.getcartcount(countQuery);
        TabUtils.updateTabBadge(Dashboard.cart, count);

        if (Dashboard.prefix == null) {
            img_Update.setVisibility(View.GONE);
            progressbar.setVisibility(View.VISIBLE);
        } else {
            img_Update.setVisibility(View.GONE);
            progressbar.setVisibility(View.VISIBLE);
            txt_Update.setText("Updating products and orders\ninformation from server.");
        }
        if (Dashboard.flag == 1) {
            progressbar.setVisibility(View.GONE);
            img_Update.setVisibility(View.VISIBLE);
            if (!Utils.isNetworkConnected(getActivity())) {
                // Utils.conDialog(Dashboard.this);
                img_Update.setBackgroundResource(R.drawable.ic_cross);
                txt_Update.setText("Sync failed. No internet connection found");
            }else {
                img_Update.setBackgroundResource(R.drawable.tick);
                txt_Update.setText("All product and order\ninformation is up-to-date.");
            }
        }
       /* if (Dashboard.flag == 2) {
            progressbar.setVisibility(View.GONE);
            img_Update.setVisibility(View.VISIBLE);
            txt_Update.setText("All product and order information are up to date.");
        }*/
        if(Dashboard.setting==1){
            Dashboard.setting=0;
//    String na=prefs.getString("company_name", null);
            txt_business.setText("Welcome " + prefs.getString("customer_business", null));
            Dashboard.actionbar.setTitle(prefs.getString("company_name", null));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // Dashboard.page=0;
    }

    //*********************************Initializing UI ***************************************************
    private void init(View root) {

        b = Utils.isNetworkConnected(getActivity());
        prefs = getActivity().getSharedPreferences("login", getActivity().MODE_PRIVATE);
        queries = new DatabaseQueries(getActivity());
        message = (RelativeLayout) root.findViewById(R.id.message);
        lay_template = (LinearLayout) root.findViewById(R.id.lay_template);
        lay_orderhistory = (LinearLayout) root.findViewById(R.id.lay_orderhistory);
        lay_neworder = (LinearLayout) root.findViewById(R.id.lay_neworder);
        img_Update = (ImageView) root.findViewById(R.id.img_Update);
        img_template = (ImageView) root.findViewById(R.id.img_template);
        img_neworder = (ImageView) root.findViewById(R.id.img_neworder);
        img_messages = (ImageView) root.findViewById(R.id.img_messages);
        img_orderhistory = (ImageView) root.findViewById(R.id.img_orderhistory);
        txt_business = (TextView) root.findViewById(R.id.txt_business);
        txt_orderplaced = (TextView) root.findViewById(R.id.txt_orderplaced);
        txt_Update = (TextView) root.findViewById(R.id.txt_Update);
        txt_msg_count = (TextView) root.findViewById(R.id.txt_msg_count);
        progressbar = (ProgressBar) root.findViewById(R.id.progressbar);

        //*************************Setting Text*****************************************
        txt_business.setText("Welcome " + prefs.getString("customer_business", null));
        //  txt_business.setText("Welcome namenamenamenamenamenamenamename");
        try {
            mTimezone = queries.get_timezone_setting(prefs.getString("prefix_name", null));
        } catch (Exception e) {
        }
        String val=prefs.getString("created_at", null);
        String xon=mTimezone;
        String order_null= Utils.timezone(prefs.getString("created_at", null), mTimezone, "dd MMM yyyy - hh:mm a");
        if(order_null==null||order_null.equals("null")){
            //txt_orderplaced.setText("You have not placed any order");
            txt_orderplaced.setText("");
        }else {
            txt_orderplaced.setText("Last order placed on " + Utils.timezone1(prefs.getString("created_at", null), mTimezone, "dd MMM yyyy - hh:mm a"));
        }
        // txt_orderplaced.setText("Last order placed on " + savedateformatter(prefs.getString("created_at", null)));
        txt_msg_count.setText(prefs.getString("message_count", "0"));
    }

    //**********************************Clickevents**********************************************
    public void clickevents() {
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Dashboard.flag == 0) {
                    dialog(android.R.drawable.ic_dialog_alert, "Sync in Progress", "Please wait. The latest data is being fetched from the server.");
                } else {
                    Intent i = new Intent(getActivity(), MessageActivity.class);
                    startActivity(i);
                }
            }
        });
        lay_orderhistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Dashboard.flag == 0) {
                    dialog(android.R.drawable.ic_dialog_alert, "Sync in Progress", "Please wait. The latest data is being fetched from the server.");
                } else {
                    Intent i = new Intent(getActivity(), OrdersActivity.class);
                    startActivity(i);
                }
            }
        });
        lay_neworder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Dashboard.flag == 0) {
                    dialog(android.R.drawable.ic_dialog_alert, "Sync in Progress", "Please wait. The latest data is being fetched from the server.");
                } else {
                    Dashboard.viewPager.setCurrentItem(1);

                }
            }
        });
        lay_template.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Dashboard.flag == 0) {
                    dialog(android.R.drawable.ic_dialog_alert, "Sync in Progress", "Please wait. The latest data is being fetched from the server.");
                } else {
                    Dashboard.viewPager.setCurrentItem(3);
                }
            }
        });
    }

    @Override
    public void refresh() {

        if (!Utils.isNetworkConnected(getActivity())) {
            // Utils.conDialog(Dashboard.this);
            img_Update.setBackgroundResource(R.drawable.ic_cross);
            txt_Update.setText("Sync failed. No internet connection found");
            if(Dashboard.tabLayout.getSelectedTabPosition()==0){
                Dashboard.actionbar.setHomeButtonEnabled(false);
                Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
            }
        }else {
            img_Update.setBackgroundResource(R.drawable.tick);
            txt_business.setText("Welcome " + prefs.getString("customer_business", null));
            txt_Update.setText("All product and order\ninformation is up-to-date.");
            // Dashboard.actionbar.setTitle(Dashboard.arrayList.get(0));
            Dashboard.actionbar.setTitle(prefs.getString("company_name", null));
            Dashboard.actionbar.setHomeButtonEnabled(false);
            Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
            setHasOptionsMenu(true);

            init(root);
        }
    }

    //*****************************Calling LogOut AsyncTasks*****************************************
    //*************************************************************************************************
    public class logout extends AsyncTask<Void, Void, Void> {
        String status = "false", data;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
            img_Update.setVisibility(View.GONE);
            txt_Update.setText("Logging Out..");
        }

        @Override
        protected Void doInBackground(Void... params) {
            String responseLogOut = LogOutMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null));

            System.out.println("LOGOUT Response::" + responseLogOut);
//*************************Parsing for Logout******************************************
            try {
                JSONObject obj = new JSONObject(responseLogOut);
                status = obj.getString("Status");


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressbar.setVisibility(View.GONE);
            if (status.equals("true")) {
                Dashboard.item.clear();

                queries.delete(DatabaseQueries.TABLE_ADD_TO_CART);
                queries.delete(DatabaseQueries.TABLE_LOGIN);
                queries.delete(DatabaseQueries.TABLE_GETCUSTOMER);
                queries.delete(DatabaseQueries.TABLE_ADD_DETAIL_CUSTOMER);
                queries.delete(DatabaseQueries.TABLE_GETCATEGORIES);
                queries.delete(DatabaseQueries.TABLE_GETPAGE);
                queries.delete(DatabaseQueries.TABLE_GET_PRODUCTS);
                queries.delete(DatabaseQueries.TABLE_GET_PRODUCT_IMAGE);
                queries.delete(DatabaseQueries.TABLE_GET_PRODUCT_INVENTORY);
                queries.delete(DatabaseQueries.TABLE_TIER_PRICING);
                queries.delete(DatabaseQueries.TABLE_ORDER_INFO);
                queries.delete(DatabaseQueries.TABLE_ORDER_PRODUCT);
                queries.delete(DatabaseQueries.TABLE_ORDER_TEMPLATE);
                SharedPreferences.Editor   editor = prefs.edit();
                editor.clear();
                editor.commit();



                try {

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                    Dashboard.page = 0;
                } catch (Exception e) {

                }
            } else {
                Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
    //************************Webservice parameter LogOutMethod *************************

    public String LogOutMethod(String prefix_name, String auth_code) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code);

        res = parser.getJSONFromUrl(Utils.logout, builder,getActivity().getApplicationContext());
        return res;
    }

    private void dialog(int ic_menu_save, String title, String msg) {


        new AlertDialog.Builder(getActivity(), Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();

                            }
                        })
//                .setNegativeButton("Cancel",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,
//                                                int which) {
//                                // continue with delete
//                                dialog.dismiss();
//
//
//                            }
//                        })
                .setIcon(ic_menu_save).show();


    }

    private void logdialog(int ic_menu_save, String title, String msg, final int count) {

        new AlertDialog.Builder(getActivity(), Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                                if (count > 1) {
                                    //Toast.makeText(getActivity(), "More Accounts", Toast.LENGTH_LONG).show();
                                } else {
                                    b = Utils.isNetworkConnected(getActivity());
                                    if (!b) {
                                        Utils.conDialog(getActivity());
                                    }else {
                                        new logout().execute();
                                    }


                                }


                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                .setIcon(ic_menu_save).show();


    }

    public String savedateformatter(String senddate) {

        // TODO Auto-generated method stub
        String setdate = "";
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = format1.parse(senddate);
            format1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            setdate = format1.format(newDate);
        } catch (Exception e) {
            setdate = "NA";
        }
        return setdate;

    }

}
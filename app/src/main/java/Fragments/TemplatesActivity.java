package Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ordertron.Dashboard;
import com.ordertron.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Adapters.TemplateAdapter;
import DataBaseUtils.DatabaseQueries;
import Interfaces.RefreshFragment;
import Model.JSONParser;
import Model.Methods;
import Model.ModelLogin;
import Model.Model_Template;
import Model.Utils;

/**
 * Created by android on 12/4/2015.
 */
public class TemplatesActivity extends Fragment implements RefreshFragment {
    public static RecyclerView temp_list;
    ProgressBar progressBar;
    SharedPreferences prefs;
    DatabaseQueries query;
    public static ArrayList<Model_Template> item = new ArrayList<Model_Template>();

    ArrayList<String> arr = new ArrayList<String>();
    String data;
    public static TemplateAdapter tadapter;
    RecyclerView.LayoutManager mLayoutManager;
    public static Menu newmenu;
    public static int tempedit = 0;
    String prefix;
    boolean b;
    View root;
    public static ImageView noordertemp;
    public static Methods methods;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.templates, container, false);
        setHasOptionsMenu(true);
        init();
        item.clear();
        //    Toast.makeText(getActivity(),"1",Toast.LENGTH_LONG).show();
        Dashboard.isVisible = false;
        methods = new Methods(getActivity());
        /// if (Dashboard.flag != 0) {
        try {

            item = query.GetTemplate(prefix, "all");
            int sizeofitem = item.size();
            System.out.println("sizeofitem" + sizeofitem);
        } catch (Exception e) {

        }
        if (item.size() != 0) {
            noordertemp.setVisibility(View.GONE);
            tadapter = new TemplateAdapter(getActivity(), item);
            temp_list.setAdapter(tadapter);
        } else {
            noordertemp.setVisibility(View.VISIBLE);
        }

        //}
        return root;
    }

    private void init() {
//        Dashboard.actionbar.setHomeButtonEnabled(false);
//        Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
        b = Utils.isNetworkConnected(getActivity());

        temp_list = (RecyclerView) root.findViewById(R.id.temp_list);
        progressBar = (ProgressBar) root.findViewById(R.id.progressbar);
        noordertemp = (ImageView) root.findViewById(R.id.noordertemp);
        query = new DatabaseQueries(getActivity());
        prefs = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        prefix = prefs.getString("prefix_name", null);
        mLayoutManager = new LinearLayoutManager(getActivity());
        temp_list.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.newmenu = menu;
        super.onCreateOptionsMenu(newmenu, inflater);
        newmenu.findItem(R.id.clear).setVisible(false);
        newmenu.findItem(R.id.logout).setVisible(false);
        newmenu.findItem(R.id.setting).setVisible(false);
        newmenu.findItem(R.id.search).setVisible(false);
        newmenu.findItem(R.id.add).setVisible(true);
        newmenu.findItem(R.id.done).setVisible(false);
        newmenu.findItem(R.id.edit).setVisible(true);
        tempedit = 0;
        if (tadapter != null) {
            tadapter.notifyDataSetChanged();
        }

        if (item.size() > 0) {
            newmenu.findItem(R.id.edit).setVisible(true);
        } else {
            newmenu.findItem(R.id.edit).setVisible(false);
        }

        MenuItem searchItem = newmenu.findItem(R.id.search);

        SearchView searchView = (SearchView) searchItem.getActionView();
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
        }
        searchItem.collapseActionView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem mitem) {
        super.onOptionsItemSelected(mitem);
        switch (mitem.getItemId()) {
            case R.id.add:
                popdialog();
//                Intent i = new Intent(getActivity(), Pop_Window_Template.class);
//                i.putExtra("Intenttype", "newtemp");
//                startActivity(i);
                return true;
            case R.id.edit:
                if (item.size() > 0) {
                    newmenu.findItem(R.id.done).setVisible(true);
                    newmenu.findItem(R.id.add).setVisible(false);
                    newmenu.findItem(R.id.edit).setVisible(false);
                    tempedit = 1;
                    tadapter.notifyDataSetChanged();
                }
                return true;
            case R.id.done:
                newmenu.findItem(R.id.done).setVisible(false);
                newmenu.findItem(R.id.add).setVisible(true);
                newmenu.findItem(R.id.edit).setVisible(true);
                tempedit = 0;
                tadapter.notifyDataSetChanged();
                return true;

        }
        return false;
    }

    private void popdialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("New Template");
        builder.setMessage("Enter the Template Name and tap on create");
        final View view = getActivity().getLayoutInflater().inflate(R.layout.edittext, null);
        final EditText input = (EditText) view
                .findViewById(R.id.etCategory);
        input.requestFocus();
        input.setHint("Enter Template Name");
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        input.setTextColor(Color.BLACK);
        builder.setView(view);
        builder.setCancelable(false);
//                i = getActivity().getIntent();
//                intentdata_popupwindow = i.getStringExtra("edit");
//                order_id = i.getStringExtra("sendtempid");
        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                hideKeyboardFrom(getActivity(), view);
                data = input.getText().toString();
                if (input.length() != 0) {
                    try {
                        arr = query.GetTemplate_name(prefix);
                    } catch (Exception e) {

                    }
                    String avial = "true";
                    for (int h = 0; h < arr.size(); h++) {
                        System.out.println("TEMP_name::" + arr.get(h));
                        if (arr.get(h).equalsIgnoreCase(data.trim())) {
                            avial = "false";

                        }
//                        else {
//                            avial = "true";
//
//                        }
                    }
                    if (avial.equals("false")) {
                        Toast.makeText(getActivity(), "Template already Exists", Toast.LENGTH_LONG).show();
                    } else {

                        //   dfs
                        if (!Utils.isNetworkConnected(getActivity())) {
                            int randomPIN = (int) (Math.random() * 9000) + 1000;
                            Model_Template a = new Model_Template();
                            a.template_name = data;
                            a.order_template_id = String.valueOf(randomPIN);
                            a.prefix_name = prefs.getString("prefix_name", "");
                            a.last_sync_id = "off";
                            a.type = "newupdate";
                            a.qty = "";
                            a.product_id = "";
                            try {
                                if (!query.isTemplateExist(a.prefix_name, a.order_template_id)) {
                                    query.gettemplate(a);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                item = query.GetTemplate(prefix, "all");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (item.size() > 0) {
                                newmenu.findItem(R.id.edit).setVisible(true);
                                noordertemp.setVisibility(View.GONE);
                                temp_list.setVisibility(View.VISIBLE);
                                tadapter = new TemplateAdapter(getActivity(), item);
                                temp_list.setAdapter(tadapter);
                            } else {
                                noordertemp.setVisibility(View.VISIBLE);
                                temp_list.setVisibility(View.GONE);
                            }
                        } else {
                            new inserttemp().execute();
                        }

                    }
                    hidekeyboard();
                } else {
                    Toast.makeText(getActivity(), "Please enter Template name.", Toast.LENGTH_LONG).show();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                hidekeyboard();
                dialog.dismiss();
            }
        });
        builder.show();

    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

        } catch (Exception e) {
            newmenu.findItem(R.id.clear).setVisible(false);

        }


        //   Toast.makeText(getActivity(),"2",Toast.LENGTH_LONG).show();
        try {

            item.clear();
            prefix = prefs.getString("prefix_name", null);
            item = query.GetTemplate(prefix, "all");

        } catch (Exception e) {
            e.printStackTrace();
        }

        tempedit = 0;
        if (item.size() != 0) {
            noordertemp.setVisibility(View.GONE);

            tadapter = new TemplateAdapter(getActivity(), item);
            temp_list.setAdapter(tadapter);
        } else {
            try {
                newmenu.findItem(R.id.edit).setVisible(false);
                noordertemp.setVisibility(View.VISIBLE);
            } catch (Exception e) {

            }

        }
//Dashboard.viewPager.setCurrentItem(Dashboard.page);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Dashboard.page=3;
    }

    @Override
    public void refresh() {
        // init();
        try {
            newmenu.findItem(R.id.add).setVisible(true);
            newmenu.findItem(R.id.edit).setVisible(true);
            newmenu.findItem(R.id.done).setVisible(false);
            newmenu.findItem(R.id.clear).setVisible(false);

            item.clear();
            prefix = prefs.getString("prefix_name", null);
            item = query.GetTemplate(prefix, "all");
            System.out.println("sssssssss= " + item.size());

        } catch (Exception e) {
            e.printStackTrace();
        }

        tempedit = 0;
        if (item.size() != 0) {
            noordertemp.setVisibility(View.GONE);

            tadapter = new TemplateAdapter(getActivity(), item);
            temp_list.setAdapter(tadapter);
            temp_list.setVisibility(View.VISIBLE);
        } else {
            temp_list.setVisibility(View.GONE);
            noordertemp.setVisibility(View.VISIBLE);
        }

    }


    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void updateTemplatefragment() {

        try {
            ArrayList<ModelLogin> interface_array = new ArrayList<ModelLogin>();
            query = new DatabaseQueries(getActivity());
            interface_array = query.getLogin();
            // SharedPreferences prefs = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
            prefix = interface_array.get(0).prefix_name;
            item.clear();
            item = query.GetTemplate(prefix, "all");
        } catch (Exception e) {
            e.printStackTrace();
        }

        tempedit = 0;
        if (item.size() != 0) {
            noordertemp.setVisibility(View.GONE);

            tadapter = new TemplateAdapter(getActivity(), item);
            temp_list.setAdapter(tadapter);
        } else {

            noordertemp.setVisibility(View.VISIBLE);
        }

    }


    //***********************Inserting Offline created Templates************************************

    class inserttemp extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        String status;
        String response;
        JSONObject data_obj;
        ArrayList<Model_Template> item_2;

        public inserttemp() {

            this.item_2 = item_2;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Inserting Template...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            response = InsertMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null),
                    data, "", "");


            try

            {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    data_obj = obj.getJSONObject("Data");
                    String temp_order_id = data_obj.getString("order_template_id");

                    Model_Template a = new Model_Template();
                    a.template_name = data;
                    a.order_template_id = temp_order_id;
                    a.prefix_name = prefs.getString("prefix_name", "");
                    a.type ="newupdate";
                    if (!query.isTemplateExist(prefs.getString("prefix_name", null), a.order_template_id)) {
                        query.gettemplate(a);
                    }
                    //  item.add(a);

                    Dashboard.tempupdate = true;

                } else {
                    status = obj.getString("Status");
                    if (status.equals("false")) {
                        JSONObject data_obj = obj.getJSONObject("Data");
                        //    result = data_obj.getString("result");

                    }
                }

            } catch (
                    JSONException e
                    )

            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            try {
                if (status.equals("true")) {
                    try {
item.clear();
                        item = query.GetTemplate(prefix, "all");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (item.size() > 0) {
                        newmenu.findItem(R.id.edit).setVisible(true);
                        noordertemp.setVisibility(View.GONE);
                        temp_list.setVisibility(View.VISIBLE);
                        tadapter = new TemplateAdapter(getActivity(), item);
                        temp_list.setAdapter(tadapter);
                    } else {
                        noordertemp.setVisibility(View.VISIBLE);
                        temp_list.setVisibility(View.GONE);
                    }
                } else {
                    try {
                        if (data_obj.has("logout")) {
                            String logout = data_obj.optString("logout");
                            if (logout.equals("yes")) {
                                JSONObject jobj = data_obj.optJSONObject("Data");
                                final String result = jobj.optString("result");


                                methods.dialog(result);

                            }
                        } else {
                            Toast.makeText(getActivity(), "Template not Saved", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
    //************************Webservice parameter InsertMethod*************************

    public String InsertMethod(String prefix_name, String auth_code, String name, String product_code, String qty) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("template_name", name)
                .appendQueryParameter("product_id", product_code)
                .appendQueryParameter("qty", qty);
        res = parser.getJSONFromUrl(Utils.insert_temp, builder, getActivity().getApplicationContext());
        return res;
    }
}

package Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ordertron.Dashboard;
import com.ordertron.PlaceOrderActivity;
import com.ordertron.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import Adapters.CartAdapter;
import DataBaseUtils.DatabaseQueries;
import DataBaseUtils.TabUtils;
import Interfaces.RefreshFragment;
import Model.JSONParser;
import Model.Methods;
import Model.ModelLogin;
import Model.Model_Add_To_Cart;
import Model.Model_Template;
import Model.Model_get_notification;
import Model.Utils;

/**
 * Created by android on 12/4/2015.
 */
public class CartActivity extends Fragment implements RefreshFragment {
    RecyclerView cart_list;
    String Shipping_cost;
    ProgressBar progressBar;
    DatabaseQueries query;
    public static CartAdapter cartAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    ArrayList<String> arr = new ArrayList<>();
    String data;
     ArrayList<ModelLogin> login12=new ArrayList<>();
    SharedPreferences prefs;
    public static ImageView nocart;
    public static LinearLayout ll_main_layout;
    View root;
    public static Menu newmenu;
    Button img_place_order, img_save_template;
    public static int showedit = 0;
    public static boolean qtyupdate = false;
    boolean network;
    JSONArray pidarr, qtyarr, skuarr;
    String prefix_name;
    Methods methods;
    TemplatesActivity tempactivity;
    onSaveTemplateListener onSaveTemplateListener;
    float ordertotal = 0,gst;
    String total,taxorgst;
    public static int addtocart = 0;
    public static int cartonres = 0;

    double prodtotalprice, original_total;
    double apl;
    String prodtax, tax_percentage;
    public static ArrayList<Model_Add_To_Cart> price_tier = new ArrayList<>();
    public static ArrayList<Model_get_notification> showdelivery = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.cart_activity, container, false);
        setHasOptionsMenu(true);
        init();

        //  Dashboard.actionbar.setTitle("Cart");
        Dashboard.item.clear();
        Dashboard.isVisible = false;
        methods = new Methods(getActivity());
        try {
            Dashboard.item = query.GetProductFromCart(prefs.getString("prefix_name", null));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            showdelivery = query.get_notification_setting(prefs.getString("prefix_name", null));
        } catch (Exception e) {
            e.printStackTrace();
        }
        TabUtils.updateTabBadge(Dashboard.cart, Dashboard.item.size());
        //----------------set adapter for add_to_cart-------------
        if (Dashboard.item.size() != 0) {

            try {
                newmenu.findItem(R.id.edit).setVisible(true);
                newmenu.findItem(R.id.clear).setVisible(true);
                newmenu.findItem(R.id.done).setVisible(false);
            }catch (Exception e){

            }

//            for(int position=0;position< Dashboard.item .size();position++) {
//                tax_percentage = prefs.getString("tax_percentage", null);
//               tierLogic( Dashboard.item .get(position).prod_id,  Dashboard.item .get(position).qty,  Dashboard.item .get(position).spcl_price,  Dashboard.item .get(position).price,  Dashboard.item .get(position).tier_price,  Dashboard.item .get(position).taxable,  Dashboard.item .get(position).order_unit,  Dashboard.item .get(position).base_unit);
//                Model_Add_To_Cart pr=new Model_Add_To_Cart();
//                pr.product_total= String.valueOf(prodtotalprice);
//                pr.original_totl= String.valueOf(original_total);
//                pr.apl= String.valueOf(apl);
//                Dashboard.item.add(pr);
//            }
            nocart.setVisibility(View.GONE);
            ll_main_layout.setVisibility(View.VISIBLE);
            cartAdapter = new CartAdapter(getActivity(), Dashboard.item);
            cart_list.setAdapter(cartAdapter);
            cartAdapter.notifyDataSetChanged();
            if (Dashboard.item.size() > 3) {
                cart_list.scrollToPosition(Dashboard.item.size());
            }
        } else {

            nocart.setVisibility(View.VISIBLE);
            ll_main_layout.setVisibility(View.GONE);
//            try {
//                newmenu.findItem(R.id.edit).setVisible(false);
//                newmenu.findItem(R.id.clear).setVisible(false);
//                newmenu.findItem(R.id.done).setVisible(false);
//            }catch (Exception e){
//
//            }
        }


        onclick();
        return root;
    }

    private void onclick() {

        img_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder idbuilder = new StringBuilder();
                StringBuilder qtybuilder = new StringBuilder();
                for (int i = 0; i < CartAdapter.products.size(); i++) {

                    idbuilder.append(CartAdapter.products.get(i).prod_sku + ",");
                    qtybuilder.append(CartAdapter.products.get(i).qty + ",");

                }
                String pid = idbuilder.toString();
                if (pid.indexOf(",") != -1) {
                    pid = pid.substring(0, pid.length() - 1);
                }
                String pqty = qtybuilder.toString();
                if (pqty.indexOf(",") != -1) {
                    pqty = pqty.substring(0, pqty.length() - 1);
                }
                int ss = Dashboard.item.size();


//**************************if sendtotal is null (done by abhijeet)***************************************************
                //if(CartAdapter.sendtotal==null){
                total = query.Setvalues();
                taxorgst = query.Setvalues222();
                String[] str = total.split(":");
                String[] str22 = taxorgst.split(":");


                try {
                    login12 =query.GetLogin(prefix_name);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                for(int i=0;i<login12.size();i++){
                    Shipping_cost = login12.get(i).shipping_cost;
                }

                tax_percentage = prefs.getString("tax_percentage", null);

//if((str22[0]).equals("TBA") || str22[1].equals("TBA")){
//    CartAdapter.sendtotal = "TBA";
//}else{
//
//
//}


                NumberFormat formatter = new DecimalFormat("#0.00");
                if ((str[0]).equals("TBA") || str[1].equals("TBA")||(str22[0]).equals("TBA") || str22[1].equals("TBA")) {
                    CartAdapter.sendtotal = "TBA";
                } else {
                    float a = CartAdapter.round((Float.parseFloat(str[0])), 2);
                    //double b=Math.floor(Float.parseFloat(str[1]) * 100) / 100;
                    float b = CartAdapter.round((Float.parseFloat(str[1])), 2);
                    // double c=Math.floor(Float.parseFloat(Shipping_cost) * 100) / 100;
                    float c = CartAdapter.round(Float.parseFloat(Shipping_cost), 2);

                    String p = String.format("%.2f", a);
                    String q = String.format("%.2f", b);
                    String r = String.format("%.2f", c);

                    float sub22 = CartAdapter.round((Float.parseFloat(str22[0])), 2);
                    String s122 = String.format("%.2f", sub22);
                    gst= CartAdapter.round(((Float.parseFloat(s122) + Float.parseFloat(Shipping_cost)) * Float.parseFloat(tax_percentage)) / 100, 2);

                    ordertotal = Float.parseFloat(p) + gst+ Float.parseFloat(r);
                    if(cartAdapter.Shipping_Status.equals("2")){
                        CartAdapter.sendtotal = "TBA";
                    }else {
                        CartAdapter.sendtotal = String.valueOf(ordertotal);
                    }
                }
                // }
//***************************************************************************************************


                Intent i = new Intent(getActivity(), PlaceOrderActivity.class);
                i.putExtra("total_cost", CartAdapter.sendtotal);

                i.putExtra("pid", pid);
                i.putExtra("pqty", pqty);
                startActivity(i);
            }
        });
        img_save_template.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pidarr = new JSONArray();
                qtyarr = new JSONArray();
                skuarr = new JSONArray();
                StringBuilder idbuilder = new StringBuilder();
                StringBuilder qtybuilder = new StringBuilder();
                StringBuilder codebuilder = new StringBuilder();
                for (int i = 0; i < CartAdapter.products.size(); i++) {

                    pidarr.put(CartAdapter.products.get(i).prod_id);
                    qtyarr.put(CartAdapter.products.get(i).qty);
                    skuarr.put(CartAdapter.products.get(i).prod_sku);

                    idbuilder.append(CartAdapter.products.get(i).prod_id + ",");
                    qtybuilder.append(CartAdapter.products.get(i).qty + ",");
                    codebuilder.append(CartAdapter.products.get(i).prod_sku + ",");

                }


                String pid = idbuilder.toString();
                if (pid.indexOf(",") != -1) {
                    pid = pid.substring(0, pid.length() - 1);
                }
                String pqty = qtybuilder.toString();
                if (pqty.indexOf(",") != -1) {
                    pqty = pqty.substring(0, pqty.length() - 1);
                }
                String pcode = codebuilder.toString();
                if (pcode.indexOf(",") != -1) {
                    pcode = pcode.substring(0, pcode.length() - 1);
                }
                String type = "new1";

                popdialog(pid, pqty, pcode);
//                Intent i = new Intent(getActivity(), Pop_Window_Template.class);
//                i.putExtra("prod_id", prod_id);
//                i.putExtra("quantity", quatity);
//                i.putExtra("pro_sku", pro_sku);
//                i.putExtra("Intenttype", "carttemp");
//

//                startActivity(i);
            }
        });
    }

    private void popdialog(final String prod_id, final String quatity, final String pro_sku) {

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), Dashboard.style);
        builder.setTitle("New Template");
        builder.setMessage("Enter the Template Name and tap on create");
        View view = getActivity().getLayoutInflater().inflate(R.layout.edittext, null);
        final EditText input = (EditText) view
                .findViewById(R.id.etCategory);
        input.requestFocus();
        input.setHint("Enter Template Name");
        input.setTextColor(Color.BLACK);
        builder.setView(view);
        builder.setCancelable(false);
//                i = getActivity().getIntent();
//                intentdata_popupwindow = i.getStringExtra("edit");
//                order_id = i.getStringExtra("sendtempid");
        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                data = input.getText().toString();
                if (input.length() != 0) {
                    try {
                        arr = query.GetTemplate_name(prefix_name);
                    } catch (Exception e) {

                    }
                    String avial = "true";
                    for (int h = 0; h < arr.size(); h++) {
                        System.out.println("TEMP_name::" + arr.get(h));
                        if (arr.get(h).equalsIgnoreCase(data.trim())) {
                            avial = "false";

                        }
//                        else {
//                            avial = "true";
//
//                        }
                    }
                    if (avial.equals("false")) {
                        Toast.makeText(getActivity(), "Template already Exists", Toast.LENGTH_LONG).show();
                    } else {

                        if (!Utils.isNetworkConnected(getActivity())) {
                            int randomPIN = (int) (Math.random() * 9000) + 1000;
//                            a.product_id = pidarr.toString();
//                            a.product_code = skuarr.toString();
//                            a.qty = qtyarr.toString();
                            Model_Template a = new Model_Template();
                            a.template_name = data;
                            a.order_template_id = String.valueOf(randomPIN);
                            a.product_id = pidarr.toString();
                            a.product_code = skuarr.toString();
                            a.qty = qtyarr.toString();
                            a.prefix_name = prefs.getString("prefix_name", "");
                            a.type = "newupdate";
                            a.last_sync_id = "off";

                            try {
                                if (!query.isTemplateExist(a.prefix_name, a.order_template_id)) {
                                    query.gettemplate(a);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            TemplatesActivity.item.add(a);
                            Dashboard.tempupdate = true;
                            Toast.makeText(getActivity(), "Template Saved", Toast.LENGTH_SHORT).show();

                        } else {
                            new inserttemp(prod_id, quatity, pro_sku).execute();
                        }
//                        if (!network) {
//                            Utils.conDialog(getActivity());
//                        } else {
//                            new inserttemp(prod_id, quatity, pro_sku).execute();
//                        }

                    }
                } else {
                    Toast.makeText(getActivity(), "Please enter Template name.", Toast.LENGTH_LONG).show();
                }
                getActivity().getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });
        builder.show();
    }

    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void init() {
        network = Utils.isNetworkConnected(getActivity());
        query = new DatabaseQueries(getActivity());
        prefs = getActivity().getSharedPreferences("login", getActivity().MODE_PRIVATE);
        prefix_name = prefs.getString("prefix_name", null);
        cart_list = (RecyclerView) root.findViewById(R.id.cart_list);
        progressBar = (ProgressBar) root.findViewById(R.id.progressbar);
        ll_main_layout = (LinearLayout) root.findViewById(R.id.ll_main_layout);
        nocart = (ImageView) root.findViewById(R.id.nocart);
        mLayoutManager = new LinearLayoutManager(getActivity());
        cart_list.setLayoutManager(mLayoutManager);


        img_place_order = (Button) root.findViewById(R.id.img_place_order);
        img_save_template = (Button) root.findViewById(R.id.img_save_template);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        this.newmenu = menu;
        super.onCreateOptionsMenu(newmenu, inflater);
        Dashboard.actionbar.setHomeButtonEnabled(false);
        Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);

        newmenu.findItem(R.id.edit).setVisible(false);
        newmenu.findItem(R.id.clear).setVisible(false);
        newmenu.findItem(R.id.done).setVisible(false);
        newmenu.findItem(R.id.add).setVisible(false);
        newmenu.findItem(R.id.logout).setVisible(false);
        newmenu.findItem(R.id.setting).setVisible(false);
        newmenu.findItem(R.id.search).setVisible(false);
        MenuItem searchItem = newmenu.findItem(R.id.search);

        SearchView searchView = (SearchView) searchItem.getActionView();

        if (!searchView.isIconified()) {
            searchView.setIconified(true);
        }
        searchItem.collapseActionView();
        if (Dashboard.item.size() != 0) {
            showedit = 0;

            newmenu.findItem(R.id.edit).setVisible(true);
            newmenu.findItem(R.id.clear).setVisible(true);
            nocart.setVisibility(View.GONE);
            ll_main_layout.setVisibility(View.VISIBLE);
            cartAdapter.notifyDataSetChanged();
        } else {
            newmenu.findItem(R.id.edit).setVisible(false);
            newmenu.findItem(R.id.clear).setVisible(false);
            nocart.setVisibility(View.VISIBLE);
            ll_main_layout.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.edit:
                newmenu.findItem(R.id.edit).setVisible(false);
                newmenu.findItem(R.id.done).setVisible(true);
                showedit = 1;
                cartAdapter.notifyDataSetChanged();
                return true;
            case R.id.done:
                newmenu.findItem(R.id.edit).setVisible(true);
                newmenu.findItem(R.id.done).setVisible(false);
                showedit = 0;
                cartAdapter.notifyDataSetChanged();
                return true;
            case R.id.clear:
                dialog();
                return true;
        }
        return false;
    }

    private void dialog() {
        new AlertDialog.Builder(getActivity(), Dashboard.style)
                .setTitle("Clear Cart")
                .setMessage("Are you sure you want to delete all items in the Cart?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        newmenu.findItem(R.id.edit).setVisible(false);
                        newmenu.findItem(R.id.clear).setVisible(false);
                        newmenu.findItem(R.id.done).setVisible(false);
                        Dashboard.item.clear();
                        cartAdapter.notifyDataSetChanged();
                        nocart.setVisibility(View.VISIBLE);
                        ll_main_layout.setVisibility(View.GONE);
                        query.delete(DatabaseQueries.TABLE_ADD_TO_CART);
                        TabUtils.updateTabBadge(Dashboard.cart, Dashboard.item.size());
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        //   TabUtils.updateTabBadge(cart, 2);

        //if (Dashboard.cartupdate) {
        if (cartonres==1) {

            cartonres = 0;
            try {
                newmenu.findItem(R.id.edit).setVisible(false);
                newmenu.findItem(R.id.clear).setVisible(false);
                newmenu.findItem(R.id.done).setVisible(false);
            } catch (Exception e) {

            }
        }

        Dashboard.item.clear();
        try {
            Dashboard.item = query.GetProductFromCart(prefs.getString("prefix_name", null));
        } catch (Exception e) {
            e.printStackTrace();
        }
        TabUtils.updateTabBadge(Dashboard.cart, Dashboard.item.size());
        if (Dashboard.item.size() != 0) {
            //  TabUtils.updateTabBadge(Dashboard.cart, Dashboard.item.size());
            if (qtyupdate) {
                qtyupdate = false;
                showedit = 1;
            } else {
                showedit = 0;
            }
            nocart.setVisibility(View.GONE);
            ll_main_layout.setVisibility(View.VISIBLE);
            cartAdapter = new CartAdapter(getActivity(), Dashboard.item);
            cart_list.setAdapter(cartAdapter);
            cartAdapter.notifyDataSetChanged();
            if (addtocart == 1) {
                addtocart = 0;

                try {
                    newmenu.findItem(R.id.edit).setVisible(true);
                    newmenu.findItem(R.id.clear).setVisible(true);
                    newmenu.findItem(R.id.done).setVisible(false);
                } catch (Exception e) {

                }
            }
//                if (Dashboard.item.size() > 3) {
//                    cart_list.scrollToPosition(Dashboard.item.size());
//        }
        } else {
            try {

            }catch (Exception e){

            }
            TabUtils.updateTabBadge(Dashboard.cart, 0);
            nocart.setVisibility(View.VISIBLE);
            ll_main_layout.setVisibility(View.GONE);
        }
        //  }
    }

    @Override
    public void refresh() {
//        showedit =0;
//        cartAdapter.notifyDataSetChanged();
//        if (showedit== 0) {
//
//            try {
//                newmenu.findItem(R.id.edit).setVisible(true);
//                newmenu.findItem(R.id.clear).setVisible(true);
//                newmenu.findItem(R.id.done).setVisible(false);
//            } catch (Exception e) {
//
//            }
//        }
//        String total = query.Setvalues();
//        String[] str = total.split(":");
//        String  Currency_symbol = prefs.getString("currency_symbol", null);
//        NumberFormat formatter = new DecimalFormat("#0.00");
//        if((str[0]).equals("TBA")){
//            CartAdapter.sendtotal="TBA";
//        }else {
//            CartAdapter.sendtotal = formatter.format(Double.parseDouble(str[0]));
//        }
    }

    //**************************************************************************************************
    class inserttemp extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        String status;
        String response, pid, qty, psku;
        JSONObject obj;

        public inserttemp(String prod_id, String quatity, String pro_sku) {
            this.pid = prod_id;
            this.qty = quatity;
            this.psku = pro_sku;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Inserting Template...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            response = InsertMethod(prefs.getString("prefix_name", null), prefs.getString("auth_code", null),
                    data, psku, qty);


            try

            {
                obj = new JSONObject(response);

                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONObject data_obj = obj.getJSONObject("Data");
                    String temp_order_id = data_obj.getString("order_template_id");

                    Model_Template a = new Model_Template();
                    a.template_name = data;
                    a.order_template_id = temp_order_id;
                    a.product_id = pidarr.toString();
                    a.product_code = skuarr.toString();
                    a.qty = qtyarr.toString();
                    a.prefix_name = prefs.getString("prefix_name", "");
                    a.type = "newupdate";
                    if (!query.isTemplateExist(a.prefix_name, a.order_template_id)) {
                        query.gettemplate(a);
                    }
                    TemplatesActivity.item.add(a);
                    Dashboard.tempupdate = true;

                } else {

                }

            } catch (
                    JSONException e
                    )

            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            if (status.equals("true")) {
                Toast.makeText(getActivity(), "Template Saved", Toast.LENGTH_SHORT).show();
                onSaveTemplateListener.onSaveTemplate();
//                android.app.Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.fragment_container);
//                FragmentTransaction fragTransaction =   (getActivity()).getFragmentManager().beginTransaction();
//                fragTransaction.detach(currentFragment);
//                fragTransaction.attach(currentFragment);
//                fragTransaction.commit();
                //tempactivity.refresh();
            } else {
                if (obj.has("logout")) {
                    String logout = obj.optString("logout");
                    if (logout.equals("yes")) {
                        JSONObject jobj = obj.optJSONObject("Data");
                        final String result = jobj.optString("result");


                        methods.dialog(result);

                    }
                } else {
                    Toast.makeText(getActivity(), "Template not Saved", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }
    //************************Webservice parameter InsertMethod*************************

    public String InsertMethod(String prefix_name, String auth_code, String name, String product_code, String qty) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("template_name", name)
                .appendQueryParameter("product_id", product_code)
                .appendQueryParameter("qty", qty);
        res = parser.getJSONFromUrl(Utils.insert_temp, builder,getActivity().getApplicationContext());
        return res;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            onSaveTemplateListener = (onSaveTemplateListener) activity;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***************************************
     * Save template listener
     *************************************/
    public interface onSaveTemplateListener {

        public void onSaveTemplate();
    }


    public void tierLogic(String prod_id, String qty, String spcl_price, String price, String tier_price, String taxable, String order_unit, String base_unit) {
        prodtotalprice = 0;
        original_total = 0;
        apl = 0;
        prodtax = "0";
        price_tier.clear();
        try {
            apl = 0;
            if (tier_price != null) {
                if (tier_price.length() != 0) {
                    JSONArray array5 = new JSONArray(tier_price);
                    for (int i = 0; i < array5.length(); i++) {

                        JSONObject obj2 = array5.getJSONObject(i);
                        String quantity_upto = obj2.getString("quantity_upto");
                        String discount_type = obj2.getString("discount_type");
                        String discount = obj2.getString("discount");

                        Model_Add_To_Cart model = new Model_Add_To_Cart();

                        model.quantity_upto = quantity_upto;
                        model.discount_type = discount_type;
                        model.discount = discount;

                        price_tier.add(model);
                    }
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (price == null) {
            price = "0";
        }
        // -----------------calculation-----------------//
        double calcicost = Double.parseDouble(price);
        if (spcl_price == null || spcl_price.equals("")) {
            spcl_price = "0";
        }
        if (price_tier.size() > 0 && (spcl_price.equals("0") || spcl_price.equals(null))) {
            for (int i = 0; i < price_tier.size(); i++) {

                double qty_upto = Double
                        .parseDouble(price_tier.get(i).quantity_upto);

                if (Double
                        .parseDouble(qty) >= qty_upto) {

                    double discount = (calcicost * (Double
                            .parseDouble(price_tier.get(i).discount))) / 100;
                    calcicost = calcicost - discount;
                    break;

                } else {

                }
            }
        } else {
            // price.setText(dSpclprice);
            if (spcl_price.equals(" ") || spcl_price.equals("0") || spcl_price.equals("0") || spcl_price.equals(null) || spcl_price == "0" || spcl_price.length() == 0) {
                calcicost = Double.parseDouble(price);
            } else {
                calcicost = Double.parseDouble(spcl_price);

            }
        }
        //     prod_calci_cost = String.valueOf(calcicost);
        if (qty == null || qty.equals("")) {
            qty = "0";
        }
        prodtotalprice = (Double
                .parseDouble(qty) * calcicost);
        original_total = (Double
                .parseDouble(qty) * Double
                .parseDouble(price));
        //
        apl = prodtotalprice / Double
                .parseDouble(qty);

        String prod_cal_price = String.valueOf(prodtotalprice);
        if (taxable == null || taxable.equals("")) {
            taxable = "0";
        }
        if (taxable.equals("1")) {
            double taxcal = (prodtotalprice * Double
                    .parseDouble(tax_percentage)) / 100;

            prodtax = String.valueOf(taxcal);
            if (order_unit.equals(base_unit)) {
                try {
                    query.updatecartprodt(prod_id, prodtax, prod_cal_price);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    query.updatecartprodt(prod_id, prodtax, "TBA");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // CartActivity.subtax.add(cprodtax);

        } else {
            if (order_unit == null) {
                order_unit = "0";
            }
            if (base_unit == null) {
                base_unit = "0";
            }
            if (order_unit.equals(base_unit)) {
                prodtax = "0";
                try {

                    query.updatecartprodt(prod_id, prodtax, prod_cal_price);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //  CartActivity.subtax.add(cprodtax);

            } else {

                prodtax = "0";
                prod_cal_price = "TBA";
                try {
                    query.updatecartprodt(prod_id, prodtax, prod_cal_price);
                    // query.updatecartprodt(prod_id, "1", "22");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //CartActivity.subtax.add(cprodtax);

            }
        }

    }
}

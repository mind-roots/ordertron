package Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ordertron.Dashboard;
import com.ordertron.Order_id;
import com.ordertron.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Adapters.Product_order_adapter;
import DataBaseUtils.DatabaseQueries;
import Model.JSONParser;
import Model.Model_Add_To_Cart;
import Model.Model_Order_Product;
import Model.Model_Template;
import Model.Utils;

/**
 * Created by Abhijeet on 12/16/2015.
 */
public class Product_Order extends Fragment {


    RecyclerView list;
    Product_order_adapter productAdapter;
    DatabaseQueries databaseQueries;
    Button rl_reorder, rl_savetemplate;
    JSONArray pidarr, qtyarr, skuarr;
    View root;
    RecyclerView.LayoutManager mLayoutManager;
    ArrayList<String> arr = new ArrayList<>();
    boolean network;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.product_in_order, container, false);
        init();

        Dashboard.isVisible = false;
        setdata();
        onClick();
        return root;
    }


    private void setdata() {

        productAdapter = new Product_order_adapter(getActivity(), Order_id.ordrprod, 1);
        list.setAdapter(productAdapter);
    }

    public void init() {
        network = Utils.isNetworkConnected(getActivity());

        databaseQueries = new DatabaseQueries(getActivity());
        list = (RecyclerView) root.findViewById(R.id.list);
        rl_reorder = (Button) root.findViewById(R.id.rl_reorder);
        rl_savetemplate = (Button) root.findViewById(R.id.rl_savetemplate);
        mLayoutManager = new LinearLayoutManager(getActivity());
        list.setLayoutManager(mLayoutManager);


    }

    private void onClick() {
        rl_savetemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
                addtotemplate();

            }
        });
        rl_reorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String countQuery = "SELECT  * FROM cart_table";
                int count = databaseQueries.getcartcount(countQuery);
                if (count > 0) {
                //dialog("Clear Order.", "Are you sure you want to clear the existing order and start new?", android.R.drawable.ic_dialog_alert);
                    popdialog2("Clear Order.", "Are you sure you want to clear the existing order and start new?", android.R.drawable.ic_dialog_alert);
                } else {
                    addcart();
                }

            }
        });
    }

    private void hidekeyboard() {
        // TODO Auto-generated method stub
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void addtotemplate() {

        pidarr = new JSONArray();
        qtyarr = new JSONArray();
        skuarr = new JSONArray();
        StringBuilder idbuilder = new StringBuilder();
        StringBuilder qtybuilder = new StringBuilder();
        StringBuilder codebuilder = new StringBuilder();
        for (int i = 0; i < Product_order_adapter.products.size(); i++) {

            pidarr.put(Product_order_adapter.products.get(i).product_id);
            qtyarr.put(Product_order_adapter.products.get(i).qty);
            skuarr.put(Product_order_adapter.products.get(i).product_code);

            idbuilder.append(Product_order_adapter.products.get(i).product_id + ",");
            qtybuilder.append(Product_order_adapter.products.get(i).qty + ",");
            codebuilder.append(Product_order_adapter.products.get(i).product_code + ",");

        }


        String pid = idbuilder.toString();
        if (pid.indexOf(",") != -1) {
            pid = pid.substring(0, pid.length() - 1);
        }
        String pqty = qtybuilder.toString();
        if (pqty.indexOf(",") != -1) {
            pqty = pqty.substring(0, pqty.length() - 1);
        }
        String pcode = codebuilder.toString();
        if (pcode.indexOf(",") != -1) {
            pcode = pcode.substring(0, pcode.length() - 1);
        }
        String type = "new1";

        popdialog(pid, pqty, pcode);
    }


    private void addcart() {
        for (int i = 0; i < Product_order_adapter.products.size(); i++) {
            Model_Order_Product get = Product_order_adapter.products.get(i);
            //*****************saving data to database through model class**************************************
            Model_Add_To_Cart model_add_to_cart = new Model_Add_To_Cart();
            model_add_to_cart.prod_id = get.product_id;
            model_add_to_cart.prod_name = get.product_name;
            model_add_to_cart.prod_sku = get.product_code;
            model_add_to_cart.qty = get.qty;
            model_add_to_cart.price = get.unit_price;
            model_add_to_cart.spcl_price = get.spcl_price;
            model_add_to_cart.max_qty = get.max_qty;
            model_add_to_cart.min_qty = get.min_qty;
            model_add_to_cart.base_unit = get.base_unit;
            model_add_to_cart.order_unit = get.order_unit;
            model_add_to_cart.taxable = get.taxable;
            model_add_to_cart.inventory = get.inventory;

            //*********calling Databasequries insert function for getcustomer***************

            //******* Also checking if Status of any product is Inactive then it should not add to cart*
            boolean exis=  false;
            try {

             exis=   databaseQueries.isgetproductActive(model_add_to_cart.prod_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(exis)
            databaseQueries.add_to_cart(model_add_to_cart);
        }
        popdialog3(android.R.drawable.ic_menu_save, "Success", "Product(s) has been added to Cart.");
    }

    private void popdialog(final String prod_id, final String quatity, final String pro_sku) {

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), Dashboard.style);
        builder.setTitle("New Template");
        builder.setMessage("Enter the Template Name and tap on create");
        View view = getActivity().getLayoutInflater().inflate(R.layout.edittext, null);
        final EditText input = (EditText) view
                .findViewById(R.id.etCategory);
        input.requestFocus();
        input.setHint("Enter Template Name");
        input.setTextColor(Color.BLACK);
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                hidekeyboard();
                final String data = input.getText().toString();
                if (input.length() != 0) {
                    try {
                        arr = databaseQueries.GetTemplate_name(Order_id.prefix_name);
                    } catch (Exception e) {

                    }
                    String avial = "true";
                    for (int h = 0; h < arr.size(); h++) {
                        System.out.println("TEMP_name::" + arr.get(h));
                        if (arr.get(h).equalsIgnoreCase(data.trim())) {
                            avial = "false";

                        }
//                        else {
//                            avial = "true";
//
//                        }
                    }
                    if (avial.equals("false")) {
                        Toast.makeText(getActivity(), "Template with this name already Exists", Toast.LENGTH_LONG).show();
                    } else {
                        if (!Utils.isNetworkConnected(getActivity())) {
                            int randomPIN = (int)(Math.random()*9000)+1000;
//                            a.product_id = pidarr.toString();
//                            a.product_code = skuarr.toString();
//                            a.qty = qtyarr.toString();
                            Model_Template a = new Model_Template();
                            a.template_name = data;
                            a.order_template_id = String.valueOf(randomPIN);
                            a.product_id = pidarr.toString();
                            a.product_code = skuarr.toString();
                            a.qty =  qtyarr.toString();
                            a.prefix_name =  Order_id.prefix_name;
                            a.type = "newupdate";
                            a.last_sync_id="off";
                            databaseQueries.gettemplate(a);
                            TemplatesActivity.item.add(a);
                            Dashboard.tempupdate = true;
                            Toast.makeText(getActivity(), "Template Saved", Toast.LENGTH_SHORT).show();
                        } else {
                            new inserttemp(prod_id, quatity, pro_sku, data).execute();
                        }


                    }
                } else {
                    Toast.makeText(getActivity(), "Please enter Template name.", Toast.LENGTH_LONG).show();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    //**************************************************************************************************
    class inserttemp extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        String status;
        String response, pid, qty, psku, tempname;

        public inserttemp(String prod_id, String quatity, String pro_sku, String tname) {
            this.pid = prod_id;
            this.qty = quatity;
            this.psku = pro_sku;
            this.tempname = tname;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Inserting Template...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            response = InsertMethod(Order_id.prefix_name, Order_id.auth_code,
                    tempname, psku, qty);
            System.out.print("res=" + response);

            try

            {
                JSONObject obj = new JSONObject(response);

                status = obj.getString("Status");

                if (status.equals("true")) {

//*****************saving data to database through model class**************************************
                    JSONObject data_obj = obj.getJSONObject("Data");
                    String temp_order_id = data_obj.getString("order_template_id");

                    Model_Template a = new Model_Template();
                    a.template_name = tempname;
                    a.order_template_id = temp_order_id;
                    a.product_id = pidarr.toString();
                    a.product_code = skuarr.toString();
                    a.qty = qtyarr.toString();
                    a.prefix_name = Order_id.prefix_name;
                    a.type = "newupdate";
                    if (!databaseQueries.isTemplateExist(a.prefix_name, a.order_template_id)) {
                        databaseQueries.gettemplate(a);
                    }

                    TemplatesActivity.item.add(a);
                    Dashboard.tempupdate = true;

                } else {

                }

            } catch (
                    JSONException e
                    )

            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            // getActivity().finish();
            if (status.equals("true")) {
                Toast.makeText(getActivity(), "Template Saved", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getActivity(), "Template not Saved", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public String InsertMethod(String prefix_name, String auth_code, String name, String product_code, String qty) {
        String res = null;
        JSONParser parser = new JSONParser();

        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("prefix_name", prefix_name)
                .appendQueryParameter("auth_code", auth_code)
                .appendQueryParameter("template_name", name)
                .appendQueryParameter("product_id", product_code)
                .appendQueryParameter("qty", qty);
        res = parser.getJSONFromUrl(Utils.insert_temp, builder,getActivity().getApplicationContext());
        return res;
    }

    public void dialog(String title, String msg, int icon) {
        new AlertDialog.Builder(getActivity(), Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                databaseQueries.delete(DatabaseQueries.TABLE_ADD_TO_CART);


                                Dashboard.cartupdate = true;
                                dialog.dismiss();
                                addcart();


                            }
                        })
                .setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();


                            }
                        })

                .setIcon(icon).show();
    }
    private void popdialog2(String title, String msg, int icon) {

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), Dashboard.style);
        builder.setTitle(title);
        builder.setMessage(msg);
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialogtxt, null);
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                databaseQueries.delete(DatabaseQueries.TABLE_ADD_TO_CART);
                Dashboard.cartupdate = true;
                dialog.dismiss();
                addcart();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setIcon(icon);
        builder.show();
    }

    private void dialogcart(int ic_menu_save, String title, String msg) {


        new AlertDialog.Builder(getActivity(),Dashboard.style)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                //   dialog.dismiss();
                                Dashboard.cartupdate = true;
                                Dashboard.reorder = true;
                                getActivity().finish();

                            }
                        })

                .setIcon(ic_menu_save).show();
    }
    private void popdialog3(int ic_menu_save, String title, String msg) {

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), Dashboard.style);
        builder.setTitle(title);
        builder.setMessage(msg);
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialogtxt, null);

        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                Dashboard.cartupdate = true;
                Dashboard.reorder = true;
                getActivity().finish();
            }
        });
        builder.setIcon(ic_menu_save);
        builder.show();
    }

}
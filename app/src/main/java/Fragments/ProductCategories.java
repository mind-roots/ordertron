package Fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ordertron.Dashboard;
import com.ordertron.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import Adapters.Get_product_catAdapter;
import Adapters.Product_Cat_Adapter;
import DataBaseUtils.DatabaseQueries;
import Interfaces.RefreshFragment;
import Model.Model_Cat_Prod;
import Model.Model_GetProduct;

/**
 * Created by android on 12/4/2015.
 */
public class ProductCategories extends Fragment implements RefreshFragment {
    public static RecyclerView recyclerview, prodrecyclerview;
    ProgressBar progressBar;
    public static DatabaseQueries databaseQueries;
    SharedPreferences prefs;
    //  public static Get_CatagoryAdapter get_catagoryAdapter;
    public static Product_Cat_Adapter get_catagoryAdapter;
    public static  String parent_cat_id = "0";
    public static LinearLayoutManager llm;
    public static ArrayList<Model_GetProduct> allprod = new ArrayList<>();
    Menu menu;
    SearchView searchView;
    public static Get_product_catAdapter getcat;
    public static ImageView noproducts;
    ProgressBar progress_loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.product_categories, container, false);
        setHasOptionsMenu(true);
        Dashboard.indicator = 0;

        init(root);

        Dashboard.isVisible = false;

        if (Dashboard.flag == 1) {
            progressBar.setVisibility(View.GONE);
            if (Dashboard.categorystack.size() != 0) {
                getdata(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).parentcatid);
                //new GetData(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).parentcatid).execute();
//                Dashboard.actionbar.setTitle(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).catname);
                Dashboard.actionbar.setHomeButtonEnabled(true);
                Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);
            } else {
                getdata(parent_cat_id);
//                Dashboard.actionbar.setTitle( "Products");
            }
        } else {
            progressBar.setVisibility(View.VISIBLE);
        }
        return root;

    }

    //************************************UI initialization*************************************************
    private void init(View root) {
        recyclerview = (RecyclerView) root.findViewById(R.id.recyclerview);
        prodrecyclerview = (RecyclerView) root.findViewById(R.id.prodrecyclerview);
        progressBar = (ProgressBar) root.findViewById(R.id.progressbar);
        noproducts = (ImageView) root.findViewById(R.id.noproducts);
        databaseQueries = new DatabaseQueries(getActivity());
        prefs = getActivity().getSharedPreferences("login", getActivity().MODE_PRIVATE);
        llm = new LinearLayoutManager(getActivity());
        //  progress_loading = (ProgressBar) root.findViewById(R.id.progress_loading);
    }


    //************************************getdata*************************************************
    public void getdata(String parent_id) {

//        prodrecyclerview.setVisibility(View.GONE);
        recyclerview.setVisibility(View.VISIBLE);

        try {
            //   Dashboard.mixlist = databaseQueries.Get_Product_Catagory(prefs.getString("prefix_name", null), parent_id);
            Dashboard.mixlist.clear();
            Dashboard.mixlist = databaseQueries.Get_Prod_Cat(prefs.getString("prefix_name", null), parent_id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Dashboard.mixlist.size() != 0) {


            for(int i=0;i<Dashboard.mixlist.size();i++){
                try {
                    if (Dashboard.mixlist.get(i).name != null) {
                        Collections.sort(Dashboard.mixlist, new Comparator<Model_Cat_Prod>() {
                            public int compare(Model_Cat_Prod v1, Model_Cat_Prod v2) {
                                return v1.name.compareToIgnoreCase(v2.name);
                            }
                        });
                    }
                }catch (Exception e){

                }

            }
            recyclerview.setVisibility(View.VISIBLE);
            noproducts.setVisibility(View.GONE);
            get_catagoryAdapter = new Product_Cat_Adapter(getActivity(), Dashboard.mixlist);
            recyclerview.setLayoutManager(llm);
            recyclerview.setAdapter(get_catagoryAdapter);
        }

        else {
            // getdata("0");

//            recyclerview.setVisibility(View.GONE);
//            noproducts.setVisibility(View.VISIBLE);
        }
//*********************Commented cz products are already reterive from above query******************


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.add).setVisible(false);
        menu.findItem(R.id.edit).setVisible(false);
        menu.findItem(R.id.logout).setVisible(false);
        menu.findItem(R.id.setting).setVisible(false);
        menu.findItem(R.id.done).setVisible(false);
        menu.findItem(R.id.search).setVisible(true);
        if (Dashboard.categorystack.size() != 0) {

            Dashboard.actionbar.setHomeButtonEnabled(true);
            Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);
        }
        final MenuItem searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setLongClickable(false);
        searchView.setClickable(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                //  searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
//                prodrecyclerview.setVisibility(View.VISIBLE);
//                recyclerview.setVisibility(View.GONE);
                try {
                    if (s.length() != 0) {
                        prodrecyclerview.setVisibility(View.VISIBLE);
                        recyclerview.setVisibility(View.GONE);
                    } else {
                        prodrecyclerview.setVisibility(View.GONE);
                        recyclerview.setVisibility(View.VISIBLE);
                    }
                    if (Dashboard.flag != 0) {
                        if (s != null) {
                            //-----------only products-------------//
                            try {

                                allprod = databaseQueries.Get_Product_id(prefs.getString("prefix_name", null));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            //****Sorting of products in search view *******************************

                            for(int i=0;i<allprod.size();i++){

                                if(allprod.get(i).name!=null)
                                {
                                    Collections.sort(allprod, new Comparator<Model_GetProduct>() {
                                        public int compare(Model_GetProduct v1, Model_GetProduct v2) {
                                            return v1.name.compareToIgnoreCase(v2.name);
                                        }
                                    });
                                }


                            }



                            getcat = new Get_product_catAdapter(getActivity(), allprod, "products");
                            prodrecyclerview.setHasFixedSize(true);
                            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                            llm.setOrientation(LinearLayoutManager.VERTICAL);
                            prodrecyclerview.setLayoutManager(llm);
                            prodrecyclerview.setAdapter(getcat);
                            getcat.filter(s);
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                return false;
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(getActivity(),"value:"+String.valueOf(Dashboard.categorystack.size()),Toast.LENGTH_SHORT).show();
                long current = System.currentTimeMillis();
                if (Dashboard.getcatsame) {
                    if (Dashboard.categorystack.size() != 0) {
                        String parent_cat_id = Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).parentcatid;
                        Dashboard.categorystack.remove(Dashboard.categorystack.size() - 1);
                        int caa= Integer.parseInt(Product_Cat_Adapter.catid);
                        int ncaa=caa-1;
                        Product_Cat_Adapter.catid= String.valueOf(ncaa);
                        if (Dashboard.categorystack.size() == 0) {

                            Dashboard.getcatsame = false;
                            Dashboard.actionbar.setHomeButtonEnabled(false);
                            Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
                            // getActivity().setTitle("Products");
                            Dashboard.actionbar.setTitle("Products");

                        } else {
                            //  getActivity().setTitle(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).catname);
                            Dashboard.actionbar.setHomeButtonEnabled(true);
                            Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);
                            Dashboard.actionbar.setTitle(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).catname);
                        }

                        //getdata(Dashboard.categorystack.get(Dashboard.categorystack.size()-1).parentcatid);

                        getdata(parent_cat_id);
                        //new GetData(parent_cat_id).execute();
                    }
                }
                long endtime = System.currentTimeMillis();
                long difference = endtime - current;
                System.out.println("curent time:: " + current);
                System.out.println("end time:: " + endtime);
                System.out.println("difference time:: " + difference);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Dashboard.page=1;
    }

    @Override
    public void onResume() {
        super.onResume();

        System.out.println("SSix"+Dashboard.categorystack.size());
        if(Dashboard.tabLayout.getSelectedTabPosition()==1) {
            if (Dashboard.categorystack.size() > 0) {
                Dashboard.actionbar.setTitle(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).catname);
                Dashboard.actionbar.setHomeButtonEnabled(true);
                Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);

            } else {
                Dashboard.actionbar.setHomeButtonEnabled(false);
                Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
                Dashboard.actionbar.setTitle("Products");
            }
        }

//        else if(Dashboard.tabLayout.getSelectedTabPosition()==0){
//
//            Dashboard.actionbar.setTitle(prefs.getString("company_name", null));
//        }else{
//            Dashboard.actionbar.setTitle(Dashboard.arrayList.get(Dashboard.tabLayout.getSelectedTabPosition()));
//        }
//        if (Dashboard.getprev) {
//            Dashboard.getprev = false;
//            if (Dashboard.categorystack.size() > 0) {
//                String parent_cat_id = Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).parentcatid;
//
//                //   getdata(parent_cat_id);
//                new GetData(parent_cat_id).execute();
//                Dashboard.categorystack.remove(Dashboard.categorystack.size() - 1);
//            }
//        }

    }

    @Override
    public void refresh() {
        if(Dashboard.tabLayout.getSelectedTabPosition()==1) {
            if (Dashboard.categorystack.size() > 0) {
                Dashboard.actionbar.setTitle(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).catname);
                Dashboard.actionbar.setHomeButtonEnabled(true);
                Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);

            } else {
                Dashboard.actionbar.setHomeButtonEnabled(false);
                Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
                Dashboard.actionbar.setTitle("Products");
            }
        }else{
            Dashboard.actionbar.setTitle(Dashboard.arrayList.get(Dashboard.tabLayout.getSelectedTabPosition()));
        }



        if (Dashboard.flag == 1) {
            progressBar.setVisibility(View.GONE);
            if (Dashboard.categorystack.size() != 0) {
                // getdata(Dashboard.categorystack.get(Dashboard.categorystack.size()-1 ).parentcatid);
                String id=Product_Cat_Adapter.catid;
                getdata(Product_Cat_Adapter.catid);
                //new GetData(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).parentcatid).execute();
//                Dashboard.actionbar.setTitle(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).catname);
                Dashboard.actionbar.setHomeButtonEnabled(true);
                Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);
            } else {
                getdata(parent_cat_id);
//                Dashboard.actionbar.setTitle( "Products");
            }
        } else {
            progressBar.setVisibility(View.VISIBLE);
        }
//        if (Dashboard.categorystack.size() > 0) {
//            Dashboard.actionbar.setHomeButtonEnabled(true);
//            Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);
//            Dashboard.actionbar.setTitle(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).catname);
//
//        } else {
//            Dashboard.actionbar.setHomeButtonEnabled(false);
//            Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
//            Dashboard.getcatsame = false;
//            Dashboard.actionbar.setTitle("Products");
//        }

        //new Refresh_view().execute();
    }

//    public class Refresh_view extends AsyncTask {
//        ProgressDialog dialog;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            dialog = new ProgressDialog(getActivity());
//            //   dialog.show();
//
//        }
//
//        @Override
//        protected Object doInBackground(Object[] params) {
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Object o) {
//            super.onPostExecute(o);
//            //  dialog.dismiss();
//            if (Dashboard.categorystack.size() > 0) {
//                Dashboard.actionbar.setHomeButtonEnabled(true);
//                Dashboard.actionbar.setDisplayHomeAsUpEnabled(true);
//                Dashboard.actionbar.setTitle(Dashboard.categorystack.get(Dashboard.categorystack.size() - 1).catname);
//
//            } else {
//                Dashboard.actionbar.setHomeButtonEnabled(false);
//                Dashboard.actionbar.setDisplayHomeAsUpEnabled(false);
//                Dashboard.getcatsame = false;
//                Dashboard.actionbar.setTitle("Products");
//            }
//        }
//    }


//    public class GetData extends AsyncTask {
//
//        String parent_id;
//
//        public GetData(String data) {
//            this.parent_id = data;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progress_loading.setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected Object doInBackground(Object[] params) {
//            try {
//                //   Dashboard.mixlist = databaseQueries.Get_Product_Catagory(prefs.getString("prefix_name", null), parent_id);
//                Dashboard.mixlist = databaseQueries.Get_Prod_Cat(prefs.getString("prefix_name", null), parent_id);
//                allprod = databaseQueries.Get_Product_id(prefs.getString("prefix_name", null));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Object o) {
//            super.onPostExecute(o);
//            try {
//                progress_loading.setVisibility(View.GONE);
//                prodrecyclerview.setVisibility(View.GONE);
//                recyclerview.setVisibility(View.VISIBLE);
//
//                if (Dashboard.mixlist.size() != 0) {
//                    //  get_catagoryAdapter = new Get_CatagoryAdapter(getActivity(), Dashboard.getcat);
//                    noproducts.setVisibility(View.GONE);
//                    get_catagoryAdapter = new Product_Cat_Adapter(getActivity(), Dashboard.mixlist);
//                    recyclerview.setLayoutManager(llm);
//                    recyclerview.setAdapter(get_catagoryAdapter);
//                } else {
//                    noproducts.setVisibility(View.VISIBLE);
//                }
//
//
//                getcat = new Get_product_catAdapter(getActivity(), allprod, "products");
//                prodrecyclerview.setHasFixedSize(true);
//                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
//                llm.setOrientation(LinearLayoutManager.VERTICAL);
//                prodrecyclerview.setLayoutManager(llm);
//                prodrecyclerview.setAdapter(getcat);
//            } catch (Exception e) {
//
//            }
//        }
//    }
}

